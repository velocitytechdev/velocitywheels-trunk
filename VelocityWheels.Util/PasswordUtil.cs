﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Helpers;

namespace VelocityWheels.Util
{
    public class PasswordUtil
    {
        public static string EncryptPassword(string password, out string salt)
        {
            salt = Crypto.GenerateSalt();
            string hashedPassword = Crypto.Hash(salt + password);

            //var hashedPassword = Crypto.HashPassword(salt + password);

            // First parameter is the previously hashed string using a Salt
            //verify = Crypto.VerifyHashedPassword("{hash_password_here}", password);

            return hashedPassword;
        }

        public static bool ComparePasswords(string passwordToVerify, string actualPassword, string salt)
        {
            string hashedPassword = Crypto.Hash(salt + passwordToVerify);

            return hashedPassword.Equals(actualPassword);
        }
    }
}
