﻿namespace VelocityWheels.Util
{
    /// <summary>
    ///     The result of a TryParse operation
    /// </summary>
    /// <typeparam name="TValue">
    ///     The value being retrieved.
    /// </typeparam>
    public class TryGetResult<TValue>
    {
        /// <summary>
        ///     Gets or sets a value indicating whether the value was parsed.
        /// </summary>
        public bool Found { get; set; }

        /// <summary>
        ///     Gets or sets the parsed value, or <c>null</c> if no value was parsed.
        /// </summary>
        public TValue Value { get; set; }
    }
}