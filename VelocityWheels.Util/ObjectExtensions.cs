﻿using System;

namespace VelocityWheels.Util
{
    public static class ObjectExtensions
    {
        public static T Chain<T>(this T receiver, Action<T> action)
        {
            action(receiver);
            return receiver;
        }
    }
}
