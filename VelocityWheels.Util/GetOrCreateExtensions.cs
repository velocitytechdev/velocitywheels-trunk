﻿using System;
using System.Collections.Generic;

namespace VelocityWheels.Util
{
    using Class = GetOrCreateExtensions;

    public static class GetOrCreateExtensions
    {


        public static TValue GetOrCreate<TKey, TValue>(
            this IDictionary<TKey, TValue> receiver, TKey key, Func<IDictionary<TKey, TValue>, TKey, TValue> creator)
        {
            return receiver.GetOrCreate(key, () => creator(receiver, key));
        }
        public static TValue GetOrCreate<TKey, TValue>(
            this IDictionary<TKey, TValue> receiver, TKey key, Func<TKey, TValue> creator)

        {
            return receiver.GetOrCreate(key, () => creator(key));
        }

        public static TValue GetOrCreate<TKey, TValue>(
            this IDictionary<TKey, TValue> receiver, TKey key, Func<TValue> creator)

        {
            var result = receiver.TryGet(key);

            if (result.Found) { return result.Value; }

            var value = creator();

            receiver[key] = value;

            return value;
        }
    }
}
