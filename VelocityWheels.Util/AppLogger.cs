﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using log4net;
using log4net.Repository.Hierarchy;

namespace VelocityWheels.Util
{
    public static class AppLogger
    {
        public static ILog GetLogger(object instance)
        {
            return LogManager.GetLogger(instance.GetType());
        }

        public static ILog GetLogger(Type type)
        {
            return LogManager.GetLogger(type);
        }

        public static void LogError(this log4net.ILog logger, string message)
        {
            logger.Error(message);
        }

        public static void LogException(this log4net.ILog logger, string message, Exception e)
        {
            string exceptionMessage = e.Message;
            Exception innerException = e.InnerException;
            while (innerException != null)
            {
                exceptionMessage += "\n" + innerException.Message;
                innerException = innerException.InnerException;
            }
            
            logger.Error($"---------------------\n{message} - Exception Message: {exceptionMessage} \nStack Trace: \n{e.StackTrace}");
        }
     
    }
}
