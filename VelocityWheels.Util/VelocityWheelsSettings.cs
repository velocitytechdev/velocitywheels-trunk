﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace VelocityWheels.Util
{
    public class VelocityWheelsSettings
    {
        public static string GetApiBaseUrl => ConfigurationManager.AppSettings["WheelsApiBaseUrl"];

        public static string GetVehicleImagesBaseUrl => ConfigurationManager.AppSettings["VehicleImagesBaseUrl"];

        public static string GetVehicleContentPath => ConfigurationManager.AppSettings["VehicleContentPath"];

        public static string LocalWheelContentPath => ConfigurationManager.AppSettings["WheelContentPath"];

        public static bool ExtraDebuggingDataEnabled => bool.Parse(ConfigurationManager.AppSettings["ExtraDebuggingDataEnabled"]);

        public static int ExternalApiId => int.Parse(ConfigurationManager.AppSettings["ExternalApiId"]);
    }
}
