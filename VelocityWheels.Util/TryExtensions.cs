﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace VelocityWheels.Util
{
    using Class = TryExtensions;

    public delegate bool TryParser<TValue>(string str, out TValue value);

    public delegate bool TryParserWithStyle<TValue>(string str, NumberStyles styles, IFormatProvider provider, out TValue value);

    public static class TryExtensions
    {
        public static TryGetResult<TValue> TryGet<TKey, TValue>(this IDictionary<TKey, TValue> receiver, TKey key)
        {
            TValue value;
            var found = receiver.TryGetValue(key, out value);
            return new TryGetResult<TValue>
            {
                Found = found,
                Value = value,
            };
        }
        public static TryGetResult<TValue> TryParse<TValue>(this string str, TryParser<TValue> parser)
        {
            TValue value;

            var parsed = parser(str, out value);

            return new TryGetResult<TValue>
            {
                Found = parsed,
                Value = value,
            };
        }

        public static TryGetResult<TValue> TryParse<TValue>(this string str, NumberStyles styles, IFormatProvider format, TryParserWithStyle<TValue> parser)
        {
            TValue value;

            var parsed = parser(str, styles, format, out value);

            return new TryGetResult<TValue>
            {
                Found = parsed,
                Value = value,
            };
        }
    }
}