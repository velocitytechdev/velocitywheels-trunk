﻿using System;
using System.Net;

namespace VelocityWheels.Util
{
    public class MyWebClient : WebClient
    {
        public MyWebClient()
            : base()
        {
            this.Headers.Add(HttpRequestHeader.UserAgent,
                "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36");
            this.Headers.Add(HttpRequestHeader.Accept,
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            this.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate, sdch");
            this.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-US,en;q=0.8");
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            HttpWebRequest request = base.GetWebRequest(address) as HttpWebRequest;
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            return request;
        }
    }
}