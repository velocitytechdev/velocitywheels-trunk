﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VelocityWheels.ExternalServices.Models
{
    public class VehicleModelEx
    {
        public int ApiId { get; set; }
        public string ChassisId { get; set; }
        public string ModelId { get; set; }       
        public int RegionId { get; set; }       
    }
}
