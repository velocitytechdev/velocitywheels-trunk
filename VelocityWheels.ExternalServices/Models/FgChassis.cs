﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.ExternalServices.Models
{
    public class FgChassis
    {
        public double BaseVehicleID { get; set; }
        public double ChassisId { get; set; }
        public string BoltPattern { get; set; }
        public double Hubbore { get; set; }
        public string WheelSize { get; set; }
        public double? OffsetMin { get; set; }
        public double? OffsetMax { get; set; }
        public decimal? Diameter { get; set; }
        public decimal? Width { get; set; }
    }
}
