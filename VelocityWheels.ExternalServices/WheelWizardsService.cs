﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VelocityWheels.ExternalServices.Models;
using VelocityWheels.ExternalServices.WheelWizards;

namespace VelocityWheels.ExternalServices
{
    public class WheelWizardsService
    {
        private webserviceSoapClient WWSoapClient { get; set; }

        public const string DRIVE_RIGHT_USERNAME = "VelocityTGM";
        public const string DRIVE_RIGHT_PASSWORD = "Config949";

        public WheelWizardsService()
        {
            WWSoapClient = new WheelWizards.webserviceSoapClient();
        }

        public List<VehicleModelEx> GetVehicles(int year, string make, string model, string bodyType, string subModel, int region = 1)
        {
            try
            {
                var vehicles = WWSoapClient.GetAAIAVehicles(DRIVE_RIGHT_USERNAME, DRIVE_RIGHT_PASSWORD, year, region, make, model, bodyType, subModel);
                if (vehicles != null && vehicles.Any())
                {
                    return vehicles.Select(v => new VehicleModelEx()
                    {
                        ApiId = 2,
                        ChassisId = v.DRChassisID,
                        ModelId = v.DRModelID,
                        RegionId = region
                    }).ToList();
                }
            }
            catch (Exception e)
            {
                // log exception
            }
            

            return null;
        }
    }
}
