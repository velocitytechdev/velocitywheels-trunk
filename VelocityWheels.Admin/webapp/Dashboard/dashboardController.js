﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('dashboardController', dashboardController);

    dashboardController.$inject = ['$scope', 'dashboardService']; 

    function dashboardController($scope, dashboardService) {
        $scope.title = 'dashboardController';

        $scope.brands = {};
        $scope.stats = {};

        var getBrandsComplete = function (data) {
            $scope.brands = data;
        };

        var getBrandsError = function(reason) {
            $scope.error = "Unable to get brands : status code= " + reason.status + ", " + reason.statusText;
        };

        var getStatsComplete = function (data) {
            $scope.stats = data;
        };

        var getStatsError = function (reason) {
            $scope.error = "Unable to get stats : status code= " + reason.status + ", " + reason.statusText;
        };

        activate();

        function activate() {
            dashboardService.getBrands().then(getBrandsComplete, getBrandsError);
            dashboardService.getStats().then(getStatsComplete,getStatsError);
        }
    }
})();
