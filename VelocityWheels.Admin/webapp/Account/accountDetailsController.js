﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('accountDetailsController', accountDetailsController);

    accountDetailsController.$inject = ['$scope', 'accountService', 'userService', '$stateParams', 'dashboardService'];

    function accountDetailsController($scope, accountService, userService, $stateParams, dashboardService) {
        $scope.title = 'accountDetailsController';

        $scope.account = {};
        $scope.detailsActive = true;
        $scope.wheelsActive = false;
        $scope.brands = [];
        $scope.featuresAvailable = false;

        $scope.hasAdminAccess = false;

        var getBrandsComplete = function (data) {
            $scope.brands = data;
        };

        var getBrandsError = function (reason) {
            $scope.error = "Unable to get brands : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetAccountError = function (reason) {
            $scope.error = "Unable to get account : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetAccountComplete = function (data) {
            if (data != null && data.length) {
                if (data[0].AccountBrands == null) {
                    data[0].AccountBrands = [];
                }
                $scope.account = data[0];
                $scope.account.selectedBrand = null;
                console.info(data);
            }
            
        };

        var onGetUserError = function (reason) {
            $scope.error = "Unable to get user : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetUserComplete = function (data) {
            $scope.hasAdminAccess = userService.isAdmin();
            accountService.getAccountDetails($stateParams.id).then(onGetAccountComplete, onGetAccountError);
            dashboardService.getBrands().then(getBrandsComplete, getBrandsError);
        };

        $scope.details = function() {
            $scope.detailsActive = true;
            $scope.wheelsActive = false;
        };

        $scope.wheelBrands = function() {
            $scope.detailsActive = false;
            $scope.wheelsActive = true;
        };

        $scope.addDomain = function () {
            $scope.account.SubAccounts.push({Id : -1, AccountUrl: "", Active: true});
        };

        $scope.addBrand = function () {
            if ($scope.account.selectedBrand == null)
                return;
            $scope.account.AccountBrands.push({ Brand: $scope.account.selectedBrand, Id: null });
            for (var i = 0; i < $scope.brands.length; ++i) {
                if ($scope.account.selectedBrand.Id === $scope.brands[i].Id) {
                    $scope.brands.splice(i, 1);
                    break;
                }
            }
        };

        $scope.addAllBrands = function () {
            $scope.account.AccountBrands.length = 0;
            for (var i = 0; i < $scope.brands.length; ++i) {
                $scope.account.AccountBrands.push({ Brand: $scope.brands[i], Id: null });
            }
        };

        $scope.removeBrand = function (id) {
            console.info(id);
            for (var i = 0; i < $scope.account.AccountBrands.length; ++i) {
                var brand = $scope.account.AccountBrands[i];
                if (id === brand.Brand.Id) {
                    $scope.account.AccountBrands.splice(i, 1);
                    break;
                }
            }
        };

        var onSaveError = function (reason) {
            $scope.error = "Unable to get user : status code= " + reason.status + ", " + reason.statusText;
            console.info(reason);
        };

        var onSaveComplete = function (data) {
            if (data.Id != null) {
                alert("Account saved!");
                accountService.getAccountDetails(data.Id).then(onGetAccountComplete, onGetAccountError);
            } else {
                alert("Error saving account. Error = " + data.Error);
            }
        };

        $scope.save = function () {
            accountService.saveAccount($scope.account).then(onSaveComplete, onSaveError);
        };

        activate();

        function activate() {
            if ($stateParams.id) {
                userService.getCurrentUser().then(onGetUserComplete, onGetUserError);
            } else {
                alert("No brand id found");
            }
        }
    }
})();
