﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('accountsController', accountsController);

    accountsController.$inject = ['$scope', 'userService', 'accountService', '$location'];

    function accountsController($scope, userService, accountService, $location) {
        $scope.title = 'accountsController';

        $scope.accounts = [];

        var onGetAccountsError = function (reason) {
            $scope.error = "Unable to get accounts : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetAccountsComplete = function (data) {
            $scope.accounts = data;
            console.info(data);
        };

        var onGetUserError = function (reason) {
            $scope.error = "Unable to get user : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetUserComplete = function (data) {
            // todo check for any security items  
            accountService.getAccounts().then(onGetAccountsComplete, onGetAccountsError);
        };

        $scope.addAccount = function() {
            $location.path("/accountdetails/-1");
        };

        activate();

        function activate() {
            userService.getCurrentUser().then(onGetUserComplete, onGetUserError);
        }
    }
})();
