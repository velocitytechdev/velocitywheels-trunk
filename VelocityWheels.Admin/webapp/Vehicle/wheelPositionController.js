﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('wheelPositionController', wheelPositionController);

    wheelPositionController.$inject = ['$scope', '$sce', '$stateParams'];

    function wheelPositionController($scope, $sce, $stateParams) {
        $scope.title = 'wheelPositionController';

        activate();

        function activate() {
            if ($stateParams.id) {
                $scope.detailFrame = $sce.trustAsResourceUrl("/Vehicle/PositionWheels/" + $stateParams.id);
            } else {
                $scope.detailFrame = $sce.trustAsResourceUrl("/Vehicle/PositionWheels");
            }
            
        }
    }
})();
