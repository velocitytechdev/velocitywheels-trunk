﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('vehicleController', vehicleController);

    vehicleController.$inject = ['$scope','$document','$http']; 

    function vehicleController($scope, $document, $http) {
        $scope.title = 'vehicleController';

        $scope.years = [];
        $scope.makes = [];
        $scope.models = [];
        $scope.bodyTypes = [];
        $scope.subModels = [];

        var $mainVehicleStage = jQuery('#vtws-mainVehicle');
        var $mainWheelleStage = jQuery('#vtws-mainWheel');

        // SETTINGS
        var baseVehicleWidth = 725 * 3;
        var baseVehicleHeight = 380 * 3;
        var vwiDir = '/Content/wheel-images/';

        var handleSize = 15;
        var handleColor = '#0f0';
        var paintLayerAlpha = 1;
        var wheelColorSet = false;
        var wheelColorLayer;
        var wheelColor;
        var flipWheelToggle = false;

        var c_mainVehicle = null;
        var vEl = jQuery('#v');
        if (vEl.length > 0)
            c_mainVehicle = new fabric.Canvas('v');
        var c_mainWheel;
        var img_vc;
        var group_wvF;
        var group_wvR;

        


        var vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;
        var setVehicleStageScale = function (startWidth, startHeight) {
            c_mainVehicle.setWidth(startWidth * vehicleStageScale);
            c_mainVehicle.setHeight(startHeight * vehicleStageScale);
        };

        // INITIATE VEHICLE CANVAS ============
        c_mainVehicle.setWidth(baseVehicleWidth * vehicleStageScale);
        c_mainVehicle.setHeight(baseVehicleHeight * vehicleStageScale);

        // Set Default Background Image
        setVehicleBackground(c_mainVehicle, '');

        function resetVehicleCanvas() {
            vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;
            if (c_mainVehicle) {
                c_mainVehicle.setWidth(baseVehicleWidth * vehicleStageScale);
                c_mainVehicle.setHeight(baseVehicleHeight * vehicleStageScale);
            }
        }

        var onError = function (reason) {
            $scope.error = "Unable to get years,makes,models,submodels "  + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var onVehicleError = function (reason) {
            $scope.error = "Unable to get vehicle " + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetVehicleComplete = function (data) {
            var vr = JSON.parse(data);
            if (vr.products[0].productFormats[0].assets.length > 0) {
                var v = vr.products[0].productFormats[0].assets[0];
                chooseVehicle(vr.id, v, vr.products[0].productFormats[0].id);

                jQuery("#btn-save-close").prop("disabled", false);
                jQuery("#btn-save-next").prop("disabled", false);
                jQuery("#btn-next").prop("disabled", false);
            }
            else {
                jQuery("#btn-save-close").prop("disabled", true);
                jQuery("#btn-save-next").prop("disabled", true);
                jQuery("#btn-next").prop("disabled", true);
            }
        };

        var onSubModelsComplete = function (data) {
            var subModels = [];
            for (var i = 0; i < data.length; ++i) {
                subModels.push(data[i]);
            }
            $scope.subModels = subModels;
            $scope.subModel = subModels[0];

            getVehicleImages().then(onGetVehicleComplete, onVehicleError);
        };

        var onModelsComplete = function (data) {
            
            var models = [];
            for (var i = 0; i < data.length; ++i) {
                models.push(data[i]);
            }
            $scope.models = models;
            $scope.model = models[0];

            if ($scope.year && $scope.make && $scope.model) {
                getSubModels().then(onSubModelsComplete, onError);
            }
        };

        var onMakesComplete = function (data) {                       
            var makes = [];
            for (var i = 0; i < data.length; ++i) {
                makes.push(data[i]);
            }
            $scope.makes = makes;
            $scope.make = makes[0];
            if ($scope.year && $scope.make) {
                getModels().then(onModelsComplete, onError);
            }
        };

        function getYears() {
            var y = [];
            for (var i = 2017; i >= 1999; i--) {
                y.push(i);
            }

            $scope.years = y;
            $scope.year = y[0];
            getMakes().then(onMakesComplete, onError);
        };

        function getMakes() {
            return $http.get("/Vehicle/GetMakes/?y=" + $scope.year).
                then(function (response) {
                    return response.data;
                });
        };

        function getModels() {            
            return $http.get("/Vehicle/GetModels/?y=" + $scope.year + "&m=" + $scope.make).
                then(function (response) {
                    return response.data;
                });
        };

        function getSubModels() {
            return $http.get("/Vehicle/GetSubModels/?y=" + $scope.year + "&m=" + $scope.make + "&mdl=" + $scope.model).
                then(function (response) {
                    return response.data;
                });
        };

        function getVehicleImages() {
            var o = "Front";
            return $http.get("/Vehicle/GetVehicleImages/?y=" + $scope.year + "&m=" + $scope.make + "&mdl=" + $scope.model + "&sm=" + $scope.subModel + "&o=" + o).
                then(function (response) {
                    return response.data;
                });
        };

        function chooseVehicle(vid, v, fid) {
            //console.log("@Url.Action("GetCroppedImage", "Vehicle")?url=" + v.url + "&vid=" + vid + "&code=" + v.shotCode.code + "&fid=" + fid);
            setVehicleBackground(c_mainVehicle, "/Vehicle/GetCroppedImage?url=" + v.url + "&vid=" + vid + "&code=" + v.shotCode.code + "&fid=" + fid);
            //loadWheelPosition(vid);
            return;
        };

        function setVehicleBackground(canvas, image) {
            if (!c_mainVehicle)
                return;

            resetVehicleCanvas();

            //Test if Wheels Already Loaded
            if (group_wvF) {
                // Remove Previous Objects since layers may be different
                c_mainVehicle.remove(group_wvF);
                c_mainVehicle.remove(group_wvR);

                group_wvF = null;
                group_wvR = null;
            }
            if (img_vc) {
                c_mainVehicle.remove(img_vc);
                img_vc = null;
            }

            canvas.overlayImage = null;
            canvas.setBackgroundImage(image, canvas.renderAll.bind(canvas), {
                width: canvas.width,
                height: canvas.height,
                // Needed to position backgroundImage at 0/0
                originX: 'left',
                originY: 'top'
            });
            if (image == '') {
                canvas.backgroundImage = false;
                jQuery('#vtws-mainImage').css('visibility', 'hidden');
                jQuery("#vtws-mainActions").hide();
            }
            else {
                jQuery("#vtws-mainImage").css('visibility', 'visible');
                jQuery("#vtws-mainActions").show();
            }

            flipWheelToggle = false;
        }

        function loadWheelPosition(vid) {
            var y = $("#vtws-vehicle-year").val();
            var m = $("#vtws-vehicle-make").val();
            var mdl = $("#vtws-vehicle-model").val();
            var sm = $("#vtws-vehicle-sub-model").val();
            var o = $("#vtws-orientation").data("orientation");

            $.get("/Vehicle/GetWheelPosition/?vid=" + vid + "&y=" + y + "&m=" + m + "&mdl=" + mdl + "&sm=" + sm + "&o=" + o, function (data) {
                console.log(data);
                var w = data;
                var wheel = o_wheel1;
                console.log("VehicleID: " + w.VehicleID);
                if (w.VehicleID == null) { //No data was found
                    if (o == "Front") {
                        wheel.wvF_left = 1001;
                        wheel.wvF_top = 694;
                        wheel.wvF_width = 184;
                        wheel.wvF_height = 268;
                        wheel.wvF_angle = 0;

                        wheel.wvR_left = 1831;
                        wheel.wvR_top = 652;
                        wheel.wvR_width = 120;
                        wheel.wvR_height = 217;
                        wheel.wvR_angle = 0;
                    }
                    else {
                        wheel.wvF_left = 1847;
                        wheel.wvF_top = 664;
                        wheel.wvF_width = 125;
                        wheel.wvF_height = 221;
                        wheel.wvF_angle = 2.431;

                        wheel.wvR_left = 1008;
                        wheel.wvR_top = 690;
                        wheel.wvR_width = 188;
                        wheel.wvR_height = 279;
                        wheel.wvR_angle = 1.838;
                    }
                }
                else { //Data was found or loaded from following year
                    wheel.wvF_left = w.FrontLeft;
                    wheel.wvF_top = w.FrontTop;
                    wheel.wvF_width = w.FrontWidth;
                    wheel.wvF_height = w.FrontHeight;
                    wheel.wvF_angle = w.FrontAngle;

                    wheel.wvR_left = w.RearLeft;
                    wheel.wvR_top = w.RearTop;
                    wheel.wvR_width = w.RearWidth;
                    wheel.wvR_height = w.RearHeight;
                    wheel.wvR_angle = w.RearAngle;
                }

                if (w.VehicleID == null || w.VehicleID == 0)
                    $("#btn-next").hide();
                else
                    $("#btn-next").show();

                loadWheels(wheel);
            });
        };

        function getVehicleImagesOld() {
            var y = $("#vtws-vehicle-year").val();
            var m = $("#vtws-vehicle-make").val();
            var mdl = $("#vtws-vehicle-model").val();
            var sm = $("#vtws-vehicle-sub-model").val();
            var o = $("#vtws-orientation").data("orientation");

            setVehicleBackground(c_mainVehicle, '');
            //console.log("@Url.Action("GetVehicleImages", "Vehicle")/?y=" + y + "&m=" + m + "&mdl=" + mdl + "&sm=" + sm + "&o=" + o);
            $.get("/Vehicle/GetVehicleImages/?y=" + y + "&m=" + m + "&mdl=" + mdl + "&sm=" + sm + "&o=" + o, function (data) {
                var vr = JSON.parse(data);
                if (vr.products[0].productFormats[0].assets.length > 0) {
                    var v = vr.products[0].productFormats[0].assets[0];
                    chooseVehicle(vr.id, v, vr.products[0].productFormats[0].id);

                    $("#btn-save-close").prop("disabled", false);
                    $("#btn-save-next").prop("disabled", false);
                    $("#btn-next").prop("disabled", false);
                }
                else {
                    $("#btn-save-close").prop("disabled", true);
                    $("#btn-save-next").prop("disabled", true);
                    $("#btn-next").prop("disabled", true);
                }
            });
        }

        $scope.changeYear = function() {
            getMakes().then(onMakesComplete, onError);
        };

        $scope.changeMake = function () {
            getModels().then(onModelsComplete, onError);
        };

        $scope.changeModel = function () {
            getSubModels().then(onSubModelsComplete, onError);
        };

        activate();

        function activate() {
            getYears();
        }
    }
})();
