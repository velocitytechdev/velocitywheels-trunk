﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('vehicleMappingController', vehicleMappingController);

    vehicleMappingController.$inject = ['$scope', '$http'];

    function vehicleMappingController($scope, $http) {
        $scope.title = 'Vehicle Mapping';

        $scope.years = [];
        $scope.makes = [];
        $scope.models = [];
        $scope.bodyTypes = [];
        $scope.subModels = [];
        $scope.matches = [];

        var onPossibleMatchesComplete = function (data) {
            $scope.savingLoading = false;
            $scope.matches = data;
        };

        var onError = function (reason) {
            $scope.savingLoading = false;
            $scope.error = "Unable to get years,makes,models,submodels " + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var onSubModelsComplete = function (data) {
            $scope.savingLoading = false;
            $scope.subModels = data;
            if (!$scope.preSelect) {
                $scope.subModel = data[0];
            } else {
                if ($scope.subModel) {
                    $scope.subModel = $scope.subModel;
                } else {
                    $scope.subModel = data[0];
                }
                
                /*for (var i; data.length; ++i) {
                    var v = data[i];
                    if (v.SubModel === $scope.subModel.SubmModel) {
                        $scope.subModel = v;
                        break;
                    }
                }*/
                
            }
            $scope.currentSubModel = $scope.subModel;
            $scope.preSelect = false;
            // get fuel data
            if ($scope.year && $scope.make) {
                getPossibleMatches($scope.subModel.Id).then(onPossibleMatchesComplete, onError);
            }
        };

        var onBodyTypesComplete = function (data) {
            $scope.bodyTypes = data;            
            if (!$scope.preSelect) {
                $scope.bodyType = data[0];
            } else {
                if ($scope.subModel && $scope.subModel.BodyType) {
                    $scope.bodyType = $scope.subModel.BodyType;
                } else {
                    $scope.bodyType = data[0];
                }                
            }
            
            if ($scope.year && $scope.make && $scope.model && $scope.bodyType) {
                if (!$scope.preSelect) {
                    getSubModels(null).then(onSubModelsComplete, onError);
                } else {
                    getSubModels($scope.subModel != null ? $scope.subModel.Id : null).then(onSubModelsComplete, onError);
                }                
            }
        };

        var onModelsComplete = function (data) {
            $scope.models = data;
            if (!$scope.preSelect) {
                $scope.model = data[0];
            } else {
                if ($scope.subModel && $scope.subModel.Model) {
                    $scope.model = $scope.subModel.Model;
                } else {
                    $scope.model = data[0];
                }                
            }
            
            if ($scope.year && $scope.make && $scope.model) {
                getBodyTypes().then(onBodyTypesComplete, onError);
            }
        };

        var onMakesComplete = function (data) {
            $scope.makes = data;
            if (!$scope.preSelect) {
                $scope.make = data[0];
            } else {
                if ($scope.subModel && $scope.subModel.Make) {
                    $scope.make = $scope.subModel.Make;
                } else {
                    $scope.make = data[0];
                }
            }
            if ($scope.year && $scope.make) {
                getModels().then(onModelsComplete, onError);
            }            
        };

        var onYearsComplete = function (data) {            
            $scope.years = data;
            $scope.year = $scope.years[0];
            getMakes().then(onMakesComplete, onError);
        };

        function getYears() {
            $scope.savingLoading = true;
            return $http.get("/Vehicle/GetAcesYears").
                then(function (response) {
                    return response.data;
                });
        };

        function getMakes() {
            $scope.savingLoading = true;
            return $http.get("/Vehicle/GetAcesMakes/?year=" + $scope.year).
                then(function (response) {
                    return response.data;
                });
        };

        function getModels() {
            $scope.savingLoading = true;
            return $http.get("/Vehicle/GetAcesModels/?year=" + $scope.year + "&make=" + $scope.make).
                then(function (response) {
                    return response.data;
                });
        };

        function getBodyTypes() {
            $scope.savingLoading = true;
            return $http.get("/Vehicle/GetAcesBodyTypes/?year=" + $scope.year + "&make=" + $scope.make + "&model=" + $scope.model).
                then(function (response) {
                    return response.data;
                });
        };

        function getSubModels(id) {
            $scope.savingLoading = true;
            return $http.get("/Vehicle/GetAcesSubModels/?year=" + $scope.year + "&make=" +
                $scope.make + "&model=" + $scope.model + "&bodyType=" + $scope.bodyType + "&id=" + (id == null ? '' : id) ).
                then(function (response) {
                    return response.data;
                });
        };

        function getPossibleMatches(id) {
            return $http.get("/Vehicle/GetFuelPossibleMatches/?year=" + $scope.year + "&make=" + $scope.make + "&id=" + id).
                then(function (response) {
                    return response.data;
                });
        };

        $scope.changeYear = function () {
            if (!$scope.preSelect) {
                getMakes().then(onMakesComplete, onError);
            }
        };

        $scope.changeMake = function () {
            if (!$scope.preSelect) {
                getModels().then(onModelsComplete, onError);
            }
        };

        $scope.changeModel = function () {
            if (!$scope.preSelect) {
                if ($scope.year && $scope.make && $scope.model) {
                    getBodyTypes().then(onBodyTypesComplete, onError);
                }
            }            
        };

        $scope.changeBodyType = function () {
            if (!$scope.preSelect) {
                if ($scope.year && $scope.make && $scope.model && $scope.bodyType) {
                    getSubModels().then(onSubModelsComplete, onError);
                }
            }
            
        };

        $scope.changeSubModel = function () {
            if (!$scope.preSelect) {
                // get fuel data
                $scope.subModel = $scope.currentSubModel;
                if ($scope.year && $scope.make) {
                    getPossibleMatches($scope.subModel.Id).then(onPossibleMatchesComplete, onError);
                }
            }
            
        };

        var onUpdateError = function (reason) {
            $scope.savingLoading = false;
            console.log(reason);
        };

        var onUpdateComplete = function (data) {
            $scope.savingLoading = false;
            if ($scope.year && $scope.make) {
                $scope.savingLoading = true;                
                getPossibleMatches($scope.subModel.Id).then(onPossibleMatchesComplete, onError);
            }
        };

        function updateMapping(fromId, toId) {
            $scope.savingLoading = true;
            return $http.post("/Vehicle/UpdateMapping/?fromId=" + fromId + "&toId=" + toId).
                then(function (response) {
                    return response.data;
                });
        };

        var onGetNextError = function (reason) {
            $scope.savingLoading = false;
            console.log(reason);
        };

        var onGetNextComplete = function (data) {            
            $scope.savingLoading = false;
            $scope.makes = [];
            $scope.models = [];
            $scope.bodyTypes = [];
            $scope.subModels = [];
            $scope.matches = [];            
            $scope.year = data.Year;
            $scope.make = data.Make;
            $scope.model = data.Model;
            $scope.bodyType = data.BodyType;
            $scope.subModel = angular.copy(data);
            console.log($scope.subModel);
            if ($scope.year && $scope.make) {
                $scope.savingLoading = true;
                $scope.preSelect = true;
                getMakes().then(onMakesComplete, onError);
            }
        };

        function getNextUnmapped(id) {
            $scope.savingLoading = true;
            return $http.get("/Vehicle/GetNextUnmapped?currentId=" + $scope.subModel.Id + "&year=" + $scope.year).
                then(function (response) {
                    return response.data;
                });
        };

        function getNext() {
            $scope.savingLoading = true;
            return $http.get("/Vehicle/GetNextVehicle?currentId=" + $scope.subModel.Id + "&year=" + $scope.year + "&make=" +
                $scope.make + "&model=" + $scope.model + "&bodyType=" + $scope.bodyType + "&subModel=" + $scope.subModel.SubModel).
                then(function (response) {
                    return response.data;
                });
        };

        $scope.updateDefault = function (fuelId) {
            updateMapping($scope.subModel.Id, fuelId).then(onUpdateComplete, onUpdateError);
        };

        $scope.nextUnmapped = function () {
            getNextUnmapped($scope.subModel.Id).then(onGetNextComplete, onGetNextError);
        };

        $scope.next = function () {
            getNext().then(onGetNextComplete, onGetNextError);
        };

        /*$scope.pageKeyUp = function (e) {
            alert(e.keyCode);
            if (e.keyCode === 78) {
                $scope.next();
            }

            if (e.keyCode === 85) {
                $scope.nextUnmapped();
            }
        };*/

        activate();

        function activate() {
            document.body.addEventListener('keypress',
                function (event) {
                    console.log(event);
                    if (event.key === 85 || event.keyCode === 117) {
                        $scope.nextUnmapped();                        
                    }

                    if (event.keyCode === 78 || event.keyCode === 110) {
                        $scope.next();
                    }
                });
            $scope.preSelect = false;
            getYears().then(onYearsComplete, onError);
        }
    }
})();
