﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('vehicleUploadImageController', vehicleUploadImageController);

    vehicleUploadImageController.$inject = ['$scope', 'userService', '$stateParams', '$http', '$state', 'vehicleService'];

    function vehicleUploadImageController($scope, userService, $stateParams, $http, $state, vehicleService) {
        $scope.title = 'vehicleUploadImageController';

        $scope.uploadMessage = "";

        $scope.isDirty = false;
        $scope.Vehicle = { Image: null, Id: -1 };

        $scope.Vehicles = null;

        $scope.years = [];
        $scope.makes = [];
        $scope.models = [];
        $scope.bodyTypes = [];
        $scope.subModels = [];       

        /*$scope.imageUploaded = function () {
            return true;
            //return $scope.Vehicle !== null && $scope.Vehicle.Id !== -1;
        };*/

        var onVehiclesError = function (reason) {
            $scope.savingLoading = false;
            $scope.error = "Unable to get vehicles " + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var onVehiclesComplete = function (data) {
            $scope.Vehicles = data;
        };

        var onError = function (reason) {
            $scope.savingLoading = false;
            $scope.error = "Unable to get years,makes,models,submodels " + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var onBodyTypesComplete = function (data) {
            $scope.bodyTypes = data;
            if (!$scope.preSelect) {
                $scope.bodyType = data[0];
            } else {
                if ($scope.subModel && $scope.subModel.BodyType) {
                    $scope.bodyType = $scope.subModel.BodyType;
                } else {
                    $scope.bodyType = data[0];
                }
            }
        };

        var onModelsComplete = function (data) {
            $scope.models = data;
            if (!$scope.preSelect) {
                $scope.model = data[0];
            } else {
                if ($scope.subModel && $scope.subModel.Model) {
                    $scope.model = $scope.subModel.Model;
                } else {
                    $scope.model = data[0];
                }
            }

            if ($scope.year && $scope.make && $scope.model) {
                //getBodyTypes().then(onBodyTypesComplete, onError);
                getVehicles().then(onVehiclesComplete, onVehiclesError);
            }
        };

        var onMakesComplete = function (data) {
            $scope.makes = data;
            if (!$scope.preSelect) {
                $scope.make = data[0];
            } else {
                if ($scope.subModel && $scope.subModel.Make) {
                    $scope.make = $scope.subModel.Make;
                } else {
                    $scope.make = data[0];
                }
            }
            if ($scope.year && $scope.make) {
                getModels().then(onModelsComplete, onError);
            }
        };

        var onYearsComplete = function (data) {
            $scope.years = data;
            $scope.year = $scope.years[0];
            getMakes().then(onMakesComplete, onError);
        };

        var onGetAssignedVehiclesError = function (reason) {
            $scope.error = "Unable to get assinged vehicles " + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetAssingedVehiclesComplete = function (data) {
            $scope.Vehicle = data;
            console.log(data);
        };

        var onGetUserComplete = function (data) {
            // todo check for any security items
            if ($stateParams.id) {
                vehicleService.getAssignedImages($stateParams.id).then(onGetAssingedVehiclesComplete, onGetAssignedVehiclesError);
            } else {
            }
            getYears().then(onYearsComplete, onError);
        };

        var onGetUserError = function (reason) {
            $scope.error = "Unable to get user : status code= " + reason.status + ", " + reason.statusText;
        };

        var onSaveError = function (reason) {
            $scope.error = "Unable to save image : status code= " + reason.status + ", " + reason.statusText;
        };                 

        function getYears() {
            $scope.savingLoading = true;
            return $http.get("/Vehicle/GetAcesYears").
                then(function (response) {
                    return response.data;
                });
        };

        function getMakes() {
            $scope.savingLoading = true;
            return $http.get("/Vehicle/GetAcesMakes/?year=" + $scope.year).
                then(function (response) {
                    return response.data;
                });
        };

        function getModels() {
            $scope.savingLoading = true;
            return $http.get("/Vehicle/GetAcesModels/?year=" + $scope.year + "&make=" + $scope.make).
                then(function (response) {
                    return response.data;
                });
        };

        function getBodyTypes() {
            $scope.savingLoading = true;
            return $http.get("/Vehicle/GetAcesBodyTypes/?year=" + $scope.year + "&make=" + $scope.make + "&model=" + $scope.model).
                then(function (response) {
                    return response.data;
                });
        };

        $scope.changeYear = function () {
            getMakes().then(onMakesComplete, onError);
        };

        $scope.changeMake = function () {
            getModels().then(onModelsComplete, onError);
        };

        $scope.changeModel = function () {
            if (!$scope.preSelect) {
                if ($scope.year && $scope.make && $scope.model) {
                    getBodyTypes().then(onBodyTypesComplete, onError);
                }
            }

            getVehicles().then(onVehiclesComplete,onVehiclesError);
        };

        function getVehicles(bodyType) {
            var url = "/Vehicle/GetVehiclesForCustomUploader/?imageId=" + $scope.Vehicle.Id + "&make=" + $scope.make + "&model=" + $scope.model;
            if (bodyType !== null && bodyType !== undefined) {
                url += "&bodyType=" + bodyType;
            }
            return $http.get(url).
                then(function (response) {
                    return response.data;
                });
        };        

        var onSetImageError = function (reason) {
            $scope.savingLoading = false;
            $scope.error = "Unable to set vehicle image " + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var onSetImageComplete = function (data) {
            if (data.Error !== null) {
                alert("Vehicle image set, error = " + data.Error);
            } else {
                vehicleService.getAssignedImages($scope.Vehicle.Id).then(onGetAssingedVehiclesComplete, onGetAssignedVehiclesError);
                getVehicles().then(onVehiclesComplete, onVehiclesError);
            }
        };

        $scope.setImage = function (acesId) {
            vehicleService.setVehicleImage(acesId, $scope.Vehicle.Id).then(onSetImageComplete, onSetImageError);
        };

        $scope.removeImage = function (acesId) {
            vehicleService.removeVehicleImage(acesId, $scope.Vehicle.Id).then(onSetImageComplete, onSetImageError);
        };

        var onSaveDescriptionComplete = function (data) {
            if (data.Error !== null) {
                alert("Vehicle description save error, " + data.Error);
            } else {
                vehicleService.getAssignedImages($scope.Vehicle.Id).then(onGetAssingedVehiclesComplete, onGetAssignedVehiclesError);
            }
        };

        var onSaveDescriptionError = function (reason) {
            $scope.savingLoading = false;
            $scope.error = "Unable to save description " + " : status code= " + reason.status + ", " + reason.statusText;
        };

        $scope.updateDescription = function() {
            vehicleService.saveDescription($scope.Vehicle.Id, $scope.Vehicle.Description).then(onSaveDescriptionComplete, onSaveDescriptionError);
        };

        activate();

        function activate() {
            userService.getCurrentUser().then(onGetUserComplete, onGetUserError);

            $scope.preSelect = false;            
        }
    }
})();
