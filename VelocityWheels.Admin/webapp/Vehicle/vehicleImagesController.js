﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('vehicleImagesController', vehicleImagesController);

    vehicleImagesController.$inject = ['$scope', 'vehicleService', 'userService', '$state', 'FileUploader'];

    function vehicleImagesController($scope, vehicleService, userService, $state, FileUploader) {
        $scope.title = 'vehicleImagesController';

        $scope.images = null;

        var onGetImagesError = function (reason) {
            $scope.error = "Unable to get images : status code= " + reason.status + ", " + reason.statusText;
            console.log(reason);
        };

        var onGetImagesComplete = function (data) {
            $scope.images = data;
        };

        var uploader = $scope.uploader = new FileUploader({
            url: "Vehicle/UploadCustomImage"
        });

        uploader.onAfterAddingFile = function (fileItem) {
            $scope.Vehicle = { Image: null, Id: -1 };
            var reader = new FileReader();

            reader.onload = function (e) {
                $scope.$apply(function () {
                    $scope.Vehicle.Image = e.target.result;
                });
            }

            reader.readAsDataURL(fileItem._file);

            fileItem.removeAfterUpload = true;
            fileItem.formData.push({ id: $scope.Vehicle.Id, name: fileItem.name });
        };

        uploader.onBeforeUploadItem = function (item) {
            $scope.uploadMessage = "Uploading ...";
            console.log(item);
        };

        uploader.onProgressItem = function (fileItem, progress) {
            $scope.uploadMessage = "Processing ...";
        };

        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            if (response.Error == null) {
                $scope.uploadMessage = "";
                $scope.Vehicle = response;
                alert("Image Uploaded");
                vehicleService.getVehicleImages().then(onGetImagesComplete, onGetImagesError);
            } else {
                // failed, need to present message to user
                $scope.uploadMessage = response.Error;
            }
        };

        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status);
            $scope.uploadMessage = "Error uploading file";
        };

        uploader.onCompleteAll = function () {
            /*wheelService.getWheelById($stateParams.id).then(onGetWheelComplete, onGetWheelError);*/

        };

        $scope.positionWheels = function (imageId) {
            $state.go('positionWheelsWithParams', { id: imageId });
        };

        $scope.saveChanges = function () {
            uploader.uploadAll();
        };

        var onRemoveImagesComplete = function (data) {
            vehicleService.getVehicleImages().then(onGetImagesComplete, onGetImagesError);
        };

        var onRemoveImagesError = function (reason) {
            $scope.error = "Unable to remove image : status code= " + reason.status + ", " + reason.statusText;
        };

        $scope.removeImage = function(id) {
            vehicleService.removeUploadedImage(id).then(onRemoveImagesComplete, onRemoveImagesError);
        };

        $scope.cancelChanges = function () {
            uploader.clearQueue();
            $scope.Vehicle.Image = null;
        };        

        var onGetUserComplete = function (data) {
            // todo check for any security items
            vehicleService.getVehicleImages().then(onGetImagesComplete, onGetImagesError);
        };

        var onGetUserError = function (reason) {
            $scope.error = "Unable to get user : status code= " + reason.status + ", " + reason.statusText;
        };

        activate();

        function activate() {
            userService.getCurrentUser().then(onGetUserComplete, onGetUserError);

        }
    }
})();
