﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('brandsController', brandsController);

    brandsController.$inject = ['$scope', 'dashboardService']; 

    function brandsController($scope, dashboardService) {
        $scope.title = 'brandsController';

        $scope.brands = {};

        var getBrandsComplete = function (data) {
            $scope.brands = data;
        };

        var getBrandsError = function (reason) {
            $scope.error = "Unable to get brands : status code= " + reason.status + ", " + reason.statusText;
        };

        activate();

        function activate() {
            dashboardService.getBrands().then(getBrandsComplete, getBrandsError);
        }
    }
})();
