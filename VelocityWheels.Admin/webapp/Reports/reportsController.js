﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('reportsController', reportsController);

    reportsController.$inject = ['$scope', 'userService', '$http'];

    function reportsController($scope, userService, $http) {
        $scope.title = 'reportsController';

        $scope.generatingFitmentReport = false;
        $scope.generatingMissingPositionsReport = false;
        $scope.hasAdminAccess = false;

        var onGetUserError = function (reason) {
            $scope.error = "Unable to get user : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetUserComplete = function (data) {
            // todo check for any security items              
        };

        var loadFile = function (blob, name) {
            var URL = (window.URL || window.webkitURL);
            var a = document.createElement('a');
            //var name = (formatter.options.filename + "." + formatter.options.fileExtension);
            if (a.download !== void 0) {
                var url = URL.createObjectURL(blob);
                a.style.display = 'none';
                a.setAttribute('href', url);
                a.setAttribute('download', name);
                document.body.appendChild(a);
                a.dispatchEvent(new MouseEvent('click'));
                document.body.removeChild(a);
                setTimeout(function () {
                    URL.revokeObjectURL(url);
                }, 100);
            } else if (navigator.msSaveOrOpenBlob) {
                navigator.msSaveOrOpenBlob(blob, name);
            }
        };

        $scope.generateMissingPositionsReport = function () {
            $scope.generatingMissingPositionsReport = true;
            $scope.reportMessage = "Creating Report, please wait ...";

            $http({
                url: 'Reports/MissingPositionsReport',
                method: 'POST',
                params: {},
                headers: {
                    'Content-type': 'application/csv'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                $scope.generatingMissingPositionsReport = false;
                $scope.reportMessage = "Report created!";
                var file = new Blob(([data]), { type: 'application/csv' });
                loadFile(file, 'wheelpositionsreport.csv');
            }).error(function (reason, status, headers, config) {
                $scope.generatingMissingPositionsReport = false;
                $scope.reportMessage = "Report failed " + reason;
                //alert("report download failed");
            });
        };

        $scope.generateFitmentReport = function () {
            $scope.generatingFitmentReport = true;
            $scope.fitmentMessage = "Creating Report, please wait ...";

            $http({
                url: 'Reports/UnmatchedFitmentReport',
                method: 'POST',
                params: {},
                headers: {
                    'Content-type': 'application/csv'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                $scope.generatingFitmentReport = false;
                $scope.fitmentMessage = "Report created!";
                var file = new Blob(([data]), { type: 'application/csv' });
                loadFile(file, 'unmatchedfitmentsreport.csv');
            }).error(function (reason, status, headers, config) {
                $scope.generatingFitmentReport = false;
                $scope.fitmentMessage = "Report failed " + reason;
            });
        };

        $scope.generateMappingReport = function () {
            $scope.generatingMappingReport = true;
            $scope.mappingMessage = "Creating Report, please wait ...";

            $http({
                url: 'Reports/MappingReport',
                method: 'POST',
                params: {},
                headers: {
                    'Content-type': 'application/csv'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                $scope.generatingMappingReport = false;
                $scope.mappingMessage = "Report created!";
                var file = new Blob(([data]), { type: 'application/csv' });
                loadFile(file, 'mappingreport.csv');
            }).error(function (reason, status, headers, config) {
                $scope.generatingMappingReport = false;
                $scope.mappingMessage = "Report failed " + reason;
            });
        };

        $scope.downloadFitmentGuide = function () {
            $scope.downloadingFitmentGuide = true;
            $scope.fitmentGuideMessage = "Downloading, please wait ...";

            $http({
                url: 'Reports/FitmentGuide',
                method: 'POST',
                params: {},
                headers: {
                    'Content-type': 'application/csv'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                $scope.downloadingFitmentGuide = false;
                $scope.fitmentGuideMessage = "Download complete!";
                var file = new Blob(([data]), { type: 'application/csv' });
                loadFile(file, 'fitmentguide.csv');
            }).error(function (reason, status, headers, config) {
                $scope.downloadingFitmentGuide = false;
                $scope.fitmentGuideMessage = "Report failed " + reason;
            });
        };


        activate();

        function activate() {
            userService.getCurrentUser().then(onGetUserComplete, onGetUserError);
        }
    }
})();
