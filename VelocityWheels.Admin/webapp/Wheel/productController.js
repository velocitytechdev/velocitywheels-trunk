﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('productController', productController);

    productController.$inject = ['$scope', 'userService', 'wheelService', '$stateParams'];

    function productController($scope, userService, wheelService, $stateParams) {
        $scope.title = 'productController';

        $scope.Product = {};

        $scope.accordingClick = function (chassis, partnumber) {
           
        };

        var onGetProductComplete = function (data) {
            $scope.Product = data;
            console.log(data);
        };

        var onGetProductError = function (reason) {
            $scope.error = "Unable to get product " + $stateParams.id + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetFitmentsComplete = function (data) {
            $scope.Product.Fitments = data.Fitments;
            $scope.Product.RestrictedFitments = data.RestrictedFitments;
        };

        var onGetFitmentsError = function (reason) {
            $scope.error = "Unable to get fitments " + $stateParams.id + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetUserError = function (reason) {
            $scope.error = "Unable to get user : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetUserComplete = function (data) {
            // todo check for any security items

            wheelService.getProduct($stateParams.id).then(onGetProductComplete, onGetProductError);
        };

        var onUpdateFitmentComplete = function (data) {
            if (data.Error) {
                alert(data.Error);
            } else {
                $scope.Product.Fitments = [];
                wheelService.getProduct($scope.Product.WheelSpecId).then(onGetFitmentsComplete, onGetFitmentsError);
            }
        };

        var onUpdateRestrictedFitmentComplete = function (data) {
            if (data.Error) {
                alert(data.Error);
            } else {
                $scope.Product.RestrictedFitments = [];
                wheelService.getProduct($scope.Product.WheelSpecId).then(onGetFitmentsComplete, onGetFitmentsError);
            }
        };

        var onUpdateFitmentError = function (reason) {
            $scope.error = "Unable to update fitment " + $stateParams.id + " : status code= " + reason.status + ", " + reason.statusText;
        };

        $scope.restrictFitment = function (fitments, partnumber, $event) {
            if ($event.StopPropagation) {
                $event.StopPropagation();
            }

            $event.preventDefault();

            var idList = [];
            for (var i = 0; i < fitments.length; ++i) {
                idList.push(fitments[i].ClassificationId);
            }            
           
            wheelService.removeFitment(idList, partnumber).then(onUpdateFitmentComplete, onUpdateFitmentError);
        };

        $scope.enableFitment = function (fitments, partnumber) {
            var idList = [];
            for (var i = 0; i < fitments.length; ++i) {
                idList.push(fitments[i].ClassificationId);
            }
            
            wheelService.enableFitment(idList, partnumber).then(onUpdateRestrictedFitmentComplete, onUpdateFitmentError);
        };

        activate();

        function activate() {
            if ($stateParams.id) {
                userService.getCurrentUser().then(onGetUserComplete, onGetUserError);
            } else {
                alert("No brand id found");
            }
        }
    }
})();
