﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('wheelController', wheelController);

    wheelController.$inject = ['$scope', 'wheelService', 'userService', "$stateParams"];

    function wheelController($scope, wheelService, userService, $stateParams) {
        $scope.title = 'wheelController';

        $scope.Wheel = {};

        var onGetWheelComplete = function (data) {
            $scope.Wheel = data;
            console.log(data);
        };

        var onGetWheelError = function (reason) {
            $scope.error = "Unable to get wheel " + $stateParams.brand + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetUserError = function (reason) {
            $scope.error = "Unable to get user : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetUserComplete = function (data) {
            // todo check for any security items
            wheelService.getWheel($stateParams.brand, $stateParams.style, $stateParams.finish).then(onGetWheelComplete, onGetWheelError);
        };

        var onSaveError = function (reason) {
            $scope.error = "Unable to save wheel : status code= " + reason.status + ", " + reason.statusText;
        };

        var onSaveComplete = function (data) {
            if (data.Error !== null) {
                $scope.error = "Unable to save wheel, " + data.Error;
                $scope.Wheel.Active = data.Active;
                alert($scope.error);
            } else {
                //wheelService.getWheel($stateParams.brand, $stateParams.style, $stateParams.finish).then(onGetWheelComplete, onGetWheelError);
                alert("Wheel Saved");
            }
            
        };

        $scope.save = function() {
            wheelService.saveWheel($scope.Wheel).then(onSaveComplete,onSaveError);
        };

        activate();

        function activate() {
            if ($stateParams.brand && $stateParams.style && $stateParams.finish) {
                userService.getCurrentUser().then(onGetUserComplete, onGetUserError);
            } else {
                alert("Wheel ids not provided");
            }
        }
    }
})();
