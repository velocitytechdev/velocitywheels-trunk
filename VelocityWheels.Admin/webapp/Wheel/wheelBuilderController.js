﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('wheelBuilderController', wheelBuilderController);

    wheelBuilderController.$inject = ['$scope', 'userService', "$stateParams", "wheelService", "FileUploader", "$window"];

    function wheelBuilderController($scope, userService, $stateParams, wheelService, FileUploader, $window) {
        $scope.title = 'wheelBuilderController';

        $scope.uploadMessage = "";

        $scope.newLayerId = -1;
        $scope.isDirty = false;

        var uploader = $scope.uploader = new FileUploader({
            url: "Wheels/UploadBaseLayer"
        });

        $scope.Wheel = {};

        var onGetWheelComplete = function (data) {
            $scope.Wheel = data;
            var layers = $scope.Wheel.WheelLayers;
            for (var i = 0; i < layers.length; ++i) {
                layers[i].boptions = { levelName: layers[i].LevelName, name: 'base', LayerId: layers[i].Id };
                layers[i].foptions = { levelName: layers[i].LevelName, name: 'front', LayerId: layers[i].Id };
                layers[i].roptions = { levelName: layers[i].LevelName, name: 'rear', LayerId: layers[i].Id };
            }
            console.log(data);
        };

        var onGetWheelError = function (reason) {
            $scope.error = "Unable to get wheel " + $stateParams.id + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var getLayerById = function(layerId) {
            var layers = $scope.Wheel.WheelLayers;
            for (var i = 0; i < layers.length; ++i) {
                if (layers[i].Id == layerId) {
                    return layers[i];
                }
            }
            return null;
        };

        uploader.onAfterAddingFile = function (fileItem) {

            var reader = new FileReader();
            var layer = getLayerById(fileItem.LayerId);

            if (fileItem.name === "base") {
                reader.onload = function (e) {
                    $scope.$apply(function () {
                         
                        if (!fileItem.LayerId) {                                                        
                            $scope.Wheel.BaseLayerImage = e.target.result;
                        } else if (layer) {
                            layer.BaseLayerImage = e.target.result;
                            fileItem.levelName = layer.LevelName;
                        }
                    });
                }
            }
            else if (fileItem.name === "front") {
                reader.onload = function(e) {
                    $scope.$apply(function () {
                        if (!fileItem.LayerId) {
                            $scope.Wheel.BaseFrontImage = e.target.result;                            
                        } else if (layer) {
                            layer.FrontLayerImage = e.target.result;
                            fileItem.levelName = layer.LevelName;
                        }                        
                    });
                }                
            }
            else if (fileItem.name === "rear") {
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        if (!fileItem.LayerId) {
                            $scope.Wheel.BaseRearImage = e.target.result;
                        } else if (layer) {
                            layer.RearLayerImage = e.target.result;
                            fileItem.levelName = layer.LevelName;
                        }
                    });
                }
            }
            else if (fileItem.name === "thumb") {
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.Wheel.Image = e.target.result;
                    });
                }
            }

            reader.readAsDataURL(fileItem._file);
            
            fileItem.removeAfterUpload = true;
            fileItem.formData.push({ id: $scope.Wheel.WheelId, name: fileItem.name, LayerId: fileItem.LayerId });
            console.info('onAfterAddingFile', fileItem);
        };

        uploader.onBeforeUploadItem = function (item) {
            $scope.uploadMessage = "Uploading ...";
        };

        uploader.onProgressItem = function (fileItem, progress) {
            $scope.uploadMessage = "Processing ...";
        };

        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            if (response.Error == null) {
                $scope.uploadMessage = "UPLOAD COMPLETE";
            } else {
                $scope.uploadMessage = response.Error;
            }
        };

        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status);
            $scope.uploadMessage = "Error uploading file";
        };

        uploader.onCompleteAll = function () {
            wheelService.getWheelById($stateParams.id).then(onGetWheelComplete, onGetWheelError);
        };

        var onGetUserError = function (reason) {
            $scope.error = "Unable to get user : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetUserComplete = function (data) {
            // todo check for any security items
            wheelService.getWheelById($stateParams.id).then(onGetWheelComplete, onGetWheelError);
        };

        var onSaveLayerError = function (reason) {
            $scope.error = "Unable to save layer : status code= " + reason.status + ", " + reason.statusText;
        };

        var onCreateLayerComplete = function (data) {
            if (data.Id !== -1 && data.WheelId != null) {
                var levelName = data.LevelName + data.Id;
                $scope.Wheel.WheelLayers.push({
                    LevelName: levelName, Id: data.Id,
                    boptions: { levelName: data.LevelName, name: 'base', LayerId: data.Id },
                    foptions: { levelName: data.LevelName, name: 'front', LayerId: data.Id },
                    roptions: { levelName: data.LevelName, name: 'rear', LayerId: data.Id }
                });
            }

        };

        var onSaveLayerComplete = function (data) {
            $scope.isDirty = false;
            if (data.Error == null) {
                uploader.uploadAll();
            } else {
                $scope.error = "Unable to save layer : status code= " + data.Error;
                console.info(data);
            }
        };

        $scope.onNameChange = function() {
            $scope.isDirty = true;
        };

        $scope.addNewLayer = function () {
            wheelService.saveLayer({Id: -1, WheelId: $scope.Wheel.WheelId, LevelName: ""}).then(onCreateLayerComplete,onSaveLayerError);
        }

        $scope.saveChanges = function () {
            if ($scope.isDirty === false) {
                uploader.uploadAll();
            } else {
                var layers = $scope.Wheel.WheelLayers;
                wheelService.saveLayers(layers).then(onSaveLayerComplete, onSaveLayerError);
            }
                                    
        };

        activate();

        function activate() {
            if ($stateParams.id) {
                userService.getCurrentUser().then(onGetUserComplete, onGetUserError);
            } else {
                alert("Wheel id not provided");
            }
        }
    }
})();
