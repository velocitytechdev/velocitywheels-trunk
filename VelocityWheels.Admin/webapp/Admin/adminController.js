﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('adminController', adminController);

    adminController.$inject = ['$scope', 'FileUploader', 'applicationService', 'dashboardService', '$http'];

    function adminController($scope, FileUploader, applicationService, dashboardService, $http) {
        $scope.title = 'adminController';

        $scope.uploadMessage = "";
        $scope.uploadGuide = false;

        $scope.preserveExisting = false;
        $scope.buildingFitments = false;

        $scope.brands = [];
        $scope.options = {};

        $scope.hasAdminAccess = false;

        var getBrandsComplete = function (data) {
            $scope.brands = data;
            $scope.options.selectedBrand = $scope.brands[0];
        };

        var getBrandsError = function (reason) {
            $scope.error = "Unable to get brands : status code= " + reason.status + ", " + reason.statusText;
        };

        var uploader = $scope.uploader = new FileUploader({
            url: "Admin/UploadBrand"
        });

        var fgUploader = $scope.fgUploader = new FileUploader({
            url: "Admin/UploadFitmentGuide"
        });

        var fgMergeUploader = $scope.fgMergeUploader = new FileUploader({
            url: "Admin/UploadFitmentGuideAdditions"
        });

        /*uploader.filters.push({
            name: 'imageFilter',
            fn: function (item /*{File|FileLikeObject}#1#, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }

        });*/

        /*FileUploader.FileSelect.prototype.isEmptyAfterSelection = function () {
            return true; // true|false
        };*/

        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        };

        uploader.onAfterAddingFile = function (fileItem) {
            fileItem.removeAfterUpload = true;
            fileItem.formData.push({ name: fileItem.id, preserveExisting: $scope.preserveExisting });
            console.info('onAfterAddingFile', fileItem);
        };

        uploader.onAfterAddingAll = function (addedFileItems) {
        };

        uploader.onBeforeUploadItem = function (item) {
            $scope.uploadMessage = "Uploading ...";
        };

        uploader.onProgressItem = function (fileItem, progress) {
            $scope.uploadMessage = "Processing ...";
        };

        uploader.onProgressAll = function (progress) {
        };

        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            if (response.Error == null) {
                $scope.uploadMessage = "UPLOAD COMPLETE";
            } else {
                $scope.uploadMessage = response.Error;
            }
        };

        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status);
            $scope.uploadMessage = "Error uploading/processing file";
        };

        uploader.onCancelItem = function (fileItem, response, status, headers) {
        };

        uploader.onCompleteItem = function (fileItem, response, status, headers) {                        
        };

        uploader.onCompleteAll = function () {
        };


        fgUploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        };

        fgUploader.onAfterAddingFile = function (fileItem) {
            fileItem.removeAfterUpload = true;            
        };

        fgUploader.onBeforeUploadItem = function (item) {
            $scope.fgUploadMessage = "Uploading ...";
        };

        fgUploader.onProgressItem = function (fileItem, progress) {
            $scope.fgUploadMessage = "Processing ...";
        };

        fgUploader.onProgressAll = function (progress) {
        };

        fgUploader.onSuccessItem = function (fileItem, response, status, headers) {
            if (response.Error == null) {
                $scope.fgUploadMessage = "UPLOAD COMPLETE";
            } else {
                $scope.fgUploadMessage = response.Error;
            }
        };

        fgUploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status);
            $scope.fgUploadMessage = "Error uploading/processing file";
        };


        // Merge
        fgMergeUploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        };

        fgMergeUploader.onAfterAddingFile = function (fileItem) {
            fileItem.removeAfterUpload = true;
        };

        fgMergeUploader.onBeforeUploadItem = function (item) {
            $scope.fgMergeUploadMessage = "Uploading ...";
        };

        fgMergeUploader.onProgressItem = function (fileItem, progress) {
            $scope.fgMergeUploadMessage = "Processing ...";
        };

        fgMergeUploader.onProgressAll = function (progress) {
        };

        fgMergeUploader.onSuccessItem = function (fileItem, response, status, headers) {
            if (response.Error == null) {
                $scope.fgMergeUploadMessage = "UPLOAD COMPLETE";
            } else {
                $scope.fgMergeUploadMessage = response.Error;
            }
        };

        fgMergeUploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status);
            $scope.fgMergeUploadMessage = "Error uploading/processing file";
        };

        var onFitmentBuildError = function (reason) {
            $scope.buildingFitments = false;
            $scope.fgMessage = "Unable to import fitment guide : status code= " + reason.status + ", " + reason.statusText;
        };

        var onFitmentBuildComplete = function (data) {
            $scope.buildingFitments = false;
            if (data.Error !== null) {
                $scope.fgMessage = "Build completed with errors -> " + data.Error;
            } else {
                $scope.fgMessage = "Build completed. Generated " + data.NumberUpdated + " fitments.";
            }            
        };

        var onBuildError = function (reason) {
            $scope.mappingsMessage = "Unable to build mappings : status code= " + reason.status + ", " + reason.statusText;
        };

        var onBuildComplete = function (data) {
            $scope.mappingsMessage = "Build completed  - " + data.Error;
        };

        var onWheelProsError = function (reason) {
            $scope.wheelProsMessage = "Unable to pull vehicles : status code= " + reason.status + ", " + reason.statusText;
        };

        var onWheelProsComplete = function (data) {
            $scope.wheelProsMessage = "Pull completed  - " + data.Error;
        };


        $scope.buildFG = function () {
            $scope.buildingFitments = true;
            applicationService.buildFitmentGuide($scope.options.selectedBrand.Id).then(onFitmentBuildComplete, onFitmentBuildError);
            $scope.fgMessage = "Building fitments ...";
        };

        $scope.importFg = function () {
            fgUploader.uploadAll();
            $scope.fgUploadMessage = "Uploading ...";
        };

        $scope.appendToFg = function () {
            console.log(fgMergeUploader);
            fgMergeUploader.uploadAll();
            $scope.fgMergeUploadMessage = "Merging ...";
        };

        $scope.buildMappings = function() {
            applicationService.buildMappings().then(onBuildComplete, onBuildError);
            $scope.mappingsMessage = "Building maps ...";
        };

        $scope.pullWheelPros = function() {
            applicationService.pullWheelPros().then(onWheelProsComplete, onWheelProsError);
            $scope.wheelProsMessage = "Pulling data ...";
        };

        var loadFile = function (blob, name) {
            var URL = (window.URL || window.webkitURL);
            var a = document.createElement('a');
            //var name = (formatter.options.filename + "." + formatter.options.fileExtension);
            if (a.download !== void 0) {
                var url = URL.createObjectURL(blob);
                a.style.display = 'none';
                a.setAttribute('href', url);
                a.setAttribute('download', name);
                document.body.appendChild(a);
                a.dispatchEvent(new MouseEvent('click'));
                document.body.removeChild(a);
                setTimeout(function () {
                    URL.revokeObjectURL(url);
                }, 100);
            } else if (navigator.msSaveOrOpenBlob) {
                navigator.msSaveOrOpenBlob(blob, name);
            }
        };

        $scope.downloadFitmentGuide = function () {
            $scope.downloadingFitmentGuide = true;
            $scope.fitmentGuideMessage = "Downloading, please wait ...";

            $http({
                url: 'Reports/FitmentGuide',
                method: 'POST',
                params: {},
                headers: {
                    'Content-type': 'application/csv'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                $scope.downloadingFitmentGuide = false;
                $scope.fitmentGuideMessage = "Download complete!";
                var file = new Blob(([data]), { type: 'application/csv' });
                loadFile(file, 'fitmentguide.csv');
            }).error(function (reason, status, headers, config) {
                $scope.downloadingFitmentGuide = false;
                $scope.fitmentGuideMessage = "Report failed " + reason;
            });
        };

        $scope.toggleUpload = function() {
            $scope.uploadGuide = !$scope.uploadGuide;
        };

        activate();

        function activate() {
            dashboardService.getBrands().then(getBrandsComplete, getBrandsError);
        }
    }
})();
