﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('brandController', brandController);

    brandController.$inject = ["$scope", "userService", "$stateParams", "brandService"]; 

    function brandController($scope, userService, $stateParams, brandService) {
        $scope.title = 'brandController';

        $scope.Wheels = {};

        var onGetBrandComplete = function(data) {
            $scope.Wheels = data;
            console.log(data);
            $scope.Brand = data[0].Brand;
            $scope.BrandId = $stateParams.id;
        };

        var onGetBrandError = function(reason) {
            $scope.error = "Unable to get brand " + $stateParams.id + " : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetUserError = function (reason) {
            $scope.error = "Unable to get user : status code= " + reason.status + ", " + reason.statusText;
        };

        var onGetUserComplete = function (data) {            
            // todo check for any security items            
            brandService.getBrand($stateParams.id).then(onGetBrandComplete, onGetBrandError);
        };

        activate();

        function activate() {
            if ($stateParams.id) {
                userService.getCurrentUser().then(onGetUserComplete, onGetUserError);
            } else {
                alert("No brand id found");
            }
        }
    }
})();
