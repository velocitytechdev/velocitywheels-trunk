﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .controller('menuController', menuController);

    menuController.$inject = ['$scope', 'userService'];

    function menuController($scope, userService) {
        $scope.title = 'menuController';

        $scope.ncyBreadcrumbIgnore = true;

        $scope.user = null;

        var onGetUserComplete = function(data) {
            $scope.user = data;
            $scope.hasAdminAccess = userService.isAdmin();
        };

        var onGetUserError = function (reason) {
            $scope.error = "Unable to get user : status code= " + reason.status + ", " + reason.statusText;
        };

        activate();

        function activate() {
            userService.getCurrentUser().then(onGetUserComplete, onGetUserError);
        }
    }
})();
