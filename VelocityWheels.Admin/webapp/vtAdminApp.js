﻿
(function () {
    "use strict";

    var app = angular.module("vtAdminApp", [
         // Angular modules 
         /*"ngRoute",*/
         "ui.router",
         //"mm.foundation",
         "ui.bootstrap",
         // Custom modules 

         // 3rd Party Modules
         /*, "ngFileUpload"*/
         "angularFileUpload"
    ]);



    var routeConfig = function ($stateProvider, $urlRouterProvider, $locationProvider) {
        //
        // For any unmatched url, redirect to /state1
//        $locationProvider.html5Mode({ enabled: false, requireBase: true });
        $urlRouterProvider.otherwise("/home");


        $stateProvider
            .state('home', {
                url: "/home",
                views: {
                    'content': {
                        templateUrl: "webapp/Dashboard/Dashboard.html",
                        controller: "dashboardController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }
            })
            .state('brands', {
                url: "/brands",
                views: {
                    'content': {
                        templateUrl: "webapp/Brands/brands.html",
                        controller: "brandsController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }
            });

        $stateProvider
            .state('brand', {
                url: "/brand/:id",
                views: {
                    'content': {
                        templateUrl: "webapp/Brand/brand.html",
                        controller: "brandController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }
            });

        $stateProvider
            .state('wheel', {
                url: "/wheel/:brand/:style/:finish",
                views: {
                    'content': {
                        templateUrl: "webapp/Wheel/Wheel.html",
                        controller: "wheelController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }
            });

        $stateProvider
            .state('product', {
                url: "/product/:id",
                views: {
                    'content': {
                        templateUrl: "webapp/Wheel/Product.html",
                        controller: "productController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }                
            });

        $stateProvider
            .state('imageBuilder', {
                url: "/imagebuilder/:id",
                views: {
                    'content': {
                        templateUrl: "webapp/Wheel/wheelImageBuilder.html",
                        controller: "wheelBuilderController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }
            });

        $stateProvider
            .state('wheelmedia', {
                url: "/media/:id",
                views: {
                    'content': {
                        templateUrl: "webapp/Wheel/wheelMedia.html",
                        controller: "wheelBuilderController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }
            });

        $stateProvider
            .state('positionWheels', {
                url: "/positionwheels",
                views: {
                    'content': {
                        templateUrl: "webapp/Vehicle/PositionWheels.html"/*,
                        controller: "vehicleController"*/
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }
            });

        $stateProvider
            .state('positionWheelsFrame', {
                url: "/positionwheelsframe",
                views: {
                    'content': {
                        templateUrl: "webapp/Vehicle/PositionWheelsFrame.html",
                        controller: "wheelPositionController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }

            });

        $stateProvider
            .state('positionWheelsWithParams', {
                url: "/positionwheelswithparams/:id",
                views: {
                    'content': {
                        templateUrl: "webapp/Vehicle/PositionWheelsFrame.html",
                        controller: "wheelPositionController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }

            });

        $stateProvider
            .state('admin', {
                url: "/admin",
                views: {
                    'content': {
                        templateUrl: "webapp/Admin/admin.html",
                        controller: "adminController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }

            });

        $stateProvider
            .state('guides', {
                url: "/guides",
                views: {
                    'content': {
                        templateUrl: "webapp/Admin/guides.html",
                        controller: "adminController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }

            });

        $stateProvider
            .state('reports', {
                url: "/reports",
                views: {
                    'content': {
                        templateUrl: "webapp/Reports/reports.html",
                        controller: "reportsController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }

            });

        $stateProvider
            .state('accounts', {
                url: "/tokens",
                views: {
                    'content': {
                        templateUrl: "webapp/Admin/tokens.html",
                        controller: "accountsController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }

            });

        $stateProvider
            .state('accountdetails', {
                url: "/accountdetails/:id",
                views: {
                    'content': {
                        templateUrl: "webapp/Admin/accountDetails.html",
                        controller: "accountDetailsController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }
            });

        $stateProvider
            .state('vehiclemappings', {
                url: "/vehiclemappings",
                views: {
                    'content': {
                        templateUrl: "webapp/Vehicle/VehicleMappings.html",
                        controller: "vehicleMappingController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }

            });

        $stateProvider
            .state('uploadvehicleimage', {
                url: "/uploadvehicleimage/:id",
                views: {
                    'content': {
                        templateUrl: "webapp/Vehicle/UploadVehicleImage.html",
                        controller: "vehicleUploadImageController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }

            });

        $stateProvider
            .state('vehicleimages', {
                url: "/vehicleimages",
                views: {
                    'content': {
                        templateUrl: "webapp/Vehicle/VehicleImages.html",
                        controller: "vehicleImagesController"
                    },
                    'topmenu': {
                        templateUrl: "webapp/Menu/Menu.html",
                        controller: "menuController"
                    }
                }

            });

        //
        // Now set up the states
        /*$stateProvider
            .state('tmhome', {
                url: "/tmhome",
                views: {
                    'content': {
                        templateUrl: "webapp/Home/Home.html",
                        controller: "defaultHomeController"
                    },
                    'leftsidemenu': {
                        templateUrl: "webapp/Main/Menu.html",
                        controller: "menuController"
                    }
                }

            });*/

        /*$stateProvider
            .state('fgHeadHome', {
                url: "/fghome",
                views: {
                    'content': {
                        templateUrl: "webapp/Home/FgHome.html",
                        controller: "defaultHomeController"
                    },
                    'leftsidemenu': {
                        templateUrl: "webapp/Main/Menu.html",
                        controller: "menuController"
                    }
                }

            });*/

        /*$stateProvider
            .state('home3', {
                url: "/home3",
                views: {
                    'content': {
                        templateUrl: "webapp/Home/HandsExample.html",
                        controller: "Home3Controller"
                    },
                    'leftsidemenu': {
                        templateUrl: "webapp/Main/Menu.html",
                        controller: "menuController"
                    }
                }

            });*/

        /*$stateProvider
            .state('employeeadmin', {
                url: "/employee",
                views: {
                    'content': {
                        templateUrl: "webapp/Employee/Employee.html",
                        controller: "employeeController"
                    },
                    'leftsidemenu': {
                        templateUrl: "webapp/Main/Menu.html",
                        controller: "menuController"
                    }
                }

            });*/

        /*$stateProvider
            .state('participantadmin', {
                url: "/admin",
                views: {
                    'content': {
                        templateUrl: "webapp/Admin/Participants.html",
                        controller: "participantController"
                    },
                    'leftsidemenu': {
                        templateUrl: "webapp/Main/Menu.html",
                        controller: "menuController"
                    }
                }

            });*/

        /*$stateProvider
            .state('details', {
                url: "/participantDetails/:id",
                views: {
                    'content': {
                        templateUrl: "webapp/Participant/ParticipantDetails.html",
                        controller: "participantDetailsController"
                    },
                    'leftsidemenu': {
                        templateUrl: "webapp/Main/Menu.html",
                        controller: "menuController"
                    }
                }
            });*/

        /*$stateProvider
            .state('compGridAdmin', {
                url: "/compgridadmin",
                views: {
                    'content': {
                        templateUrl: "webapp/Admin/CompGrid.html",
                        controller: "compGridController"
                    },
                    'leftsidemenu': {
                        templateUrl: "webapp/Main/Menu.html",
                        controller: "menuController"
                    }
                }

            });*/

        /*$stateProvider
            .state('compGridAdmin.gridentry', {
                url: "/gridentry/:year/:country",
                views: {
                    'content@': {
                        templateUrl: "webapp/Admin/CompGridEntry.html",
                        controller: "compGridEntryController"
                    }
                }

            });*/

        /*$stateProvider
            .state('compGridAdmin.gridentrycopy', {
                url: "/gridentrycopy/:year/:country/:copy",
                views: {
                    'content@': {
                        templateUrl: "webapp/Admin/CompGridEntry.html",
                        controller: "compGridEntryController"
                    }
                }

            });*/

        /*$stateProvider
            .state('userManagement', {
                url: "/userManagement",
                views: {
                    'content': {
                        templateUrl: "webapp/Admin/UserManagement.html",
                        controller: "userController"
                    },
                    'leftsidemenu': {
                        templateUrl: "webapp/Main/Menu.html",
                        controller: "menuController"
                    }
                }

            });*/

        /*$stateProvider
            .state('userdetails', {
                url: "/userDetails/:id",
                views: {
                    'content': {
                        templateUrl: "webapp/Admin/UserDetails.html",
                        controller: "userDetailsController"
                    },
                    'leftsidemenu': {
                        templateUrl: "webapp/Main/Menu.html",
                        controller: "menuController"
                    }
                }
            });*/
    };

    /*app.config(["$routeProvider", "$locationProvider", routeConfig]);*/
    app.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", routeConfig]);


    /*app.filter('percentage', ['$filter', function ($filter) {
        return function (input, decimals) {
            return $filter('number')(input * 100, decimals) + '%';
        };
    }]);*/

})();