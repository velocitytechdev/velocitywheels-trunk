﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .factory('userService', userService);

    userService.$inject = ['$http', '$q'];

    function userService($http, $q) {
        var currentUser;

        var service = {
            getCurrentUser: getCurrentUser,
            getUser: getUser,
            matchPermission: matchPermission,
            isAdmin: isAdmin
        };

        return service;

        function getCurrentUser() {
            if (currentUser !== undefined) {
                var deferred = $q.defer();
                deferred.resolve(currentUser);
                return deferred.promise;
            }

            return $http.get("Account/GetCurrentUser").
                 then(function (response) {
                     currentUser = response.data;
                     return response.data;
                 });
        };

        function getUser(id) {
            return $http.get("api/UserApi/" + id).
                 then(function (response) {
                     return response.data;
                 });
        };

        function matchPermission(permissionName) {
            for (var i = 0; i < currentUser.Role.Permissions.length; i++) {
                var permission = currentUser.Role.Permissions[i];
                if (permission.PermissionName === permissionName) {
                    return true;
                }
            }
            return false;
        };

        function isAdmin() {
            return matchPermission("IsSuperAdmin");
        };
    }
})();