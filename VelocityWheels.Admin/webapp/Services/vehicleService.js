﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .factory('vehicleService', vehicleService);

    vehicleService.$inject = ['$http'];

    function vehicleService($http) {
        var service = {
            getData: getData,
            setVehicleImage: setVehicleImage,
            removeVehicleImage: removeVehicleImage,
            getVehicleImages: getVehicleImages,
            removeUploadedImage: removeUploadedImage,
            getAssignedImages: getAssignedImages,
            saveDescription: saveDescription
        };

        return service;

        function getData() { }

        function setVehicleImage(id, imageId) {
            var url = "/Vehicle/SetVehicleImage/?id=" + id + "&vehicleImageId=" + imageId;
            return $http.get(url).
                then(function (response) {
                    return response.data;
                });
        };

        function removeVehicleImage(id, imageId) {
            var url = "/Vehicle/RemoveVehicleImage/?id=" + id + "&vehicleImageId=" + imageId;
            return $http.get(url).
                then(function (response) {
                    return response.data;
                });
        };

        function removeUploadedImage(id) {
            var url = "/Vehicle/RemoveUploadedImage/?id=" + id;
            return $http.get(url).
                then(function (response) {
                    return response.data;
                });
        };

        function getVehicleImages() {
            var url = "/Vehicle/GetUploadedVehicleImages";
            return $http.get(url).
                then(function (response) {
                    return response.data;
                });
        };

        function getAssignedImages(imageId) {
            var url = "/Vehicle/GetAssignedVehiclesForImage?imageId=" + imageId;
            return $http.get(url).
                then(function (response) {
                    return response.data;
                });
        };

        function saveDescription(imageId, desc) {
            var postData = {
                Id: imageId,
                Description: desc
            };
            return $http.post("Vehicle/SaveDescription", postData).
                then(function (response) {
                    return response.data;
                });
        };
    }
})();