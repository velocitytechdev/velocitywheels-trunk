﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .factory('accountService', accountService);

    accountService.$inject = ['$http'];

    function accountService($http) {
        var service = {
            getAccounts: getAccounts,
            getAccountDetails: getAccountDetails,
            saveAccount: saveAccount
        };

        return service;

        function getAccounts() {
            return $http.get("Account/Get").
                then(function (response) {
                    return response.data;
                });
        };

        function getAccountDetails(id) {
            return $http.get("Account/Get/" + id).
                then(function (response) {
                    return response.data;
                });
        };

        function saveAccount(account) {
            return $http.post("Account/Save", account).
                then(function (response) {
                    return response.data;
                });
        };
    }
})();