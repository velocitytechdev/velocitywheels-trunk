﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .factory('applicationService', applicationService);

    applicationService.$inject = ['$http'];

    function applicationService($http) {

        var currentBrand = {
            brandId: "",
            brand: ""
        };

        var currentStyle = {
            styleId: "",
            style: ""
        };

        var currentFinish = {
            finishId: "",
            finish: ""
        };

        var currentProduct = {
            productId: "",
            product: ""
        };

        var service = {
            currentBrand: currentBrand,
            currentStyle: currentStyle,
            currentFinish: currentFinish,
            buildFitmentGuide: buildFitmentGuide,
            buildMappings: buildMappings,
            pullWheelPros: pullWheelPros
        };

        return service;

        function buildFitmentGuide(brandId) {
            return $http.post("Admin/ImportFitmentGuide?brandId=" + brandId).
                then(function (response) {
                    return response.data;
                });
        };

        function buildMappings() {
            return $http.post("Admin/BuildAcesMapping").
                then(function (response) {
                    return response.data;
                });
        };

        function pullWheelPros() {
            return $http.post("Admin/PullWheelProsVehicles").
                then(function (response) {
                    return response.data;
                });
        };
    }
})();