﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .factory('brandService', brandService);

    brandService.$inject = ['$http'];

    function brandService($http) {
        var service = {
            getBrand: getBrand
        };

        return service;

        function getBrand(id) {
            return $http.get("Brand/Get/" + id).
                then(function (response) {
                    return response.data;
                });
        }
    }
})();