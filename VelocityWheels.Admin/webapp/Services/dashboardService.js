﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .factory('dashboardService', dashboardService);

    dashboardService.$inject = ['$http'];

    function dashboardService($http) {
        var service = {
            getBrands: getBrands,
            getStats: getStats
        };

        return service;

        function getBrands() {
            return $http.get("Brand/GetBrands").
                 then(function (response) {
                     return response.data;
                 });
        };

        function getStats() {
            return $http.get("Home/GetDashboardStats").
                 then(function (response) {
                     return response.data;
                 });
        };
    }
})();