﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .factory('reportService', reportService);

    reportService.$inject = ['$http'];

    function reportService($http) {
        var service = {
            unmatchedFitmentsReport: unmatchedFitmentsReport,
            missingPositionsReport: missingPositionsReport,
            mappingReport: mappingReport
        };

        return service;


        function unmatchedFitmentsReport() {
            var postData = { };
            return $http.post("Reports/UnmatchedFitmentsReport", postData).
                then(function (response) {
                    return response.data;
                });
        };

        function missingPositionsReport() {
            var postData = { };
            return $http.post("Reports/MissingPositionsReport", postData).
                then(function (response) {
                    return response.data;
                });
        };

        function mappingReport() {
            var postData = {};
            return $http.post("Reports/MappingReport", postData).
                then(function (response) {
                    return response.data;
                });
        };
    }
})();