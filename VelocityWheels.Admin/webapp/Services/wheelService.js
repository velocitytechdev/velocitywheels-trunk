﻿(function () {
    'use strict';

    angular
        .module('vtAdminApp')
        .factory('wheelService', wheelService);

    wheelService.$inject = ['$http'];

    function wheelService($http) {
        var service = {
            getWheel: getWheel,
            getProduct: getProduct,
            removeFitment: removeFitment,
            enableFitment: enableFitment,
            getWheelById: getWheelById,
            saveLayer: saveLayer,
            saveLayers: saveLayers,
            saveWheel: saveWheel,
        };

        return service;

        function getWheel(brand,style,finish) {
            return $http.get("Wheels/Get?brand=" + brand + "&style=" + style + "&finish=" + finish).
                then(function (response) {
                    return response.data;
                });
        };

        function getWheelById(id) {
            return $http.get("Wheels/GetById?id=" + id).
                then(function (response) {
                    return response.data;
                });
        };

        function getProduct(id) {
            return $http.get("Products/Get/" + id).
                then(function (response) {
                    return response.data;
                });
        };

        function removeFitment(ids, partNumber) {
            var postData = { ids: ids, partNumber: partNumber };
            return $http.post("Products/RemoveFitment", postData).
                then(function (response) {
                    return response.data;
                });
        };

        function enableFitment(ids, partNumber) {
            var postData = { ids: ids, partNumber: partNumber };
            return $http.post("Products/EnableFitment", postData).
                then(function (response) {
                    return response.data;
                });
        };

        function saveLayer(layer) {
            var postData = { layer: layer };
            return $http.post("Wheels/SaveLayer", postData).
                then(function (response) {
                    return response.data;
                });
        };

        function saveLayers(layers) {
            var postData = { layers: layers };
            return $http.post("Wheels/SaveLayers", postData).
                then(function (response) {
                    return response.data;
                });
        };

        function saveWheel(wheel) {
            var postData = { wheel: wheel };
            return $http.post("Wheels/SaveWheel", postData).
                then(function (response) {
                    return response.data;
                });
        };
    }
})();