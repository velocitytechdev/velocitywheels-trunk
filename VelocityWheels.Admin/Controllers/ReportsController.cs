﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Web;
using System.Web.Mvc;
using VelocityWheels.Attributes;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Controllers
{
    public class ReportsController : BaseController
    {
        public ReportsController()
        {
            Logger = AppLogger.GetLogger(this);
        }

        //
        // GET: /Reports/

        [VTWebAuthorize("IsSuperAdmin")]
        [HttpPost]
        public FileResult MissingPositionsReport()
        {
            var model = new BaseSaveModel();

            try
            {
                var reportService = new ReportService();
                var fileName = reportService.GenerateMissingPositionsReport(Server);
                return File(fileName, "application/csv", fileName);
             }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
                throw new ServerException("Unexpected error while creating report " + e.Message);
            }
        }

        [VTWebAuthorize("IsSuperAdmin")]
        [HttpPost]
        public FileResult UnmatchedFitmentReport()
        {
            try
            {
                var reportService = new ReportService();
                var fileName = reportService.GenerateUnmatchedFitmentReport(Server);
                return File(fileName, "application/csv", fileName);
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
                throw new ServerException("Unexpected error while creating report " + e.Message);
            }
        }

        [VTWebAuthorize("IsSuperAdmin")]
        [HttpPost]
        public FileResult MappingReport()
        {
            try
            {
                var reportService = new ReportService();
                var fileName = reportService.GenerateAcesVehicleMappingReport(Server);
                return File(fileName, "application/csv", fileName);
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
                throw new ServerException("Unexpected error while creating report " + e.Message);
            }
        }

        [VTWebAuthorize("IsSuperAdmin")]
        [HttpPost]
        public FileResult FitmentGuide()
        {
            try
            {
                var reportService = new ReportService();
                var fileName = reportService.GenerateFitmentGuide(Server);
                return File(fileName, "application/csv", fileName);
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
                throw new ServerException("Unexpected error while creating report " + e.Message);
            }
        }
    }
}
