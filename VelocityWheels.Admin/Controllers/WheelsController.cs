﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net.Repository.Hierarchy;
using VelocityWheels.Attributes;
using VelocityWheels.Data.Models;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Controllers
{
    public class WheelsController : BaseController
    {
        public WheelsController()
        {
            Logger = AppLogger.GetLogger(this);
        }

        //
        // GET: /Wheels/
        [VTWebAuthorize()]
        public ActionResult Index(long id)
        {
            return View();
        }

        private WheelModel GetWheelModel(long brand, long style, long finish)
        {
            IWheelService service = new WheelService();
            WheelModel wheel = new WheelModel();
            try
            {
                wheel = service.GetWheel(brand, style, finish);
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
            }
            return wheel;
        }

        private WheelModel GetWheelModel(long id)
        {
            IWheelService service = new WheelService();
            WheelModel wheel = new WheelModel();
            try
            {
                wheel = service.GetWheelById(id);
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
            }
            return wheel;
        }

        [VTWebAuthorize()]
        public ActionResult Edit(long brand, long style, long finish)
        {
            var wheel = GetWheelModel(brand, style, finish);

            return View("WheelDetails", wheel);
        }

        [VTWebAuthorize()]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Get(long brand, long style, long finish)
        {
            var wheel = GetWheelModel(brand, style, finish);

            return Json(wheel, JsonRequestBehavior.AllowGet);
        }

        [VTWebAuthorize()]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult GetById(long id)
        {
            var wheel = GetWheelModel(id);

            return Json(wheel, JsonRequestBehavior.AllowGet);
        }

        [VTWebAuthorize()]
        [HttpPost]
        public ActionResult SaveWheel(WheelModel wheel)
        {
            try
            {
                WheelService service = new WheelService();
                var wheelToSave = service.GetWheel(wheel.WheelId);
                wheelToSave.Active = wheel.Active;
                if (!service.SaveOrUpdate(wheelToSave))
                {
                    wheel.Active = wheelToSave.Active;
                    wheel.Error = "Unable to save changes";
                    return Json(wheel);
                }                
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
                wheel.Error = e.Message;
                return Json(wheel);
            }

            return Json(wheel);
        }

        [VTWebAuthorize()]
        [HttpPost]
        public ActionResult SaveLayer(WheelLayerModel layer)
        {
            BaseModel model = new BaseModel();
            WheelService service = new WheelService();
            WheelLayer wheelLayer = service.CreateOrUpdateLayerName(layer);
            if (wheelLayer == null)
            {
                wheelLayer = new WheelLayer() {Id = -1};
            }
            return Json(wheelLayer);
        }

        [VTWebAuthorize()]
        [HttpPost]
        public ActionResult SaveLayers(List<WheelLayerModel> layers)
        {
            BaseModel model = new BaseModel();
            try
            {
                WheelService service = new WheelService();
                foreach (var wheelLayerModel in layers)
                {
                    WheelLayer wheelLayer = service.CreateOrUpdateLayerName(wheelLayerModel);                    
                }                
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
                model.Error = e.Message;
            }
            
            return Json(model);
        }

        [HttpPost]
        public ActionResult UploadBaseLayer(HttpPostedFileWrapper file, string levelName, string name, string id, int? layerId = null)
        {
            BaseModel model = new BaseModel();
            IWheelService service = new WheelService();
            if (file == null || string.IsNullOrWhiteSpace(name))
            {
                return Json(model);
            }

            try
            {
                var wheel = service.GetWheel(long.Parse(id));
                if (wheel == null)
                {
                    return Json(model);
                }

                service.SetAuditFields(wheel);

                var fileName = Path.GetFileName(file.FileName);

                if (name.Equals("thumb"))
                {
                    if (wheel.ThumbnailImage == null)
                    {
                        BuildThumbnailFileName(wheel, fileName);
                    }
                    else if (wheel.ThumbnailImage.Contains("thumb_"))
                    {
                        var imgPath = WheelService.GetFileSystemThumbnailImagePath(wheel.Brand.Name, wheel.ThumbnailImage);
                        if (System.IO.File.Exists(imgPath))
                        {
                            // we have uploaded a file before, delete the old one and create a new one
                            System.IO.File.Delete(imgPath);                            
                        }
                        BuildThumbnailFileName(wheel, fileName);
                    }
                    else
                    {
                        // comes from upload files, can be assigned to multiple wheels, so we need to create a new name.
                        BuildThumbnailFileName(wheel, fileName);
                    }
                    var img = WheelService.GetFileSystemThumbnailImagePath(wheel.Brand.Name, wheel.ThumbnailImage);
                    
                    file.SaveAs(img);
                    wheel.ThumbnailImage = wheel.ThumbnailImage;
                }
                else if (name.Equals("base"))
                {
                    string img;
                    if (!layerId.HasValue)
                    {
                        if (wheel.BaseLayerImage == null)
                        {
                            wheel.BaseLayerImage = "base_" + Guid.NewGuid() + Path.GetExtension(fileName);
                        }
                        img = WheelService.GetFileSystemFaceImagePath(wheel.Brand.Name, wheel.BaseLayerImage);
                    }
                    else
                    {
                        var layer = GetWheelLayer(layerId, wheel);
                        if (layer.BaseLayerImage == null)
                        {
                            layer.BaseLayerImage = "base_" + Guid.NewGuid() + Path.GetExtension(fileName);
                        }
                        img = WheelService.GetFileSystemLayerImagePath(wheel.Brand.Name, layer.BaseLayerImage);
                    }
                    file.SaveAs(img);
                }
                else if (name.Equals("front"))
                {
                    string img;
                    if (!layerId.HasValue)
                    {
                        img = WheelService.GetFileSystemFaceImagePath(wheel.Brand.Name, fileName);
                        wheel.FrontMainImage = fileName;
                    }
                    else
                    {
                        var layer = GetWheelLayer(layerId, wheel);
                        if (layer.FrontLayerImage == null)
                        {
                            layer.FrontLayerImage = "front_" + Guid.NewGuid() + Path.GetExtension(fileName);
                        }
                        img = WheelService.GetFileSystemLayerImagePath(wheel.Brand.Name, layer.FrontLayerImage);
                    }
                    file.SaveAs(img);
                }
                else if (name.Equals("rear"))
                {
                    string img;
                    if (!layerId.HasValue)
                    {
                        img = WheelService.GetFileSystemFaceImagePath(wheel.Brand.Name, fileName);
                        wheel.RearMainImage = fileName;
                    }
                    else
                    {
                        var layer = GetWheelLayer(layerId, wheel);
                        if (layer.RearLayerImage == null)
                        {
                            layer.RearLayerImage = "rear_" + Guid.NewGuid() + Path.GetExtension(fileName);
                        }
                        img = WheelService.GetFileSystemLayerImagePath(wheel.Brand.Name, layer.RearLayerImage);
                    }
                    file.SaveAs(img);
                }

                service.SaveOrUpdate(wheel);
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);

                model.Error = e.Message;
            }

            return Json(model);
        }

        private static void BuildThumbnailFileName(Wheel wheel, string fileName)
        {
            wheel.ThumbnailImage = "thumb_" + Guid.NewGuid() + Path.GetExtension(fileName);
        }

        [VTWebAuthorize()]
        private static WheelLayer GetWheelLayer(int? layerId, Wheel wheel)
        {
            if (wheel.WheelLayers == null)
            {
                wheel.WheelLayers = new List<WheelLayer>();
            }
            var layer = wheel.WheelLayers.FirstOrDefault(l => l.Id == layerId);
            if (layer == null)
            {
                layer = new WheelLayer();
                int max = wheel.WheelLayers.Any() ? wheel.WheelLayers.Max(l => l.Level) : 0;
                int level = max == 0 ? 1 : max + 1;
                layer.Level = (short)level;                
                wheel.WheelLayers.Add(layer);
            }
            return layer;
        }
    }
}
