﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VelocityWheels.Attributes;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController()
        {
            Logger = AppLogger.GetLogger(this);
        }

        //
        // GET: /Account/
        [VTWebAuthorize()]
        public ActionResult Index()
        {
            return View();
        }

        [VTWebAuthorize()]
        public ActionResult GetCurrentUser()
        {
            var user = User as WebUser;
            UserModel model = new UserModel()
            {
                Id = user.DbUser.Id,
                UserName = user.DbUser.UserName,
                Name = user.DbUser.Name,
                Account = new AccountModel() {AccountName = user.DbUser.Name},
                Role = new RoleModel(user.DbUser.Role)
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [VTWebAuthorize("IsSuperAdmin")]
        public ActionResult Get(long? id = null)
        {
            List<AccountModel> accounts = new List<AccountModel>();
            try
            {
                AccountService accountService = new AccountService();
                if (!id.HasValue)
                {
                    accounts = accountService.GetAccounts();
                    return Json(accounts, JsonRequestBehavior.AllowGet);
                }

                var account = accountService.GetAccount(id.Value);
                if (account != null)
                {
                    accounts = new List<AccountModel>() {account};
                    return Json(accounts, JsonRequestBehavior.AllowGet);
                }

                string salt;
                string guid = Guid.NewGuid().ToString();
                string token = PasswordUtil.EncryptPassword(guid, out salt);
                if (token.Length > 32)
                {
                    token = token.Substring(token.Length - 32);
                }
                accounts.Add(new AccountModel()
                {
                    Id = -1,
                    Active = true,
                    SubAccounts = new List<SubAccountModel>(),
                    Token = token,
                    AccountBrands = new List<AccountBrandModel>()
                });
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
            }

            return Json(accounts, JsonRequestBehavior.AllowGet);
        }

        [VTWebAuthorize("IsSuperAdmin")]
        [HttpPost]
        public ActionResult Save(AccountModel account)
        {
            var model = new BaseSaveModel();

            if (!ModelState.IsValid)
            {
                model.Error = "Validation Error";
                return Json(model);
            }
            
            try
            {
                AccountService accountService = new AccountService();
                var newAccount = accountService.SaveOrUpdate(account);
                model.Id = newAccount.Id;
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
                model.Error = e.Message;
            }
            return Json(model);
        }
    }
}
