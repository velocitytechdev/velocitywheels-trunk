﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using VelocityWheels.Attributes;
using VelocityWheels.Data.Enum;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Util;

namespace VelocityWheels.Controllers
{
    public class AdminController : BaseController
    {
        public AdminController()
        {
            Logger = AppLogger.GetLogger(this);
        }

        //
        // GET: /Admin/
        [VTWebAuthorize(Permissions = "IsSuperAdmin")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PullProducts()
        {
            return View("Index");
        }

        [HttpPost]
        public ActionResult UploadBrandMvc(HttpPostedFileBase brandUploadFile, HttpPostedFileBase uniquesUploadFile)
        {
            //string filePath = Path.Combine(Server.MapPath("~/App_Data"), Path.GetFileName(brandUploadFile.FileName));
            //csvFile.SaveAs(filePath);

            IWheelService service = new WheelService();
            try
            {
                if (brandUploadFile != null)
                {
                    var imported = service.ImportWheelDataFromFile(brandUploadFile.InputStream);
                    if (imported != null && imported.Any())
                    {
                        if (uniquesUploadFile != null)
                        {
                            service.ImportWheelsThumbnailsFromFile(uniquesUploadFile.InputStream, new ImportWheelsThumbnailsFromFileOptions
                            {
                                DiscardChanges = false,
                                ResizeThumb = true,
                            });
                        }
                        ModelState.AddModelError("brandUploadFile", "UPLOAD COMPLETE");
                        return View("Index");
                    }
                   
                    ModelState.AddModelError("brandUploadFile",
                           "Unalbe to process file, invalid file type or columns don't match");
                }
                else if (uniquesUploadFile != null)
                {
                    service.ImportWheelsThumbnailsFromFile(uniquesUploadFile.InputStream, new ImportWheelsThumbnailsFromFileOptions
                    {
                        DiscardChanges = false,
                        ResizeThumb = true,
                    });
                    ModelState.AddModelError("brandUploadFile", "UPLOAD COMPLETE");
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message,e);
                ModelState.AddModelError("brandUploadFile", e.Message);
            }

            return View("Index");
        }

        [HttpPost]
        public ActionResult ImportFitmentGuide(long? brandId)
        {
            BaseModel model = new BaseModel();
            FitmentGuideService service = new FitmentGuideService();
            try
            {
                int? vid = null;
                var cService = new ClassificationService();

                var subModels = cService.GetFullSubModels(ApiIdEnum.Aces, 2016, "Chevrolet", "Silverado 1500", "Crew Cab Pickup");
                var vehicle = subModels.FirstOrDefault();
                if (vehicle != null)
                {
                    // Comment this back in when testing
                    //vid = (int)vehicle.AcesVehicleId.GetValueOrDefault();
                }

                if (!brandId.HasValue)
                {
                    model.Error = "Please select a brand";
                    return Json(model);
                }

                string error = null;
                var count = service.BuildFromFitmentGuideData(brandId, vid, out error);                
                return Json(new FitmetBuilderModel() {NumberUpdated = count, BrandId = brandId.GetValueOrDefault(), Error = error});
                //service.BuildAcesVehicleIdForBrand();                                
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);

                model.Error = e.Message;
            }
            finally
            {
            }

            return Json(model);
        }

        [HttpPost]
        public ActionResult BuildAcesMapping()
        {
            BaseModel model = new BaseModel();
            MappingService mappingService = new MappingService();
            try
            {                
                mappingService.BuildMappingForAces();
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);

                model.Error = e.Message;
            }

            return Json(model);
        }

        [HttpPost]
        public ActionResult PullWheelProsVehicles()
        {
            BaseModel model = new BaseModel();
            WheelProsService wheelProsService = new WheelProsService();
            try
            {
                wheelProsService.PullVehicles();
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);

                model.Error = e.Message;
            }

            return Json(model);
        }

        [HttpPost]
        public ActionResult UploadBrand(HttpPostedFileWrapper file, string name, bool preserveExisting)
        {
            BaseModel model = new BaseModel();
            IWheelService service = new WheelService();
            try
            {
                if (file != null && !string.IsNullOrWhiteSpace(name) && name.Equals("bsfFile"))
                {
                    var imported = service.ImportWheelDataFromFile(file.InputStream, new ImportWheelDataFromFileOptions
                    {
                        FileDataMergeMode = preserveExisting ? FileDataMergeMode.Preserve : FileDataMergeMode.Overwrite,
                    });
                    if (imported == null)
                    {
                        model.Error = "Unable to process file, invalid file type or columns don't match";
                    }
                    else if (!imported.Any())
                    {
                        model.Error = "No records in file found that match import criteria";
                    }

                    model.ProcessedCount = imported?.Count ?? 0;
                    return Json(model);
                }
                else if (file != null && !string.IsNullOrWhiteSpace(name) && name.Equals("uniquesFile"))
                {
                    service.ImportWheelsThumbnailsFromFile(file.InputStream, new ImportWheelsThumbnailsFromFileOptions
                    {
                        DiscardChanges = false,
                        FileDataMergeMode = preserveExisting ? FileDataMergeMode.Preserve : FileDataMergeMode.Overwrite,
                        ResizeThumb = false,
                    });
                }
                else
                {
                    model.Error = "No matching files, unable to upload";
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
                ModelState.AddModelError("brandUploadFile", e.Message);
                model.Error = e.Message;
            }
            finally
            {
                /*service.EndHistoryImportWait();*/
            }

            return Json(model);
        }

        [HttpPost]
        public ActionResult UploadFitmentGuide(HttpPostedFileWrapper file)
        {
            BaseModel model = new BaseModel();
            var service = new FitmentGuideService();
            try
            {
                if (file != null)
                {
                    var imported = service.ImportFitmentGuide(file.InputStream);
                    if (imported == null)
                    {
                        model.Error = "Unable to process file, invalid file type or columns don't match";
                    }
                    else if (!imported.Any())
                    {
                        model.Error = "No records in file found that match import criteria";
                    }

                }
                else
                {
                    model.Error = "No matching files, unable to upload";
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
                ModelState.AddModelError("fitmentGuideUploadFile", e.Message);
                model.Error = e.Message;
            }
            finally
            {
                /*service.EndHistoryImportWait();*/
            }

            return Json(model);
        }

        [HttpPost]
        public ActionResult UploadFitmentGuideAdditions(HttpPostedFileWrapper file)
        {
            BaseModel model = new BaseModel();
            var service = new FitmentGuideService();
            try
            {
                if (file != null)
                {
                    var imported = service.UpdateFitmentGuide(file.InputStream);
                    if (imported == null)
                    {
                        model.Error = "Unable to process file, invalid file type or columns don't match";
                    }
                    else if (!imported.Any())
                    {
                        model.Error = "No records in file found that match import criteria";
                    }

                }
                else
                {
                    model.Error = "No matching files, unable to upload";
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
                ModelState.AddModelError("fitmentGuideUploadFile", e.Message);
                model.Error = e.Message;
            }
            finally
            {
                /*service.EndHistoryImportWait();*/
            }

            return Json(model);
        }
    }
}
