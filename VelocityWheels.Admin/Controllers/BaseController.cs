﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VelocityWheels.Controllers
{
    public class BaseController : Controller
    {
        public log4net.ILog Logger { get; set; }

        public BaseController()
        {
        }
    }
}
