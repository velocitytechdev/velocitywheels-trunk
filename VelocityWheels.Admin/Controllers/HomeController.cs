﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.ClientServices;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using VelocityWheels.Attributes;
using VelocityWheels.Data.Models;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Util;

namespace VelocityWheels.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(HomeController));

        [VTWebAuthorize()]
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            ViewBag.UserName = User.Identity.Name;

            

            return View("Index");
        }

        /*[VTWebAuthorize()]
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            ViewBag.UserName = User.Identity.Name;

            if (User.Identity.IsAuthenticated)
            {
                _logger.Info("Home page login ... " + User.Identity.Name);
            }

            DashboardModel dashboard = new DashboardModel();
            var user = User.Identity.Name;
            // TODO filter by user - refactor into service
            try
            {
                using (var context = VelocityWheelsContext.Create())
                {
                    dashboard.Wheels = context.WheelSpecs.Count();
                    dashboard.Vehicles = context.VehicleImages.Count();
                    dashboard.Fitments = context.Fitments.Count();
                    dashboard.CustomerCars = 0;
                }
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
            }

            return View("Dashboard", dashboard);
        }*/

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [VTWebAuthorize()]
        public ActionResult GetDashboardStats()
        {
            DashboardModel dashboard = new DashboardModel();

            // TODO filter by user - refactor into service
            try
            {
                using (var context = VelocityWheelsContext.Create())
                {
                    dashboard.Wheels = context.WheelSpecs.Count();
                    dashboard.Vehicles = context.VehicleImages.Count();
                    dashboard.Fitments = context.Fitments.Count();
                    dashboard.CustomerCars = 0;
                }
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
            }
            return Json(dashboard, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Login()
        {
            ViewBag.Title = "VTWS Login Page";

            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel loginModel)
        {
            UserService service = new UserService();
            if (!service.VerifyPassword(loginModel.UserName, loginModel.Password))
            {
                ModelState.AddModelError("Password", "The username or password you provided are invalid, please try again.");
                return View();
            }

            FormsAuthentication.SetAuthCookie(loginModel.UserName, false);

            return RedirectToAction("Index");
        }        

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }
    }
}
