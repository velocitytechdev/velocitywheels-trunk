﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VelocityWheels.Attributes;
using VelocityWheels.Data.Enum;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Controllers
{
    public class ProductsController : BaseController
    {
        public ProductsController()
        {
            Logger = AppLogger.GetLogger(this);
        }

        //
        // GET: /Products/
        [VTWebAuthorize()]
        public ActionResult Details(long id)
        {
            var product = GetProductModel(id);

            return View("ProductDetails", product);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [VTWebAuthorize()]
        public ActionResult Get(long id)
        {
            var product = GetProductModel(id);

            return Json(product, JsonRequestBehavior.AllowGet);
        }

        private ProductModel GetProductModel(long id)
        {
            ProductModel product = new ProductModel();
            try
            {
                IWheelService service = new WheelService();
                product = service.GetProduct(id);
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
            }
            return product;
        }

        [VTWebAuthorize()]
        [HttpPost]
        public ActionResult RemoveFitment(List<long> ids, string partNumber)
        {
            ProductService service = new ProductService();
            if (service.RestictFitment(ids, partNumber))
            {
                return Json(new BaseModel() { Error = null });
            }

            return Json(new BaseModel() { Error = partNumber + "Not Removed"});
        }

        [VTWebAuthorize()]
        [HttpPost]
        public ActionResult EnableFitment(List<long> ids, string partNumber)
        {
            ProductService service = new ProductService();
            if (service.EnableFitment(ids, partNumber))
            {
                return Json(new BaseModel() { Error = null });
            }

            return Json(new BaseModel() { Error = partNumber + "Not Enabled" });
        }
    }
}
