﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using VelocityWheels.Attributes;
using VelocityWheels.Data.Models;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Controllers
{
    public class BrandController : Controller
    {
        log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(BrandController));

        //
        // GET: /Brand/
        [VTWebAuthorize()]
        public ActionResult Index(string brandId = null)
        {
            if (string.IsNullOrWhiteSpace(brandId))
            {
                return View("Brand", new List<WheelResultsModel>());
            }

            try
            {
                var user = User as WebUser;
                IWheelService service = new WheelService();
                WheelResults results = service.GetWheels(null, new WheelFilter() {Brand = brandId}, user.DbUser.AccountId);                
                return View("Brand", results?.WheelResultsModels);
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
            }

            return View("Brand", new List<WheelResultsModel>());
        }

        [VTWebAuthorize()]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Json(new List<WheelResultsModel>(), JsonRequestBehavior.AllowGet);
            }

            try
            {
                var user = User as WebUser;
                IWheelService service = new WheelService();
                IList<WheelResultsModel> results = service.GetWheels(long.Parse(id), user.DbUser.AccountId);
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
            }

            return Json(new List<WheelResultsModel>(), JsonRequestBehavior.AllowGet);
        }

        [VTWebAuthorize()]
        public ActionResult GetAll()
        {
            var brands = BrandPartialModels();
            return View("Brands", brands);
        }

        private IList<BrandPartialModel> BrandPartialModels()
        {
            var user = User as WebUser;
            IWheelService service = new WheelService();
            var brands = service.GetWheelsBrands(user.DbUser);
            return brands;
        }

        [VTWebAuthorize()]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult GetBrands()
        {
            var brands = BrandPartialModels();
            return Json(brands, JsonRequestBehavior.AllowGet);
        }
    }
}
