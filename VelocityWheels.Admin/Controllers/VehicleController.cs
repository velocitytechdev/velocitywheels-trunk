﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using VelocityWheels.Attributes;
using VelocityWheels.Data.Enum;
using VelocityWheels.Data.Models;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;
using FuelAPIMake = VelocityWheels.Models.FuelAPIMake;
using FuelAPIModel = VelocityWheels.Models.FuelAPIModel;
using FuelAPISubModel = VelocityWheels.Models.FuelAPISubModel;
using VehicleImage = VelocityWheels.Models.VehicleImage;
using WheelPosition = VelocityWheels.Models.WheelPosition;

namespace VelocityWheels.Controllers
{
    public class VehicleController : Controller
    {
        log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(VehicleController));

        [VTWebAuthorize]
        public ActionResult Images()
        {
            return View();
        }

        /*[VTWebAuthorize("IsSuperAdmin")]
        public ActionResult PositionWheels()
        {
            return View();
        }*/

        [VTWebAuthorize("IsSuperAdmin")]
        public ActionResult PositionWheels(int? id)
        {
            if (id.HasValue)
            {
                return View(new VehicleMatchModel() { VehicleImageId = id });
            }
            else
            {
                return View();
            }
        }

        private enum DataSource
        {
            DriveRight,
            Iconfigurators,
            FuelAPI
        };

        private const DataSource ACTIVE_DATA_SOURCE = DataSource.FuelAPI;

        private const string DRIVE_RIGHT_USERNAME = "VelocityTGM";
        private const string DRIVE_RIGHT_PASSWORD = "Config949";
        private const string ICONFIGURATORS_TOKEN = "GUUMSNJUBJTUWYFABBWIYIBJAJWJGKPAYXFGQCTEHFMMQNYOZE";
        private const string FUEL_API_TOKEN = "8738f621-4eaf-4f5b-9aa3-a9f5ed8c311a";

        #region Stub Actions -- they will call other Actions based on ACTIVE_DATA_SOURCE

        public ActionResult GetYears()
        {
            return GetYearsFuelApi();
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult GetAcesYears()
        {
            ClassificationService service = new ClassificationService();
            var years = service.GetYears(ApiIdEnum.Aces);
            return Json(years, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult GetAcesMakes(int year)
        {
            ClassificationService service = new ClassificationService();
            List<string> makes = service.GetMakes(ApiIdEnum.Aces, year);
            return Json(makes, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult GetAcesModels(int year, string make)
        {
            ClassificationService service = new ClassificationService();
            var models = service.GetModels(ApiIdEnum.Aces, year, make);
            return Json(models, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult GetAcesBodyTypes(int year, string make, string model)
        {
            ClassificationService service = new ClassificationService();
            var bodyTypes = service.GetBodyTypes(ApiIdEnum.Aces, year, make, model);
            return Json(bodyTypes, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult GetAcesSubModels(int year, string make, string model, string bodyType, long? id)
        {
            ClassificationService service = new ClassificationService();
            var subModels = service.GetFullSubModels(ApiIdEnum.Aces, year, make, model, bodyType);

            var results = subModels.Select(s => new
            {
                s.Id,
                s.AcesVehicleId,
                s.Year,
                s.Make,
                s.Model,
                s.BodyType,
                SubModel = s.SubModel + $"(Region {s.RegionId})",
                s.RegionId,
                s.Region,
                s.DriveTrain,
                s.VehicleMappingId,
                ChassisId = s.Id                   
            }).ToList();

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFuelPossibleMatches(int year, string make, long id)
        {
            ClassificationService service = new ClassificationService();
            List<VehicleMatchModel> makes = service.GetFullMakes(ApiIdEnum.FuelApi, year, make.Trim());

            VehicleService vehicleService = new VehicleService(service.Context);
            MappingService mappingService = new MappingService(service.Context);
            var mapping = mappingService.GetVehicleMapping(id);
            if (mapping.Any())
            {
                var idList = mapping.Select(m => m.ToVehicleId);
                var vehicles = makes.Where(m => idList.Contains(m.AcesVehicleId));
                foreach (var vehicleMatchModel in vehicles)
                {
                    vehicleMatchModel.IsMappedItem = true;
                }
            }

            var vList = makes.Select(m => new Services.Models.FuelAPISubModel()
            {
                Year = year,
                make_name = m.Make,
                model_name = m.Model,
                bodytype_desc = m.BodyType,
                trim = m.SubModel,
                id = (int)m.AcesVehicleId.GetValueOrDefault(),
            }).ToList();

            var results = vehicleService.GetVehicleThumbs(year, vList);

            foreach (var vehicle in makes)
            {
                var image = results.FirstOrDefault(v => v.VehicleID == vehicle.AcesVehicleId);
                if (image != null)
                {
                    vehicle.Image = image.Thumbnail;
                }
            }

            return Json(makes, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateMapping(long fromId, long toId)
        {
            var model = new BaseModel();
            MappingService mappingService = new MappingService();
            try
            {
                mappingService.UpdateMappingForUserOverride(fromId, toId);
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
                model.Error = e.Message;
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNextUnmapped(long currentId, int year)
        {
            try
            {
                MappingService mappingService = new MappingService();
                return Json(mappingService.GetNextUnmappedVehicle(currentId, year), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
                throw;
            }
        }

        public ActionResult GetNextVehicle(long currentId, int year, string make, string model, string bodyType, string subModel)
        {
            try
            {
                MappingService mappingService = new MappingService();
                return Json(mappingService.GetNextVehicle(currentId, year, make, model, bodyType, subModel), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
                throw;
            }
        }        

        public ActionResult GetMakes(string y)
        {
                return GetMakesFuelAPI(y);
        }

        public ActionResult GetModels(string y, string m)
        {
                return GetModelsFuelAPI(y, m);
        }

        public ActionResult GetBodyTypes(string y, string m, string mdl)
        {
                return GetBodyTypesFuelAPI(y, m, mdl);
        }

        public ActionResult GetSubModels(string y, string m, string mdl, string b)
        {
                return GetSubModelsFuelAPI(y, m, mdl, b);
        }

        public ActionResult GetVehicleImages(string y, string m, string mdl, string b, string sm, string o)
        {
                return GetVehicleImagesFuelAPI(y, m, mdl, "", sm, o);
        }

        public ActionResult GetVehicleImage(int id)
        {
            VehicleService service = new VehicleService();
            var vehicleImage = service.GetVehicleImageById(id);
            if (vehicleImage != null)
            {
                var model = new VehicleMatchModel()
                {
                    VehicleImageId = vehicleImage.VehicleImageID,
                    FullImage = vehicleImage.FileName,                   
                };

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(new BaseModel() {Error = $"Unable to find image matching {id}"}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVehicleImageSwatches(int vid)
        {
            return GetVehicleImageSwatchesIconfigurators(vid);
        }

        #endregion

        #region Drive Right Actions

        /*public ActionResult GetYearsDriveRight()
        {
            var wsc = new DriveRight.webserviceSoapClient();
            var years = wsc.GetAAIAYears(DRIVE_RIGHT_USERNAME, DRIVE_RIGHT_PASSWORD);
            return Json(years
                .Where(y => Convert.ToInt32(y.Year) >= 1980)
                .Select(y => y.Year)
                .OrderByDescending(y => y), JsonRequestBehavior.AllowGet);
        }*/

        /*public ActionResult GetMakesDriveRight(string y)
        {
            var wsc = new DriveRight.webserviceSoapClient();
            var makes = wsc.GetAAIAManufacturers(DRIVE_RIGHT_USERNAME, DRIVE_RIGHT_PASSWORD, Convert.ToInt32(y), 1
                /*USA#1#);
            return Json(makes
                .Select(m => m.Manufacturer)
                .OrderBy(m => m), JsonRequestBehavior.AllowGet);
        }*/

        /*public ActionResult GetModelsDriveRight(string y, string m)
        {
            var wsc = new DriveRight.webserviceSoapClient();
            var models = wsc.GetAAIAModels(DRIVE_RIGHT_USERNAME, DRIVE_RIGHT_PASSWORD, Convert.ToInt32(y), 1 /*USA#1#, m);
            return Json(models
                .Select(mdl => mdl.Model)
                .OrderBy(mdl => mdl), JsonRequestBehavior.AllowGet);
        }*/

        /*public ActionResult GetBodyTypesDriveRight(string y, string m, string mdl)
        {
            var wsc = new DriveRight.webserviceSoapClient();
            var bodyTypes = wsc.GetAAIABodyTypes(DRIVE_RIGHT_USERNAME, DRIVE_RIGHT_PASSWORD, Convert.ToInt32(y), 1
                /*USA#1#, m, mdl);
            return Json(bodyTypes
                .Select(b => b.BodyType)
                .OrderBy(b => b), JsonRequestBehavior.AllowGet);
        }*/

        /*public ActionResult GetSubModelsDriveRight(string y, string m, string mdl, string b)
        {
            var wsc = new DriveRight.webserviceSoapClient();
            var subModels = wsc.GetAAIASubModels(DRIVE_RIGHT_USERNAME, DRIVE_RIGHT_PASSWORD, Convert.ToInt32(y), 1
                /*USA#1#, m, mdl, b);
            return Json(subModels
                .Select(sm => sm.SubModel)
                .OrderBy(sm => sm), JsonRequestBehavior.AllowGet);
        }*/

        #endregion

        #region Iconfigurators Actions

        public ActionResult GetYearsIconfigurators()
        {
            var jsonData = "";
            var url = string.Format("http://iconfigurators.com/api/?function=getYears&key={0}", ICONFIGURATORS_TOKEN);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            IconfiguratorYearsResult yr = JsonConvert.DeserializeObject<IconfiguratorYearsResult>(jsonData);
            return Json(yr.years
                .Where(y => y.year >= 1980)
                .Select(y => y.year)
                .OrderByDescending(y => y), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMakesIconfigurators(string y)
        {
            var jsonData = "";
            var url = string.Format("http://iconfigurators.com/api/?function=getMakes&year={1}&key={0}",
                ICONFIGURATORS_TOKEN, y);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            IconfiguratorMakesResult mr = JsonConvert.DeserializeObject<IconfiguratorMakesResult>(jsonData);
            return Json(mr.makes
                .Select(m => m.make)
                .OrderBy(m => m), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetModelsIconfigurators(string y, string m)
        {
            var jsonData = "";
            var url = string.Format("http://iconfigurators.com/api/?function=getModels&year={1}&make={2}&key={0}",
                ICONFIGURATORS_TOKEN, y, m);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            IconfiguratorModelsResult mr = JsonConvert.DeserializeObject<IconfiguratorModelsResult>(jsonData);
            return Json(mr.models
                .Select(mdl => mdl.model)
                .OrderBy(mdl => mdl), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBodyTypesIconfigurators(string y, string m, string mdl)
        {
            var jsonData = "";
            var url = string.Format("http://iconfigurators.com/api/?function=getVehicleTypes&key={0}",
                ICONFIGURATORS_TOKEN);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            IconfiguratorVehicleTypesResult vtr =
                JsonConvert.DeserializeObject<IconfiguratorVehicleTypesResult>(jsonData);
            return Json(vtr.Vehicle_Types
                .Select(vt => vt.vehicleTypeName)
                .OrderBy(vt => vt), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSubModelsIconfigurators(string y, string m, string mdl, string b)
        {
            var jsonData = "";
            var url =
                string.Format(
                    "http://iconfigurators.com/api/?function=getSubmodels&year={1}&make={2}&model={3}&key={0}",
                    ICONFIGURATORS_TOKEN, y, m, mdl);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            IconfiguratorSubModelsResult smr = JsonConvert.DeserializeObject<IconfiguratorSubModelsResult>(jsonData);
            return Json(smr.submodels
                .Select(sm => sm.submodel)
                .Distinct()
                .OrderBy(sm => sm), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVehicleImagesIconfigurators(string y, string m, string mdl, string b, string sm)
        {
            var jsonData = "";
            var url =
                string.Format(
                    "http://iconfigurators.com/api/?function=getVehicleImages&year={1}&make={2}&model={3}&submodel={4}&key={0}",
                    ICONFIGURATORS_TOKEN, y, m, mdl, sm);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVehicleImageSwatchesIconfigurators(int vid)
        {
            var jsonData = "";
            var url =
                string.Format("http://iconfigurators.com/api/?function=getVehicleColors&vehicleImageID={1}&key={0}",
                    ICONFIGURATORS_TOKEN, vid);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region FuelAPI Actions

        public ActionResult GetYearsFuelApi()
        {
            var jsonData = "";
            var url = $"https://api.fuelapi.com/v1/json/modelYears/?api_key={FUEL_API_TOKEN}";
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<string> years = JsonConvert.DeserializeObject<List<string>>(jsonData);
            return Json(years
                .Select(y => y)
                .OrderByDescending(y => y), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMakesFuelAPI(string y)
        {
            var jsonData = "";
            var url = string.Format("https://api.fuelapi.com/v1/json/makes/?year={1}&api_key={0}", FUEL_API_TOKEN, y);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<FuelAPIMake> makes = JsonConvert.DeserializeObject<List<FuelAPIMake>>(jsonData);
            Global.Session.Instance.FuelAPIMakes = makes;
            return Json(makes
                .Select(m => m.name)
                .OrderBy(m => m), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetModelsFuelAPI(string y, string m)
        {
            var jsonData = "";
            var mID = Global.Session.Instance.FuelAPIMakes.FirstOrDefault(make => make.name == m).id;
            var url = string.Format("https://api.fuelapi.com/v1/json/models/{2}/?year={1}&api_key={0}", FUEL_API_TOKEN,
                y, mID);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<FuelAPIModel> models = JsonConvert.DeserializeObject<List<FuelAPIModel>>(jsonData);
            return Json(models
                .Select(mdl => mdl.name)
                .OrderBy(mdl => mdl), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBodyTypesFuelAPI(string y, string m, string mdl)
        {
            return null;
        }

        public ActionResult GetSubModelsFuelAPI(string y, string m, string mdl, string b)
        {
            var jsonData = "";
            var url = string.Format(
                "https://api.fuelapi.com/v1/json/vehicles/?year={1}&make={2}&model={3}&api_key={0}", FUEL_API_TOKEN, y,
                m, mdl);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<FuelAPISubModel> subModels = JsonConvert.DeserializeObject<List<FuelAPISubModel>>(jsonData);
            foreach (var sm in subModels)
            {
                if ((sm.trim ?? "") == "")
                    sm.trim = "Base";

                if (!string.IsNullOrWhiteSpace(sm.bodytype))
                {
                    sm.trim = $"{sm.bodytype_desc} {sm.trim} {sm.id} ({sm.DriveTrain})";
                }
            }
            Global.Session.Instance.FuelAPISubModels = subModels;
            return Json(subModels
                .Select(sm => sm.trim)
                .Distinct()
                .OrderBy(sm => sm), JsonRequestBehavior.AllowGet);
        }

        [VTWebAuthorize]
        public ActionResult GetVehicleImagesFuelAPI(string y, string m, string mdl, string b, string sm, string o)
        {
            var jsonData = "";
            var smID = Global.Session.Instance.FuelAPISubModels.FirstOrDefault(subm => subm.trim == sm).id;
            var productFormatIDs = 43; //Front
            if (o == "Rear")
                productFormatIDs = 41; //Rear
            var url =
                string.Format(
                    "https://api.fuelapi.com/v1/json/vehicle/{1}?productID=2&productFormatIDs={2}&api_key={0}",
                    FUEL_API_TOKEN, smID, productFormatIDs);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Wheel functions
        [VTWebAuthorize]
        public ActionResult GetWheelPosition(int? vid, string y, string m, string mdl, string sm, string o, int? imageId = null)
        {
            VehicleService service = new VehicleService();
            var wheelPosition = service.GetWheelPosition(vid, y, m, mdl, sm, o, imageId);
            if (wheelPosition != null)
            {
                WheelPositionModel model = new WheelPositionModel()
                {
                    VehicleID = wheelPosition.VehicleID,
                    VehicleImageId = wheelPosition.VehicleImageId,
                    Model = wheelPosition.Model,
                    Year = wheelPosition.Year,
                    Make =  wheelPosition.Make,
                    SubModel = wheelPosition.SubModel,
                    Color = wheelPosition.Color,
                    BodyStyle = wheelPosition.BodyStyle,
                    FrontAngle = wheelPosition.FrontAngle,
                    FrontHeight = wheelPosition.FrontHeight,
                    FrontLeft = wheelPosition.FrontLeft,
                    FrontTop = wheelPosition.FrontTop,
                    FrontWidth = wheelPosition.FrontWidth,
                    Orientation = wheelPosition.Orientation,
                    RearAngle = wheelPosition.RearAngle,
                    RearHeight = wheelPosition.RearHeight,
                    RearLeft = wheelPosition.RearLeft,
                    RearTop = wheelPosition.RearTop,
                    RearWidth = wheelPosition.RearWidth,
                    FlipX = wheelPosition.FlipX,
                    WheelPositionID = wheelPosition.WheelPositionID
                };
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new WheelPositionModel() {VehicleID = null}, JsonRequestBehavior.AllowGet);
            }                      
        }        

        [VTWebAuthorize]
        public ActionResult SaveWheelPosition(string y, string m, string mdl, string sm, decimal fl, decimal ft,
            decimal fw, decimal fh, decimal fa, decimal rl, decimal rt, decimal rw, decimal rh, decimal ra, string o, string flipX,
            int? imageId = null)
        {
            try
            {
                int? smID = null;
                var context = VelocityWheelsContext.Create();
                Data.Models.WheelPosition wheelPosition = null;
                if (imageId.HasValue)
                {
                    var vehicleImage = context.VehicleImages.Include("WheelPositions").FirstOrDefault(v => v.VehicleImageID == imageId.Value);
                    if (vehicleImage != null)
                    {
                        wheelPosition = vehicleImage.WheelPositions.FirstOrDefault();

                        if (wheelPosition == null)
                        {
                            wheelPosition = new Data.Models.WheelPosition
                            {
                                VehicleImage = vehicleImage
                            };
                            context.WheelPositions.Add(wheelPosition);
                        }
                    }                    
                }
                else
                {
                    smID = Global.Session.Instance.FuelAPISubModels.FirstOrDefault(subm => subm.trim == sm).id;
                    wheelPosition = context.WheelPositions.FirstOrDefault(wp => wp.VehicleID == smID && wp.Orientation == o);
                }
                                
                if (wheelPosition == null)
                {
                    wheelPosition = new Data.Models.WheelPosition();
                    context.WheelPositions.Add(wheelPosition);
                }

                wheelPosition.VehicleID = smID;
                wheelPosition.Year = y;
                wheelPosition.Make = m;
                wheelPosition.Model = mdl;
                wheelPosition.SubModel = sm;
                wheelPosition.Orientation = o;
                wheelPosition.FrontLeft = fl;
                wheelPosition.FrontTop = ft;
                wheelPosition.FrontWidth = fw;
                wheelPosition.FrontHeight = fh;
                wheelPosition.FrontAngle = fa;
                wheelPosition.RearLeft = rl;
                wheelPosition.RearTop = rt;
                wheelPosition.RearWidth = rw;
                wheelPosition.RearHeight = rh;
                wheelPosition.RearAngle = ra;
                wheelPosition.FlipX = flipX != null && bool.Parse(flipX);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return Content($"Error saving wheel position. {ex.Message}");
                //return Content(string.Format("Error {0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14}", vid, y, m, mdl, sm, fl, ft, fw, fh, fa, rl, rt, rw, rh, ra));
            }
            return Content("");
        }

        #endregion

        #region General functions

        public ActionResult GetImage(string url)
        {
            byte[] data;
            using (var wc = new MyWebClient())
            {
                data = wc.DownloadData(url);
            }
            return File(data, "image/" + url.Substring(url.LastIndexOf('.') + 1));
        }

        public ActionResult GetVehicleImageFromFile(string imageName)
        {
            _logger.Info("DownloadImage");
            try
            {
                var localPath = VelocityWheelsSettings.GetVehicleContentPath;

                return File(string.Format(localPath, imageName), "image/" + imageName.Substring(imageName.LastIndexOf('.') + 1));
            }
            catch (Exception e)
            {
                _logger.Error("DownloadImage - exeption " + e.Message + e.StackTrace);
                throw;
            }

        }

        public ActionResult GetCroppedImage(string url, string vid, string code, int fid)
        {
            _logger.Info("GetCroppedImage");
            try
            {
                var localPath = VelocityWheelsSettings.GetVehicleContentPath;
                VehicleService service = new VehicleService();

                int id;
                int.TryParse(vid, out id);
                var image = service.GetVehicleImage(url, id, code, fid);

                if (image != null)
                {
                    if (!string.IsNullOrWhiteSpace(image.FileName))
                    {                                               
                        var imgPath = string.Format(localPath, image.FileName);
                        if (!System.IO.File.Exists(imgPath))
                        {
                            service.Delete(image);
                            image = service.DownloadVehicleImageFromFuelApi(url, id, code, fid);

                            // Add thumbnail ... admin only creates thumbnail(along with background process at startup)
                            ImageService imageService = new ImageService();
                            imageService.CreateThumbnailFromVehicleImage(id);
                        }
                    }
                    else if (!image.VehicleID.HasValue)
                    {
                        image.VehicleID = id;
                        image.ImageCode = code;
                        image.ProductFormatId = fid;
                        service.SaveOrUpdate(image);
                    }
                }
                else
                {
                    image = service.DownloadVehicleImageFromFuelApi(url, id, code, fid);

                    // Add thumbnail ... admin only creates thumbnail(along with background process at startup)
                    ImageService imageService = new ImageService();
                    imageService.CreateThumbnailFromVehicleImage(id);
                }

                return File(string.Format(localPath, image.FileName), image.ImageType);                
            }
            catch (Exception e)
            {
                _logger.Error("GetCroppedImage - exeption " + e.Message + e.StackTrace);
                throw;
            }
            
        }
        #endregion

        public ActionResult GetUploadedVehicleImages()
        {
            var service = new VehicleService();
            var images = service.GetAllVehicleUploadedImages(ApiIdEnum.Custom);

            foreach (var vimage in images)
            {
                if (vimage.FileName != null)
                {
                    vimage.Thumbnail = vimage.Thumbnail != null ? $"{service.VehicleImagesBaseUrl}/thumbnails/{vimage.Thumbnail}" : null;
                    vimage.DisplayDate = vimage.CreatedDate?.ToShortDateString();
                }
            }

            return Json(images, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UploadCustomImage(HttpPostedFileWrapper file, string name, string id)
        {
            CustomeImageModel model = new CustomeImageModel();
            //IWheelService service = new WheelService();
            if (file == null || string.IsNullOrWhiteSpace(name))
            {
                return Json(model);
            }

            try
            {
                ImageService service = new ImageService();
                VehicleService vehicleService = new VehicleService(service.Context);
                int imageId = -1;
                int.TryParse(id, out imageId);
                if (imageId != -1)
                {
                    var vImage = vehicleService.GetVehicleImageById(imageId);
                    if (vImage == null)
                    {
                        model.Error = "No Image Found for id";
                        return Json(model);
                    }

                    model.Image = vImage.FileName;
                    model.Id = vImage.VehicleImageID;
                }
                else
                {
                    var fileName = Path.GetFileName(file.FileName);

                    var newFileName = $"custom_{Guid.NewGuid()}_{fileName}";
                    var img = ImageService.GetFileSystemVehicleImagePath(newFileName);
                    file.SaveAs(img);

                    Data.Models.VehicleImage image = new Data.Models.VehicleImage()
                    {
                        FileName = newFileName,
                        ImageCode = null,
                        ApiId = (int)ApiIdEnum.Custom,
                        CreatedDate = DateTime.Now
                    };
                    image = vehicleService.SaveOrUpdate(image);
                    service.CreateThumbnailFromVehicleImageId(image.VehicleImageID);
                    model.Id = image.VehicleImageID;
                    model.Image = $"{service.VehicleImagesBaseUrl}/{image.FileName}";
                }

                //service.SetAuditFields(wheel);

            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);

                model.Error = e.Message;
            }

            return Json(model);
        }

        public ActionResult GetVehiclesForCustomUploader(int imageId, string make, string model, string bodyType)
        {
            ClassificationService service = new ClassificationService();
            List<VehicleMatchModel> makes = service.GetVehiclesNotAssignedToImageId(ApiIdEnum.Aces, imageId, make.Trim(), model.Trim(), bodyType);


            Dictionary<string, VehicleMatchModel> dedupedVehicles = new Dictionary<string, VehicleMatchModel>();

            foreach (var vehicle in makes)
            {
                string key = vehicle.Year + vehicle.Make + ";" + vehicle.Model;
                //if (bodyType != null)
                {
                    key += ";" + vehicle.BodyType;
                }
                VehicleMatchModel v;
                if (!dedupedVehicles.TryGetValue(key, out v))
                {
                    if (vehicle.Image != null)
                    {
                        vehicle.HasImage = true;
                        vehicle.Image = $"{service.VehicleImagesBaseUrl}/thumbnails/{vehicle.Image}";
                    }
                    dedupedVehicles[key] = vehicle;
                }
            }

            var vehicles =
                dedupedVehicles.Select(v => v.Value)
                    .OrderByDescending(v => v.Year)
                    .ThenBy(v => v.Make)
                    .ThenBy(v => v.Model)
                    .ThenBy(v => v.BodyType);

            return Json(vehicles, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAssignedVehiclesForImage(int imageId)
        {
            VehicleImageModel model = new VehicleImageModel();
            ClassificationService service = new ClassificationService();
            List<VehicleMatchModel> vehicles = service.GetAssignedVehiclesForImage(ApiIdEnum.Aces, imageId);
            var vehicleService = new VehicleService(service.Context);
            var vehicleImage = vehicleService.GetVehicleImageById(imageId);

            Dictionary<string, VehicleMatchModel> dedupedVehicles = new Dictionary<string, VehicleMatchModel>();

            foreach (var vehicle in vehicles)
            {
                string key = vehicle.Year + vehicle.Make + ";" + vehicle.Model + ";" + vehicle.BodyType;
                VehicleMatchModel v;
                if (!dedupedVehicles.TryGetValue(key, out v))
                {
                    if (vehicle.Image != null)
                    {
                        vehicle.HasImage = true;
                        vehicle.Image = $"{service.VehicleImagesBaseUrl}/thumbnails/{vehicle.Image}";
                    }
                    dedupedVehicles[key] = vehicle;
                }
            }

            vehicles =
                dedupedVehicles.Select(v => v.Value)
                    .OrderByDescending(v => v.Year)
                    .ThenBy(v => v.Make)
                    .ThenBy(v => v.Model)
                    .ThenBy(v => v.BodyType).ToList();

            model.Id = imageId;
            model.Description = vehicleImage?.Description;
            model.Image = vehicleImage != null ? $"{service.VehicleImagesBaseUrl}/{vehicleImage.FileName}" : null;
            model.AssignedVehicles = vehicles;

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetVehicleImage(int? id, int? vehicleImageId)
        {
            BaseSaveModel model = new BaseSaveModel();
            if (vehicleImageId.HasValue && id.HasValue && vehicleImageId != -1)
            {
                ClassificationService service = new ClassificationService();
                var vehicles = service.GetLikeVehiclesByApiVehicleId(ApiIdEnum.Aces, id.GetValueOrDefault());
                VehicleService vehicleService = new VehicleService(service.Context);
                var vehicleImage = vehicleService.GetVehicleImageById(vehicleImageId.Value);
                if (vehicles.Any() && vehicleImage != null)
                {                    
                    vehicles.ForEach(v => vehicleImage.Classifications.Add(v));
                    service.SaveModels();
                }
            }
            else
            {
                model.Error = $"Ids not provided for save, please contact support.";
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveVehicleImage(int? id, int? vehicleImageId)
        {
            BaseSaveModel model = new BaseSaveModel();
            if (id.HasValue && vehicleImageId.HasValue)
            {
                ClassificationService service = new ClassificationService();
                var vehicles = service.GetLikeVehiclesByApiVehicleId(ApiIdEnum.Aces, id.GetValueOrDefault());
                VehicleService vehicleService = new VehicleService(service.Context);
                var vehicleImage = vehicleService.GetVehicleImageById(vehicleImageId.Value);
                if (vehicles.Any())
                {
                    foreach (var classification in vehicles)
                    {
                        vehicleImage.Classifications.Remove(classification);
                        classification.VehicleImages.Remove(vehicleImage);
                    }
                    
                    service.SaveModels();
                }
            }
            else
            {
                model.Error = $"Id not provided for remove, please contact support.";
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveDescription(VehicleImageModel imageModel)
        {
            BaseSaveModel model = new BaseSaveModel();
            if (imageModel != null)
            {
                VehicleService vehicleService = new VehicleService();
                var vehicleImage = vehicleService.GetVehicleImageById((int)imageModel.Id);
                if (vehicleImage != null)
                {
                    vehicleImage.Description = imageModel.Description;
                    vehicleService.SaveModels();
                }
            }
            else
            {
                model.Error = $"Id not provided for update, please contact support.";
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveUploadedImage(int? id)
        {
            BaseSaveModel model = new BaseSaveModel();
            model.Error = "Not Implemented";
            if (id.HasValue)
            {
                ClassificationService service = new ClassificationService();
                service.RemoveImageAndAssignedVehicles(id.Value);
            }
            else
            {
                model.Error = $"Id not provided for remove, please contact support.";
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }        
    }
}