﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace VelocityWheels.Filters {
  public class ErrorFilter : HandleErrorAttribute {
    log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ErrorFilter));

    public override void OnException(ExceptionContext filterContext) {
      Exception ex = filterContext.Exception;
      filterContext.ExceptionHandled = true;
      var model = new HandleErrorInfo(filterContext.Exception, (string) filterContext.RouteData.Values["Controller"] ?? "", (string) filterContext.RouteData.Values["Action"] ?? "");

      try {
        logger.ErrorFormat("{0}Host: {1}{0}URL: {2}{0}Controller: {3}{0}Action: {4}{0}Session: {5}{0}Exception: {6}{0}Stack Trace: {7}{0}Inner Exception: {8}",
          Environment.NewLine,
          filterContext.RequestContext.HttpContext.Request.Url.Host,
          filterContext.RequestContext.HttpContext.Request.RawUrl,
          ((string) filterContext.RouteData.Values["Controller"] ?? ""),
          ((string) filterContext.RouteData.Values["Action"] ?? ""),
          Global.Session.Instance.Serialize(),
          ex.Message,
          ex.StackTrace,
          (ex.InnerException == null ? "" : ex.InnerException.Message));
      }
      catch(Exception ex2) {
        Console.WriteLine(ex2.Message);
      }

      filterContext.Result = new ViewResult() {
        ViewName = "Error",
        ViewData = new ViewDataDictionary(model)
      };
    }
  }
}