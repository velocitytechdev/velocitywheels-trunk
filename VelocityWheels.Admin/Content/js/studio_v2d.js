//$(document).foundation();

// OBJECTS
var $mainVehicleStage = $('#vtws-mainVehicle');
var $mainWheelleStage = $('#vtws-mainWheel');

// Front Wheel Group
var wvF_main;
var wvF_window;
var wvF_face;

// Rear Wheel Group
var wvR_main;
var wvR_window;
var wvR_face;

// SETTINGS
var baseVehicleWidth = 725 * 3; 
var baseVehicleHeight = 380 * 3;
var vwiDir = '/Content/wheel-images/';
var vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;
var wheelStageScale = $mainVehicleStage.width() / baseVehicleWidth;

var handleSize = 15;
var handleColor = '#0f0';
var paintLayerAlpha = 1;
var wheelColorSet = false;
var wheelColorLayer;
var wheelColor;
var flipWheelToggle = false;

// INITIATE VEHICLE CANVAS ============
var c_mainVehicle = null;
if ($("#v").length > 0)
  c_mainVehicle = new fabric.Canvas('v');
var c_mainWheel;
var img_vc;
var group_wvF;
var group_wvR;
var clientImageUpload = false;

var setVehicleStageScale = function (startWidth, startHeight) {
    c_mainVehicle.setWidth(startWidth * vehicleStageScale);
    c_mainVehicle.setHeight(startHeight * vehicleStageScale);
};

var setVehicleStageScaleToDefault = function () {
    vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;
    wheelStageScale = $mainVehicleStage.width() / baseVehicleWidth;
    setVehicleStageScale(baseVehicleWidth, baseVehicleHeight);
};


function resetVehicleCanvas() {
    vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;
    wheelStageScale = $mainVehicleStage.width() / baseVehicleWidth;
  if (c_mainVehicle) {
    c_mainVehicle.setWidth(baseVehicleWidth * vehicleStageScale);
    c_mainVehicle.setHeight(baseVehicleHeight * vehicleStageScale);
  }
}
// Main Vehicle Image
function setVehicleBackground(canvas, image, callback, params) {
  if (!c_mainVehicle)
    return;

    //resetVehicleCanvas();

  if (callback !== undefined && callback != null) {      
      fabric.Image.fromURL(image, function (img) {

          vehicleStageScale = $mainVehicleStage.width() / img.width;
          wheelStageScale = $mainVehicleStage.width() / baseVehicleWidth;
          setVehicleStageScale(img.width, img.height);
          clientImageUpload = true;

          //Test if Wheels Already Loaded
          if (group_wvF) {
              // Remove Previous Objects since layers may be different
              c_mainVehicle.remove(group_wvF);
              c_mainVehicle.remove(group_wvR);

              group_wvF = null;
              group_wvR = null;
          }
          if (img_vc) {
              c_mainVehicle.remove(img_vc);
              img_vc = null;
          }

          canvas.overlayImage = null;
          canvas.setBackgroundImage(image, canvas.renderAll.bind(canvas), {
              width: canvas.width,
              height: canvas.height,
              // Needed to position backgroundImage at 0/0
              originX: 'left',
              originY: 'top'
          });
          if (image == '') {
              canvas.backgroundImage = false;
              $("#vtws-mainImage").css('visibility', 'hidden');
              $("#vtws-mainActions").hide();
          }
          else {
              $("#vtws-mainImage").css('visibility', 'visible');
              $("#vtws-mainActions").show();
          }

          flipWheelToggle = false;

          callback(params);
      });

      //vehicleStageScale = $mainVehicleStage.width()/350;
      //setVehicleStageScale(350, 263);

  } else {
      //Test if Wheels Already Loaded
      if (group_wvF) {
          // Remove Previous Objects since layers may be different
          c_mainVehicle.remove(group_wvF);
          c_mainVehicle.remove(group_wvR);

          group_wvF = null;
          group_wvR = null;
      }
      if (img_vc) {
          c_mainVehicle.remove(img_vc);
          img_vc = null;
      }

      canvas.overlayImage = null;
      canvas.setBackgroundImage(image, canvas.renderAll.bind(canvas), {
          width: canvas.width,
          height: canvas.height,
          // Needed to position backgroundImage at 0/0
          originX: 'left',
          originY: 'top'
      });
      if (image == '') {
          canvas.backgroundImage = false;
          $("#vtws-mainImage").css('visibility', 'hidden');
          $("#vtws-mainActions").hide();
      }
      else {
          $("#vtws-mainImage").css('visibility', 'visible');
          $("#vtws-mainActions").show();
      }

      flipWheelToggle = false;
  }

  
}

// TEST POSITION OF WHEELS	
function updatePositions(wheel) {
  // Front Wheel
  wheel.wvF_left = group_wvF.left;
  wheel.wvF_top = group_wvF.top;
  wheel.wvF_width = group_wvF.getWidth();
  wheel.wvF_height = group_wvF.getHeight();
  wheel.wvF_angle = group_wvF.getAngle();
  wheel.wvF_flipX = group_wvF.flipX;
 /* console.log("group_wvF = " + group_wvF.flipX);
 console.log(group_wvF);*/


  // Rear Wheel
  wheel.wvR_left = group_wvR.left;
  wheel.wvR_top = group_wvR.top;
  wheel.wvR_width = group_wvR.getWidth();
  wheel.wvR_height = group_wvR.getHeight();
  wheel.wvR_angle = group_wvR.getAngle();
    wheel.wvR_flipX = group_wvR.flipX;
}

// LOAD WHEELS	
function loadWheels(wheel) {
  wheelColorSet = false;

  //Scaling
  wheel.wvF_left = wheelStageScale * wheel.wvF_left;
  wheel.wvF_top = wheelStageScale * wheel.wvF_top;
  wheel.wvF_width = wheelStageScale * wheel.wvF_width;
  wheel.wvF_height = wheelStageScale * wheel.wvF_height;

  wheel.wvR_left = wheelStageScale * wheel.wvR_left;
  wheel.wvR_top = wheelStageScale * wheel.wvR_top;
  wheel.wvR_width = wheelStageScale * wheel.wvR_width;
  wheel.wvR_height = wheelStageScale * wheel.wvR_height;

  //Test if Wheels Already Loaded
  if (group_wvF) {
    // Remove Previous Objects since layers may be different
    c_mainVehicle.remove(group_wvF);
    c_mainVehicle.remove(group_wvR);

    // Don't Save Position
    //updatePositions(wheel);
  }

  //Set Layer Array (used to load group for customizable wheels)
  var wheelLayersFront = [];
  var wheelLayersRear = [];

  // Load Front Wheel
  fabric.Image.fromURL(wheel.frontWheelBase, function (img) {
    wvF_main = img.set({
      id: 'main', visible: true, width: wheel.wvF_width, height: wheel.wvF_height
    });
    wheelLayersFront.push(wvF_main);

    // Load Window
    if (wheel.frontWheelWindow) {
      fabric.Image.fromURL(wheel.frontWheelWindow, function (img) {
        wvF_window = img.set({
          id: 'window', visible: wheelColorSet, opacity: paintLayerAlpha, left: -wheel.wvF_width / 2, top: -wheel.wvF_height / 2, width: wheel.wvF_width, height: wheel.wvF_height
        });
        wheelLayersFront.push(wvF_window);
      });
    }

    // Load Face
    if (wheel.frontWheelFace) {
      fabric.Image.fromURL(wheel.frontWheelFace, function (img) {
        wvF_face = img.set({
          id: 'face', visible: wheelColorSet, opacity: paintLayerAlpha, left: -wheel.wvF_width / 2, top: -wheel.wvF_height / 2, width: wheel.wvF_width, height: wheel.wvF_height
        });
        wheelLayersFront.push(wvF_face);
      });
    }

    group_wvF = new fabric.Group(wheelLayersFront, {
      left: wheel.wvF_left,
      top: wheel.wvF_top,
      angle: wheel.wvF_angle,
      cornerSize: handleSize,
      cornerColor: handleColor,
      borderColor: handleColor
    });

    console.log(wheel.wvF_flipX);
    if (wheel.wvF_flipX === true) {
        flipWheelToggle = true;
    }

    if (flipWheelToggle)
      group_wvF.toggle('flipX');

    c_mainVehicle.add(group_wvF);
  });

  // Load Rear Wheel
  fabric.Image.fromURL(wheel.rearWheelBase, function (img) {
    wvR_main = img.set({
      id: 'main', visible: true, width: wheel.wvR_width, height: wheel.wvR_height
    });
    wheelLayersRear.push(wvR_main);

    // Load Window
    if (wheel.rearWheelWindow) {
      fabric.Image.fromURL(wheel.rearWheelWindow, function (img) {
        wvR_window = img.set({
          id: 'window', visible: wheelColorSet, opacity: paintLayerAlpha, left: -wheel.wvR_width / 2, top: -wheel.wvR_height / 2, width: wheel.wvR_width, height: wheel.wvR_height
        });
        wheelLayersRear.push(wvR_window);
      });
    }

    // Load Face
    if (wheel.rearWheelFace) {
      fabric.Image.fromURL(wheel.rearWheelFace, function (img) {
        wvR_face = img.set({
          id: 'face', visible: wheelColorSet, opacity: paintLayerAlpha, left: -wheel.wvR_width / 2, top: -wheel.wvR_height / 2, width: wheel.wvR_width, height: wheel.wvR_height
        });
        wheelLayersRear.push(wvR_face);
      });
    }

    group_wvR = new fabric.Group(wheelLayersRear, {
      left: wheel.wvR_left,
      top: wheel.wvR_top,
      angle: wheel.wvR_angle,
      cornerSize: handleSize,
      cornerColor: handleColor,
      borderColor: handleColor
    });

    console.log(wheel.wvR_flipX);
    if (wheel.wvR_flipX === true) {
        flipWheelToggle = true;
    }

    if (flipWheelToggle) {
        group_wvR.toggle('flipX');
    }

    c_mainVehicle.add(group_wvR);
  });
}

// HIDE LAYER
function hideLayer(canvas, id) {
  var layer = getObjectById(canvas, id);
  layer.set({ visible: false });
  canvas.renderAll();
}
function hideObject(layer) {
  layer.set({ visible: false });
}

// SET LAYER ORDER
function setLayerOrder() {
  //var innerBarrel = getObjectById(c_mainWheel,'ib');
  var main = getObjectById(c_mainWheel, 'main');

  c_mainWheel.sendToBack(main);
  //c_mainWheel.sendToBack(innerBarrel);
}

function getObjectById(canvas, id) {
  var objsInCanvas = canvas.getObjects();
  for (var obj in objsInCanvas) {
    if (objsInCanvas[obj].get('id') === id) {
      return objsInCanvas[obj];
    }
  }
}

// Load Wheel Images
var defaultWheel = vwiDir;

var o_wheel1 = {
  frontWheelBase: vwiDir + 'base_brake_front.png',
  rearWheelBase: vwiDir + 'base_brake_rear.png'
};

$(function () {
  "use strict";

  if (!c_mainVehicle)
    return;

  var vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;
  var setVehicleStageScale = function (startWidth, startHeight) {
    c_mainVehicle.setWidth(startWidth * vehicleStageScale);
    c_mainVehicle.setHeight(startHeight * vehicleStageScale);
  };

  // INITIATE VEHICLE CANVAS ============
  c_mainVehicle.setWidth(baseVehicleWidth * vehicleStageScale);
  c_mainVehicle.setHeight(baseVehicleHeight * vehicleStageScale);

  // Set Default Background Image
  setVehicleBackground(c_mainVehicle, '');

  // ScrollTo 
  var scrollTarget = 'stage'; // stage, top
  function scrollTo(target) {
    $("html,body").animate({ scrollTop: $(target).offset().top });
    return false;
  }
  
  // INITIATE WHEEL CANVAS FOR CUSTOM WHEELS ============
  // If Wheel Canvas is present
  if ($mainWheelleStage.length) {
    c_mainWheel = new fabric.Canvas('w');
    c_mainWheel.setHeight(250).setWidth(250);
    //c_mainWheel.setHeight($mainWheelleStage.width()).setWidth($mainWheelleStage.width());

    // Load Wheels
    function loadWheelLayer(canvas, target, imageSrc, opacity, name) {
      fabric.Image.fromURL(defaultWheel + '/layers/' + imageSrc, function (img) {
        target = img.set({
          id: name,
          opacity: paintLayerAlpha,
          visible: opacity,
          width: c_mainWheel.width,
          height: c_mainWheel.height,
          selectable: false,
          crossOrigin: "Anonymous"
        });
        canvas.add(target);
        //canvas.setActiveObject(target);
        if (name === 'ib' || name === 'main') {
          canvas.sendToBack(target);
        } else {
          canvas.bringToFront(target);
        }
        canvas.renderAll();
        //setLayerOrder();
      });
    }
    // Wheel Layers
    //var w_ib;
    //loadWheelLayer(c_mainWheel, w_ib, 'Inner-Barrel_base.png', true, 'ib');
    var w_main;
    loadWheelLayer(c_mainWheel, w_main, 'main.png', true, 'main');
    var w_window;
    loadWheelLayer(c_mainWheel, w_window, 'window.png', false, 'window');
    var w_face;
    loadWheelLayer(c_mainWheel, w_face, 'face.png', false, 'face');
  }

  // CONTROLS ========================
  // Flip X
  $('#vtws-flipx').click(function () {
    // Angles
    var wvF_angle = group_wvF.getAngle();
    var wvR_angle = group_wvR.getAngle();

    // Left
    var width = c_mainVehicle.getWidth();
    var wvF_left = group_wvF.getLeft();
    var wvF_newLeft = width - wvF_left;

    var wvR_left = group_wvR.getLeft();
    var wvR_newLeft = width - wvR_left;

    var newFAngle = -wvF_angle;
    var newRAngle = -wvR_angle;
    wvF_angle = newFAngle;
    wvR_angle = newRAngle;

    group_wvF.toggle('flipX').set({ angle: wvF_angle, left: wvF_newLeft }).setCoords();
    group_wvR.toggle('flipX').set({ angle: wvR_angle, left: wvR_newLeft }).setCoords();

    console.log(group_wvF);
    console.log(group_wvR);

    c_mainVehicle.renderAll();

    flipWheelToggle = !flipWheelToggle;
  });
});

