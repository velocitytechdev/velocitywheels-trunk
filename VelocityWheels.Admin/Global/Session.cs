﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VelocityWheels.Models;

namespace VelocityWheels.Global {
  [Serializable]
  public class Session {
    private static string KEY = "__USER_SESSION_ABCD0123__";
    private static Session _instance;
    public static Session Instance {
      get {
        try {
          _instance = (Session) HttpContext.Current.Session[KEY];
        }
        catch { }
        if(_instance == null) {
          _instance = new Session();
          try {
            HttpContext.Current.Session.Add(KEY, _instance);
          }
          catch { }
        }
        return _instance;
      }
    }

    public List<FuelAPIMake> FuelAPIMakes { get; set; }
    public List<FuelAPISubModel> FuelAPISubModels { get; set; } 
  }
}