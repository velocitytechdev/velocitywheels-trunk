﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace VelocityWheels {
  public static class Helpers {
    public static int LowestOf(params int[] nums) {
      var n = nums.Where(i => i > 0);
      if(n.Count() == 0)
        return 0;
      else
        return n.Min();
    }
    public static int HighestOf(params int[] nums) {
      return nums.Max();
    }
    public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs) {
      // Get the subdirectories for the specified directory.
      DirectoryInfo dir = new DirectoryInfo(sourceDirName);

      if(!dir.Exists) {
        throw new DirectoryNotFoundException(
            "Source directory does not exist or could not be found: "
            + sourceDirName);
      }

      DirectoryInfo[] dirs = dir.GetDirectories();
      // If the destination directory doesn't exist, create it.
      if(!Directory.Exists(destDirName)) {
        Directory.CreateDirectory(destDirName);
      }

      // Get the files in the directory and copy them to the new location.
      FileInfo[] files = dir.GetFiles();
      foreach(FileInfo file in files) {
        string temppath = Path.Combine(destDirName, file.Name);
        file.CopyTo(temppath, false);
      }

      // If copying subdirectories, copy them and their contents to new location.
      if(copySubDirs) {
        foreach(DirectoryInfo subdir in dirs) {
          string temppath = Path.Combine(destDirName, subdir.Name);
          DirectoryCopy(subdir.FullName, temppath, copySubDirs);
        }
      }
    }   
  }
}