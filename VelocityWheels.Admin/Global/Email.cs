﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace VelocityWheels {
  public static class Email {
    private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Email));

    static public void SendEmail(string emailFrom, string emailTo, string subject, string body, List<string> attachmentFilesNames = null) {
      try {
        var client = new SmtpClient("smtp.gmail.com", 587) {
          Credentials = new NetworkCredential("noreply@addthesocks.com", "Elkgrove1"),
          EnableSsl = true
        };
        MailMessage m = new MailMessage();
        m.From = new MailAddress("noreply@addthesocks.com", emailFrom);
        //if((Global.Session.Instance.CurrentUser.Email ?? "") != "")
          //m.ReplyToList.Add(new MailAddress(Global.Session.Instance.CurrentUser.Email));
        foreach(var t in emailTo.Split(','))
          m.To.Add(new MailAddress(t));
        m.Subject = subject;
        m.Body = body;
        m.IsBodyHtml = true;
        m.Bcc.Add("amol.oak@gmail.com");
        if(attachmentFilesNames != null && attachmentFilesNames.Count() > 0)
          foreach(var afn in attachmentFilesNames)
            m.Attachments.Add(new Attachment(afn));
#if !DEBUGs
        client.Send(m);
#endif
        logger.InfoFormat("EmailTo: {0}, Subject: {1}", emailTo, subject);
      }
      catch(Exception ex) {
        logger.ErrorFormat("EmailTo: {0}, Subject: {1}, Error: {2}", emailTo, subject, ex.Message);
      }
    }
  }
}