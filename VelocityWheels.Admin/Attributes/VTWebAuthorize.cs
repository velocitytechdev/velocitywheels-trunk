﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Security;
using VelocityWheels.Data.Models;
using VelocityWheels.Models;
using VelocityWheels.Services;

namespace VelocityWheels.Attributes
{
    public class VTWebAuthorize : AuthorizeAttribute, IFilter
    {
        public string Permissions { get; set; }

        //public string Roles { get; set; }

        public VTWebAuthorize(params string[] permissions)
        {
        }

        private bool TestUserMode()
        {
            bool value;
            var x = bool.TryParse(ConfigurationManager.AppSettings["TestUserMode"], out value);
            return value;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var contextUser = httpContext.User.Identity.Name;

            if (HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                string username = FormsAuthentication.Decrypt(HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;

                using (var context = VelocityWheelsContext.Create())
                {
                    var user = context.Users.
                        Include("Role").
                        Include("Role.Permissions").                        
                        FirstOrDefault(u => u.UserName == username && u.Active);
                    if (user != null)
                    {
                        if (Permissions != null)
                        {
                            var permissions = Permissions.Split(',');
                            if (!PermissionService.HasPermission(user, permissions))
                            {
                                return false;
                            }
                        }

                        httpContext.User = new WebUser()
                        {
                            DbUser = user
                        };
                        return true;
                    }
                }
            }            

            return false;
        }

        private void SetPrincipal(IPrincipal principal)
        {
            Thread.CurrentPrincipal = principal;
            if (HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }
    }
}