﻿using System.Web.Optimization;

namespace VelocityWheels
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Content/studio/bundles/WheelStudioScripts").Include(
                "~/Content/studio/js/fabric1.6.6.js",
                "~/Content/studio/js/colorpicker.js",
                "~/Content/studio/js/jquery.leanModal.min.js",
                "~/Content/studio/js/studio_v2d.js",
                "~/Content/js/vt-vehicle-selector.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/admin/style").Include(
                "~/Content/css/bootstrap.css",
                "~/Content/ui-bootstrap-csp.css",
                "~/Content/css/foundation.min.css",                                
                "~/Content/css/nv.d3.css",
                "~/Content/css/font-awesome.min.css",
                "~/Content/css/admin.css"             
                ));

            bundles.Add(new ScriptBundle("~/bundles/admin/script").Include(                      
                      "~/Scripts/angular.js",
                      "~/Scripts/angular-ui-router.js",
                      "~/Scripts/angular-file-upload.js",
                      "~/webapp/vtAdminApp.js",             
                      "~/webapp/Dashboard/dashboardController.js",              
                      "~/webapp/Menu/menuController.js",
                      "~/webapp/Services/dashboardService.js",
                      "~/webapp/Services/applicationService.js",
                      "~/webapp/Services/userService.js",
                      "~/webapp/Services/brandService.js",
                      "~/webapp/Services/wheelService.js",
                      "~/webapp/Services/accountService.js",
                      "~/webapp/Services/reportService.js",
                      "~/webapp/Services/vehicleService.js",
                      "~/webapp/Brand/brandController.js",
                      "~/webapp/Brands/brandsController.js",
                      "~/webapp/Wheel/wheelController.js",
                      "~/webapp/Wheel/productController.js",
                      "~/webapp/Wheel/wheelBuilderController.js",
                      "~/webapp/Vehicle/wheelPositionController.js",
                      "~/webapp/Admin/adminController.js",
                      "~/webapp/Account/accountsController.js",
                      "~/webapp/Account/accountDetailsController.js",
                      "~/webapp/Reports/reportsController.js",
                      "~/webapp/Vehicle/vehicleMappingController.js",
                      "~/webapp/Vehicle/vehicleUploadImageController.js",
                      "~/webapp/Vehicle/vehicleImagesController.js",
                      //"~/webapp/Vehicle/vehicleController.js",
                      /*"~/Scripts/mm-foundation-tpls-0.6.0.js",*/
                      "~/Scripts/angular-ui/ui-bootstrap-tpls.js"
                      ));

            BundleTable.EnableOptimizations = false;
        }
    }
}