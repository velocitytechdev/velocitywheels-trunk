﻿using System.Web;
using System.Web.Mvc;

namespace VelocityWheels {
  public class FilterConfig {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
#if !DEBUG
      filters.Add(new Filters.ErrorFilter());
#endif
      //filters.Add(new AuthorizeAttribute());
    }
  }
}