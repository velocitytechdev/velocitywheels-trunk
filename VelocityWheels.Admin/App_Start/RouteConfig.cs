﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace VelocityWheels
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Brands",                                           // Route name
                "Brand/Index/{brandId}",                                    // URL with parameters
                new { controller = "Brand", action = "Index", brandId = "" }
            );

            routes.MapRoute(
                "Account",                                           // Route name
                "Account/Get/{id}",                                    // URL with parameters
                new { controller = "Account", action = "Get", id = "" }
            );

            routes.MapRoute(
                "Vehicle",                                           // Route name
                "Vehicle/PositionWheels/{id}",                                    // URL with parameters
                new { controller = "Vehicle", action = "PositionWheels", id = "" }
            );

            /*routes.MapRoute(
                name: "Brands",
                url: "Brand/Index/{brand}",
                defaults: new
                {
                    controller = "Brand",
                    action = "All",
                    brand = UrlParameter.Optional
                }

            );*/

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new {controller = "Home", action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}