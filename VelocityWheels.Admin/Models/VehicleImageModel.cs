﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VelocityWheels.Services.Models;

namespace VelocityWheels.Models
{
    public class VehicleImageModel : BaseModel
    {
        public long Id { get; set; }

        public string Image { get; set; }

        public string Description { get; set; }

        public List<VehicleMatchModel> AssignedVehicles { get; set; }
    }
}