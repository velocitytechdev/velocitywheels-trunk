﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VelocityWheels.Models
{
    public class BreadCrumb
    {
        public List<Tuple<string,string>> CrumbUrlList;

        public string CurrentCrumb { get; set; }
    }
}