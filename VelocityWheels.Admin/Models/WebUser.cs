﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using VelocityWheels.Data.Models;

namespace VelocityWheels.Models
{
    public class WebUser : IPrincipal
    {
        private IIdentity _identity;
        public User DbUser { get; set; }               

        public bool IsInRole(string role)
        {
            return true;
        }

        public IIdentity Identity
        {
            get
            {
                _identity = _identity ?? new DbUserIdentity()
                {
                    IsAuthenticated = true,
                    AuthenticationType = string.Empty,
                    Name = DbUser.Name ?? DbUser.UserName
                };
                return _identity;
            }
        }
    }

    public class DbUserIdentity : IIdentity
    {
        public string Name { get; set; }
        public string AuthenticationType { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}