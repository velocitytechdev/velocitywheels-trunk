﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VelocityWheels.Models
{
    public class DashboardModel : BaseModel
    {
        public int Wheels { get; set; }

        public int Vehicles { get; set; }

        public int Fitments { get; set; }

        public int CustomerCars { get; set; }
    }
}