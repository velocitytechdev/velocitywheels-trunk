﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VelocityWheels.Models
{
    public class CustomeImageModel : BaseModel
    {
        public long Id { get; set; }

        public string Image { get; set; }
    }
}