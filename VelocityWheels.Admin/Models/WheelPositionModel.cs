﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VelocityWheels.Models
{
    public class WheelPositionModel
    {
        public int WheelPositionID { get; set; }

        public int? VehicleID { get; set; }

        public string Year { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string BodyStyle { get; set; }

        public string SubModel { get; set; }

        public string Color { get; set; }

        public decimal? FrontLeft { get; set; }

        public decimal? FrontTop { get; set; }

        public decimal? FrontWidth { get; set; }

        public decimal? FrontHeight { get; set; }

        public decimal? FrontAngle { get; set; }

        public decimal? RearLeft { get; set; }

        public decimal? RearTop { get; set; }

        public decimal? RearWidth { get; set; }

        public decimal? RearHeight { get; set; }

        public decimal? RearAngle { get; set; }

        public string Orientation { get; set; }

        public bool FlipX { get; set; }

        public int? VehicleImageId { get; set; }
    }
}