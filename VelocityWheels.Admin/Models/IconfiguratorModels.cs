﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VelocityWheels.Models {
  public class IconfiguratorYearsResult {
    public int Result { get; set; }
    public List<IconfiguratorYear> years { get; set; }
  }
  public class IconfiguratorYear {
    public int year { get; set; }
  }

  public class IconfiguratorMakesResult {
    public int Result { get; set; }
    public List<IconfiguratorMake> makes { get; set; }
  }
  public class IconfiguratorMake {
    public int makeid { get; set; }
    public string make { get; set; }
  }

  public class IconfiguratorModelsResult {
    public int Result { get; set; }
    public List<IconfiguratorModel> models { get; set; }
  }
  public class IconfiguratorModel {
    public int modelid { get; set; }
    public string model { get; set; }
  }

  public class IconfiguratorVehicleTypesResult {
    public int Result { get; set; }
    public List<IconfiguratorVehicleType> Vehicle_Types { get; set; }
  }
  public class IconfiguratorVehicleType {
    public int vehicleTypeID { get; set; }
    public string vehicleTypeName { get; set; }
  }

  public class IconfiguratorSubModelsResult {
    public int Result { get; set; }
    public List<IconfiguratorSubModel> submodels { get; set; }
  }
  public class IconfiguratorSubModel {
    public int submodelid { get; set; }
    public string submodel { get; set; }
    public string option { get; set; }
  }

  public class IconfiguratorVehicleColorsResult {
    public int Result { get; set; }
    public List<IconfiguratorVehicleColor> colors { get; set; }
  }
  public class IconfiguratorVehicleColor {
    public int swatchID { get; set; }
    public string swatchName { get; set; }
    public string swatchImage { get; set; }
    public string vehicleColorImage { get; set; }
  }

  public class IconfiguratorWheelBrandsResult {
    public int Result { get; set; }
    public List<IconfiguratorWheelBrand> wheelBrands { get; set; }
  }
  public class IconfiguratorWheelBrand {
    public int brandID { get; set; }
    public string brandName { get; set; }
    public string logoSmall { get; set; }
    public string logoMedium { get; set; }
  }

  public class IconfiguratorWheelSpecResult {
    public int Result { get; set; }
    public List<IconfiguratorWheelSpec> WheelSpecs { get; set; }
  }
  public class IconfiguratorWheelSpec {
    public int wheelID { get; set; }
    public int wheelImageID { get; set; }
    public ProductImage productImages { get; set; }
    public class ProductImage {
      public string maxImage { get; set; }
      public string highAngleImage { get; set; }
      public string faceImage { get; set; }
    }
    public VehicleImage vehicleImages { get; set; }
    public class VehicleImage {
      public string frontWheelImage { get; set; }
      public string rearWheelImage { get; set; }
    }
    public int brandID { get; set; }
    public string brandName { get; set; }
    public string partNumber { get; set; }
    public decimal productMSRP { get; set; }
    public string producttDescription { get; set; }
    public string styleName { get; set; }
    public string shortFinishName { get; set; }
    public int FinishID { get; set; }
    public string longFinishName { get; set; }
    public decimal decDiameter { get; set; }
    public decimal decWidth { get; set; }
    public string boltPat1 { get; set; }
    public string boltPat2 { get; set; }
    public string boltPat3 { get; set; }
    public string boltPat4 { get; set; }
    public decimal bore { get; set; }
    public string offSet { get; set; }
    public decimal BSM { get; set; }
    public string capNumber { get; set; }
    public decimal loadRating { get; set; }
    public decimal weight { get; set; }
    public string lugType { get; set; }
    public decimal bigBreak { get; set; }
    public string boltPatternList { get; set; }
    public string lipSize { get; set; }
    public string dateUpdated { get; set; }
  }

  //public class IconfiguratorWheelSpecResultDB {
  //  public int Result { get; set; }
  //  public List<IconWheelSpec> WheelSpecs { get; set; }
  //}

  public class IconfiguratorWheelNoImage {
    public int wheelID { get; set; }
    public string brandName { get; set; }
    public string partNumber { get; set; }
    public decimal productMSRP { get; set; }
    public string producttDescription { get; set; }
    public string styleName { get; set; }
    public string shortFinishName { get; set; }
    public string longFinishName { get; set; }
    public decimal decDiameter { get; set; }
    public decimal decWidth { get; set; }
    public string boltPat1 { get; set; }
    public string boltPat2 { get; set; }
    public string boltPat3 { get; set; }
    public string boltPat4 { get; set; }
    public decimal bore { get; set; }
    public string offSet { get; set; }
    public decimal BSM { get; set; }
    public string capNumber { get; set; }
    public decimal loadRating { get; set; }
    public decimal weight { get; set; }
    public string lugType { get; set; }
    public decimal bigBreak { get; set; }
    public string boltPatternList { get; set; }
    public string lipSize { get; set; }
    public string dateUpdated { get; set; }
  }
}