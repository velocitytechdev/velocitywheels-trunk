﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VelocityWheels.Models
{
    [Serializable]
    public class FuelAPIMake
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class FuelAPIModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
    }

    [Serializable]
    public class FuelAPISubModel
    {
        public int id { get; set; }
        public string trim { get; set; }
        public string bodytype_desc { get; set; }
        public string bodytype { get; set; }
        public string DriveTrain { get; set; }       
    }
}