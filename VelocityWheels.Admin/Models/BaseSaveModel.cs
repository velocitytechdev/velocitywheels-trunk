﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VelocityWheels.Models
{
    public class BaseSaveModel : BaseModel
    {
        public long? Id { get; set; }
    }
}