﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VelocityWheels.Models
{
    public class FitmetBuilderModel : BaseModel
    {
        public int NumberUpdated { get; set; }

        public long BrandId { get; set; }

        public string BrandName { get; set; }
    }
}