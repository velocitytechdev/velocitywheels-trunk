namespace VelocityWheels.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VehicleImages
    {
        [Key]
        public int VehicleImageID { get; set; }

        public int? VehicleID { get; set; }

        public int? ChassisID { get; set; }

        [StringLength(50)]
        public string Year { get; set; }

        [StringLength(100)]
        public string Make { get; set; }

        [StringLength(100)]
        public string Model { get; set; }

        [StringLength(100)]
        public string BodyStyle { get; set; }

        [StringLength(100)]
        public string SubModel { get; set; }

        public string URL { get; set; }

        [StringLength(255)]
        public string FileName { get; set; }

        [StringLength(255)]
        public string ImageType { get; set; }
    }
}
