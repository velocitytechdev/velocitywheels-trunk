﻿namespace VelocityWheels.Services
{
    /// <summary>
    ///     Options for the <see cref="IWheelService.ImportWheelsThumbnailsFromFile"/> operation.
    /// </summary>
    public class ImportWheelsThumbnailsFromFileOptions
    {
        /// <summary>
        ///     Gets or sets whether changes should be saved or discarded. Default is <c>false</c>.
        /// </summary>
        public bool DiscardChanges { get; set; }

        /// <summary>
        ///     Gets or sets a value inidcating how data collisions should be handled. Default is to 
        ///     <see cref="FileDataMergeMode.Overwrite"/>.
        /// </summary>
        public FileDataMergeMode FileDataMergeMode { get; set; }

        /// <summary>
        ///     Gets or sets a value whether the thumbnails should be resized. Default is <c>false</c>.
        /// </summary>
        public bool ResizeThumb { get; set; }
    }
}
