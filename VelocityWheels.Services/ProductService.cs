﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VelocityWheels.Data.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    public class ProductService : ServiceBase
    {
        public ProductService() : this(null)
        {
        }

        public ProductService(VelocityWheelsContext context) : base(context)
        {
            Logger = AppLogger.GetLogger(this);
        }

        public bool RestictFitment(List<long> classificationIds, string partNumber)
        {
            try
            {
                var fitments = Context.Fitments.Where(f => classificationIds.Contains(f.ClassificationId.Value) && f.PartNumber == partNumber && !f.Restricted).ToList();
                foreach (var fitment in fitments)
                {
                    fitment.Restricted = true;
                }

                Context.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
            }

            return false;
        }

        public bool EnableFitment(List<long> classificationIds, string partNumber)
        {
            try
            {
                var fitments = Context.Fitments.Where(f => classificationIds.Contains(f.ClassificationId.Value) && f.PartNumber == partNumber && f.Restricted).ToList();
                foreach (var fitment in fitments)
                {
                    fitment.Restricted = false;
                }
                Context.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
            }

            return false;
        }

        public WheelSpec GetByPartNumber(string partNumber)
        {
            var wheelSpec = Context.WheelSpecs.
                Include("Wheel").Include("Wheel.Brand").Include("Wheel.Style").Include("Wheel.Finish").
                FirstOrDefault(ws => ws.PartNumber == partNumber);
            return wheelSpec;
        }

        public bool ProductExists(string partNumber)
        {
            return Context.WheelSpecs.Include("Wheel").
                Any(ws => ws.PartNumber == partNumber && 
                ws.Wheel.FrontMainImage != null && ws.Wheel.RearMainImage != null);
        }

        public IQueryable<Fitment> GetFitmentsByChassis(long chassisId)
        {
            return Context.Fitments.Where(f => f.ChassisId == chassisId && !f.Restricted);
        }
    }
}
