﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VelocityWheels.Data.Enum;
using VelocityWheels.Data.Migrations;
using VelocityWheels.Data.Models;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    public class MappingService : ServiceBase, IMappingService
    {
        private readonly Object _lockObj = new object();

        public MappingService() : this(null)
        {
        }

        public MappingService(VelocityWheelsContext context) : base(context)
        {
            Logger = AppLogger.GetLogger(this);
        }

        public VehicleMappingModel MapVehicle(ApiIdEnum fromApi, ApiIdEnum toApi, int year, string make, string model, string bodyType)
        {
            var mapping =
                Context.VehicleMappings.FirstOrDefault(
                    v =>
                        v.FromApiId == (int)fromApi && v.ToApiId == (int) toApi && v.FromYear == year && v.FromMake == make &&
                        v.FromModel == model);

            if (mapping != null)
            {               
                return new VehicleMappingModel() {
                    Id = mapping.Id,
                    ToMake = mapping.ToMake,
                    ToModel = mapping.ToModel,
                    FromBodyType = bodyType,
                    ToBodyType = mapping.ToBodyType
                };
            }

            // mapping doesn't exist ... use what was passed in
            return new VehicleMappingModel() {Id = -1, ToMake = make, ToModel = model, ToBodyType = bodyType};
        }

        public VehicleMappingModel MapVehicle(ApiIdEnum fromApi, ApiIdEnum toApi, int year, string make, string model, string bodyType, string subModel)
        {
            var mapping =
                Context.VehicleMappings.FirstOrDefault(
                    v =>
                        v.FromApiId == (int)fromApi && v.FromYear == year && v.ToApiId == (int)toApi && v.FromMake == make && v.ToBodyType == bodyType &&
                        v.FromModel == model && v.FromSubModel == subModel) ?? Context.VehicleMappings.FirstOrDefault(
                    v =>
                        v.FromApiId == (int)fromApi && v.FromYear == year && v.ToApiId == (int)toApi && v.FromMake == make &&
                        v.FromModel == model && v.ToBodyType == bodyType) ?? Context.VehicleMappings.FirstOrDefault(
                    v =>
                        v.FromApiId == (int)fromApi && v.FromYear == year && v.ToApiId == (int)toApi && v.FromMake == make &&
                        v.FromModel == model && v.FromSubModel == subModel);


            if (mapping != null)
            {
                return new VehicleMappingModel()
                {
                    Id = mapping.Id,
                    ToMake = mapping.ToMake,
                    ToModel = mapping.ToModel,
                    FromBodyType = mapping.FromBodyType,
                    ToBodyType = mapping.ToBodyType,
                    ToSubModel = mapping.ToSubModel,
                    ToVehicleId = mapping.ToVehicleId
                };
            }

            return null;
        }

        public List<VehicleMapping> GetVehicleMapping(long fromId)
        {
            return Context.VehicleMappings.Where(v => v.FromVehicleId == fromId && v.FromApiId == (int)ApiIdEnum.Aces).ToList();
        }

        public List<VehicleMapping> GetVehicleMapping(long fromId, long toId)
        {
            return Context.VehicleMappings.Where(v => v.FromVehicleId == fromId && v.ToVehicleId == toId && v.FromApiId == (int)ApiIdEnum.Aces).ToList();
        }

        public Classification GetNextUnmappedVehicle(long currentId, int year)
        {
            Classification nextVehicle = GetNextVehicleByYear(currentId, year, true) ??
                                         GetNextVehicleByYear(null, year - 1, true);

            return nextVehicle;
        }

        public Classification GetNextVehicle(long currentId, int year, string make, string model, string bodyType, string subModel)
        {
            Classification nextVehicle = GetNextVehicleByYear(currentId, year) ??
                                         GetNextVehicleByYear(null, year-1);

            return nextVehicle;
        }

        private Classification GetNextVehicleByYear(long? currentId, int year, bool unMatched = false)
        {
            var vehicles =
                Context.Classifications.Where(c => c.ApiId == (int) ApiIdEnum.Aces
                                                   && c.Year == year && (!unMatched || c.VehicleMappingId == null))
                    .OrderByDescending(c => c.Year)
                    .ThenBy(c => c.Make)
                    .ThenBy(c => c.Model)
                    .ThenBy(c => c.BodyType)
                    .ThenBy(c => c.SubModel);

            if (!currentId.HasValue)
            {
                return vehicles.FirstOrDefault();
            }

            bool getNext = false;
            foreach (var classification in vehicles)
            {
                if (getNext)
                {
                    return classification;
                }
                if (classification.Id == currentId)
                {
                    getNext = true;
                }
            }
            return null;
        }

        public Classification GetNextVehicle(int row)
        {
            var nextVehicle =
                Context.Classifications.Where(c => c.ApiId == (int) ApiIdEnum.Aces)
                    .OrderByDescending(c => c.Year)
                    .ThenBy(c => c.Make)
                    .ThenBy(c => c.Model)
                    .ThenBy(c => c.BodyType)
                    .ThenBy(c => c.SubModel)
                    .Skip(row).FirstOrDefault();
            

            return nextVehicle;
        }

        public void UpdateMappingForUserOverride(long fromId, long toId)
        {
            var classificationService = new ClassificationService(Context);
            var fromVehicles = classificationService.GetLikeVehicleById(fromId);
            var vmlist = fromVehicles.Where(f => f.Mapping != null).Select(f => f.Mapping).ToList();
            Context.VehicleMappings.RemoveRange(vmlist);
            var toVehicle = classificationService.GetVehicleByApiVehicleId(ApiIdEnum.FuelApi, toId);

            List<VehicleMapping> newMappings = new List<VehicleMapping>();
            foreach (var fromVehicle in fromVehicles)
            {                
                var mapping = new VehicleMapping
                {
                    FromApiId = (int)ApiIdEnum.Aces,
                    ToApiId = (int)ApiIdEnum.FuelApi,
                    FromYear = fromVehicle.Year,
                    FromMake = fromVehicle.Make,
                    FromModel = fromVehicle.Model,
                    FromBodyType = fromVehicle.BodyType,
                    FromSubModel = fromVehicle.SubModel,
                    FromVehicleId = fromVehicle.Id,
                    ToYear = toVehicle.Year,
                    ToMake = toVehicle.Make,
                    ToModel = toVehicle.Model,
                    ToSubModel = toVehicle.SubModel,
                    ToBodyType = toVehicle.BodyType,
                    ToVehicleId = toVehicle.AcesVehicleId,
                    OverrideByUserName = GetLoggedInUserName(),
                    OverrideDate = DateTime.UtcNow
                };
                newMappings.Add(mapping);
                fromVehicle.Mapping = mapping;
            }
            Context.VehicleMappings.AddRange(newMappings);
            Context.SaveChanges();
        }

        public void SaveOrUpdate(VehicleMapping mapping)
        {
            mapping.OverrideByUserName = this.GetLoggedInUserName();
            mapping.OverrideDate = DateTime.UtcNow;
            if (mapping.Id == 0)
            {
                Context.VehicleMappings.Add(mapping);
            }
            Context.SaveChanges();
        }

        public void BuildMappingForAces()
        {
            Logger.Info("BuildMappingForAces starting ...");
            // Read aces classifications

            FuelApiService fuelApi = new FuelApiService();
            fuelApi.MergeFuelApiEntries();
            List<FuelAPISubModel> fuelApiList = fuelApi.GetAllSubModels().ToList();

            Logger.Info("BuildMappingForAces pulled fuel api data");

            // clear aces vehicle mapping
            var aceLinks = Context.Classifications.Where(c => c.VehicleMappingId != null).ToList();
            aceLinks.ForEach(a => a.VehicleMappingId = null);
            Context.SaveChanges();

            using (var context = CreateNewContext())
            {
                if (context.VehicleMappings.Any(v => v.FromApiId == (int) ApiIdEnum.Aces))
                {
                    return;
                }

                var mappings = context.VehicleMappings.Where(v => v.FromApiId == (int) ApiIdEnum.Aces);
                context.VehicleMappings.RemoveRange(mappings);
                context.SaveChanges();

                var drRows = context.VehicleMappings.Where(v => v.FromApiId == (int) ApiIdEnum.DriveRight).ToList();   
                
                var drMappings = new Dictionary<string, VehicleMapping>();
                drRows.ForEach(m =>
                {
                    var key = (m.FromMake.Trim() + ";" + m.FromModel.Trim()).ToLower();
                    drMappings[key] = m;
                });

                List<VehicleMapping> newMappings = new List<VehicleMapping>();

                // Filter out duplicates and make a dictionary for easy lookup
                var mappingsDictionary = new Dictionary<string, FuelAPISubModel>();
                fuelApiList.ForEach(m =>
                {
                    var key = m.Year + ";" + m.make_name + ";" + m.model_name + ";" + m.bodytype_desc + ";" + m.trim;
                    mappingsDictionary[key.ToLower()] = m;
                });

                int minYear = fuelApiList.Min(f => f.Year);
                var aces = context.Classifications.Where(c => c.ApiId == (int)ApiIdEnum.Aces && c.Year >= minYear).OrderByDescending(a => a.Year).ToList();

                aces.ForEach(a =>
                {
                    var key = a.Year + ";" + a.Make + ";" + a.Model + ";" + a.BodyType + ";" + a.SubModel;
                    key = key.ToLower();
                    FuelAPISubModel apiSubModel;
                    if (mappingsDictionary.TryGetValue(key, out apiSubModel))
                    {
                        BuildNewMapping(a, newMappings, apiSubModel);
                    }
                    else
                    {
                        TryMappingWithVariations(a, mappingsDictionary, newMappings, drMappings);
                    }

                    if (newMappings.Count < 1000)
                    {
                        return;
                    }

                    context.VehicleMappings.AddRange(newMappings);
                    Logger.Info($"BuildMappingForAces - saving {newMappings.Count} entries");
                    context.SaveChanges();
                    newMappings.Clear();
                });

                context.VehicleMappings.AddRange(newMappings);
                Logger.Info($"BuildMappingForAces - saving final {newMappings.Count} entries");
                context.SaveChanges();

            }

            Logger.Info("BuildMappingForAces completed");
        }

        private static void TryMappingWithVariations(Classification classification, Dictionary<string, FuelAPISubModel> mappingsDictionary,
            List<VehicleMapping> newMappings, Dictionary<string, VehicleMapping> drMappings)
        {
            FuelAPISubModel apiSubModel;
            VehicleMapping oldMapping;

            string toBodyType = classification.BodyType;
            if (!string.IsNullOrWhiteSpace(classification.BodyType))
            {
                if (classification.BodyType.Contains("Extended Cab"))
                {
                    toBodyType = "Ext. Cab Pickup";
                }
                else if (classification.BodyType.Contains("Standard Cab"))
                {
                    toBodyType = "Regular Cab Pickup";
                }
            }

            if (toBodyType == null)
            {
                return;
            }

            string drKey = classification.Make.Trim() + ";" + classification.Model.Trim();
            drKey = drKey.ToLower();
            if (drMappings.TryGetValue(drKey, out oldMapping))
            {                             
                var key = classification.Year + ";" + oldMapping.ToMake?.Trim() + ";" + oldMapping.ToModel?.Trim() + ";" + toBodyType + ";" + classification.SubModel;
                key = key.ToLower();
                if (mappingsDictionary.TryGetValue(key, out apiSubModel))
                {
                    BuildNewMapping(classification, newMappings, apiSubModel);
                    return;
                }

                if (classification.Make.ToLower() == "bmw")
                {   // bmw mapping is very different, try model as submodel
                    key = classification.Year + ";" + oldMapping.ToMake?.Trim() + ";" + oldMapping.ToModel?.Trim() + ";" + toBodyType + ";" + classification.Model;
                    key = key.ToLower();
                    if (mappingsDictionary.TryGetValue(key, out apiSubModel))
                    {
                        BuildNewMapping(classification, newMappings, apiSubModel);
                        return;
                    }
                }
                
            }           

            var key1 = classification.Year + ";" + classification.Make + ";" + classification.Model + ";" + toBodyType + ";" + classification.SubModel;
            key1 = key1.ToLower();
            if (!mappingsDictionary.TryGetValue(key1, out apiSubModel))
            {
                return;
            }

            BuildNewMapping(classification, newMappings, apiSubModel);
        }

        private static void BuildNewMapping(Classification c, List<VehicleMapping> newMappings, FuelAPISubModel apiSubModel)
        {
            var mapping = new VehicleMapping
            {
                FromApiId = (int) ApiIdEnum.Aces,
                ToApiId = (int) ApiIdEnum.FuelApi,
                FromYear = c.Year,
                FromMake = c.Make,
                FromModel = c.Model,
                FromBodyType = c.BodyType,
                FromSubModel = c.SubModel,
                FromVehicleId = c.Id,
                ToYear = c.Year,
                ToMake = apiSubModel.make_name,
                ToModel = apiSubModel.model_name,
                ToSubModel = apiSubModel.trim,
                ToBodyType = apiSubModel.bodytype_desc ?? apiSubModel.bodytype,
                ToVehicleId = apiSubModel.id
            };

            c.Mapping = mapping;

            newMappings.Add(mapping);
        }
    }
}
