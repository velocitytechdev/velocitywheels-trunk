﻿using System;
using System.Collections.Generic;
using VelocityWheels.Data.Models;

namespace VelocityWheels.Services
{
    public interface IModelService
    {
        void SetAuditFields(IEnumerable<IAuditedModel> models, DateTime? timestamp = null);
    }
}