﻿namespace VelocityWheels.Services
{
    /// <summary>
    ///     Options for the <see cref="IWheelService.ImportWheelDataFromFile"/> operation.
    /// </summary>
    public class ImportWheelDataFromFileOptions
    {
        /// <summary>
        ///     Gets or sets whether changes should be saved or discarded. Default is <c>false</c>.
        /// </summary>
        public bool DiscardChanges { get; set; }

        /// <summary>
        ///     Gets or sets a value inidcating how data collisions should be handled. Default is to 
        ///     <see cref="FileDataMergeMode.Overwrite"/>.
        /// </summary>
        public FileDataMergeMode FileDataMergeMode { get; set; }
    }
}