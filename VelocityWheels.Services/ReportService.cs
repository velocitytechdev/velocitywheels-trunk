﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using VelocityWheels.Data.Enum;
using VelocityWheels.Data.Models;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    public class ReportService : ServiceBase
    {
        private object _lockObj = new object();

        public ReportService() : this(null)
        {
        }

        public ReportService(VelocityWheelsContext context) : base(context)
        {
            Logger = AppLogger.GetLogger(this);
        }

        public string GenerateUnmatchedFitmentReport(HttpServerUtilityBase server)
        {
            string reportfileName = "UnmatchedFitmentReport.csv";
            var reportsPath = server.MapPath("~/App_Data/Reports");
            string filePath = Path.Combine(reportsPath, reportfileName);

            var date = DateTime.Now;
            if (!Directory.Exists(reportsPath))
            {
                Directory.CreateDirectory(reportsPath);
            }

           // csv.AppendLine($"Year,Make,Model,Submodel,BodyType,VehicleId");

            try
            {
                File.Delete(filePath);
                //File.AppendAllText(filePath, csv.ToString());
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
            }

            using (VelocityWheelsContext context = VelocityWheelsContext.Create())
            {
                /*var acesVehicles = context.Classifications.
                    Where(c => c.ApiId == (int) ApiIdEnum.Aces && c.ChassisId == null).
                    OrderByDescending(c => c.Year).ThenBy(c => c.Make).ThenBy(c => c.Model).ThenBy(c => c.BodyType).ThenBy(c => c.SubModel).
                    ToList();

                using (var csv = new CsvHelper.CsvWriter(new StreamWriter(filePath)))
                {
                    csv.WriteRecords(acesVehicles.Select(a => new
                    {
                        a.Year,
                        a.Make,
                        a.Model,
                        a.BodyType,
                        a.SubModel,
                        a.RegionId,
                        a.AcesVehicleId,
                        a.ChassisId
                    }));
                }*/
                                   
                //var fitments = context.Fitments.Where(f => f.ApiId ==)
            }

            return filePath;
        }

        public string GenerateMissingPositionsReport(HttpServerUtilityBase server)
        {
            string reportfileName = "VehiclesMissingWheelPositions.csv";
            var reportsPath = server.MapPath("~/App_Data/Reports");
            string filePath = Path.Combine(reportsPath, reportfileName);

            var date = DateTime.Now;
            if (!Directory.Exists(reportsPath))
            {
                Directory.CreateDirectory(reportsPath);
            }

            StringBuilder csv = new StringBuilder();
            csv.AppendLine($"Year,Make,Model,BodyType,SubModel,VehicleId,Wheel Positions");

            try
            {
                
                File.Delete(filePath);
                File.AppendAllText(filePath, csv.ToString());
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
            }

            using (VelocityWheelsContext context = VelocityWheelsContext.Create())
            {
                Dictionary<int, string> wheelMap = new Dictionary<int, string>();
                var wpositions = context.WheelPositions.ToList();
                foreach (var wheelPosition in wpositions)
                {
                    string orientation;
                    int vId = wheelPosition.VehicleID.GetValueOrDefault();
                    if (wheelMap.TryGetValue(vId, out orientation))
                    {                       
                        wheelMap[vId] = wheelPosition.Orientation == "front" ? wheelPosition.Orientation + "/" + orientation : orientation + "/" + wheelPosition.Orientation;
                    }
                    else
                    {
                        wheelMap.Add(vId, wheelPosition.Orientation);
                    }
                }

                FuelApiService fuelApi = new FuelApiService();

                List<string> years = fuelApi.GetYearsFuelApi();
                foreach (string year in years)
                {
                    if (string.IsNullOrWhiteSpace(year))
                        continue;

                    Logger.Info($"Building report - processing year {year}");

                    csv.Clear();
                    List<FuelAPIMake> makes = fuelApi.GetMakesFuelApi(year);
                    Parallel.ForEach(makes, m =>
                    {
                        List<FuelAPIModel> models = fuelApi.GetModelsFuelApi(year, m.id);
                        foreach (var fuelApiModel in models)
                        {
                            var submodels = fuelApi.GetSubModelsFuelApi(year, m.name, fuelApiModel.name);
                            foreach (FuelAPISubModel fuelApiSubModel in submodels)
                            {
                                string orientation;
                                wheelMap.TryGetValue(fuelApiSubModel.id, out orientation);

                                lock (_lockObj)
                                {
                                    csv.AppendLine($"{year},{fuelApiSubModel.make_name},{fuelApiSubModel.model_name},{fuelApiSubModel.bodytype_desc},{fuelApiSubModel.trim},{fuelApiSubModel.id},{orientation}");
                                }
                            }
                        }
                    });

                    try
                    {
                        File.AppendAllText(filePath, csv.ToString());
                    }
                    catch (Exception e)
                    {
                        Logger.LogException(e.Message, e);
                    }
                }
            }

            return filePath;
        }


        public string GenerateAcesVehicleMappingReport(HttpServerUtilityBase server)
        {
            string reportfileName = "AcesMappingReport.csv";
            var reportsPath = server.MapPath("~/App_Data/Reports");
            string filePath = Path.Combine(reportsPath, reportfileName);

            var date = DateTime.Now;
            if (!Directory.Exists(reportsPath))
            {
                Directory.CreateDirectory(reportsPath);
            }

            try
            {
                File.Delete(filePath);
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
            }

            using (VelocityWheelsContext context = VelocityWheelsContext.Create())
            {
                var acesVehicles = context.Classifications.ToList();
                var missingMaps = context.VehicleMappings.Where(m => m.FromApiId == (int)ApiIdEnum.Aces && m.FromVehicleId == null).ToList();
                var mappedItems = context.VehicleMappings.Where(m => m.FromApiId == (int)ApiIdEnum.Aces && m.FromVehicleId != null).ToDictionary(m => m.FromVehicleId);

                List<Classification> noMaps = new List<Classification>();
                foreach (var c in acesVehicles)
                {
                    if (!mappedItems.ContainsKey(c.Id))
                    {
                        noMaps.Add(c);
                    }
                }

                noMaps = noMaps.OrderByDescending(n => n.Year).ThenBy(n => n.Make).ThenBy(n => n.Model).ToList();

                using (var csv = new CsvHelper.CsvWriter(new StreamWriter(filePath)))
                {
                    /*csv.WriteRecords(missingMaps.Select(a => new
                    {
                        FromYear = a.ToYear,                        
                        FromMake = "",
                        FromModel = "",
                        FromBodyType = "",
                        FromSubModel = "",
                        FromVehicleId = "",
                        a.ToYear,
                        a.ToMake,
                        a.ToModel,
                        a.ToBodyType,
                        a.ToSubModel,
                        a.ToVehicleId,
                    }));*/

                    csv.WriteRecords(noMaps.Select(a => new
                    {
                        FromYear = a.Year,
                        FromMake = a.Make,
                        FromModel = a.Model,
                        FromBodyType = a.BodyType,
                        FromSubModel = a.SubModel,
                        FromVehicleId = a.Id,
                        ToYear = a.Year,
                        ToMake = "",
                        ToModel = "",
                        ToBodyType = "",
                        ToSubModel = "",
                        ToVehicleId = "",
                    }));
                }
            }

            return filePath;
        }

        public string GenerateFitmentGuide(HttpServerUtilityBase server)
        {
            string reportfileName = "FitmentGuide.csv";
            var reportsPath = server.MapPath("~/App_Data/Reports");
            string filePath = Path.Combine(reportsPath, reportfileName);

            var date = DateTime.Now;
            if (!Directory.Exists(reportsPath))
            {
                Directory.CreateDirectory(reportsPath);
            }

            try
            {
                File.Delete(filePath);
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
            }

            using (VelocityWheelsContext context = VelocityWheelsContext.Create())
            {
                var fitmentGuides = context.FitmentGuides.ToList();


                using (var csv = new CsvHelper.CsvWriter(new StreamWriter(filePath)))
                {
                    csv.WriteRecords(fitmentGuides.Select(a => new
                    {
                        a.YearFrom,
                        a.YearTo,
                        a.Make,
                        a.Model,
                        a.BodyStyle,
                        a.Submodel,
                        a.Brakes,
                        a.Suspension,
                        a.MinDia,
                        a.MaxDia,
                        a.MinWidth,
                        a.MaxWidth,
                        a.BoltMetric,
                        a.BoltStandard,
                        a.OffsetMin,
                        a.OffsetMax,
                        a.ChassisId
                    }));
                }
            }

            return filePath;
        }

    }
}
