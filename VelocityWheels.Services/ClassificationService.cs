﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using VelocityWheels.Data.Enum;
using VelocityWheels.Data.Models;
using VelocityWheels.ExternalServices;
using VelocityWheels.ExternalServices.Models;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    public class ClassificationService : ServiceBase, IClassificationService
    {
        public ClassificationService() : this(null)
        {
        }

        public ClassificationService(VelocityWheelsContext context) : base(context)
        {
            Logger = AppLogger.GetLogger(this);
        }

        public List<int> GetYears(ApiIdEnum apiId)
        {
            var years =
                Context.Classifications.Where(c => c.ApiId == (int) apiId).
                    GroupBy(c => new {c.Year}).Select(g => g.Key.Year);

            return years.OrderByDescending(y => y).ToList();
        }

        public List<string> GetMakes(ApiIdEnum apiId, int year)
        {
            var makes =
                Context.Classifications.Where(c => c.ApiId == (int) apiId && c.Year == year).
                    GroupBy(c => new {c.Make}).Select(g => g.Key.Make);

            return makes.OrderBy(m => m).ToList();
        }

        public List<string> GetModels(ApiIdEnum apiId, int year, string make)
        {
            var models =
                Context.Classifications.Where(c => c.ApiId == (int) apiId && c.Year == year && c.Make == make).
                    GroupBy(c => new {c.Model}).Select(g => g.Key.Model);

            return models.OrderBy(m => m).ToList();
        }

        public List<string> GetBodyTypes(ApiIdEnum apiId, int year, string make, string model)
        {
            var bodyTypes =
                Context.Classifications.
                    Where(c => c.ApiId == (int) apiId && c.Year == year && c.Make == make && c.Model == model).
                    GroupBy(c => new {c.BodyType}).Select(g => g.Key.BodyType);

            return bodyTypes.OrderBy(b => b).ToList();
        }

        public List<string> GetSubModels(ApiIdEnum apiId, int year, string make, string model, string bodyType)
        {
            var subModles =
                Context.Classifications.
                    Where(
                        c =>
                            c.ApiId == (int) apiId && c.Year == year && c.Make == make && c.Model == model &&
                            c.BodyType == bodyType).
                    GroupBy(c => new {c.SubModel}).Select(g => g.Key.SubModel);

            return subModles.OrderBy(s => s).ToList();
        }

        public List<Classification> GetFullSubModels(ApiIdEnum apiId, int year, string make, string model, string bodyType)
        {
            var subModles =
                Context.Classifications.
                    Where(
                        c =>
                            c.ApiId == (int) apiId && c.Year == year && c.Make == make && c.Model == model &&
                            c.BodyType == bodyType);                    

            return subModles.OrderBy(s => s.SubModel).ThenBy(s => s.RegionId).ToList();
        }

        public List<Classification> GetFullVehicles(ApiIdEnum apiId, int year, string make, string model, string bodyType, string subModel)
        {
            var subModles =
                Context.Classifications.
                    Where(
                        c =>
                            c.ApiId == (int)apiId && c.Year == year && c.Make == make && c.Model == model &&
                            c.BodyType == bodyType && c.SubModel == subModel);

            return subModles.OrderBy(s => s.SubModel).ThenBy(s => s.RegionId).ToList();
        }

        public List<VehicleMatchModel> GetFullMakes(ApiIdEnum apiId, int year, string make)
        {
            var subModles =
                Context.Classifications.
                    Where(c => c.ApiId == (int)apiId && c.Year == year && c.Make == make).
                    Select(c => new VehicleMatchModel()
                    {
                        Id = c.Id,
                        AcesVehicleId = c.AcesVehicleId,
                        Year = c.Year,
                        Make = c.Make,
                        Model = c.Model,
                        BodyType = c.BodyType,
                        SubModel = c.SubModel,
                        RegionId = c.RegionId,
                        IsMappedItem = false,
                        
                    });

            return subModles.OrderBy(s => s.Year).ThenBy(s => s.Make).ThenBy(s => s.Model).ThenBy(s => s.BodyType).ThenBy(s => s.SubModel).ToList();
        }

        public List<VehicleMatchModel> GetVehiclesNotAssignedToImageId(ApiIdEnum apiId, int imageId, string make, string model, string bodyType = null)
        {
            var subModles =
                Context.Classifications.
                    Include("VehicleImage").
                    Where(c => c.ApiId == (int)apiId && c.Make == make && c.Model == model && c.VehicleImages.All(v => v.VehicleImageID != imageId) && (bodyType == null ? true : c.BodyType == bodyType)).
                    Select(c => new VehicleMatchModel()
                    {
                        Id = c.Id,
                        AcesVehicleId = c.AcesVehicleId,
                        Year = c.Year,
                        Make = c.Make,
                        Model = c.Model,
                        BodyType = c.BodyType,
                        SubModel = c.SubModel,
                        RegionId = c.RegionId,
                        //VehicleImageId = c.VehicleImageId,
                        //Image = c.VehicleImage.Thumbnail
                    });

            return subModles.OrderByDescending(s => s.Year).ThenBy(s => s.Make).ThenBy(s => s.Model).ThenBy(s => s.BodyType).ThenBy(s => s.SubModel).ToList();
        }

        public List<VehicleMatchModel> GetAssignedVehiclesForImage(ApiIdEnum apiId, int imageId)
        {
            var image = Context.VehicleImages.FirstOrDefault(v => v.VehicleImageID == imageId);
            if (image != null)
            {
                var subModles = image.Classifications.Select(c => new VehicleMatchModel()
                {
                    Id = c.Id,
                    AcesVehicleId = c.AcesVehicleId,
                    Year = c.Year,
                    Make = c.Make,
                    Model = c.Model,
                    BodyType = c.BodyType,
                    SubModel = c.SubModel,
                    RegionId = c.RegionId,
                    VehicleImageId = image.VehicleImageID,
                    Image = image.Thumbnail,
                });

                return subModles.OrderByDescending(s => s.Year).ThenBy(s => s.Make).ThenBy(s => s.Model).ThenBy(s => s.BodyType).ThenBy(s => s.SubModel).ToList();
            }
            /*var subModles =
                Context.Classifications.
                    Include("VehicleImage").
                    Where(c => c.ApiId == (int)apiId && c.VehicleImages.Contains(imageId)).
                    Select(c => new VehicleMatchModel()
                    {
                        Id = c.Id,
                        AcesVehicleId = c.AcesVehicleId,
                        Year = c.Year,
                        Make = c.Make,
                        Model = c.Model,
                        BodyType = c.BodyType,
                        SubModel = c.SubModel,
                        RegionId = c.RegionId,
                        VehicleImageId = c.VehicleImageId,
                        Image = c.VehicleImage.Thumbnail,
                    });*/

            return new List<VehicleMatchModel>();
        }

        public bool RemoveImageAndAssignedVehicles(int imageId)
        {
            /*var subModles =
                Context.Classifications.
                    Include("VehicleImage").Include("VehicleImage.WheelPositions").
                    Where(c => c.VehicleImageId == imageId).ToList();

            var vehicle = subModles.FirstOrDefault();*/
            var vehicleImage = Context.VehicleImages.FirstOrDefault(v => v.VehicleImageID == imageId);
            if (vehicleImage != null)
            {
                vehicleImage.Classifications.Clear();
                vehicleImage.Classifications = null;

                RemoveVehicleImage(vehicleImage);
            }


            /*if (vehicle != null)
            {
                
            }
            else
            {   // Try Vehicle Images as there may be no assigned vehicles.
                var vehicleImage = Context.VehicleImages.FirstOrDefault(v => v.VehicleImageID == imageId);
                RemoveVehicleImage(vehicleImage);
            }*/

            return Context.SaveChanges() > 0;
        }

        private void RemoveVehicleImage(VehicleImage vehicleImage)
        {
            var imgFile = ImageService.GetFileSystemVehicleImagePath(vehicleImage.FileName);
            if (!string.IsNullOrEmpty(imgFile))
            {
                File.Delete(imgFile);
            }

            var thumbFile = ImageService.GetFileSystemVehicleThumbnailPath(vehicleImage.Thumbnail);
            if (!string.IsNullOrEmpty(thumbFile))
            {
                File.Delete(thumbFile);
            }

            if (vehicleImage.WheelPositions != null && vehicleImage.WheelPositions.Any())
            {
                Context.WheelPositions.RemoveRange(vehicleImage.WheelPositions);
            }

            Context.VehicleImages.Remove(vehicleImage);
        }

        public Classification GetVehicleById(long id)
        {
            return Context.Classifications.Include("Mapping").FirstOrDefault(c => c.Id == id);
        }

        public List<Classification> GetLikeVehicleById(long id)
        {
            var cl = Context.Classifications.Include("Mapping").FirstOrDefault(c => c.Id == id);
            if (cl == null)
            {
                return null;
            }
            return
                Context.Classifications.Include("Mapping").Where(
                    c =>
                        c.Year == cl.Year && c.Make == cl.Make && c.Model == cl.Model && c.BodyType == cl.BodyType &&
                        c.SubModel == cl.SubModel).ToList();
        }

        public Classification GetVehicleByApiVehicleId(ApiIdEnum apiId, long acesId)
        {
            return Context.Classifications.FirstOrDefault(c => c.AcesVehicleId == acesId && c.ApiId == (int)apiId);
        }

        public List<Classification> GetVehiclesByApiVehicleId(ApiIdEnum apiId, long id)
        {
            return Context.Classifications.Where(c => c.Id == id && c.ApiId == (int)apiId).ToList();
        }

        public List<Classification> GetLikeVehiclesByApiVehicleId(ApiIdEnum apiId, long id)
        {
            var vehicle = Context.Classifications.FirstOrDefault(c => c.Id == id && c.ApiId == (int)apiId);
            if (vehicle != null)
            {
                var list = Context.Classifications.Where(c => c.Year == vehicle.Year && c.Make == vehicle.Make && c.Model == vehicle.Model && c.BodyType == vehicle.BodyType && c.ApiId == (int)apiId).ToList();
                if (list.Any())
                {
                    return list;
                }
            }
            else
            {
                return new List<Classification>();
            }

            return new List<Classification>() {vehicle};
        }

        public List<long> GetChassis(int year, string make, string model, string bodyType,
            string subModel)
        {
            var chassis =
                Context.Classifications.
                    Where(c => c.Year == year && c.Make == make &&
                               c.Model == model && 
                               c.BodyType == bodyType && 
                               c.SubModel == subModel && 
                               c.ApiId == VelocityWheelsSettings.ExternalApiId).
                    Select(c => c.Id);

            var result = chassis.OrderBy(s => s).ToList();

            /*if (!result.Any())
            {
                // Can't find a local version of chassis, try drive right api then save to db for quick lookup
                try
                {
                    WheelWizardsService wwservice = new WheelWizardsService();
                    List<VehicleModelEx> vehicles = wwservice.GetVehicles(Convert.ToInt32(year), make, model, bodyType, subModel);
                    if (vehicles.Any())
                    {
                        var vehicle = vehicles.FirstOrDefault();
                        if (vehicle != null)
                        {
                            Context.Classifications.Add(new Classification()
                            {
                                Year = year,
                                Make = make,
                                Model = model,
                                BodyType = bodyType,
                                SubModel = subModel,
                                RegionId = vehicle.RegionId,
                                ApiId = vehicle.ApiId,
                                ChassisId = vehicle.ChassisId,
                                ModelId = vehicle.ModelId
                            });

                            Context.SaveChanges();

                            result = new List<string>() { vehicle.ChassisId };
                        }
                    }
                }
                catch (Exception e)
                {
                    _logger.LogException("GetChassis exception", e);
                }
                
            }*/

            return result;
        }
    }
}
