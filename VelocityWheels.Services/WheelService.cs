﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.WebPages;
using CsvHelper;
using VelocityWheels.Data.Enum;
using VelocityWheels.Data.Models;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    using Class = WheelService;

    public class WheelService : ServiceBase, IWheelService
    {
        #region Public Constructors
        public WheelService() : this(null)
        {
        }

        public WheelService(VelocityWheelsContext context) : base(context)
        {
            Logger = AppLogger.GetLogger(this);
        }
        #endregion Public Constructors

        #region Public Methods
        public WheelLayer CreateOrUpdateLayerName(WheelLayerModel model)
        {
            var layer = Context.WheelLayers.FirstOrDefault(l => l.Id == model.Id);
            if (layer == null)
            {
                layer = new WheelLayer()
                {
                    WheelId = model.WheelId,
                    LevelName = model.LevelName
                };
                Context.WheelLayers.Add(layer);
                Context.SaveChanges();
            }
            else
            {
                layer.LevelName = model.LevelName;
                Context.SaveChanges();
            }

            return layer;
        }

        public List<string> GetChassisFitments(long? chassisId)
        {
            List<string> parts;
            if (VelocityWheelsSettings.ExternalApiId == (int)ApiIdEnum.Aces)
            {
                // Aces (FG) Chassis is found in the ChassisId field - The Aces Chassis Id is the "master" id.
                parts =
                    Context.Fitments.Where(
                            f => f.ClassificationId == chassisId && !f.Restricted && f.ApiId == (int)ApiIdEnum.Aces).
                        Select(f => f.PartNumber).Distinct().ToList();

                // All other brands map to the aces (FG) chassis by use of AcesVehicleId (populated by service processing). The ChassisId for non
                // aces fitments are the original chassis provided by the manufacturer.
                IQueryable<string> nonAces =
                    Context.Fitments.Where(
                            f =>
                                f.ClassificationId == chassisId && !f.Restricted &&
                                f.ApiId != (int)ApiIdEnum.Aces).
                        Select(f => f.PartNumber).Distinct();

                parts.AddRange(nonAces);
            }
            else
            {
                parts = Context.Fitments.
                    Where(
                        f => f.ChassisId == chassisId && !f.Restricted && f.ApiId == (int)VelocityWheelsSettings.ExternalApiId).
                            Select(f => f.PartNumber).Distinct().ToList();
            }
            return parts;
        }        

        public static string GetFileSystemLayerImagePath(string brand, string image)
        {
            if (string.IsNullOrWhiteSpace(image))
            {
                return null;
            }

            var baseUrl = VelocityWheelsSettings.LocalWheelContentPath;
            var dir = $"{baseUrl}layers\\{brand}";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            var filePath = $"{baseUrl}layers\\{brand}\\{image}";
            return filePath;
        }

        public static string GetFileSystemThumbnailImagePath(string brand, string image)
        {
            if (string.IsNullOrWhiteSpace(image))
            {
                return null;
            }

            var baseUrl = VelocityWheelsSettings.LocalWheelContentPath;
            var dir = $"{baseUrl}thumbnails\\{brand}";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            var filePath = $"{baseUrl}thumbnails\\{brand}\\{image}";
            return filePath;
        }

        public ProductModel GetProduct(long id)
        {
            var baseUrl = VelocityWheelsSettings.GetApiBaseUrl;

            ProductModel product = null;
            var wheelSpec = Context.WheelSpecs.
                Include("Wheel").Include("Wheel.Brand").Include("Wheel.Style").Include("Wheel.Finish").
                FirstOrDefault(w => w.WheelSpecID == id);
            if (wheelSpec != null)
            {
                product = new ProductModel()
                {
                    PartNumber = wheelSpec.PartNumber,
                    BoltPattern = wheelSpec.BoltPattern1 ?? wheelSpec.BoltPattern2,
                    BoltPattern1 = wheelSpec.BoltPattern1,
                    BoltPattern2 = wheelSpec.BoltPattern2,
                    BoltPattern3 = wheelSpec.BoltPattern3,
                    BoltPattern4 = wheelSpec.BoltPattern4,
                    Bore = wheelSpec.Bore,
                    Brand = wheelSpec.Wheel.Brand.Name,
                    BrandId = wheelSpec.Wheel.BrandId,
                    Bsm = wheelSpec.BSM,
                    BigBrakes = wheelSpec.BigBreak != null,
                    Cost = 0,
                    Msrp = wheelSpec.ProductMSRP,
                    Offset = wheelSpec.Offset,
                    Width = wheelSpec.Width,
                    Finish = wheelSpec.Wheel.Finish.Name,
                    FinishId = wheelSpec.Wheel.FinishId,
                    Weight = wheelSpec.Weight,
                    ShortFinish = wheelSpec.Wheel.Finish.ShortName,
                    Diameter = wheelSpec.Diameter,
                    Style = wheelSpec.Wheel.Style.Name,
                    StyleId = wheelSpec.Wheel.StyleId,
                    Description = wheelSpec.ProductDescription,
                    LoadRating = wheelSpec.LoadRating,
                    WheelSpecId = wheelSpec.WheelSpecID,
                    MapPricing = 0,
                    OffRoad = false
                };

                product.BoltPattern = string.Empty;
                if (wheelSpec.BoltPattern1 != null)
                {
                    product.BoltPattern = wheelSpec.BoltPattern1;
                }

                if (!string.IsNullOrWhiteSpace(wheelSpec.BoltPattern2))
                {
                    if (!product.BoltPattern.IsEmpty())
                    {
                        product.BoltPattern += ", ";
                    }
                    product.BoltPattern += wheelSpec.BoltPattern2;
                }

                if (!string.IsNullOrWhiteSpace(wheelSpec.BoltPattern3))
                {
                    if (!product.BoltPattern.IsEmpty())
                    {
                        product.BoltPattern += ", ";
                    }
                    product.BoltPattern += wheelSpec.BoltPattern3;
                }

                if (!string.IsNullOrWhiteSpace(wheelSpec.BoltPattern4))
                {
                    if (!product.BoltPattern.IsEmpty())
                    {
                        product.BoltPattern += ", ";
                    }
                    product.BoltPattern += wheelSpec.BoltPattern4;
                }

                var filePath = GetImageFilePath(wheelSpec.Wheel.Brand.Name, wheelSpec.Wheel.ThumbnailImage);
                product.Thumbnail = filePath;

                var allfitments = Context.Fitments.Where(f => f.PartNumber == product.PartNumber).
                    GroupBy(f => new { f.Restricted, f.ClassificationId }).
                    Select(f => f.Key).ToList();

                var fitments = allfitments.Where(f => !f.Restricted).Select(f => f.ClassificationId).ToList();
                var restrictedFitments = allfitments.Where(f => f.Restricted).Select(f => f.ClassificationId).ToList();

                var vehicles = GetVehicleFitmentModels(fitments);
                var restrictedVehicles = GetVehicleFitmentModels(restrictedFitments);

                product.Fitments = new Dictionary<string, List<ProductFitmentModel>>();
                product.RestrictedFitments = new Dictionary<string, List<ProductFitmentModel>>();

                BuildProductFitmentDictionary(vehicles, product.Fitments);
                BuildProductFitmentDictionary(restrictedVehicles, product.RestrictedFitments);
            }

            return product;
        }

        public Wheel GetWheel(long id)
        {
            var wheel = Context.Wheels.Include("Brand").Include("Style").Include("Finish").FirstOrDefault(w => w.Id == id);
            return wheel;
        }

        public IList<BrandPartialModel> GetWheelsBrands(User user)
        {
            UserService userService = new UserService(Context);
            var accountBrands = userService.GetUserAccountBrands(user).Select(b => b.BrandId);
            bool superAdmin = PermissionService.IsSuperAdmin(user);

            try
            {
                var brands = Context.Wheels.Include("Brand").
                        Where(b => b.BrandId != null).
                        GroupBy(g => new { g.Brand.Name, g.BrandId }).
                        Select(w => new BrandPartialModel()
                        {
                            BrandName = w.Key.Name,
                            Id = w.Key.BrandId,
                            Count = w.Count()
                        });

                if (!superAdmin)
                {
                    brands = brands.Where(b => accountBrands.Contains(b.Id));
                }

                brands = brands.OrderBy(b => b.BrandName);


                return brands.ToList();
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
            }

            return new List<BrandPartialModel>();
        }

        public WheelModel GetWheel(long brandId, long styleId, long finishId)
        {
            var baseUrl = VelocityWheelsSettings.GetApiBaseUrl;

            WheelModel model = null;
            var wheels =
                Context.WheelSpecs.
                Include("Wheel").Include("Wheel.Brand").Include("Wheel.Style").Include("Wheel.Finish").Include("Wheel.WheelLayers").
                Where(s => s.Wheel.BrandId == brandId && s.Wheel.StyleId == styleId && s.Wheel.FinishId == finishId)
                    .OrderBy(s => s.PartNumber);
            foreach (var wheelSpec in wheels)
            {
                if (model == null)
                {
                    model = new WheelModel()
                    {
                        WheelId = wheelSpec.Wheel.Id,
                        Brand = wheelSpec.Wheel.Brand.Name,
                        BrandId = wheelSpec.Wheel.BrandId,
                        Finish = wheelSpec.Wheel.Finish.Name,
                        FinishId = wheelSpec.Wheel.FinishId,
                        ShortFinish = wheelSpec.Wheel.Finish.ShortName,
                        Style = wheelSpec.Wheel.Style.Name,
                        StyleId = wheelSpec.Wheel.StyleId,
                        Products = new List<ProductModel>(),
                        Image = wheelSpec.Wheel.ThumbnailImage,
                        Customizable = wheelSpec.Wheel.WheelLayers.Any(),
                        Active = wheelSpec.Wheel.Active
                    };
                    if (!string.IsNullOrWhiteSpace(model.Image))
                    {
                        var filePath = GetImageFilePath(model.Brand, model.Image);
                        model.Image = filePath;
                    }
                }

                model.Products.Add(new ProductModel()
                {
                    WheelSpecId = wheelSpec.WheelSpecID,
                    Size = $"{wheelSpec.Diameter.GetValueOrDefault():N1} x {wheelSpec.Width.GetValueOrDefault():N1}",
                    BoltPattern = wheelSpec.BoltPattern1 ?? wheelSpec.BoltPattern2,
                    Offset = wheelSpec.Offset,
                    PartNumber = wheelSpec.PartNumber,
                    TotalFitments = Context.Fitments.Count(f => f.PartNumber == wheelSpec.PartNumber)
                });

            }

            return model;
        }

        public WheelModel GetWheelById(long wheelId, bool includeProducts = false)
        {
            var baseUrl = VelocityWheelsSettings.GetApiBaseUrl;

            WheelModel model = null;
            Wheel wheel = null;
            if (includeProducts)
            {
                wheel = Context.Wheels.
                    Include("Brand").
                    Include("Style").
                    Include("Finish").
                    Include("WheelLayers").
                    Include("WheelSpecs").
                    FirstOrDefault(s => s.Id == wheelId);
            }
            else
            {
                wheel = Context.Wheels.Include("Brand").Include("Style").
                    Include("Finish").Include("WheelLayers").FirstOrDefault(s => s.Id == wheelId);
            }

            if (wheel != null)
            {
                model = new WheelModel()
                {
                    WheelId = wheel.Id,
                    Brand = wheel.Brand.Name,
                    BrandId = wheel.BrandId,
                    Finish = wheel.Finish.Name,
                    FinishId = wheel.FinishId,
                    ShortFinish = wheel.Finish.ShortName,
                    Style = wheel.Style.Name,
                    StyleId = wheel.StyleId,
                    Image = wheel.ThumbnailImage,
                    BaseFrontImage = wheel.FrontMainImage,
                    BaseRearImage = wheel.RearMainImage,
                    BaseLayerImage = wheel.BaseLayerImage,
                    Customizable = wheel.WheelLayers.Any(),
                    Active = wheel.Active
                };

                var filePath = GetImageFilePath(model.Brand, model.Image);
                model.Image = filePath;
                filePath = GetFaceImageFilePath(model.Brand, model.BaseFrontImage);
                model.BaseFrontImage = filePath;
                filePath = GetFaceImageFilePath(model.Brand, model.BaseRearImage);
                model.BaseRearImage = filePath;
                filePath = GetFaceImageFilePath(model.Brand, model.BaseLayerImage);
                model.BaseLayerImage = filePath;

                MapLayers(model, wheel);

                if (includeProducts)
                {
                    model.Products = new List<ProductModel>();
                    foreach (var wheelWheelSpec in wheel.WheelSpecs)
                    {
                        string boltpattern = wheelWheelSpec.BoltPattern1 ?? wheelWheelSpec.BoltPattern2;

                        if (wheelWheelSpec.BoltPattern1 != null)
                        {
                            boltpattern = wheelWheelSpec.BoltPattern1;
                        }

                        if (!string.IsNullOrWhiteSpace(wheelWheelSpec.BoltPattern2))
                        {
                            if (!boltpattern.IsEmpty())
                            {
                                boltpattern += ", ";
                            }
                            boltpattern += wheelWheelSpec.BoltPattern2;
                        }

                        if (!string.IsNullOrWhiteSpace(wheelWheelSpec.BoltPattern3))
                        {
                            if (!boltpattern.IsEmpty())
                            {
                                boltpattern += ", ";
                            }
                            boltpattern += wheelWheelSpec.BoltPattern3;
                        }

                        if (!string.IsNullOrWhiteSpace(wheelWheelSpec.BoltPattern4))
                        {
                            if (!boltpattern.IsEmpty())
                            {
                                boltpattern += ", ";
                            }
                            boltpattern += wheelWheelSpec.BoltPattern4;
                        }

                        var product = new ProductModel()
                        {
                            PartNumber = wheelWheelSpec.PartNumber,
                            BoltPattern = boltpattern,
                            Size = $"{wheelWheelSpec.Diameter.GetValueOrDefault():N1} x {wheelWheelSpec.Width.GetValueOrDefault():N1}",
                            Offset = wheelWheelSpec.Offset,
                            Msrp = wheelWheelSpec.ProductMSRP,
                        };
                        model.Products.Add(product);
                    }
                }
            }

            return model;
        }

        

        public IList<WheelResultsModel> GetWheels(long brandId, long? accountId = null)
        {
            var results = GetWheels(null, null, accountId, brandId);
            return results?.WheelResultsModels;
        }

        public WheelResults GetWheels(long? chassisId, WheelFilter filters, long? accountId = null,
            long? brandId = null, SortOption sortOption = null)
        {
            try
            {
                IQueryable<WheelSpec> specs = null;
                if (chassisId.HasValue)
                {
                    var parts = GetChassisFitments(chassisId);

                    specs = Context.WheelSpecs.Include("Wheel").Include("Wheel.Brand").
                        Include("Wheel.Finish").
                        Include("Wheel.Style").
                        Include("Wheel.WheelLayers").
                        Where(w => parts.Contains(w.PartNumber));
                }
                else
                {
                    specs = Context.WheelSpecs.Include("Wheel").
                        Include("Wheel.Brand").
                        Include("Wheel.Finish").
                        Include("Wheel.Style");
                }                

                if (!string.IsNullOrWhiteSpace(filters?.Brand))
                {
                    specs = specs.Where(s => s.Wheel.Brand.Name == filters.Brand);
                }
                else if (brandId.HasValue)
                {
                    specs = specs.Where(s => s.Wheel.BrandId == brandId);
                }                

                if (accountId != null)
                {
                    specs = specs.Where(s => s.Wheel.Active);
                    var brands = Context.AccountBrands.Where(a => a.AccountId == accountId).Select(b => b.BrandId).ToList();
                    specs = specs.Where(s => brands.Contains(s.Wheel.BrandId));
                }                

                var wheelQuery = specs.Select(w => new WheelResultsModel()
                {
                    Id = w.WheelSpecID,
                    WheelId = w.WheelId,
                    PartNumber = w.PartNumber,
                    Brand = w.Wheel.Brand.Name,
                    BrandId = w.Wheel.BrandId,
                    Finish = w.Wheel.Finish.Name,
                    FinishId = w.Wheel.FinishId,
                    ProductName = w.Wheel.Style.Name,
                    StyleId = w.Wheel.StyleId,
                    Msrp = w.ProductMSRP,
                    Diameter = w.Diameter,
                    Width = w.Width,
                    Offset = w.Offset,
                    Customizable = w.Wheel.WheelLayers.Any(),
                    Image = w.Wheel.ThumbnailImage,
                    FrontImage = w.Wheel.FrontMainImage,
                    RearImage = w.Wheel.RearMainImage
                });

                if (filters?.Diameter != null)
                {
                    wheelQuery = wheelQuery.Where(s => s.Diameter == filters.Diameter);
                }

                Dictionary<string, decimal?> diameterMap = new Dictionary<string, decimal?>();

                bool debugData = VelocityWheelsSettings.ExtraDebuggingDataEnabled;
                var resultsModels = new Dictionary<string, WheelResultsModel>();
                foreach (var model in wheelQuery)
                {
                    // Admin application will use brand id, widget will use brand name ... don't display empty images to widget.
                    if (string.IsNullOrWhiteSpace(model.Image) && !brandId.HasValue && !debugData)
                    {
                        continue;
                    }

                    string key = model.Finish + model.ProductName;
                    WheelResultsModel resultsModel;
                    if (!resultsModels.TryGetValue(key, out resultsModel))
                    {
                        resultsModel = model;
                        model.Sizes = new List<string>();
                        resultsModels.Add(key, model);
                    }
                    resultsModel.Msrp = model.Msrp.GetValueOrDefault();
                    if (!string.IsNullOrWhiteSpace(model.Image))
                    {
                        var filePath = GetImageFilePath(model.Brand, model.Image);
                        resultsModel.Image = filePath;
                    }
                    string size = $"{model.Diameter.GetValueOrDefault():N1} x {model.Width.GetValueOrDefault():N1}" /*+ "  et: " + model.Offset*/;
                    if (!resultsModel.Sizes.Any(s => s.Equals(size, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        resultsModel.Sizes.Add(size);
                    }

                    diameterMap[$"{model.Diameter.GetValueOrDefault():N1}"] = model.Diameter;
                }

                var results = resultsModels.Select(m => m.Value);               

                if (sortOption != null)
                {
                    results = sortOption.Direction == 0 ? 
                        results.OrderBy(m => m.GetType().GetProperty(sortOption.Field).GetValue(m, null)) : 
                        results.OrderByDescending(m => m.GetType().GetProperty(sortOption.Field).GetValue(m, null));
                }
                else
                {
                    results = results.OrderBy(m => m.ProductName);
                }

                WheelResults wheelResults = new WheelResults()
                {
                    WheelResultsModels = results.ToList(),
                    Diameters = diameterMap.OrderByDescending(d => d.Value).Select(d => d.Key).ToList()
                };

                return wheelResults;
            }
            catch (Exception e)
            {
                Logger.LogException("GetWheelsByChassis exception", e);
            }

            return null;
        }

        

        

        public WheelModel GetWheelByPartNumber(string partNumber, bool includeProducts = false)
        {
            int? wheelId = Context.WheelSpecs.
                Where(s => s.PartNumber == partNumber).
                Select(s => s.WheelId).FirstOrDefault();

            if (wheelId.HasValue)
            {
                return GetWheelById(wheelId.Value, includeProducts);
            }

            return null;
        }

        public Wheel GetWheelWithParts(long id)
        {
            var wheel = Context.Wheels.
                Include("Brand").
                Include("Style").
                Include("Finish").
                Include("WheelSpecs").
                FirstOrDefault(w => w.Id == id);
            return wheel;
        }

        public IList<WheelSpec> ImportWheelDataFromFile(Stream fileStream, ImportWheelDataFromFileOptions options = null)
        {
            var localOptions = new ImportWheelDataFromFileOptions
            {
                DiscardChanges = options?.DiscardChanges ?? false,
                FileDataMergeMode = options?.FileDataMergeMode ?? FileDataMergeMode.Overwrite,
            };

            var records = GetWheelData(fileStream);

            // TODO Validate Import
            if (!records.Any())
            {
                Logger.Warn($"File stream has no data.");
                return null;
            }

            var record = records.FirstOrDefault();
            if (string.IsNullOrWhiteSpace(record.Partnumber) || string.IsNullOrWhiteSpace(record.Brand) ||
                string.IsNullOrWhiteSpace(record.Style) || string.IsNullOrWhiteSpace(record.Finish))
            {
                throw new ArgumentException("Partnumber, Brand, Style and Finish are required");
            }

            List<WheelSpec> importItems = new List<WheelSpec>();
            List<WheelSpec> toSaveItems = new List<WheelSpec>();

            int batchSize = 0;

            Dictionary<string, List<WheelSpec>> brandWheelMap = new Dictionary<string, List<WheelSpec>>();
            Dictionary<string, Brand> brandMap = Context.Brands.Where(b => b.Name != null).ToDictionary(b => b.Name, b => b);

            var styleMap = GetStyleMap();
            var finishMap = GetFinishMap();

            Dictionary<string, Wheel> wheelMap = new Dictionary<string, Wheel>();

            foreach (var specImportData in records)
            {
                batchSize++;
                List<WheelSpec> wheelSpecs;
                if (!brandWheelMap.TryGetValue(specImportData.Brand, out wheelSpecs))
                {
                    wheelSpecs = Context.WheelSpecs.
                        Include("Wheel").Include("Wheel.Brand").Include("Wheel.Style").Include("Wheel.Finish").
                        Where(w => w.Wheel.Brand.Name == specImportData.Brand).ToList();
                    brandWheelMap.Add(specImportData.Brand, wheelSpecs);
                    foreach (var wheelSpec in wheelSpecs)
                    {
                        var wheelkey = wheelSpec.Wheel.Brand.Name.Trim().ToLower() + ";" + wheelSpec.Wheel.Style.Name.Trim().ToLower() + ";" + wheelSpec.Wheel.Finish.Name.Trim().ToLower();
                        wheelMap[wheelkey] = wheelSpec.Wheel;
                    }
                }

                string key = specImportData.Brand.Trim().ToLower() + ";" + specImportData.Style.Trim().ToLower() + ";" + specImportData.Finish.Trim().ToLower();
                
                var part = wheelSpecs.FirstOrDefault(w => String.Equals(w.PartNumber.Trim(), specImportData.Partnumber.Trim(), StringComparison.CurrentCultureIgnoreCase));
                if (part == null)
                {                    
                    part = new WheelSpec();
                    Wheel wheel;
                    if (wheelMap.TryGetValue(key, out wheel))
                    {
                        part.Wheel = wheel;
                    }
                }
                

                if (localOptions.FileDataMergeMode == FileDataMergeMode.Preserve && part.WheelSpecID != 0)
                {
                    continue;
                }

                var brand = GetOrCreateBrandFromImport(brandMap, specImportData);
                var style = GetOrCreateStyleFromImport(brandMap, styleMap, specImportData);
                var finish = GetOrCreateFinishFromImport(brandMap, finishMap, specImportData);

                Class.MapDataOntoModel(specImportData, part);
                part.Wheel.Brand = brand;
                part.Wheel.Style = style;
                part.Wheel.Finish = finish;

                wheelMap[key] = part.Wheel;

                importItems.Add(part);
                if (part.WheelSpecID == 0)
                {   // only add new parts, others are just updates
                    toSaveItems.Add(part);
                }

                // For now save each one so we don't loose any data on import. Import can be run again to merge.
                if (!localOptions.DiscardChanges && batchSize >= 1000)
                {
                    if (part.WheelSpecID == 0 && !localOptions.DiscardChanges)
                    {
                        Context.WheelSpecs.AddRange(toSaveItems);
                    }
                    toSaveItems.Clear();
                    SaveModels(Context, timestamp: null, activationStatus: true);
                    batchSize = 0;
                }
            }

            if (!localOptions.DiscardChanges && toSaveItems.Any())
            {
                Context.WheelSpecs.AddRange(toSaveItems);
                SaveModels(Context, timestamp: null, activationStatus: true);
            }
            else if (!localOptions.DiscardChanges)
            {   // save any updates
                SaveModels(Context, timestamp: null, activationStatus: true);
            }

            return importItems;
        }

        private Dictionary<string, Finish> GetFinishMap()
        {
            Dictionary<string, Finish> finishMap = new Dictionary<string, Finish>();
            var finishes = Context.Finishes.Where(s => s.Name != null).ToList();
            foreach (var finsh in finishes)
            {
                if (!finishMap.ContainsKey(finsh.Name))
                {
                    finishMap.Add(finsh.Name, finsh);
                }
            }
            return finishMap;
        }

        private Dictionary<string, Style> GetStyleMap()
        {
            Dictionary<string, Style> styleMap = new Dictionary<string, Style>();
            var styles = Context.Styles.Where(s => s.Name != null).ToList();
            foreach (var style in styles)
            {
                if (!styleMap.ContainsKey(style.Name))
                {
                    styleMap.Add(style.Name, style);
                }
            }
            return styleMap;
        }

        /*public IList<WheelSpec> ImportWheelDataFromFile(Stream fileStream, ImportWheelDataFromFileOptions options = null)
        {
            var localOptions = new ImportWheelDataFromFileOptions
            {
                DiscardChanges = options?.DiscardChanges ?? false,
                FileDataMergeMode = options?.FileDataMergeMode ?? FileDataMergeMode.Overwrite,
            };


            var records = this.GetWheelDataFromFile(fileStream);

            // TODO Validate Import
            if (!records.Any())
            {
                return null;
            }

            var firstRecord = records.FirstOrDefault();

            if (firstRecord == null)
            {
                this.Logger.Warn($"File stream has no data.");

                return new List<WheelSpec>();
            }

            if (string.IsNullOrWhiteSpace(firstRecord.Partnumber) || string.IsNullOrWhiteSpace(firstRecord.Brand) ||
                string.IsNullOrWhiteSpace(firstRecord.Style) || string.IsNullOrWhiteSpace(firstRecord.Finish))
            {
                throw new ArgumentException("Partnumber, Brand, Style and Finish are required");
            }


            var dbContext = VelocityWheelsContext.Create(Context);

            var importContexts = Class.CreateImportContexts(dbContext, records);

            var recordsToImport = (localOptions.FileDataMergeMode == FileDataMergeMode.Overwrite
                ? importContexts
                : importContexts.Where(ctx => ctx.ExistingWheelSpec == null)).ToArray();

            var importedItems = new List<WheelSpec>();

            var batchSize = 0;

            foreach (var context in recordsToImport)
            {
                ++batchSize;

                var brand = context.ExistingBrand ?? context.NewBrands.GetOrCreate(context.Record.Brand, () =>
                    dbContext.Brands.Add(new Brand
                    {
                        Name = context.Record.Brand,
                    }));

                var style = context.ExistingStyle;

                if (style == null)
                {
                    var dictionaryKey = Tuple.Create(brand.Name, context.Record.Style);

                    style = context.NewStyles.GetOrCreate(dictionaryKey, () =>
                        dbContext.Styles.Add(new Style
                        {
                            Brand = brand,
                            Name = context.Record.Style,
                        }));
                }


                var finish = context.ExistingFinish;

                if (finish == null)
                {
                    var dictionaryKey = Tuple.Create(brand.Name, context.Record.Finish);

                    finish = context.NewFinishes.GetOrCreate(dictionaryKey, () =>
                        dbContext.Finishes.Add(new Finish
                        {
                            Brand = brand,
                            Name = context.Record.Finish,
                        }));
                }

                var wheel = context.ExistingWheel;

                if (wheel == null)
                {
                    var dictionaryKey = Tuple.Create(brand.Name, style.Name, finish.Name);

                    wheel = context.NewWheels.GetOrCreate(dictionaryKey, () =>
                        dbContext.Wheels.Add(new Wheel
                        {
                            Brand = brand,
                            Finish = finish,
                            Style = style,
                        }));

                }

                var wheelSpec = context.ExistingWheelSpec ?? dbContext.WheelSpecs.Add(new WheelSpec
                {
                    Wheel = wheel
                });

                importedItems.Add(wheelSpec);

                Class.MapDataOntoModel(context.Record, wheelSpec);

                if (batchSize < 100) { continue; }


                this.SaveModels(dbContext, timestamp: null, activationStatus: true);

                batchSize = 0;
            }

            this.SaveModels(dbContext, timestamp: null, activationStatus: true);

            return importedItems;
        }*/

        public void ImportWheelsThumbnailsFromFile(Stream fileStream, ImportWheelsThumbnailsFromFileOptions options = null)
        {
            var localOptions = new ImportWheelsThumbnailsFromFileOptions
            {
                DiscardChanges = options?.DiscardChanges ?? false,
                FileDataMergeMode = options?.FileDataMergeMode ?? FileDataMergeMode.Overwrite,
                ResizeThumb = options?.ResizeThumb ?? false,
            };


            var records = GetWheelDataFromFile(fileStream);

            var imageService = new ImageService(Context);

            foreach (var specImportData in records)
            {
                var wheels = Context.Wheels.Include("Brand").Include("Style").Include("Finish").
                    Where(w => w.Active && w.Brand.Name == specImportData.Brand && w.Style.Name == specImportData.Style && w.Finish.Name == specImportData.Finish);

                foreach (var wheel in wheels)
                {
                    if (wheel == null) { continue; }

                    wheel.FrontMainImage = specImportData.OnVehicle + "_front.png";
                    wheel.RearMainImage = specImportData.OnVehicle + "_rear.png";

                    if (!string.IsNullOrWhiteSpace(specImportData.Thumbnail))
                    {
                        if (localOptions.ResizeThumb)
                        {
                            var thumb = imageService.CreateWheelThumbnail(specImportData.Thumbnail, wheel.Brand.Name);
                            wheel.ThumbnailImage = thumb;
                        }
                        else
                        {
                            wheel.ThumbnailImage = specImportData.Thumbnail;
                        }
                    }
                    

                    this.SetAuditFields(wheel);
                }


                if (!localOptions.DiscardChanges)
                {
                    Context.SaveChanges();
                }
            }
        }

        public bool SaveOrUpdate(Wheel wheel)
        {
            return Context.SaveChanges() > 0;
        }

        public int UpdateImagesForAllWheels()
        {
            int count = 0;
            var allWheels = Context.Wheels.ToList();
            var neededUpdates = allWheels.Where(w => w.FrontMainImage == null || w.ThumbnailImage == null).ToList();
            foreach (var wheelSpec in neededUpdates)
            {
                if (SetWheelImagesFromLikeWheels(allWheels, wheelSpec))
                {
                    count++;
                }
            }
            int updateCount = Context.SaveChanges();

            return count;
        }

        public void UpdateImagesForWheel(string partNumber)
        {
            var wheel = Context.WheelSpecs.Include("Wheel").FirstOrDefault(w => w.PartNumber == partNumber);
            if (wheel != null)
            {
                SetWheelImagesFromLikeWheels(Context.Wheels, wheel.Wheel);
                Context.SaveChanges();
            }
        }
        #endregion Public Methods


        #region Private Methods
        private static void BuildProductFitmentDictionary(List<ProductFitmentModel> vehicles, Dictionary<string, List<ProductFitmentModel>> fitmentDictionary)
        {
            foreach (var pfModel in vehicles)
            {
                List<ProductFitmentModel> models;
                string key = pfModel.Make.Trim() + ";" + pfModel.Model.Trim();
                if (!fitmentDictionary.TryGetValue(key, out models))
                {
                    models = new List<ProductFitmentModel>();
                    fitmentDictionary.Add(key, models);
                }
                models.Add(pfModel);
            }


            foreach (var pair in fitmentDictionary.ToList())
            {
                var models = pair.Value;
                fitmentDictionary[pair.Key] = models.OrderByDescending(m => m.Year).ToList();
            }
        }

        private static IEnumerable<WheelDataImportContext> CreateImportContexts(
            VelocityWheelsContext dataContext, IEnumerable<SpecImportData> records)
        {
            var localRecords = records.ToArray();

            var allBrandsByName = dataContext.Brands
                .Where(b => b.Name != null)
                .GroupBy(b => b.Name)
                .Select(bg => bg.FirstOrDefault())
                .ToArray()
                .ToDictionary(m => m.Name);

            var allStylesByName = dataContext.Styles
                .Where(s => s.Name != null)
                .GroupBy(b => b.Name)
                .Select(bg => bg.FirstOrDefault())
                .ToArray()
                .ToDictionary(m => m.Name);

            var allFinishesByName = dataContext.Finishes
                .Where(b => b.Name != null)
                .GroupBy(b => b.Name)
                .Select(bg => bg.FirstOrDefault())
                .ToArray()
                .ToDictionary(m => m.Name);

            var allWheelsByNames = (
                from w in dataContext.Wheels
                from b in dataContext.Brands
                where b.Id == w.BrandId
                from s in dataContext.Styles
                where s.Id == w.StyleId
                from f in dataContext.Finishes
                where f.Id == w.FinishId
                select new { BrandName = b.Name, StyleName = s.Name, FinishName = f.Name, Wheel = w }
            ).GroupBy(ns => new { ns.BrandName, ns.StyleName, ns.FinishName })
            .Select(nsg => nsg.FirstOrDefault())
            .ToArray()
            .ToDictionary(w => Tuple.Create(w.BrandName, w.StyleName, w.FinishName), w => w.Wheel);

            var allSpecsByWheel = dataContext.WheelSpecs.ToArray()
                .GroupBy(wsp => wsp.WheelId)
                .ToDictionary(wspg => wspg.Key, wspg => wspg.ToDictionary(wsp => wsp.PartNumber));

            return localRecords.Select(r =>
            {
                var newBrands = new Dictionary<string, Brand>();
                var newStyles = new Dictionary<Tuple<string, string>, Style>();
                var newFinishes = new Dictionary<Tuple<string, string>, Finish>();
                var newWheels = new Dictionary<Tuple<string, string, string>, Wheel>();

                var brand = allBrandsByName.TryGet(r.Brand).Value;
                var style = allStylesByName.TryGet(r.Style).Value;
                var finish = allFinishesByName.TryGet(r.Finish).Value;

                var wheel = (brand == null || style == null || finish == null)
                    ? (Wheel)null
                    : allWheelsByNames.TryGet(Tuple.Create(brand.Name, style.Name, finish.Name)).Value;

                var spec = (wheel == null)
                    ? (WheelSpec)null
                    : allSpecsByWheel[wheel.Id].TryGet(r.Partnumber).Value;

                return new WheelDataImportContext
                {
                    DataContext = dataContext,
                    ExistingBrand = brand,
                    ExistingFinish = finish,
                    ExistingStyle = style,
                    ExistingWheel = wheel,
                    ExistingWheelSpec = spec,
                    Record = r,
                    NewBrands = newBrands,
                    NewFinishes = newFinishes,
                    NewStyles = newStyles,
                    NewWheels = newWheels,
                };
            })
                .ToArray();
        }

        private static IEnumerable<WheelDataImportContext> CreateImportContextsObsolete(
            VelocityWheelsContext dataContext, IEnumerable<SpecImportData> records)
        {
            var localRecords = records.ToArray();

            var wheelSpecRepository = (
                from b in dataContext.Brands
                join s in dataContext.Styles on b.Id equals s.BrandId into sg
                from sv in sg.DefaultIfEmpty()
                join f in dataContext.Finishes on b.Id equals f.BrandId into fg
                from fv in fg.DefaultIfEmpty()
                join w in dataContext.Wheels
                    on new { Brand = b, Style = sv, Finish = fv } equals new { w.Brand, w.Style, w.Finish }
                    into wg
                from wv in wg.DefaultIfEmpty()
                join wsp in dataContext.WheelSpecs
                    on wv.Id equals wsp.WheelId into wspg
                from wspv in wspg.DefaultIfEmpty()
                let data = new { Brand = b, Style = sv, Finish = fv, Wheel = wv, WheelSpec = wspv }
                group data by data.Brand.Name
                into bg
                select new
                {
                    bg.FirstOrDefault().Brand,
                    AllBrandStyles = (
                        from s in bg
                        group s by s.Style.Id
                        into sg
                        select sg.FirstOrDefault().Style
                    ),
                    AllBrandFinishes = (
                        from f in bg
                        group f by f.Finish.Id
                        into fg
                        select fg.FirstOrDefault().Finish
                    ),
                    WheelStyles = (
                        from s in bg
                        where s.WheelSpec != null
                        group s by s.Style.Name
                        into sg
                        select new
                        {
                            sg.FirstOrDefault().Style,
                            WheelFinishes = (
                                from f in sg
                                group f by f.Finish.Name
                                into fg
                                let first = fg.FirstOrDefault()
                                select new
                                {
                                    first.Finish,
                                    first.Wheel,
                                    WheelSpecs = fg.Select(f => f.WheelSpec)
                                }
                            )
                        }
                    ),
                }
            ).ToArray();

            var wheelSpecRepository2 = (
                from b in wheelSpecRepository
                select new
                {
                    b.Brand,
                    AllBrandStyles = b.AllBrandStyles.ToDictionary(s => s.Name),
                    WheelStyles = b.WheelStyles.Select(s => new
                    {
                        s.Style,
                        WheelFinishes = s.WheelFinishes.Select(f => new
                        {
                            f.Finish,
                            f.Wheel,
                            WheelSpecs = f.WheelSpecs.ToDictionary(wsp => wsp.PartNumber)
                        }).ToDictionary(f => f.Finish.Name),
                    }).ToDictionary(s => s.Style.Name),
                    AllBrandFinishes = b.AllBrandFinishes.ToDictionary(f => f.Name),
                }
            ).ToDictionary(be => be.Brand.Name);


            var newBrands = new Dictionary<string, Brand>();
            var newStyles = new Dictionary<Tuple<string, string>, Style>();
            var newFinishes = new Dictionary<Tuple<string, string>, Finish>();
            var newWheels = new Dictionary<Tuple<string, string, string>, Wheel>();

            return localRecords.Select(record =>
            {
                var context = new WheelDataImportContext
                {
                    DataContext = dataContext,
                    Record = record,
                    NewBrands = newBrands,
                    NewFinishes = newFinishes,
                    NewStyles = newStyles,
                    NewWheels = newWheels,
                };

                if (!wheelSpecRepository2.ContainsKey(record.Brand)) { return context; }


                var brandData = wheelSpecRepository2[record.Brand];
                context.ExistingBrand = brandData.Brand;


                context.ExistingStyle = brandData.AllBrandStyles.TryGet(record.Style).Value;
                context.ExistingFinish = brandData.AllBrandFinishes.TryGet(record.Finish).Value;

                var wheelData = brandData.WheelStyles.TryGet(record.Style)
                    .Value?.WheelFinishes.TryGet(record.Finish)
                    .Value;

                context.ExistingWheel = wheelData?.Wheel;
                context.ExistingWheelSpec = wheelData?.WheelSpecs.TryGet(record.Partnumber).Value;


                return context;
            }).ToList();
        }

        private Wheel GetCommonWheelWithImage(IEnumerable<Wheel> wheels, Wheel wheel)
        {
            return wheels.FirstOrDefault(
                w =>
                    w.StyleId == wheel.StyleId && w.FinishId == wheel.FinishId &&
                    w.Id != wheel.Id && w.FrontMainImage != null);
        }

        public static string GetFaceImageFilePath(string brand, string image)
        {
            if (string.IsNullOrWhiteSpace(image))
            {
                return null;
            }

            var baseUrl = VelocityWheelsSettings.GetApiBaseUrl;
            var filePath = $"{baseUrl}Content/wheel-images/{brand}/{image}";
            filePath = HttpUtility.UrlPathEncode(filePath);
            return filePath;
        }

        public static string GetFileSystemFaceImagePath(string brand, string image)
        {
            if (string.IsNullOrWhiteSpace(image))
            {
                return null;
            }

            var baseUrl = VelocityWheelsSettings.LocalWheelContentPath;
            var dir = $"{baseUrl}{brand}";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            var filePath = $"{baseUrl}{brand}\\{image}";
            return filePath;
        }

        public static string GetImageFilePath(string brand, string image)
        {
            if (string.IsNullOrWhiteSpace(image))
            {
                return null;
            }

            var baseUrl = VelocityWheelsSettings.GetApiBaseUrl;
            var filePath = $"{baseUrl}Content/wheel-images/thumbnails/{brand}/{image}";
            filePath = HttpUtility.UrlPathEncode(filePath);
            return filePath;
        }

        public static string GetLayerImagePath(string brand, string image)
        {
            if (string.IsNullOrWhiteSpace(image))
            {
                return null;
            }

            var baseUrl = VelocityWheelsSettings.GetApiBaseUrl;
            var filePath = $"{baseUrl}Content/wheel-images/layers/{brand}/{image}";
            filePath = HttpUtility.UrlPathEncode(filePath);
            return filePath;
        }

        private Brand GetOrCreateBrandFromImport(Dictionary<string, Brand> brandMap, SpecImportData specImportData)
        {
            Brand brand;
            if (!brandMap.TryGetValue(specImportData.Brand, out brand))
            {
                brand = new Brand()
                {
                    Name = specImportData.Brand.Trim(),
                    Active = true,
                    Code = specImportData.Brand.Replace(' ', '_'),
                    LastUpdatedBy = GetLoggedInUserName(),
                    LastUpdated = DateTime.UtcNow
                };
                Context.Brands.Add(brand);
                brandMap.Add(brand.Name, brand);
            }
            return brand;
        }

        private Finish GetOrCreateFinishFromImport(Dictionary<string, Brand> brandMap, Dictionary<string, Finish> finishMap, SpecImportData specImportData)
        {
            Finish finish;
            if (!finishMap.TryGetValue(specImportData.Finish, out finish))
            {
                Brand brand;
                brandMap.TryGetValue(specImportData.Brand, out brand);
                if (string.IsNullOrWhiteSpace(specImportData.Finish))
                {
                    throw new Exception($"Finish blank for part number {specImportData.Partnumber}, please provide a value");
                }
                finish = new Finish()
                {
                    Name = specImportData.Finish.Trim(),
                    ShortName = specImportData.ShortFinish?.Trim(),
                    Active = true,
                    LastUpdatedBy = GetLoggedInUserName(),
                    LastUpdated = DateTime.UtcNow,
                    Brand = brand
                };
                Context.Finishes.Add(finish);
                finishMap.Add(finish.Name, finish);
            }
            return finish;
        }

        private Style GetOrCreateStyleFromImport(Dictionary<string, Brand> brandMap, Dictionary<string, Style> styleMap, SpecImportData specImportData)
        {
            Style style;
            if (!styleMap.TryGetValue(specImportData.Style, out style))
            {
                Brand brand;
                brandMap.TryGetValue(specImportData.Brand, out brand);
                style = new Style()
                {
                    Name = specImportData.Style.Trim(),
                    Active = true,
                    LastUpdatedBy = GetLoggedInUserName(),
                    LastUpdated = DateTime.UtcNow,
                    Brand = brand
                };
                Context.Styles.Add(style);
                styleMap.Add(style.Name, style);
            }
            return style;
        }

        private List<SpecImportData> GetWheelData(Stream fileStream)
        {
            List<SpecImportData> records = null;
            using (var stream = new StreamReader(fileStream))
            {
                var reader = new CsvReader(stream);
                reader.Configuration.WillThrowOnMissingField = false;
                reader.Configuration.IsHeaderCaseSensitive = false;
                records = reader.GetRecords<SpecImportData>().ToList();
                records.ForEach(r =>
                {
                    r.Brand = r.Brand.Trim();
                    r.Style = r.Style.Trim();
                    r.Finish = r.Finish.Trim();
                });
            }
            return records;
        }

        

        private List<SpecImportData> GetWheelDataFromFile(Stream fileStream)
        {
            List<SpecImportData> records = null;
            using (var stream = new StreamReader(fileStream))
            {
                var reader = new CsvReader(stream);
                reader.Configuration.WillThrowOnMissingField = false;
                reader.Configuration.IsHeaderCaseSensitive = false;
                records = reader.GetRecords<SpecImportData>().ToList();
            }
            return records;
        }

        private List<ProductFitmentModel> GetVehicleFitmentModels(List<long?> fitments)
        {
            var vehicles = Context.Classifications.Where(c => fitments.Contains(c.Id)).
                Select(c => new ProductFitmentModel()
                {
                    Make = c.Make,
                    Model = c.Model,
                    BodyType = c.BodyType,
                    SubModel = c.SubModel,
                    Year = c.Year,
                    ChassisId = c.Id,
                    Region = c.Region,
                    RegionId = c.RegionId,
                    ClassificationId = c.Id
                }).
                OrderByDescending(c => c.ChassisId).
                ThenByDescending(c => c.Year).
                ThenBy(c => c.Make).
                ThenBy(c => c.Model).
                ThenBy(c => c.RegionId).
                ThenBy(c => c.BodyType).
                ThenBy(c => c.SubModel).
                ToList();
            return vehicles;
        }


        // Populates the wheel spec based on the keys. 
        private static void MapDataOntoModel(SpecImportData record, WheelSpec wheelSpec)
        {
            wheelSpec.PartNumber = record.Partnumber;

            wheelSpec.ProductDescription = record.PartnumberDescription;
            wheelSpec.BoltPattern1 = record.BoltPattern1;
            wheelSpec.BoltPattern2 = record.BoltPattern2;
            wheelSpec.BoltPattern3 = record.BoltPattern3;
            wheelSpec.BoltPattern4 = record.BoltPattern4;
            wheelSpec.CapNumber = record.Cap;
            wheelSpec.LipSize = record.Lip;
            wheelSpec.Offset = record.Offset;

            MapIfParsed(record.Bore, v => wheelSpec.Bore = v);
            MapIfParsed(record.Diameter, v => wheelSpec.Diameter = v);
            MapIfParsed(record.Msrp, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, v => wheelSpec.ProductMSRP = v);
            MapIfParsed(record.Weight, v => wheelSpec.Weight = v);
            MapIfParsed(record.Width, v => wheelSpec.Width = v);
            MapIfParsed(record.LoadRating, v => wheelSpec.LoadRating = v);

            if (!MapIfParsed(record.Backspacing, v => wheelSpec.BSM = v))
            {
                MapIfParsed(record.Offset, v => wheelSpec.BSM = (wheelSpec.Width + 1) / 2 + (v / 25.4m));
            }

            if (wheelSpec.Wheel == null)
            {
                wheelSpec.Wheel = new Wheel();
            }
        }


        private static void MapLayers(WheelModel model, Wheel wheel)
        {
            model.WheelLayers = new List<WheelLayerModel>();
            foreach (var layer in wheel.WheelLayers)
            {
                var newLayer = new WheelLayerModel()
                {
                    Id = layer.Id,
                    WheelId = layer.WheelId,
                    LevelName = layer.LevelName,
                    BaseLayerImage = layer.BaseLayerImage,
                    FrontLayerImage = layer.FrontLayerImage,
                    RearLayerImage = layer.RearLayerImage,
                    Level = layer.Level
                };

                model.WheelLayers.Add(newLayer);
                var filePath = GetLayerImagePath(model.Brand, newLayer.BaseLayerImage);
                newLayer.BaseLayerImage = filePath;
                filePath = GetLayerImagePath(model.Brand, newLayer.FrontLayerImage);
                newLayer.FrontLayerImage = filePath;
                filePath = GetLayerImagePath(model.Brand, newLayer.RearLayerImage);
                newLayer.RearLayerImage = filePath;
            }
        }

        private bool SetWheelImagesFromLikeWheels(IEnumerable<Wheel> wheels, Wheel wheel)
        {
            var commonWheel = GetCommonWheelWithImage(wheels, wheel);
            if (commonWheel != null)
            {
                wheel.FrontMainImage = commonWheel.FrontMainImage ?? wheel.FrontMainImage;
                wheel.RearMainImage = commonWheel.RearMainImage ?? wheel.RearMainImage;
                wheel.ThumbnailImage = commonWheel.ThumbnailImage ?? wheel.ThumbnailImage;
                return true;
            }

            return false;
        }
        #endregion Private Methods
    }
}
