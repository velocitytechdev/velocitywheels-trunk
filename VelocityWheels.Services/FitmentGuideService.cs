﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CsvHelper;
using VelocityWheels.Data.Enum;
using VelocityWheels.Data.Models;
using VelocityWheels.ExternalServices.Models;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    public class FitmentGuideService : ServiceBase
    {
        private readonly Object _lockObj = new Object();

        private static readonly long VTChassisIdBaseId = 990000000;
        //private static readonly long VTChassisIdBaseDaiId = 901000000;

        public FitmentGuideService() : this(null)
        {
        }

        public FitmentGuideService(VelocityWheelsContext context) : base(context)
        {
            Logger = AppLogger.GetLogger(this);
        }

        private List<T> GetFitmentGuide<T>(Stream fileStream)
        {
            List<T> records = null;
            using (var stream = new StreamReader(fileStream))
            {
                var reader = new CsvReader(stream);
                reader.Configuration.WillThrowOnMissingField = false;
                reader.Configuration.IsHeaderCaseSensitive = false;
                records = reader.GetRecords<T>().ToList();
            }
            return records;
        }

        private static void MapDataOntoModel(FitmentGuideImport record, FitmentGuide fitmentGuide)
        {
            fitmentGuide.Make = record.Make;
            fitmentGuide.Model = record.Model;
            fitmentGuide.BodyStyle = record.BodyStyle;
            fitmentGuide.Submodel = record.Submodel;
            fitmentGuide.BoltMetric = record.BoltMetric;
            fitmentGuide.BoltStandard = record.BoltStandard;
            fitmentGuide.Brakes = record.Brakes;
            fitmentGuide.Suspension = record.Suspension;

            MapIfParsedInt(record.YearFrom, v => fitmentGuide.YearFrom = v);
            MapIfParsedInt(record.YearTo, v => fitmentGuide.YearTo = v);
            MapIfParsedLong(record.MinDia, v => fitmentGuide.MinDia = v);
            MapIfParsedLong(record.MaxDia, v => fitmentGuide.MaxDia = v);
            MapIfParsed(record.MinWidth, v => fitmentGuide.MinWidth = v);
            MapIfParsed(record.MaxWidth, v => fitmentGuide.MaxWidth = v);
            MapIfParsedLong(record.OffsetMax, v => fitmentGuide.OffsetMax = v);
            MapIfParsedLong(record.OffsetMin, v => fitmentGuide.OffsetMin = v);
        }

        public IList<FitmentGuide> UpdateFitmentGuide(Stream fileStream)
        {
            Logger.Info($"Updating fitment guide starting ...");
            var records = GetFitmentGuide<FitmentGuideImport>(fileStream);
            // Validate Import
            if (!records.Any())
            {
                Logger.Warn($"File stream has no data.");
                return null;
            }
            var testrecord = records.FirstOrDefault(f => !string.IsNullOrWhiteSpace(f.YearFrom) && 
            !string.IsNullOrWhiteSpace(f.YearTo) && !string.IsNullOrWhiteSpace(f.Make) && !string.IsNullOrWhiteSpace(f.Model));
            if (testrecord == null)
            {
                throw new ArgumentException("YearFrom, YearTo, Make and Model are required");
            }

            // clear out duplicates
            List<FitmentGuide> toDelete = new List<FitmentGuide>();
            Dictionary<string, FitmentGuide> guideMap = new Dictionary<string, FitmentGuide>();
            var guides = Context.FitmentGuides.OrderBy(f => f.Id).ToList();
            foreach (var fitmentGuide in guides)
            {
                string key = fitmentGuide.YearFrom + ";" + fitmentGuide.YearTo + ";" +
                             fitmentGuide.Make.ToLower().Trim() + ";" +
                             fitmentGuide.Model.ToLower().Trim() + ";" + fitmentGuide.BodyStyle?.ToLower().Trim() + ";" +
                             fitmentGuide.Submodel?.ToLower().Trim() + ";" + fitmentGuide.Brakes?.ToLower().Trim() + ";" +
                             fitmentGuide.Suspension?.ToLower().Trim() + ";" + fitmentGuide.BoltMetric?.ToLower().Trim() + ";" +
                             fitmentGuide.BoltStandard?.ToLower().Trim() + ";" + fitmentGuide.MinWidth + ";" +
                             fitmentGuide.MaxWidth + ";" + fitmentGuide.MinDia + ";" + fitmentGuide.MaxDia + ";" +
                             fitmentGuide.OffsetMin + ";" + fitmentGuide.OffsetMax;
                FitmentGuide oldGuide;
                // replace old guide
                if (guideMap.TryGetValue(key, out oldGuide))
                {
                    toDelete.Add(oldGuide);
                }
                guideMap[key] = fitmentGuide;
            }

            if (toDelete.Any())
            {
                Logger.Info($"ImportFitmentGuide deleting old duplicates " + toDelete.Count);
                Context.FitmentGuides.RemoveRange(toDelete);
                Context.SaveChanges();
            }

            toDelete.Clear();

            Dictionary<string, FitmentGuide> newGuideMap = new Dictionary<string, FitmentGuide>();
            foreach (var record in records.Where(r => !string.IsNullOrWhiteSpace(r.YearFrom) && !string.IsNullOrWhiteSpace(r.YearTo)))
            {
                string key = record.YearFrom + ";" + record.YearTo + ";" + record.Make.ToLower().Trim() + ";" +
                             record.Model.ToLower().Trim() + ";" + record.BodyStyle?.ToLower().Trim() + ";" +
                             record.Submodel?.ToLower().Trim() + ";" + record.Brakes?.ToLower().Trim() + ";" +
                             record.Suspension?.ToLower().Trim() + ";" + record.BoltMetric?.ToLower().Trim() + ";" +
                             record.BoltStandard?.ToLower().Trim() + ";" + record.MinWidth?.ToLower().Trim() + ";" +
                             record.MaxWidth?.ToLower().Trim() + ";" + record.MinDia?.ToLower().Trim() + ";" +
                             record.MaxDia?.ToLower().Trim() + ";" + record.OffsetMin?.ToLower().Trim() + ";" +
                             record.OffsetMax?.ToLower().Trim();
                FitmentGuide oldGuide;
                // replace old guide
                if (guideMap.TryGetValue(key, out oldGuide))
                {
                    toDelete.Add(oldGuide);
                    guideMap.Remove(key);
                }

                // skip blank rows
                if (string.IsNullOrWhiteSpace(record.YearFrom) || string.IsNullOrWhiteSpace(record.Make) ||
                    string.IsNullOrWhiteSpace(record.Model))
                {
                    continue;
                }

                FitmentGuide newGuide;
                if (!newGuideMap.TryGetValue(key, out newGuide))
                {
                    FitmentGuide guide = new FitmentGuide();
                    MapDataOntoModel(record, guide);
                    newGuideMap[key] = guide;
                }
            }

            

            if (toDelete.Any())
            {
                Logger.Info($"ImportFitmentGuide deleting duplicates based on newly loaded items " + toDelete.Count);
                Context.FitmentGuides.RemoveRange(toDelete);
                Context.SaveChanges();
            }

            Logger.Info($"ImportFitmentGuide saving new records ..." + newGuideMap.Count);

            Context.FitmentGuides.AddRange(newGuideMap.Values);
            Context.SaveChanges();

            ClearOutExistingFitments();

            Logger.Info($"Updating fitment guide complete");

            return newGuideMap.Values.ToList();
        }

        public IList<FitmentGuide> ImportFitmentGuide(Stream fileStream)
        {
            Logger.Info($"ImportFitmentGuide starting ...");
            var records = GetFitmentGuide<FitmentGuideImport>(fileStream);

            if (!records.Any())
            {
                Logger.Warn($"File stream has no data.");
                return null;
            }

            var testrecord = records.FirstOrDefault();
            if (string.IsNullOrWhiteSpace(testrecord.YearFrom) || string.IsNullOrWhiteSpace(testrecord.YearTo) ||
                string.IsNullOrWhiteSpace(testrecord.Make) || string.IsNullOrWhiteSpace(testrecord.Model))
            {
                throw new ArgumentException("YearFrom, YearTo, Make and Model are required");
            }

            // delete old records
            var oldRecords = Context.FitmentGuides;
            Context.FitmentGuides.RemoveRange(oldRecords);

            Dictionary<string, FitmentGuide> newGuideMap = new Dictionary<string, FitmentGuide>();
            foreach (var record in records)
            {
                // skip blank rows
                if (string.IsNullOrWhiteSpace(record.YearFrom) || string.IsNullOrWhiteSpace(record.Make) ||
                    string.IsNullOrWhiteSpace(record.Model))
                {
                    continue;
                }

                string key = record.YearFrom + ";" + record.YearTo + ";" + record.Make.ToLower().Trim() + ";" +
                             record.Model.ToLower().Trim() + ";" + record.BodyStyle?.ToLower().Trim() + ";" +
                             record.Submodel?.ToLower().Trim() + ";" + record.Brakes?.ToLower().Trim() + ";" +
                             record.Suspension?.ToLower().Trim() + ";" + record.BoltMetric?.ToLower().Trim() + ";" +
                             record.BoltStandard?.ToLower().Trim() + ";" + record.MinWidth?.ToLower().Trim() + ";" +
                             record.MaxWidth?.ToLower().Trim() + ";" + record.MinDia?.ToLower().Trim() + ";" +
                             record.MaxDia?.ToLower().Trim() + ";" + record.OffsetMin?.ToLower().Trim() + ";" +
                             record.OffsetMax?.ToLower().Trim();

                FitmentGuide newGuide;
                if (!newGuideMap.TryGetValue(key, out newGuide))
                {
                    FitmentGuide guide = new FitmentGuide();
                    MapDataOntoModel(record, guide);
                    newGuideMap[key] = guide;
                }
            }

            Logger.Info($"ImportFitmentGuide saving new records ..." + newGuideMap.Count);

            Context.FitmentGuides.AddRange(newGuideMap.Values);
            Context.SaveChanges();

            //ClearOutExistingFitments();

            Logger.Info($"ImportFitmentGuide finished");

            return newGuideMap.Values.ToList();
        }

        private void ClearOutExistingFitments()
        {
            Logger.Info($"ImportFitmentGuide cleaning up existing data");
            var oldFitments = Context.Fitments.Where(f => f.ApiId == (int) ApiIdEnum.Aces);
            Context.Fitments.RemoveRange(oldFitments);
            Context.SaveChanges();

            var otherFitments = Context.Fitments.Where(f => f.ApiId != (int)ApiIdEnum.Aces && f.ClassificationId != null);
            foreach (var otherFitment in otherFitments)
            {
                otherFitment.ClassificationId = null;
            }

            Context.SaveChanges();

            /*var aces = Context.Classifications.Where(f => f.ApiId == (int) ApiIdEnum.Aces).ToList();
            aces.ForEach(a =>
            {
                if (a.ChassisId != null)
                {
                    a.ChassisId = null;
                }
            });
            Context.SaveChanges();*/
        }

        /*public void BuildAcesVehicleIdForBrand(int? vehicleId = null)
        {
            using (VelocityWheelsContext context = CreateNewContext())
            {
                Logger.Info($"BuildAcesVehicleIdForBrand starting ...");
                List<Fitment> fitments = context.Fitments.Where(f => f.ApiId != (int)ApiIdEnum.Aces && f.ApiId != (int)ApiIdEnum.FuelApi && f.AcesVehicleId == null).ToList();
                if (!fitments.Any())
                {
                    Logger.Info($"BuildAcesVehicleIdForBrand, no fitments found that need processing");
                    return;
                }

                fitments = context.Fitments.Where(f => f.ApiId != (int)ApiIdEnum.Aces && f.ApiId != (int)ApiIdEnum.FuelApi).ToList();

                List<Classification> aces = !vehicleId.HasValue ? context.Classifications.Where(c => c.ApiId == (int)ApiIdEnum.Aces).ToList() :
                    context.Classifications.Where(c => c.ApiId == (int)ApiIdEnum.Aces && c.AcesVehicleId == vehicleId).ToList();

                var acesFitments = context.Fitments.Where(f => f.ApiId == (int)ApiIdEnum.Aces).ToList();
                long baseId = acesFitments.Max(a => a.ChassisId) ?? VTChassisIdBaseId;

                foreach (Classification classification in aces)
                {
                    classification.Model = classification.Model?.Trim();
                    classification.Make = classification.Make?.Trim();
                    classification.BodyType = classification.BodyType?.Trim();
                    classification.SubModel = classification.SubModel?.Trim();
                }

                List<Classification> dr = context.Classifications.Where(c => c.ApiId == (int)ApiIdEnum.DriveRight).ToList();
                foreach (Classification classification in dr)
                {
                    classification.Model = classification.Model?.Trim();
                    classification.Make = classification.Make?.Trim();
                    classification.BodyType = classification.BodyType?.Trim();
                    classification.SubModel = classification.SubModel?.Trim();
                }

                // cleanup old data
                Logger.Info($"BuildAcesVehicleIdForBrand cleaning up duplicates ...");
                Dictionary<string, Fitment> oldFitments = new Dictionary<string, Fitment>();
                List<Fitment> toRemove = new List<Fitment>();
                foreach (Fitment fitment in fitments)
                {
                    string key = fitment.ChassisId + ";" + fitment.PartNumber.ToLower().Trim();
                    if (!oldFitments.ContainsKey(key))
                    {
                        oldFitments.Add(key, fitment);
                        if (fitment.AcesVehicleId.HasValue)
                        {   // clear out old values to remap correctly.
                            fitment.AcesVehicleId = null;
                        }
                    }
                    else
                    {
                        toRemove.Add(fitment);
                    }
                }

                context.Fitments.RemoveRange(toRemove);
                context.SaveChanges();

                toRemove.Clear();
                oldFitments.Clear();

                Logger.Info($"BuildAcesVehicleIdForBrand buiding maps ...");

                Dictionary<long, List<Fitment>> fitmentDictionary = new Dictionary<long, List<Fitment>>();

                foreach (Fitment fitment in fitments)
                {
                    List<Fitment> fits;
                    if (!fitmentDictionary.TryGetValue(fitment.ChassisId.GetValueOrDefault(), out fits))
                    {
                        fits = new List<Fitment>();
                        fitmentDictionary.Add(fitment.ChassisId.GetValueOrDefault(), fits);
                    }
                    fits.Add(fitment);
                }

                Dictionary<string, Classification> acesDictionary = new Dictionary<string, Classification>();
                foreach (Classification c in aces)
                {
                    string key = c.Year + c.Make.ToLower() + c.Model.ToLower() + c.SubModel.ToLower() +
                                 c.BodyType?.ToLower() + c.RegionId;

                    acesDictionary[key] = c;
                }

                Logger.Info($"BuildAcesVehicleIdForBrand gathered data, processing ...");

                int batch = 0;
                int total = 0;
                Parallel.ForEach(dr, v =>
                {
                    Classification match;
                    string key = v.Year + v.Make.ToLower() + v.Model.ToLower() + v.SubModel.ToLower() + v.BodyType?.ToLower() + v.RegionId;
                    if (!acesDictionary.TryGetValue(key, out match))
                    {
                        return;
                    }

                    if (match != null)
                    {
                        /*if (v.ChassisId == null)
                        {
                            return;
                        }#1#

                        // Multiple threads may be trying to modify the same chassis, lock
                        lock (_lockObj)
                        {
                            /*long fgChassis;
                            long.TryParse(match.ChassisId, out fgChassis);#1#
                            List<Fitment> parts;
                            if (fitmentDictionary.TryGetValue(long.Parse(v.ChassisId), out parts))
                            {
                                foreach (Fitment fitment in parts)
                                {
                                    if (!fitment.AcesVehicleId.HasValue)
                                    {
                                        if (string.IsNullOrWhiteSpace(match.ChassisId))
                                        {
                                            //long newId = VTChassisIdBaseDaiId + fitment.ChassisId.GetValueOrDefault();
                                            baseId++;
                                            long newId = baseId;
                                            if (match.ChassisId != null)
                                            {
                                                Logger.Info($"Changing {match.ChassisId} to {newId}");
                                            }
                                            match.ChassisId = newId + string.Empty;
                                            fgChassis = newId;
                                        }

                                        fitment.AcesVehicleId = fgChassis;
                                        batch++;
                                        total++;
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrWhiteSpace(match.ChassisId))
                                        {
                                            continue;
                                        }
                                        if (match.ChassisId != null)
                                        {
                                            Logger.Info($"Changing {match.ChassisId} to {fitment.AcesVehicleId}");
                                        }
                                        match.ChassisId = fitment.AcesVehicleId + string.Empty;
                                        batch++;
                                        total++;
                                    }
                                }
                            }

                        }

                    }

                    // Multiple threads may be to modify the data, lock until save complete
                    lock (_lockObj)
                    {
                        if (batch >= 1000)
                        {
                            Logger.Info($"BuildAcesVehicleIdForBrand - updated {total}");
                            context.SaveChanges();
                            batch = 0;
                        }
                    }

                });

                context.SaveChanges();

                Logger.Info($"BuildAcesVehicleIdForBrand Complete");
            }
        }*/

        private void DeleteAcesDuplicates()
        {
            Logger.Info($"Deleting duplicate Aces");
            List<Classification> toDelete = new List<Classification>();
            Dictionary<string, Classification> acesMap = new Dictionary<string, Classification>();
            var aces = Context.Classifications.Where(c => c.ApiId == (int)ApiIdEnum.Aces).ToList();
            foreach (var c in aces)
            {
                string key = c.Year + ";" + c.Make.ToLower().Trim() + ";" + c.Model.ToLower().Trim() + ";" + c.SubModel.ToLower().Trim() + ";" + c.BodyType?.ToLower().Trim() + ";" + c.RegionId;
                Classification current;
                if (acesMap.TryGetValue(key, out current))
                {
                    toDelete.Add(c);
                }
                else
                {
                    acesMap[key] = c;
                }
            }

            if (toDelete.Any())
            {
                Logger.Info($"Deleting duplicate Aces - deleting {toDelete.Count}");
                Context.Classifications.RemoveRange(toDelete);
            }
        }

        public int BuildFromFitmentGuideData(long? brandId, int? vehicleId, out string error)
        {
            // clean out duplicates from ACES data
            //DeleteAcesDuplicates();

            error = null;

            Logger.Info($"BuildFromFitmentGuideData deleting current fitments for brand {brandId}...");
            ClearBrandFitments(brandId);

            string forMake = string.Empty;
            string forModel = string.Empty;

            Logger.Info($"BuildFromFitmentGuideData starting for brand {brandId}...");

            using (VelocityWheelsContext context = CreateNewContext())
            {
                context.Configuration.AutoDetectChangesEnabled = true;

                Dictionary<long, List<Classification>> acesIdDictionary;
                var acesDictionary = BuildAcesDictionary(context, vehicleId, out acesIdDictionary);
                var regions = context.Classifications.GroupBy(c => c.RegionId).Select(g => g.Key).ToList();                

                Dictionary<string, Fitment> currentFitments = null;
                
               // if (!vehicleId.HasValue)
                {
                    List<FitmentGuide> fitmentGuides = null;
                    if (!vehicleId.HasValue)
                    {
                        //currentFitments = context.Fitments.Where(f => f.ApiId == (int) ApiIdEnum.Aces).ToDictionary(f => f.ChassisId.GetValueOrDefault() + f.PartNumber);
                        fitmentGuides = context.FitmentGuides.ToList();
                    }
                    else
                    {
                        /*chassisList = context.Database.SqlQuery<FitmentGuide>(
                        $"select BaseVehicleID, ch.ChassisId as ChassisId, BoltPattern, Hubbore, OffsetMin, OffsetMax, Diameter, Width " +
                        $"from FGChassis ch, FGMinusSizes m, FGVehicles v " +
                        $"where ch.chassisid = m.chassisid and ch.chassisid = v.FG_ChassisID and v.BaseVehicleID = " + vehicleId +
                        $" group by BaseVehicleID, ch.ChassisID, BoltPattern, Hubbore, OffsetMin, OffsetMax, Diameter, Width").
                        ToList();*/

                        var firstVehicle = acesDictionary.Select(a => a.Value.FirstOrDefault()).FirstOrDefault();
                        forMake = firstVehicle.Make;
                        forModel = firstVehicle.Model;

                        fitmentGuides = context.FitmentGuides.Where(f => f.Make == firstVehicle.Make && f.Model == firstVehicle.Model).ToList();
    
                        FitmentGuide vehicle = fitmentGuides.FirstOrDefault();
    
                        /*IQueryable<Fitment> oldList = context.Fitments.Where(f => f.ApiId == (int)ApiIdEnum.Aces && f.ChassisId == vehicle.ChassisId);                        
                        context.Fitments.RemoveRange(oldList);
                        context.SaveChanges();*/

                        currentFitments = context.Fitments.Where(f => f.ApiId == (int)ApiIdEnum.Aces).ToDictionary(f => f.ChassisId.GetValueOrDefault() + f.PartNumber);
                    }


                    /*List<string> linkedParts =
                        context.Fitments.Where(f => f.ApiId == (int) ApiIdEnum.DriveRight)
                            .Select(f => f.PartNumber)
                            .Distinct()
                            .ToList();*/

                    IQueryable<WheelSpec> allParts = context.WheelSpecs.Include("Wheel").Include("Wheel.Brand").
                        Where(
                            w =>
                                (w.BoltPattern1 != null || w.BoltPattern2 != null || w.BoltPattern3 != null || w.BoltPattern4 != null) &&
                                w.Bore != null);

                    /*List<long?> processedBrands = context.WheelSpecs.Include("Wheel").Include("Wheel.Brand").
                        Where(w => linkedParts.Contains(w.PartNumber)).Select(w => w.Wheel.BrandId).Distinct().ToList();*/

                    /*List<WheelSpec> allWheels =
                        allParts.Where(
                                w => /*!linkedParts.Contains(w.PartNumber) && #1#!processedBrands.Contains(w.Wheel.BrandId))
                            .ToList();*/

                    List<WheelSpec> wheels = allParts.Where(p => p.Wheel.BrandId == brandId).ToList();


                    int batch = 0;
                    int total = 0;
                    int wheel = 0;
                    long baseId = VTChassisIdBaseId;
                    /*if (currentFitments.Any())
                    {
                        baseId = currentFitments.Max(c => c.Value.ChassisId.GetValueOrDefault());
                    }*/

                    var addedFitments = new Dictionary<string, Fitment>();

                    if (wheels.Count == 0)
                    {
                        error = $"No wheels matching basic criteria (bolt patterns and bore not null) were found in brand {brandId}";
                        Logger.Error(error);
                        return 0;
                    }

                    Logger.Info($"BuildFromFitmentGuideData, gathered data, {wheels.Count} parts in brand {wheels.First().Wheel.Brand.Name}, {fitmentGuides.Count} guides");

                    Parallel.ForEach(wheels, w =>
                    {
                        int processed = ProcessFitmentForWheel(w, fitmentGuides, ref wheel, currentFitments, addedFitments,
                            acesDictionary, acesIdDictionary, regions, ref baseId, forMake, forModel);
    
                        lock (_lockObj)
                        {
                            total += processed;
                            if (wheel % 1000 == 0)
                            {
                                Logger.Info($"BuildFromFitmentGuideData - found {total} matches with {addedFitments.Count} new matches from {wheel} parts");
                            }
                        }
                    });

                    // passing in a vehicle ... test only
                    //if (!vehicleId.HasValue)
                    {
                        // save Classification updates
                        context.SaveChanges();

                        context.Configuration.AutoDetectChangesEnabled = false;
                        // Save new additions
                        Logger.Info($"BuildFromFitmentGuideData, saving {addedFitments.Count} matches");
                        batch = 0;
                        int saved = 0;
                        List<Fitment> toSave = new List<Fitment>();
                        foreach (Fitment newFitment in addedFitments.Select(a => a.Value).ToList())
                        {                            
                            toSave.Add(newFitment);
                            batch++;
                            saved++;
                            if (batch >= 20000)
                            {
                                context.Fitments.AddRange(toSave);
                                context.SaveChanges();
                                batch = 0;
                                toSave.Clear();
                            }

                            if (saved % 20000 == 0)
                            {
                                Logger.Info($"BuildFromFitmentGuideData, saving batch {saved}");
                            }
                        }

                        context.Fitments.AddRange(toSave);
                        context.SaveChanges();
                    }

                    
                    Logger.Info($"BuildFromFitmentGuideData complete - found {addedFitments.Count} matches");

                    context.Configuration.AutoDetectChangesEnabled = true;

                    return addedFitments.Count;
                }
            }
        }

        private void ClearBrandFitments(long? brandId)
        {
            using (VelocityWheelsContext context = CreateNewContext())
            {
                var brandFitments = context.Fitments.Where(f => f.ApiId == (int) ApiIdEnum.Aces && f.BrandId == brandId);
                //Logger.Info($"BuildFromFitmentGuideData complete - deleting {brandFitments.Count}");
                context.Fitments.RemoveRange(brandFitments);
                context.SaveChanges();
            }
        }


        private Dictionary<string, List<Classification>> BuildAcesDictionary(VelocityWheelsContext context, int? vehicleId, out Dictionary<long, List<Classification>> acDictionary)
        {
            List<Classification> aces = !vehicleId.HasValue
                ? context.Classifications.Where(c => c.ApiId == (int) ApiIdEnum.Aces).ToList()
                : context.Classifications.Where(
                    c => c.ApiId == (int) ApiIdEnum.Aces && c.AcesVehicleId == vehicleId).ToList();

            var acesDictionary = new Dictionary<string, List<Classification>>();
            var acesIdDictionary = new Dictionary<long, List<Classification>>();
            foreach (Classification c in aces)
            {
                string key1 = c.Year + ";" + c.Make.ToLower().Trim() + ";" + c.Model.ToLower().Trim() + ";" + c.SubModel.ToLower().Trim() + ";" + c.BodyType?.ToLower().Trim() + ";" + c.RegionId;
                string key2 = c.Year + ";" + c.Make.ToLower().Trim() + ";" + c.Model.ToLower().Trim() + ";" + "null" + ";" + c.BodyType?.ToLower().Trim() + ";" + c.RegionId;
                string key3 = c.Year + ";" + c.Make.ToLower().Trim() + ";" + c.Model.ToLower().Trim() + ";" + c.SubModel.ToLower().Trim() + ";" + "null" + ";" + c.RegionId;
                string key4 = c.Year + ";" + c.Make.ToLower().Trim() + ";" + c.Model.ToLower().Trim() + ";" + "null" + ";" + "null" + ";" + c.RegionId;

                List<Classification> clist1;
                if (!acesDictionary.TryGetValue(key1, out clist1))
                {
                    clist1 = new List<Classification>() {c};
                    acesDictionary.Add(key1, clist1);
                }
                else
                {
                    clist1.Add(c);
                }

                List<Classification> clist2;
                if (!acesDictionary.TryGetValue(key2, out clist2))
                {
                    clist2 = new List<Classification>() { c };
                    acesDictionary.Add(key2, clist2);
                }
                else
                {
                    clist2.Add(c);
                }

                List<Classification> clist3;
                if (!acesDictionary.TryGetValue(key3, out clist3))
                {
                    clist3 = new List<Classification>() { c };
                    acesDictionary.Add(key3, clist3);
                }
                else
                {
                    clist3.Add(c);
                }

                List<Classification> clist4;
                if (!acesDictionary.TryGetValue(key4, out clist4))
                {
                    clist4 = new List<Classification>() { c };
                    acesDictionary.Add(key4, clist4);
                }
                else
                {
                    clist4.Add(c);
                }

                /*acesDictionary[key1] = c;
                acesDictionary[key2] = c;
                acesDictionary[key3] = c;
                acesDictionary[key4] = c;*/

                if (c.AcesVehicleId.HasValue)
                {
                    List<Classification> list;
                    if (!acesIdDictionary.TryGetValue(c.AcesVehicleId.Value, out list))
                    {
                       list = new List<Classification>();
                        acesIdDictionary.Add(c.AcesVehicleId.Value, list);
                    }
                    list.Add(c);
                }
                
            }

            acDictionary = acesIdDictionary;

            return acesDictionary;
        }

        private int ProcessFitmentForWheel(WheelSpec w, List<FitmentGuide> chassisList, ref int wheel, 
            Dictionary<string, Fitment> currentFitments, Dictionary<string, Fitment> addedFitments,
            Dictionary<string, List<Classification>> acesDictionary, Dictionary<long, List<Classification>> acesIdDictionary, 
            List<int> regions, ref long baseId, string forMake, string forModel)
        {                        
            int batch = 0;
            double offset = 0;
            if (double.TryParse(w.Offset, out offset) && w.Bore.HasValue)
            {
                double bore = (double) w.Bore;
                foreach (var chassis in chassisList)
                {
                    if (chassis.Make == forMake && chassis.Model == forModel)
                    {
                        //Logger.Info($"{forMake} {forModel} - Testing parts");
                    }   
                             
                    if ((w.Width >= chassis.MinWidth && w.Width <= chassis.MaxWidth) && 
                        (w.Diameter >= chassis.MinDia && w.Diameter <= chassis.MaxDia) && 
                        (offset >= chassis.OffsetMin && offset <= chassis.OffsetMax) && 
                        string.IsNullOrWhiteSpace(chassis.Brakes) && string.IsNullOrWhiteSpace(chassis.Suspension)
                        /*&& chassis.Hubbore <= bore*/)
                    {
                        if (chassis.Make == forMake && chassis.Model == forModel)
                        {
                            Logger.Info($"{forMake} {forModel} - Found match on basics, checking bolt pattern {chassis.BoltMetric} against {w.BoltPattern1} {w.BoltPattern2} {w.BoltPattern3} {w.BoltPattern4}");
                        }

                        // check bolt pattern
                        if (string.IsNullOrWhiteSpace(chassis.BoltMetric))
                        {                                                      
                            continue;   
                        }                        

                        if (!CompareBoltPatterns(w, chassis))
                            continue;

                        if (chassis.Make == forMake && chassis.Model == forModel)
                        {
                            Logger.Info($"{forMake} {forModel} - Found match on all points, building fitments");
                        }

                        int fromYear = chassis.YearFrom.GetValueOrDefault();
                        int toYear = chassis.YearTo.GetValueOrDefault();
                        for (int y = fromYear; y <= toYear; ++y)
                        {
                            var basekey = y + ";" + chassis.Make.ToLower().Trim() + ";" + chassis.Model.ToLower().Trim() + ";" +
                                (string.IsNullOrWhiteSpace(chassis.Submodel) ? "null" : chassis.Submodel.ToLower().Trim()) + ";" +
                                  (string.IsNullOrWhiteSpace(chassis.BodyStyle) ? "null" : chassis.BodyStyle.ToLower().Trim());
                            
                            lock (_lockObj)
                            {
                                foreach (var region in regions)
                                {
                                    string key = basekey + ";" + region;
                                    ProcessMatch(w, addedFitments, currentFitments, acesDictionary, acesIdDictionary, ref baseId, key, chassis, ref batch);
                                }
                            }
                        }
                    }
                }
            }

            Interlocked.Increment(ref wheel);
            return batch;
        }

        private void ProcessMatch(WheelSpec w, Dictionary<string, Fitment> addedFitments, 
            Dictionary<string, Fitment> currentFitments,
            Dictionary<string, List<Classification>> acesDictionary,
            Dictionary<long, List<Classification>> acesIdDictionary, 
            ref long baseId, string key,
            FitmentGuide fitmentGuide, ref int batch)
        {
            List<Classification> match;
            if (acesDictionary.TryGetValue(key, out match))
            {
                if (match == null)
                    return;
                int count = 0;
                match.ForEach(m =>
                {
                    var newFitment = new Fitment()
                    {
                        PartNumber = w.PartNumber,
                        ClassificationId = m.Id,
                        ApiId = (int)ApiIdEnum.Aces,
                        BrandId = w.Wheel.BrandId,
                        Restricted = false,
                    };
                    addedFitments[m.Id + w.PartNumber] = newFitment;
                    count++;
                });

                batch += count;
            }
            return;
        }

        /*private Fitment AddNewFitment(WheelSpec w, Dictionary<string, Fitment> addedFitments, ref long baseId, FitmentGuide chassis, List<Classification> match)
        {
            var firstMatch = match.First();
            var currentFitment = new Fitment()
            {
                PartNumber = w.PartNumber,
                ClassificationId = firstMatch.Id,
                ApiId = (int) ApiIdEnum.Aces,
                Restricted = false,
                //ChassisId = firstMatch.ChassisId == null ? (long?) null : long.Parse(firstMatch.ChassisId)
            };

            if (!currentFitment.ChassisId.HasValue)
            {
                baseId++;
                string newChassisId = baseId + string.Empty;
                currentFitment.ChassisId = baseId;
                if (firstMatch.ChassisId == null)
                {
                    firstMatch.ChassisId = newChassisId;
                }
                UpdateAllSameAcesWithChassis(match, newChassisId);
            }

            chassis.ChassisId = chassis.ChassisId ?? currentFitment.ChassisId;
            addedFitments[currentFitment.ChassisId.GetValueOrDefault() + w.PartNumber] = currentFitment;
            return currentFitment;
        }

        private void UpdateAllSameAcesIdWithChassis(Dictionary<long, List<Classification>> acesIdDictionary, long vehicleId, string chassisId)
        {
            List<Classification> vehicles;
            if (acesIdDictionary.TryGetValue(vehicleId, out vehicles))
            {
                vehicles.ForEach(v =>
                {
                    if (string.IsNullOrWhiteSpace(v.ChassisId))
                    {
                        v.ChassisId = chassisId;
                    }
                });
            }
        }*/

        /*private void UpdateAllSameAcesWithChassis(List<Classification> classifications, string chassisId)
        {
            classifications.ForEach(v =>
            {
                if (string.IsNullOrWhiteSpace(v.ChassisId))
                {
                    v.ChassisId = chassisId;
                }
            });
        }*/

        private static bool CompareBoltPatterns(WheelSpec w, FitmentGuide chassis)
        {
            Decimal bolt1 = 0;
            Decimal bolt2 = 0;
            GetBoltPatterns(chassis.BoltMetric, ref bolt1, ref bolt2);

            Decimal wBolt1 = 0;
            Decimal wBolt2 = 0;
            GetBoltPatterns(w.BoltPattern1, ref wBolt1, ref wBolt2);
            if (bolt1 == wBolt1 && bolt2 == wBolt2)
            {
                return true;
            }

            GetBoltPatterns(w.BoltPattern2, ref wBolt1, ref wBolt2);
            if (bolt1 == wBolt1 && bolt2 == wBolt2)
            {
                return true;
            }

            GetBoltPatterns(w.BoltPattern3, ref wBolt1, ref wBolt2);
            if (bolt1 == wBolt1 && bolt2 == wBolt2)
            {
                return true;
            }

            GetBoltPatterns(w.BoltPattern4, ref wBolt1, ref wBolt2);
            if (bolt1 == wBolt1 && bolt2 == wBolt2)
            {
                return true;
            }

            return false;
        }

        private static void GetBoltPatterns(string boltPattern, ref decimal bolt1, ref decimal bolt2)
        {
            if (string.IsNullOrWhiteSpace(boltPattern))
            {
                bolt1 = 0;
                bolt2 = 0;
                return;
            }
            var bolts = boltPattern.ToLower().Split('x');
            if (bolts.Length > 1)
            {
                Decimal.TryParse(bolts[0], out bolt1);
                Decimal.TryParse(bolts[1], out bolt2);
            }
        }
    }
}
