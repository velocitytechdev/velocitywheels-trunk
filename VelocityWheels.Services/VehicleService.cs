﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using VelocityWheels.Data.Enum;
using VelocityWheels.Data.Models;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    public class VehicleService : ServiceBase
    {
        public VehicleService() : this(null)
        {
        }

        public VehicleService(VelocityWheelsContext context) : base(context)
        {
            Logger = AppLogger.GetLogger(this);
        }

        public List<FuelAPISubModel> GetVehicles(string year, string make, string model, string bodyType, string subModel, out string json)
        {
            json = null;

            IMappingService mappingService = new MappingService(Context);
            var mapping = mappingService.MapVehicle(ApiIdEnum.Aces, ApiIdEnum.FuelApi, int.Parse(year), make, model, bodyType, subModel);
            if (mapping != null)
            {
                return new List<FuelAPISubModel>()
                {
                    new FuelAPISubModel()
                    {                        
                        Year = int.Parse(year),
                        make_name = mapping.ToMake,
                        model_name = mapping.ToModel,
                        bodytype_desc = mapping.ToBodyType,
                        bodytype = mapping.ToBodyType,
                        trim = mapping.ToSubModel,
                        id = (int)mapping.ToVehicleId.GetValueOrDefault()
                    }
                };
            }
                
            mapping = mappingService.MapVehicle(ApiIdEnum.Aces, ApiIdEnum.FuelApi, int.Parse(year), make, model, bodyType);
            if (mapping == null)
            {
                return new List<FuelAPISubModel>();
            }

            var url = "";
            url = $"https://api.fuelapi.com/v1/json/vehicles/?year={year}&make={mapping.ToMake}&model={mapping.ToModel}&api_key={VehicleConfiguration.FUEL_API_TOKEN}";

            try
            {
                using (var wc = new MyWebClient())
                {
                    json = wc.DownloadString(url);                                                
                }
            }
            catch (WebException e)
            {
                json = "[]";
                Logger.Error("GetVehicleImagesFuelAPI exception " + e.Message + e.StackTrace);
            }

            List<FuelAPISubModel> subModels = JsonConvert.DeserializeObject<List<FuelAPISubModel>>(json);
            foreach (var subm in subModels)
            {
                if ((subm.trim ?? "") == "")
                    subm.trim = "Base";

                if (subm.bodytype_desc != null && bodyType != null && subm.bodytype_desc.Equals(mapping.ToBodyType, StringComparison.CurrentCultureIgnoreCase))
                {
                    subm.bodytype_desc = mapping.FromBodyType??subm.bodytype_desc;
                }
            }

            return subModels;
        }

        public List<VehicleImageResult> GetCustomImageVehicles(string year, string make, string model)
        {
            int yr = int.Parse(year);
            var vehicles =
                Context.Classifications.Include("VehicleImages").
                    Where(v => v.VehicleImages.Any() && v.Year == yr && v.Make == make && v.Model == model).ToList();

            var resultsMap = new Dictionary<int, VehicleImageResult>();

            vehicles.ForEach(v =>
            {
                foreach (var vehicleImage in v.VehicleImages)
                {
                    if (!resultsMap.ContainsKey(vehicleImage.VehicleImageID) && vehicleImage.Thumbnail != null)
                    {
                        var result = new VehicleImageResult()
                        {
                            Thumbnail = vehicleImage.Thumbnail != null ? $"{VehicleImagesBaseUrl}/thumbnails/{vehicleImage.Thumbnail}" : null,
                            ApiId = (int)ApiIdEnum.Aces,
                            VehicleImageID = vehicleImage.VehicleImageID,
                            Year = v.Year,
                            Make = v.Make,
                            Model = v.Model,
                            BodyStyle = v.BodyType,
                            SubModel = v.SubModel,
                            FileName = vehicleImage.FileName
                        };
                        
                        resultsMap[vehicleImage.VehicleImageID] = result;
                    }
                }

                
            });

            return resultsMap.Select(m => m.Value).ToList();
        }

        public FuelApiVehicle GetVehicleFromJson(string json)
        {
            FuelApiVehicle vehicle = JsonConvert.DeserializeObject<FuelApiVehicle>(json);

            return vehicle;
        }

        public List<VehicleImageResult> GetVehicleThumbs(int year, IList<FuelAPISubModel> vehicles)
        {
            var vehicleMap = vehicles.ToDictionary(v => v.id, v => v);
            
            var images = GetVehicleImagesFromIds(vehicles.Select(v => v.id));

            // check for no thumbs and populate            
            if (images.Any(i => i.Thumbnail == null))
            {
                var missingThumbs = images.Where(i => i.Thumbnail == null && i.VehicleID != null).Select(i => i.VehicleID.Value).Distinct();
                ImageService imageService = new ImageService(Context);
                imageService.CreateThumbnailFromVehicleImage(missingThumbs);
                images = GetVehicleImagesFromIds(vehicles.Select(v => v.id));
            }

            var resultsMap = new Dictionary<string, VehicleImageResult>();
            foreach (VehicleImageResult vehicleImage in images)
            {
                FuelAPISubModel model;
                if (!vehicleMap.TryGetValue(vehicleImage.VehicleID.GetValueOrDefault(), out model))
                {
                    continue;
                }

                var key = year + model.make_name + model.model_name + model.trim + model.bodytype_desc;
                if (!resultsMap.ContainsKey(key))
                {
                    var filePath = vehicleImage.Thumbnail != null ? $"{VehicleImagesBaseUrl}/thumbnails/{vehicleImage.Thumbnail}" : null;
                    vehicleImage.Thumbnail = filePath;
                    vehicleImage.Year = year;
                    vehicleImage.Make = model.make_name;
                    vehicleImage.Model = model.model_name;
                    vehicleImage.SubModel = model.trim;
                    vehicleImage.BodyStyle = model.bodytype_desc;
                    vehicleImage.FileName = vehicleImage.FileName;
                    vehicleImage.ApiId = vehicleImage.ApiId;
                    resultsMap[key] = vehicleImage;
                }
            }

            var thumbs = resultsMap.Where(v => v.Value.Thumbnail != null).Select(i => i.Value).ToList();
            return thumbs;
        }

        private List<VehicleImageResult> GetVehicleImagesFromIds(IEnumerable<int> vehicleIds)
        {
            var images = Context.VehicleImages.Where(v => vehicleIds.Contains(v.VehicleID.Value) && v.ProductFormatId == 43).
                Select(v => new VehicleImageResult()
                {
                    VehicleID = v.VehicleID,
                    VehicleImageID = v.VehicleImageID,
                    Thumbnail = v.Thumbnail,
                    ApiId = v.ApiId,
                    FileName = v.FileName
                }).ToList();
            return images;
        }

        public VehicleImage DownloadVehicleImageFromFuelApi(string url, int? vehicleId, string code, int formatId)
        {
            var localPath = VelocityWheelsSettings.GetVehicleContentPath;
            ImageService imageService = new ImageService(Context);

            Logger.Info("GetCroppedImage - getting from fuelapi");
            byte[] data;
            using (var wc = new MyWebClient())
            {
                data = wc.DownloadData(url);
            }
            Image img = Image.FromStream(new MemoryStream(data));
            var croppedImg = imageService.CropImage(img, new Rectangle(0, 300, 2175, 1140));
            using (var ms = new MemoryStream())
            {
                croppedImg.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] b = ms.ToArray();
                var ext = url.Substring(url.LastIndexOf('.') + 1);
                var newFileName = $"{Guid.NewGuid()}.{ext}";
                var imageType = "image/" + ext;
                string fullFileName = string.Format(localPath, newFileName);
                File.WriteAllBytes(fullFileName, b);

                imageService.ShrinkVehicleImage(fullFileName);

                var image = new VehicleImage()
                {
                    URL = url,
                    FileName = newFileName,
                    ImageType = imageType,
                    VehicleID = vehicleId,
                    ImageCode = code,
                    ProductFormatId = formatId
                };
                SaveOrUpdate(image);

                return image;
            }
        }


        public VehicleImage GetVehicleImage(string url, int vehicleId, string code, int formatId)
        {
            var image = Context.VehicleImages.FirstOrDefault(v => v.VehicleID == vehicleId && v.ImageCode == code && v.ProductFormatId == formatId) ??
                                 Context.VehicleImages.FirstOrDefault(vi => vi.URL == url);
            return image;
        }

        public VehicleImage GetVehicleImageById(int vehicleImageId)
        {
            var image = Context.VehicleImages.FirstOrDefault(v => v.VehicleImageID == vehicleImageId);
            return image;
        }

        public List<VehicleImageResult> GetAllVehicleUploadedImages(ApiIdEnum apiId)
        {
            var images =
                Context.VehicleImages.
                    Where(c => c.ApiId == (int)apiId).
                    Select(c => new VehicleImageResult()
                    {
                        VehicleImageID = c.VehicleImageID,
                        ApiId = c.ApiId,
                        Thumbnail = c.Thumbnail,
                        FileName = c.FileName,
                        VehicleID = c.VehicleID,
                        CreatedDate = c.CreatedDate
                    }).OrderByDescending(v => v.VehicleImageID);

            return images.ToList();
        }

        public VehicleImage SaveOrUpdate(VehicleImage vehicleImage)
        {
            if (vehicleImage.VehicleImageID == 0)
            {                
                Context.VehicleImages.Add(vehicleImage);
            }

            Context.SaveChanges();

            return vehicleImage;
        }

        public void Delete(VehicleImage vehicleImage)
        {
            Context.VehicleImages.Remove(vehicleImage);
            Context.SaveChanges();
        }

        public WheelPosition GetWheelPosition(int? vid, string y, string m, string mdl, string sm, string o, int? imageId = null)
        {
            WheelPosition wheelPosition = null;
            if (imageId.HasValue)
            {
                var vehicleImage = Context.VehicleImages.Include("WheelPositions").FirstOrDefault(v => v.VehicleImageID == imageId.Value);
                return vehicleImage?.WheelPositions.FirstOrDefault();
            }
            else
            {
                wheelPosition = Context.WheelPositions.FirstOrDefault(wp => wp.VehicleID == vid && wp.Orientation == o);
            }

            if (wheelPosition != null)
            {
                return wheelPosition;
            }

            //Try getting one year higher
            y = (Convert.ToInt32(y) + 1).ToString();
            wheelPosition = Context.WheelPositions.FirstOrDefault(
                wp => wp.Year == y
                      && wp.Make == m
                      && wp.Model == mdl
                      && wp.SubModel == sm
                      && wp.Orientation == o);

            return wheelPosition;
        }

        public WheelPosition GetWheelPosition(int? vid, string o, int? imageId = null)
        {
            WheelPosition wheelPosition = null;
            if (imageId.HasValue)
            {
                var vehicleImage = Context.VehicleImages.Include("WheelPositions").FirstOrDefault(v => v.VehicleImageID == imageId.Value);
                return vehicleImage?.WheelPositions.FirstOrDefault();
            }
            else
            {
                wheelPosition = Context.WheelPositions.FirstOrDefault(wp => wp.VehicleID == vid && wp.Orientation == o);
            }

            return wheelPosition;
        }
    }
}
