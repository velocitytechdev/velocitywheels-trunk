﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VelocityWheels.Data.Models;

namespace VelocityWheels.Services
{
    public class PermissionService : ServiceBase
    {
        public static string SuperAdminRole = "SuperAdmin";
        public static string AccountAdminRole = "AccountAdmin";
        public static string AccountUserRole = "AccountUser";

        public static bool HasPermission(User user, string permission)
        {
            return user != null && user.Role.Permissions.Any(p => p.PermissionName.Equals(permission, StringComparison.InvariantCultureIgnoreCase));
        }

        public static bool HasPermission(User user, string[] permissions)
        {
            return user != null && user.Role.Permissions.Any(p => permissions.Contains(p.PermissionName));
        }

        public static bool IsSuperAdmin(User user)
        {
            return user != null && user.Role.Permissions.Any(p => p.PermissionName == "IsSuperAdmin");
        }

        public static bool IsAccountAdmin(User user)
        {
            return user != null && user.Role.Permissions.Any(p => p.PermissionName == "IsAccountAdmin");
        }

        public static bool IsAccountUser(User user)
        {
            return user != null && user.Role.Permissions.Any(p => p.PermissionName == "IsAccountUser");
        }
    }
}
