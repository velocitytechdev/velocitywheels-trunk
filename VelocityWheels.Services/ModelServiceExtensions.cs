﻿using System;
using System.Collections.Generic;
using VelocityWheels.Data.Models;

namespace VelocityWheels.Services
{
    public static class ModelServiceExtensions
    {
        /// <summary>
        ///     Sets the audit fields of auditable models.
        /// </summary>
        /// <param name="timestamp">
        ///     The timestamp, in UTC, to use or <c>null</c> if the current time should be used.
        /// </param>
        /// <param name="models">
        ///     The models to be changed.
        /// </param>
        public static void SetAuditFields(this IModelService receiver, DateTime? timestamp, params IAuditedModel[] models)
        {
            receiver.SetAuditFields(models, timestamp);
        }

        /// <summary>
        ///     Sets the audit fields of auditable models.
        /// </summary>
        /// <param name="models">
        ///     The models to be changed.
        /// </param>
        public static void SetAuditFields(this IModelService receiver, params IAuditedModel[] models)
        {
            receiver.SetAuditFields(models, null);
        }
    }
}