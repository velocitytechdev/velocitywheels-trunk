﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class ProductFitmentModel
    {
        public long ClassificationId { get; set; }

        public long ChassisId { get; set; }

        public int Year { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string BodyType { get; set; }

        public string SubModel { get; set; }

        public string Region { get; set; }

        public int RegionId { get; set; }

        public bool Restricted { get; set; }
    }
}
