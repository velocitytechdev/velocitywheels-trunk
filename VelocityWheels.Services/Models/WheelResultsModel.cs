﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class WheelResultsModel
    {
        public int Id { get; set; }
        public int? WheelId { get; set; }
        public string PartNumber { get; set; }
        public string Brand { get; set; }
        public long? BrandId { get; set; }
        public string Finish { get; set; }
        public long? FinishId { get; set; }
        public string ProductName { get; set; }
        public long? StyleId { get; set; }
        public decimal? Msrp { get; set; }
        public decimal? Diameter { get; set; }
        public decimal? Width { get; set; }
        public string Offset { get; set; }
        public bool Customizable { get; set; }
        public string Image { get; set; }
        public string FrontImage { get; set; }
        public string RearImage { get; set; }
        public List<string> Sizes { get; set; }
    }
}
