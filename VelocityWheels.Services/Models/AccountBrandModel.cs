﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VelocityWheels.Data.Models;

namespace VelocityWheels.Services.Models
{
    public class AccountBrandModel
    {
        public long? Id { get; set; }

        public BrandPartialModel Brand { get; set; }

        public AccountBrandModel()
        {
        }

        public AccountBrandModel(AccountBrands accountBrand)
        {
            Id = accountBrand.Id;
            Brand = new BrandPartialModel(accountBrand.Brand);
        }
    }
}
