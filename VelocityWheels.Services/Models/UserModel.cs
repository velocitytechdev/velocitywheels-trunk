﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class UserModel
    {
        public long Id { get; set; }

        public AccountModel Account { get; set; }

        public string UserName { get; set; }

        public string Name { get; set; }

        public string SortName { get; set; }

        public bool Active { get; set; }        

        public RoleModel Role { get; set; }    
    }
}
