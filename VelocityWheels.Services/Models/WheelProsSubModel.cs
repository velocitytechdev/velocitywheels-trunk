﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class WheelProsSubModel
    {
        public string Name { get; set; }

        public bool MoreData { get; set; }

        public string VehicleCode { get; set; }
    }
}
