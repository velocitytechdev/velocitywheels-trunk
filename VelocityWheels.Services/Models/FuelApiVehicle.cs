﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class FuelApiVehicle
    {
        public string Id { get; set; }
        public int? ApiId { get; set; }
        public string Created { get; set; }
        public string Modified { get; set; }
        public string Trim { get; set; }
        public string Num_doors { get; set; }
        public string Drivetrain { get; set; }
        public string Bodytype { get; set; }
        public FuelAPIModel Model { get; set; }
        public List<FuelApiProduct> Products { get; set; }
        public int? VehicleImageId { get; set; }         
    }
}