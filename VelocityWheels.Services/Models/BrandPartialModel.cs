﻿using VelocityWheels.Data.Models;

namespace VelocityWheels.Services.Models
{
    public class BrandPartialModel
    {
        public long? Id { get; set; }

        public string BrandName { get; set; }

        public int Count { get; set; }

        public BrandPartialModel()
        {
        }

        public BrandPartialModel(Brand brand)
        {
            Id = brand.Id;
            BrandName = brand.Name;
        }        
    }
}