﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class WheelResults
    {
        public IList<WheelResultsModel> WheelResultsModels { get; set; }
        public IList<string> Diameters { get; set; }
        public string TireWebBaseUrl { get; set; } 
        public bool TireWeb { get; set; } 
        public string Error { get; set; }
    }
}
