﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class FuelApiAssets
    {
        public string Url { get; set; }
        public FuelApiShotCode ShotCode { get; set; }
    }
}
