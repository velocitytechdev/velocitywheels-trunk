﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class PartModel
    {
        public long WheelSpecId { get; set; }

        public string PartNumber { get; set; }

        public string Size { get; set; }

        public string BoltPattern { get; set; }

        public string Offset { get; set; }

        public int Fitments { get; set; }
    }
}
