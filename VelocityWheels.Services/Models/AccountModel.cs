﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using VelocityWheels.Data.Models;

namespace VelocityWheels.Services.Models
{
    public class AccountModel
    {
        public long Id { get; set; }

        public string AccountName { get; set; }

        [MaxLength(50)]
        public string Token { get; set; }

        public bool Active { get; set; }

        public bool VehicleSelection { get; set; }

        public bool VehicleImages { get; set; }

        public bool WheelProducts { get; set; }

        public bool TireProducts { get; set; }

        public bool TireWeb { get; set; }

        [StringLength(100)]
        public string TireWebBaseUrl { get; set; }

        public List<SubAccountModel> SubAccounts { get; set; }

        public List<AccountBrandModel> AccountBrands { get; set; }

        public AccountModel()
        {
        }

        public AccountModel(Account account)
        {
            Id = account.Id;
            AccountName = account.AccountName;
            Active = account.Active;
            Token = account.Token;
            TireWeb = account.TireWeb;
            TireProducts = account.TireProducts;
            VehicleImages = account.VehicleImages;
            VehicleSelection = account.VehicleSelection;
            TireWebBaseUrl = account.TireWebBaseUrl;
            WheelProducts = account.WheelProducts;

            if (account.SubAccounts != null)
            {
                SubAccounts = account.SubAccounts.Select(s => new SubAccountModel(s)).ToList();
            }

            if (account.AccountBrands != null)
            {
                AccountBrands = account.AccountBrands.Select(b => new AccountBrandModel(b)).ToList();
            }
        }
    }
}
