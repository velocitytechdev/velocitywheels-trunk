﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class FuelApiShotCode
    {
        public string Code { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public FuelApiColor Color { get; set; }
    }
}
