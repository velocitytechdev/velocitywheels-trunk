﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class VehicleImageResult
    {
        public int VehicleImageID { get; set; }

        public int? ApiId { get; set; }

        public int? VehicleID { get; set; }

        public int Year { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string BodyStyle { get; set; }

        public string SubModel { get; set; }

        public string FileName { get; set; }

        public string Thumbnail { get; set; }
        
        public DateTime? CreatedDate { get; set; }

        public string DisplayDate { get; set; }
    }
}
