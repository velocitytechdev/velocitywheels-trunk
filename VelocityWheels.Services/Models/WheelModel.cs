﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VelocityWheels.Data.Models;

namespace VelocityWheels.Services.Models
{
    public class WheelModel
    {
        public long WheelId { get; set; }

        public string Brand { get; set; }

        public long? BrandId { get; set; }

        public string Style { get; set; }

        public long? StyleId { get; set; }

        public string Finish { get; set; }

        public long? FinishId { get; set; }

        public string ShortFinish { get; set; }

        public string Status { get; set; }

        public int StatusId { get; set; }

        public string Image { get; set; }

        public string BaseFrontImage { get; set; }

        public string BaseRearImage { get; set; }

        public string BaseLayerImage { get; set; }

        public bool Customizable { get; set; }

        public bool Active { get; set; }

        public List<ProductModel> Products { get; set; }

        public List<WheelLayerModel> WheelLayers { get; set; }

        public string Error { get; set; }
    }
}
