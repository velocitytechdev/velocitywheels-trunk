﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class FuelApiProductFormats
    {
        public string Id { get; set; }        
        public string Type { get; set; }
        public string Code { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public List<FuelApiAssets> Assets { get; set; }
    }
}
