﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class FuelApiColor
    {
        public string Oem_name { get; set; }
        public string Simple_name { get; set; }
        public string Rgb1 { get; set; }
    }
}
