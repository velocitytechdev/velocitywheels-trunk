﻿using System;

namespace VelocityWheels.Services.Models
{
    [Serializable]
    public class FuelAPIMake
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}