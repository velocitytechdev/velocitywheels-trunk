﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class PermissionModel
    {
        public long Id { get; set; }

        [MaxLength(40)]
        public string PermissionName { get; set; }

        public bool Enabled { get; set; }
    }
}
