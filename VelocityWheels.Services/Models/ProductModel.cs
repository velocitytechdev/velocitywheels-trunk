﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class ProductModel
    {
        public long WheelSpecId { get; set; }

        public string PartNumber { get; set; }

        public string Brand { get; set; }
        public long? BrandId { get; set; }

        public string Style { get; set; }
        public long? StyleId { get; set; }

        public string Finish { get; set; }
        public long? FinishId { get; set; }

        public string ShortFinish { get; set; }

        public string Description { get; set; }

        public string Thumbnail { get; set; }

        public Decimal? Diameter { get; set; }

        public Decimal? Width { get; set; }

        public String Size { get; set; }

        public string BoltPattern { get; set; }

        public string BoltPattern1 { get; set; }

        public string BoltPattern2 { get; set; }

        public string BoltPattern3 { get; set; }

        public string BoltPattern4 { get; set; }

        public string Offset { get; set; }

        public Decimal? LoadRating { get; set; }

        public Decimal? Weight { get; set; }

        public Decimal? Bsm { get; set; }

        public Decimal? Bore { get; set; }

        public Decimal? Cost { get; set; }

        public Decimal? MapPricing { get; set; }

        public Decimal? Msrp { get; set; }

        public bool BigBrakes { get; set; }

        public bool OffRoad { get; set; }

        public int TotalFitments { get; set; }

        public bool FitsChassis { get; set; }

        public Dictionary<string, List<ProductFitmentModel>> Fitments { get; set; }

        public Dictionary<string, List<ProductFitmentModel>> RestrictedFitments { get; set; }
    }
}
