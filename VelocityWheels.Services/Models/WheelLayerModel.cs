﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using VelocityWheels.Data.Models;

namespace VelocityWheels.Services.Models
{
    public class WheelLayerModel
    {
        [Key]
        public long? Id { get; set; }

        public int? WheelId { get; set; }

        public short Level { get; set; }

        public string LevelName { get; set; }

        public string BaseLayerImage { get; set; }

        public string FrontLayerImage { get; set; }

        public string RearLayerImage { get; set; }
    }
}
