﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class FitmentGuideImport
    {
        public string YearFrom { get; set; }

        public string YearTo { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string Submodel { get; set; }

        public string BodyStyle { get; set; }

        public string Brakes { get; set; }

        public string Suspension { get; set; }

        public string MinDia { get; set; }

        public string MinWidth { get; set; }

        public string MaxDia { get; set; }

        public string MaxWidth { get; set; }

        public string BoltMetric { get; set; }

        public string BoltStandard { get; set; }

        public string OffsetMin { get; set; }

        public string OffsetMax { get; set; }

        private sealed class FitmentGuideImportEqualityComparer : IEqualityComparer<FitmentGuideImport>
        {
            public bool Equals(FitmentGuideImport x, FitmentGuideImport y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return string.Equals(x.YearFrom, y.YearFrom, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.YearTo, y.YearTo, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.Make, y.Make, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.Model, y.Model, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.Submodel, y.Submodel, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.BodyStyle, y.BodyStyle, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.Brakes, y.Brakes, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.Suspension, y.Suspension, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.MinDia, y.MinDia, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.MinWidth, y.MinWidth, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.MaxDia, y.MaxDia, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.MaxWidth, y.MaxWidth, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.BoltMetric, y.BoltMetric, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.BoltStandard, y.BoltStandard, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.OffsetMin, y.OffsetMin, StringComparison.InvariantCultureIgnoreCase) && string.Equals(x.OffsetMax, y.OffsetMax, StringComparison.InvariantCultureIgnoreCase);
            }

            public int GetHashCode(FitmentGuideImport obj)
            {
                unchecked
                {
                    var hashCode = (obj.YearFrom != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.YearFrom) : 0);
                    hashCode = (hashCode*397) ^ (obj.YearTo != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.YearTo) : 0);
                    hashCode = (hashCode*397) ^ (obj.Make != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.Make) : 0);
                    hashCode = (hashCode*397) ^ (obj.Model != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.Model) : 0);
                    hashCode = (hashCode*397) ^ (obj.Submodel != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.Submodel) : 0);
                    hashCode = (hashCode*397) ^ (obj.BodyStyle != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.BodyStyle) : 0);
                    hashCode = (hashCode*397) ^ (obj.Brakes != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.Brakes) : 0);
                    hashCode = (hashCode*397) ^ (obj.Suspension != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.Suspension) : 0);
                    hashCode = (hashCode*397) ^ (obj.MinDia != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.MinDia) : 0);
                    hashCode = (hashCode*397) ^ (obj.MinWidth != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.MinWidth) : 0);
                    hashCode = (hashCode*397) ^ (obj.MaxDia != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.MaxDia) : 0);
                    hashCode = (hashCode*397) ^ (obj.MaxWidth != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.MaxWidth) : 0);
                    hashCode = (hashCode*397) ^ (obj.BoltMetric != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.BoltMetric) : 0);
                    hashCode = (hashCode*397) ^ (obj.BoltStandard != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.BoltStandard) : 0);
                    hashCode = (hashCode*397) ^ (obj.OffsetMin != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.OffsetMin) : 0);
                    hashCode = (hashCode*397) ^ (obj.OffsetMax != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.OffsetMax) : 0);
                    return hashCode;
                }
            }
        }

        private static readonly IEqualityComparer<FitmentGuideImport> FitmentGuideImportComparerInstance = new FitmentGuideImportEqualityComparer();

        public static IEqualityComparer<FitmentGuideImport> FitmentGuideImportComparer
        {
            get { return FitmentGuideImportComparerInstance; }
        }
    }
}
