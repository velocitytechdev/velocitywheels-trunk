﻿namespace VelocityWheels.Services.Models
{
    public class FuelAPIModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
    }
}