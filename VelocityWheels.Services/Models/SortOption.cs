﻿namespace VelocityWheels.Services.Models
{
    public class SortOption
    {
        public string Field { get; set; }
        public int Direction { get; set; }
    }
}