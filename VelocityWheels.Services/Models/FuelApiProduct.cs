﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class FuelApiProduct
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public List<FuelApiProductFormats> ProductFormats { get; set; }
    }
}
