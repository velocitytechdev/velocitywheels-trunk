﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VelocityWheels.Data.Models;

namespace VelocityWheels.Services.Models
{
    public class SubAccountModel
    {
        public long Id { get; set; }

        public string SubAccountName { get; set; }

        public string AccountUrl { get; set; }

        public bool Active { get; set; }

        public SubAccountModel()
        {
        }

        public SubAccountModel(SubAccount sub)
        {
            Id = sub.Id;
            SubAccountName = sub.SubAccountName;
            AccountUrl = sub.AccountUrl;
            Active = Active;
        }
    }
}
