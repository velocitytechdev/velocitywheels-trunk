﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class WheelFilter
    {
        public string Brand { get; set; }

        public decimal? Diameter { get; set; }
    }
}
