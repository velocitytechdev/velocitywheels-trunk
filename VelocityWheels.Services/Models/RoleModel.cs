﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using VelocityWheels.Data.Models;

namespace VelocityWheels.Services.Models
{
    public class RoleModel
    {
        public long Id { get; set; }

        [MaxLength(40)]
        public string RoleName { get; set; }

        public bool Enabled { get; set; }

        public List<PermissionModel> Permissions { get; set; }

        public RoleModel()
        {
        }

        public RoleModel(Role role)
        {
            RoleName = role.RoleName;
            Enabled = role.Enabled;
            Id = role.Id;
            Permissions = new List<PermissionModel>();
            foreach (var featurePermission in role.Permissions)
            {
                Permissions.Add(new PermissionModel()
                {
                    Id = featurePermission.Id,
                    Enabled = featurePermission.Enabled,
                    PermissionName = featurePermission.PermissionName
                });
            }
            
        }
    }
}
