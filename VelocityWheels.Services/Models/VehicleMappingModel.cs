﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class VehicleMappingModel
    {
        [Key]
        public long Id { get; set; }

        public int FromApiId { get; set; }

        public int ToApiId { get; set; }

        public string FromModel { get; set; }

        public string FromMake { get; set; }

        public string FromBodyType { get; set; }

        public string FromSubModel { get; set; }

        public string ToModel { get; set; }

        public string ToMake { get; set; }

        public string ToBodyType { get; set; }

        public string ToSubModel { get; set; }

        public long? ToVehicleId { get; set; }

        public string Notes { get; set; }
    }
}
