﻿using System;
using System.Globalization;
using VelocityWheels.Data.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services.Models
{
    using Class = SpecImportData;

    public class SpecImportData
    {
        public string Partnumber { get; set; }

        public string PartnumberDescription { get; set; }

        public string Thumbnail { get; set; }

        public string OnVehicle { get; set; }

        public string Style { get; set; }

        public string Brand { get; set; }

        public string Finish { get; set; }

        public string ShortFinish { get; set; }

        public string Width { get; set; }

        public string Offset { get; set; }

        public string Diameter { get; set; }

        public string Backspacing { get; set; }

        public string BoltPattern1 { get; set; }

        public string BoltPattern2 { get; set; }

        public string BoltPattern3 { get; set; }

        public string BoltPattern4 { get; set; }

        public string LoadRating { get; set; }

        public string Bore { get; set; }

        public string Weight { get; set; }

        public string Lip { get; set; }

        public string Cap { get; set; }

        public string Msrp { get; set; }

        public string Map { get; set; }
    }
}
