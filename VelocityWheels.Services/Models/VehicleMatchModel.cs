﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace VelocityWheels.Services.Models
{
    public class VehicleMatchModel
    {
        public long Id { get; set; }

        public int Year { get; set; }

        [StringLength(100)]
        public string Make { get; set; }

        [StringLength(100)]
        public string Model { get; set; }

        [StringLength(100)]
        public string BodyType { get; set; }

        [StringLength(100)]
        public string SubModel { get; set; }

        [StringLength(100)]
        public string DriveTrain { get; set; }

        public string Image { get; set; }

        public string FullImage { get; set; }

        public int? VehicleImageId { get; set; }

        public bool IsMappedItem { get; set; }

        public bool HasImage { get; set; }

        public int RegionId { get; set; }

        public long? AcesVehicleId { get; set; }
    }
}
