﻿using System;

namespace VelocityWheels.Services.Models
{
    [Serializable]
    public class FuelAPISubModel
    {
        public int id { get; set; }
        public int Year { get; set; }
        public string trim { get; set; }
        public string make_name { get; set; }
        public string model_name { get; set; }
        public string bodytype { get; set; }
        public string bodytype_desc { get; set; }
        public string DriveTrain { get; set; }
    }
}