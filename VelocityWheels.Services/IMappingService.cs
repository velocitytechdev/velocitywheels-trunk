﻿using VelocityWheels.Data.Enum;
using VelocityWheels.Services.Models;

namespace VelocityWheels.Services
{
    public interface IMappingService
    {
        VehicleMappingModel MapVehicle(ApiIdEnum fromApi, ApiIdEnum toApi, int year, string make, string model, string bodyType);
        VehicleMappingModel MapVehicle(ApiIdEnum fromApi, ApiIdEnum toApi, int year, string make, string model, string bodyType, string subModel);
    }
}