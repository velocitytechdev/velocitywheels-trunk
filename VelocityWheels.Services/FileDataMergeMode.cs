﻿namespace VelocityWheels.Services
{
    /// <summary>
    ///     Specifies how existing data should be handled during data file import.
    /// </summary>
    public enum FileDataMergeMode
    {
        /// <summary>
        ///     Specifies that the existing file should be overwritten.
        /// </summary>
        Overwrite = 0,

        /// <summary>
        ///     Specifies that the existing file should preserved and the new data discarded.
        /// </summary>
        Preserve = 1,
    }
}