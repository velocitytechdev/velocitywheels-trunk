﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Helpers;
using System.Web.Security;
using VelocityWheels.Data.Models;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    public class UserService : ServiceBase
    {
        public UserService() : this (null)
        {
        }

        public UserService(VelocityWheelsContext context) : base(context)
        {
            Logger = AppLogger.GetLogger(this);
        }

        public User GetUser(long id)
        {
            return Context.Users.
                Include("Role").
                Include("Role.Permissions").
                Include("Account").
                Include("Account.Brands").
                FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<AccountBrands> GetUserAccountBrands(User user)
        {
            var accountBrands = Context.AccountBrands.
                Where(u => u.AccountId == user.AccountId);
            return accountBrands;
        }

        public bool SaveOrUpdateUser(User user, string updatedPassword = null)
        {
            var currentUser = Context.Users.Include("Role").FirstOrDefault(u => u.Id == user.Id);
            if (currentUser != null)
            { // update
                if (updatedPassword != null)
                {
                    string salt;
                    var password = PasswordUtil.EncryptPassword(updatedPassword, out salt);
                    user.Password = password;
                    user.Salt = salt;
                }
                currentUser.Active = user.Active;
                currentUser.LastUpdated = DateTime.UtcNow;
                currentUser.LastUpdatedBy = GetLoggedInUserName();
            }
            else
            {
                // check for duplicate user
                if (Context.Users.Any(u => u.UserName == user.UserName))
                {
                    throw new InvalidDataException("Username already exists");
                }

                string salt;
                var password = PasswordUtil.EncryptPassword(user.Password, out salt);
                user.Password = password;
                user.Salt = salt;
                user.LastUpdated = DateTime.UtcNow;
                user.LastUpdatedBy = GetLoggedInUserName();
                Context.Users.Add(user);                
            }

            Context.SaveChanges();

            return true;
        }

        public void SaveOrUpdate(UserModel userModel, string updatedPassword = null)
        {
            User user = Context.Users.Include("Role").Include("Account").
                FirstOrDefault(u => u.Id == userModel.Id);

            if (user != null)
            {
                user.Active = userModel.Active;
                user.RoleId = userModel.Role.Id;
                user.Name = userModel.Name;
                user.LastUpdated = DateTime.UtcNow;
                user.LastUpdatedBy = GetLoggedInUserName();
                user.AccountId = userModel.Account?.Id;

                if (updatedPassword != null)
                {
                    string salt;
                    var password = PasswordUtil.EncryptPassword(updatedPassword, out salt);
                    user.Password = password;
                    user.Salt = salt;
                }
            }
            else
            {
                // check for duplicate user
                if (Context.Users.Any(u => u.UserName == userModel.Name))
                {
                    throw new InvalidDataException("Username already exists");
                }

                user = new User()
                {
                    Active = userModel.Active,
                    RoleId = userModel.Role.Id,
                    Name = userModel.Name,
                    UserName = userModel.UserName,
                    AccountId = userModel.Account?.Id,                    
                };

                string salt;
                var password = PasswordUtil.EncryptPassword(updatedPassword, out salt);
                user.Password = password;
                user.Salt = salt;

                Context.Users.Add(user);
            }

            Context.SaveChanges();
        }

        public void AddPermissionToRoles(string permissionName)
        {
            var permission = Context.Permissions.FirstOrDefault(p => p.PermissionName == permissionName);
            if (permission == null)
                return;

            var roles = Context.Roles.Where(r => r.Permissions.All(p => p.Id != permission.Id)).ToList();
            foreach (var role in roles)
            {
                role.Permissions.Add(permission);
            }

            Context.SaveChanges();
        }

        public void RemovePermissionFromRoles(string permissionName)
        {
            var permission = Context.Permissions.FirstOrDefault(p => p.PermissionName == permissionName);
            if (permission == null)
                return;

            var roles = Context.Roles.Where(r => r.Permissions.Any(p => p.Id == permission.Id)).ToList();
            foreach (var role in roles)
            {
                role.Permissions.Remove(permission);
            }

            Context.SaveChanges();
        }

        public bool VerifyPassword(string username, string password)
        {
            var user = Context.Users.FirstOrDefault(u => u.UserName == username);
            if (user == null)
            {
                return false;
            }

            return PasswordUtil.ComparePasswords(password, user.Password, user.Salt);
        }

        
    }

}
