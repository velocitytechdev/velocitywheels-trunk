﻿using System.Collections.Generic;
using VelocityWheels.Data.Enum;

namespace VelocityWheels.Services
{
    public interface IClassificationService
    {
        List<int> GetYears(ApiIdEnum apiId);
        List<string> GetMakes(ApiIdEnum apiId, int year);
        List<string> GetModels(ApiIdEnum apiId, int year, string make);
        List<string> GetBodyTypes(ApiIdEnum apiId, int year, string make, string model);
        List<string> GetSubModels(ApiIdEnum apiId, int year, string make, string model, string bodyType);       
        List<long> GetChassis(int year, string make, string model, string bodyType,
            string subModel);
    }
}