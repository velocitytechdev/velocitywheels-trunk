﻿using System.Collections.Generic;
using System.IO;
using VelocityWheels.Data.Models;
using VelocityWheels.Services.Models;

namespace VelocityWheels.Services
{
    public interface IWheelService : IModelService
    {
        ProductModel GetProduct(long id);

        Wheel GetWheel(long id);

        WheelModel GetWheel(long brandId, long styleId, long finishId);

        WheelModel GetWheelById(long wheelId, bool includeProducts = false);

        IList<WheelResultsModel> GetWheels(long brandId, long? accountId = null);

        WheelResults GetWheels(long? chassisId, WheelFilter filters, long? accountId = null, long? brandId = null, SortOption sortOption = null);

        IList<BrandPartialModel> GetWheelsBrands(User user);

        IList<WheelSpec> ImportWheelDataFromFile(Stream fileStream, ImportWheelDataFromFileOptions options = null);

        void ImportWheelsThumbnailsFromFile(Stream fileStream, ImportWheelsThumbnailsFromFileOptions options = null);

        bool SaveOrUpdate(Wheel wheel);

        int UpdateImagesForAllWheels();
    }
}
