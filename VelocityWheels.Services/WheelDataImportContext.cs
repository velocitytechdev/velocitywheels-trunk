﻿using System;
using System.Collections;
using System.Collections.Generic;
using VelocityWheels.Data.Models;
using VelocityWheels.Services.Models;

namespace VelocityWheels.Services
{
    /// <summary>
    ///     Used by <see cref="WheelService.ImportWheelDataFromFile"/> to track changes during the 
    ///     import process.
    /// </summary>
    public class WheelDataImportContext
    {
        public VelocityWheelsContext DataContext { get; internal set; }
        public Brand ExistingBrand { get; set; }
        public Finish ExistingFinish { get; set; }
        public Style ExistingStyle { get; set; }
        public Wheel ExistingWheel { get; set; }
        public WheelSpec ExistingWheelSpec { get; set; }
        public SpecImportData Record { get; set; }

        public IDictionary<string, Brand> NewBrands { get; set; }

        public IDictionary<Tuple<string, string>, Finish> NewFinishes { get; set; }

        public IDictionary<Tuple<string, string>, Style> NewStyles { get; set; }

        public IDictionary<Tuple<string, string, string>, Wheel> NewWheels { get; set; }
    }
}