﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;
using VelocityWheels.Data.Enum;
using VelocityWheels.Data.Models;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    public class WheelProsService : ServiceBase
    {
        private readonly Object _lockObj = new object();

        public WheelProsService() : this(null)
        {
        }

        public WheelProsService(VelocityWheelsContext context) : base(context)
        {
            Logger = AppLogger.GetLogger(this);
        }

        public void PullVehicles()
        {
            Logger.Info("WheelProsService.PullVehicles Starting ...");
            var wpVehicles = Context.Classifications.Where(c => c.ApiId == (int)ApiIdEnum.WheelPros);
            Context.Classifications.RemoveRange(wpVehicles);

            using (var context = CreateNewContext())
            {
                List<Classification> newVehicles = new List<Classification>();

                var years = GetYears();
                foreach (var year in years)
                {
                    Logger.Info($"WheelProsService.PullVehicles pulling year {year}");
                    var makes = GetMakes(year);
                    Parallel.ForEach(makes, make =>
                    {
                        var models = GetModels(year, make);
                        foreach (var model in models)
                        {
                            if (model.MoreData)
                            {
                                var subModels = GetSubModels(year, make, model.Name);
                                lock (_lockObj)
                                {
                                    foreach (var subModel in subModels)
                                    {
                                        newVehicles.Add(new Classification()
                                        {
                                            ApiId = (int)ApiIdEnum.WheelPros,
                                            //ChassisId = subModel.VehicleCode,
                                            Year = year,
                                            Make = make,
                                            Model = model.Name,
                                            SubModel = subModel.Name
                                        });
                                    }
                                }
                            }
                            else
                            {
                                lock (_lockObj)
                                {
                                    newVehicles.Add(new Classification()
                                    {
                                        ApiId = (int)ApiIdEnum.WheelPros,
                                        //ChassisId = model.VehicleCode,
                                        Year = year,
                                        Make = make,
                                        Model = model.Name
                                    });
                                }                                
                            }
                        }
                    });

                }

                if (!newVehicles.Any())
                {
                    return;
                }

                List<Classification> toSave = new List<Classification>();
                foreach (var c in newVehicles)
                {
                    toSave.Add(c);
                    if (toSave.Count >= 10000)
                    {
                        Logger.Info("WheelProsService.PullVehicles processing " + toSave.Count);
                        Context.Classifications.AddRange(toSave);
                        Context.SaveChanges();
                        toSave.Clear();
                    }
                }

                Context.Classifications.AddRange(toSave);
                Context.SaveChanges();
            }

            Logger.Info("WheelProsService.PullVehicles Complete ");
        }

        public IList<int> GetYears()
        {
            string url = "http://asp.wheelpros.com/cs/ws/dealerline.ashx?service=years";

            var xmlData = "";
            using (var wc = new MyWebClient())
            {
                xmlData = wc.DownloadString(url);
            }

            //var doc = XDocument.Load(xmlData);
            var yearsDoc = XDocument.Parse(xmlData);
            if (yearsDoc.Root == null)
            {
                return null;
            }
            var query = from d in yearsDoc.Root.Descendants("year") select d;

            return query.Select(el => int.Parse(el.Value)).ToList();
        }

        public IList<string> GetMakes(int year)
        {
            string url = $"http://asp.wheelpros.com/cs/ws/dealerline.ashx?service=makes&year={year}";

            var xmlData = "";
            using (var wc = new MyWebClient())
            {
                xmlData = wc.DownloadString(url);
            }

            var doc = XDocument.Parse(xmlData);
            if (doc.Root == null)
            {
                return null;
            }

            var query = from d in doc.Root.Descendants("make") select d;

            return query.Select(el => el.Value).ToList();
        }

        public IList<WheelProsModel> GetModels(int year, string make)
        {
            string url = $"http://asp.wheelpros.com/cs/ws/dealerline.ashx?service=models&year={year}&make={make}";

            var xmlData = "";
            using (var wc = new MyWebClient())
            {
                xmlData = wc.DownloadString(url);
            }

            var doc = XDocument.Parse(xmlData);
            if (doc.Root == null)
            {
                return null;
            }

            var query = from d in doc.Root.Descendants("model") select d;

            return query.Select(el => new WheelProsModel()
            {
                Name = el.Value,
                MoreData = el.Attribute("moredata")?.Value == "1",
                VehicleCode = el.Attribute("vehiclecd")?.Value
            }).ToList();
        }

        public IList<WheelProsSubModel> GetSubModels(int year, string make, string model)
        {
            string url = $"http://asp.wheelpros.com/cs/ws/dealerline.ashx?service=submodel&year={year}&make={make}&model={model}";

            var xmlData = "";
            using (var wc = new MyWebClient())
            {
                xmlData = wc.DownloadString(url);
            }

            var doc = XDocument.Parse(xmlData);
            if (doc.Root == null)
            {
                return null;
            }

            var query = from d in doc.Root.Descendants("submodel") select d;

            return query.Select(el => new WheelProsSubModel()
            {
                Name = el.Value,
                MoreData = el.Attribute("moredata")?.Value == "1",
                VehicleCode = el.Attribute("vehiclecd")?.Value
            }).ToList();
        }
    }
}
