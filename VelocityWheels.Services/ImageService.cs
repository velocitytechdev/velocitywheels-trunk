﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using ImageMagick;
using VelocityWheels.Data.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    using Class = ImageService;

    public class ImageService : ServiceBase
    {
        readonly log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(ImageService));

        private static string LocalContentPath { get; } = ConfigurationManager.AppSettings["VehicleContentPath"];

        private static string LocalWheelContentPath { get; } = ConfigurationManager.AppSettings["WheelContentPath"];

        private static string ThumbnailsPath => string.Format(LocalContentPath, "thumbnails\\");

        private static string WheelThumbnailsPath => $"{LocalWheelContentPath}thumbnails\\";

        public ImageService()
        {
        }

        public ImageService(VelocityWheelsContext context) : base(context)
        {
        }

        public static string GetFileSystemVehicleThumbnailPath(string imageName)
        {
            if (string.IsNullOrWhiteSpace(imageName))
            {
                return null;
            }

            var filePath = ThumbnailsPath + imageName;
            return filePath;
        }
        
        public static string GetFileSystemVehicleImagePath(string imageName)
        {
            if (string.IsNullOrWhiteSpace(imageName))
            {
                return null;
            }

            var baseUrl = LocalContentPath;

            var filePath = string.Format(baseUrl, imageName);
            return filePath;
        }

        public void CreateThumbnailsFromVehicleImages()
        {
            var thumbnails = ThumbnailsPath;
            var vehicleImages = Context.VehicleImages.Where(v => v.FileName != null && v.Thumbnail == null).ToList();
            foreach (var vehicleImage in vehicleImages)
            {
                CreateThumbnail(LocalContentPath, thumbnails, vehicleImage);
                Context.SaveChanges();
            }
        }

        public void CreateThumbnailFromVehicleImage(int vehicleId)
        {
            var thumbnails = ThumbnailsPath;
            var vehicleImages = Context.VehicleImages.Where(v => v.VehicleID == vehicleId && v.FileName != null && v.Thumbnail == null).ToList();
            foreach (var vehicleImage in vehicleImages)
            {
                CreateThumbnail(LocalContentPath, thumbnails, vehicleImage);
            }

            Context.SaveChanges();
        }

        public void CreateThumbnailFromVehicleImageId(int vehicleImageId)
        {
            var thumbnails = ThumbnailsPath;
            var vehicleImages = Context.VehicleImages.Where(v => v.VehicleImageID == vehicleImageId && v.FileName != null && v.Thumbnail == null).ToList();
            foreach (var vehicleImage in vehicleImages)
            {
                CreateThumbnail(LocalContentPath, thumbnails, vehicleImage);
            }

            Context.SaveChanges();
        }

        public void CreateThumbnailFromVehicleImage(IEnumerable<int> vehicleIds)
        {
            var thumbnails = ThumbnailsPath;
            var vehicleImages = Context.VehicleImages.Where(v => vehicleIds.Contains(v.VehicleID.Value) && v.FileName != null && v.Thumbnail == null).ToList();
            foreach (var vehicleImage in vehicleImages)
            {
                CreateThumbnail(LocalContentPath, thumbnails, vehicleImage);
            }

            Context.SaveChanges();
        }


        private void CreateThumbnail(string localPath, string thumbnails, VehicleImage vehicleImage)
        {
            if (vehicleImage.Thumbnail == null)
            {
                string filePath = string.Format(localPath, vehicleImage.FileName);
                try
                {
                    using (MagickImage image = new MagickImage(filePath))
                    {
                        MagickGeometry size = new MagickGeometry(310, 150) { IgnoreAspectRatio = true };
                        image.Resize(size);
                        string thumb = thumbnails + vehicleImage.FileName;
                        image.Write(thumb);
                    }
                    vehicleImage.Thumbnail = vehicleImage.FileName;
                }
                catch (Exception e)
                {
                    _logger.LogException(e.Message, e);
                }
            }
        }

        public string CreateWheelThumbnail(string wheelUrl, string brand)
        {
            var localPath = Class.WheelThumbnailsPath;
            var brandPath = Path.Combine(localPath, brand);

            // Check for already existing file
            string urlfileName;

            Class.GetFileNameAndExt(wheelUrl, out urlfileName);

            try
            {
                // download file
                var fileName = this.GetImageFromUrl(wheelUrl, WheelThumbnailsPath);

                // resize for storage
                var originalFile = Path.Combine(localPath, fileName);

                using (var image = new MagickImage(originalFile))
                {
                    var size = new MagickGeometry(300, 300) { IgnoreAspectRatio = true };

                    image.Resize(size);

                    if (!Directory.Exists(brandPath))
                    {
                        Directory.CreateDirectory(brandPath);
                    }

                    var thumb = Path.Combine(brandPath, fileName);

                    image.Write(thumb);
                }

                // delete downladed original file
                File.Delete(originalFile);

                return fileName;
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
            }

            return null;
        }

        public string GetImageFromUrl(string url, string localPath)
        {
            byte[] data;
            using (var wc = new MyWebClient())
            {
                data = wc.DownloadData(url);
            }
            Image img = Image.FromStream(new MemoryStream(data));
            using (var ms = new MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] b = ms.ToArray();
                string fileName;
                var ext = GetFileNameAndExt(url, out fileName);
                var filePath = localPath + fileName;
                //var fn = $"{Guid.NewGuid()}.{ext}";
                var imageType = "image/" + ext;
                System.IO.File.WriteAllBytes(filePath, b);
                return fileName;
            }
        }

        private static string GetFileNameAndExt(string url, out string fileName)
        {
            var ext = url.Substring(url.LastIndexOf('.') + 1);
            fileName = url.Substring(url.LastIndexOf('/') + 1);
            return ext;
        }

        public void ShrinkVehicleImage(string fromFile, string toFile = null)
        {
            // resize for storage
            string originalFile = fromFile;

            using (MagickImage image = new MagickImage(originalFile))
            {
                MagickGeometry size = new MagickGeometry(1200, 629) { IgnoreAspectRatio = true };
                image.Resize(size);
                if (toFile != null)
                {
                    image.Write(toFile);
                }
                else
                {
                    image.Write(fromFile);
                }
            }
        }

        public Bitmap CropImage(Image originalImage, Rectangle sourceRectangle, Rectangle? destinationRectangle = null)
        {
            if (destinationRectangle == null)
            {
                destinationRectangle = new Rectangle(Point.Empty, sourceRectangle.Size);
            }

            var croppedImage = new Bitmap(destinationRectangle.Value.Width,
                destinationRectangle.Value.Height);
            using (var graphics = Graphics.FromImage(croppedImage))
            {
                graphics.DrawImage(originalImage, destinationRectangle.Value, sourceRectangle, GraphicsUnit.Pixel);
            }
            return croppedImage;
        }
    }
}
