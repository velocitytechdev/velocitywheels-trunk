﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VelocityWheels.Data.Models;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    public class AccountService : ServiceBase
    {
        readonly log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(AccountService));

        public AccountService()
        {
        }

        public AccountService(VelocityWheelsContext context) : base(context)
        {
        }

        public bool IsValidToken(string token, string url)
        {
            _logger.Info($"validating token {token} for {url}");
            var account = GetAccountFromToken(token, url);
            if (account != null)
            {
                return true;
            }

            _logger.Warn($"token {token} for {url} not valid");
            return false;
        }

        public SubAccount GetAccountFromToken(string token, string url)
        {
            try
            {
                var account = Context.SubAccounts.Include("Account").
                    FirstOrDefault(sa => sa.Account.Token == token && sa.AccountUrl == url && sa.Active && sa.Account.Active);
                return account;
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
            }

            return null;
        }

        public Account GetAccountFromToken(string token)
        {
            try
            {
                var account = Context.Accounts.FirstOrDefault(a => a.Token == token && a.Active);
                return account;
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
            }

            return null;
        }

        public List<AccountModel> GetAccounts()
        {
            var accounts = Context.Accounts.Include("SubAccounts").ToList();
            return accounts.Select(a => new AccountModel(a)).ToList();
        }

        public AccountModel GetAccount(long id)
        {
            var accounts = Context.Accounts.
                Include("SubAccounts").
                Include("AccountBrands").
                Include("AccountBrands.Brand").
                Where(a => a.Id == id).
                ToList();
            return accounts.Select(a => new AccountModel(a)).FirstOrDefault();
        }

        public Account SaveOrUpdate(AccountModel accountModel)
        {
            var account = Context.Accounts.
                Include("SubAccounts").
                Include("AccountBrands").
                Include("AccountBrands.Brand").
                FirstOrDefault(a => a.Id == accountModel.Id);

            if (account == null)
            {
                // check for existing token first
                ValidateToken(accountModel);

                account = new Account()
                {
                    Active = true,
                    AccountName = accountModel.AccountName,
                    LastUpdated = DateTime.UtcNow,
                    LastUpdatedBy = GetLoggedInUserName(),
                    Token = accountModel.Token,
                    TireWeb = accountModel.TireWeb,
                    TireWebBaseUrl = accountModel.TireWebBaseUrl,
                    TireProducts = accountModel.TireProducts,
                    WheelProducts = accountModel.WheelProducts,
                    VehicleImages = accountModel.VehicleImages,
                    VehicleSelection = accountModel.VehicleSelection,
                    AccountBrands = new List<AccountBrands>(),
                    SubAccounts = new List<SubAccount>()
                };
                // Save sub accounts
                UpdateSubAccounts(accountModel, account);

                // save new account brands
                UpdateAccountBrands(accountModel, account);

                Context.Accounts.Add(account);
            }
            else
            {
                if (accountModel.Token != account.Token)
                {
                    ValidateToken(accountModel);
                }

                account.Token = accountModel.Token;
                account.AccountName = accountModel.AccountName;
                account.Active = account.Active;
                account.TireWeb = accountModel.TireWeb;
                account.TireWebBaseUrl = accountModel.TireWebBaseUrl;
                account.TireProducts = accountModel.TireProducts;
                account.WheelProducts = accountModel.WheelProducts;
                account.VehicleImages = accountModel.VehicleImages;
                account.VehicleSelection = accountModel.VehicleSelection;
                account.LastUpdated = DateTime.UtcNow;
                account.LastUpdatedBy = GetLoggedInUserName();

                // Save sub accounts
                UpdateSubAccounts(accountModel, account);

                // save new account brands
                UpdateAccountBrands(accountModel, account);
            }

            Context.SaveChanges();

            return account;
        }

        private void UpdateAccountBrands(AccountModel accountModel, Account account)
        {
            if (accountModel.AccountBrands == null) return;

            var selectedIds = accountModel.AccountBrands.Select(ab => ab.Id).ToList();
            List<AccountBrands> newAccountBrands = (from accountBrand in accountModel.AccountBrands
                let acbrand = account.AccountBrands.FirstOrDefault(ab => ab.Id == accountBrand.Id)
                where acbrand == null
                select new AccountBrands()
                {
                    BrandId = accountBrand.Brand.Id, LastUpdated = DateTime.UtcNow, LastUpdatedBy = GetLoggedInUserName()
                }).ToList();

            var deleted = account.AccountBrands.Where(ab => !selectedIds.Contains(ab.Id)).ToList();
            deleted.ForEach(d => account.AccountBrands.Remove(d));
            account.AccountBrands.AddRange(newAccountBrands);
        }

        private void UpdateSubAccounts(AccountModel accountModel, Account account)
        {
            List<SubAccount> newSubAccounts = new List<SubAccount>();
            List<SubAccount> deleted = new List<SubAccount>();
            foreach (var subAccount in accountModel.SubAccounts)
            {
                var sub = account.SubAccounts.FirstOrDefault(s => s.Id == subAccount.Id);
                if (sub == null)
                {
                    newSubAccounts.Add(new SubAccount()
                    {
                        Active = true,                        
                        AccountUrl = subAccount.AccountUrl,
                        LastUpdated = DateTime.UtcNow,
                        LastUpdatedBy = GetLoggedInUserName()
                    });
                }
                else if (string.IsNullOrWhiteSpace(subAccount.AccountUrl))
                {
                    deleted.Add(sub);
                }
                else
                {
                    sub.AccountUrl = subAccount.AccountUrl;
                }
            }

            account.SubAccounts.AddRange(newSubAccounts);
            deleted.ForEach(d => account.SubAccounts.Remove(d));
        }

        private void ValidateToken(AccountModel accountModel)
        {
            if (Context.Accounts.Any(a => a.Token == accountModel.Token))
            {
                throw new Exception("Token already used, please choose another.");
            }
        }
    }
}
