﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VelocityWheels.Data.Enum;
using VelocityWheels.Data.Models;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    public class FuelApiService : ServiceBase
    {
        public FuelApiService() : this(null)
        {
        }

        public FuelApiService(VelocityWheelsContext context) : base(context)
        {
            Logger = AppLogger.GetLogger(this);
        }

        public List<string> GetYearsFuelApi()
        {
            var jsonData = "";
            var url = $"https://api.fuelapi.com/v1/json/modelYears/?api_key={VehicleConfiguration.FUEL_API_TOKEN}";
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<string> years = JsonConvert.DeserializeObject<List<string>>(jsonData);
            return years.Select(y => y).OrderByDescending(y => y).ToList();
        }

        public List<FuelAPIMake> GetMakesFuelApi(string year)
        {
            var jsonData = "";
            var url = string.Format("https://api.fuelapi.com/v1/json/makes/?year={1}&api_key={0}",
                VehicleConfiguration.FUEL_API_TOKEN, year);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<FuelAPIMake> makes = JsonConvert.DeserializeObject<List<FuelAPIMake>>(jsonData);
            return makes.ToList();
        }

        public List<FuelAPIModel> GetModelsFuelApi(string year, int makeId)
        {
            try
            {
                string jsonData;
                //var mID = Global.Session.Instance.FuelAPIMakes.FirstOrDefault(make => make.name == m).id;
                var url =
                    $"https://api.fuelapi.com/v1/json/models/{makeId}/?year={year}&api_key={VehicleConfiguration.FUEL_API_TOKEN}";
                using (var wc = new MyWebClient())
                {
                    jsonData = wc.DownloadString(url);
                }
                List<FuelAPIModel> models = JsonConvert.DeserializeObject<List<FuelAPIModel>>(jsonData);
                return models;
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
            }

            return new List<FuelAPIModel>();
        }

        public List<FuelAPISubModel> GetSubModelsFuelApi(string y, string m, string mdl)
        {
            var url = string.Format(
                "https://api.fuelapi.com/v1/json/vehicles/?year={1}&make={2}&model={3}&api_key={0}",
                VehicleConfiguration.FUEL_API_TOKEN, y, m, mdl);
            try
            {
                var jsonData = "";
                using (var wc = new MyWebClient())
                {
                    jsonData = wc.DownloadString(url);
                }
                List<FuelAPISubModel> subModels = JsonConvert.DeserializeObject<List<FuelAPISubModel>>(jsonData);
                foreach (var sm in subModels)
                {
                    if ((sm.trim ?? "") == "")
                        sm.trim = "Base";
                }
                return subModels.ToList();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message + "request = " + url);
                return new List<FuelAPISubModel>();
            }

        }

        public List<FuelAPISubModel> GetAllSubModels()
        {
            var list = Context.Classifications.
                Where(c => c.ApiId == (int) ApiIdEnum.FuelApi).
                Select(c => new FuelAPISubModel()
                {
                    Year = c.Year,
                    id = (int)c.AcesVehicleId,
                    bodytype_desc = c.BodyType,
                    model_name = c.Model,
                    make_name = c.Make,
                    trim = c.SubModel,
                    DriveTrain = c.DriveTrain,
                    bodytype = c.BodyType,
                }).ToList();
            return list;
        }

        public void MergeFuelApiEntries(bool refresh = true)
        {
            Logger.Info("MergeFuelApiEntries ... checking current data");
            object lockObj = new object();

            List<Classification> newClassifications = new List<Classification>();

            if (refresh)
            {
                var fuelItems = Context.Classifications.Where(c => c.ApiId == (int)ApiIdEnum.FuelApi);
                Context.Classifications.RemoveRange(fuelItems);
                Context.SaveChanges();
            }

            if (Context.Classifications.Any(c => c.ApiId == (int) ApiIdEnum.FuelApi))
                return;

            Logger.Info("MergeFuelApiEntries ... merging new data");

            List<string> years = GetYearsFuelApi();
            foreach (string year in years)
            {
                if (string.IsNullOrWhiteSpace(year))
                    continue;

                int apiyear = int.Parse(year);

                Logger.Info($"Building fuel api entries - processing year {year}");

                List<FuelAPIMake> makes = GetMakesFuelApi(year);
                Parallel.ForEach(makes, m =>
                {
                    List<FuelAPIModel> models = GetModelsFuelApi(year, m.id);
                    foreach (var fuelApiModel in models)
                    {
                        var submodels = GetSubModelsFuelApi(year, m.name, fuelApiModel.name);
                        foreach (FuelAPISubModel fuelApiSubModel in submodels)
                        {
                            lock (lockObj)
                            {
                                newClassifications.Add(new Classification()
                                {
                                    ApiId = (int)ApiIdEnum.FuelApi,
                                    Year = apiyear,
                                    AcesVehicleId = fuelApiSubModel.id,
                                    Make = fuelApiSubModel.make_name,
                                    Model = fuelApiSubModel.model_name,
                                    BodyType = fuelApiSubModel.bodytype_desc ?? fuelApiSubModel.bodytype,
                                    SubModel = fuelApiSubModel.trim,
                                    DriveTrain = fuelApiSubModel.DriveTrain
                                });
                            }
                        }
                    }
                });

                List<Classification> saveList = new List<Classification>();
                foreach (var newClassification in newClassifications)
                {
                    saveList.Add(newClassification);
                    if (saveList.Count >= 1000)
                    {
                        Logger.Info($"Building fuel api, saving changes {saveList.Count}");
                        Context.Classifications.AddRange(saveList);
                        Context.SaveChanges();
                        saveList.Clear();
                    }
                }

                Context.Classifications.AddRange(saveList);
                Context.SaveChanges();

                newClassifications.Clear();

                Logger.Info($"Building fuel api, saving changes, complete");
            }
        }
    }
}
