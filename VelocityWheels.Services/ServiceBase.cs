﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using VelocityWheels.Data.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Services
{
    public class ServiceBase : IModelService
    {
        private VelocityWheelsContext _context;
        private string _apiBaseUrl;
        private string _vehicleImagesBaseUrl;

        protected log4net.ILog Logger { get; set; }

        protected string ApiBaseUrl => _apiBaseUrl ?? (_apiBaseUrl = VelocityWheelsSettings.GetApiBaseUrl);

        public string VehicleImagesBaseUrl
            => _vehicleImagesBaseUrl ?? (_vehicleImagesBaseUrl = VelocityWheelsSettings.GetVehicleImagesBaseUrl);

        public VelocityWheelsContext Context
        {
            get { return _context ?? (_context = VelocityWheelsContext.Create()); }
            set { _context = value; }
        }

        public VelocityWheelsContext CreateNewContext()
        {
            return VelocityWheelsContext.Create();
        }

        public ServiceBase()
        {
        }

        public ServiceBase(VelocityWheelsContext context)
        {
            _context = context;

        }

        public string GetLoggedInUserName()
        {
            try
            {
                string username = FormsAuthentication.Decrypt(HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                return username;
            }
            catch (Exception e)
            {
                // cookie or request invalid, no user
                Logger?.LogException(e.Message, e);
            }

            return null;
        }

        /// <summary>
        ///     Saves the models to the specified context or <see cref="Context"/> if <c>null</c>.
        /// </summary>
        /// <param name="context">
        ///     The context to which to save, or <c>null</c> if <see cref="Context"/> should be used.
        /// </param>
        /// <param name="timestamp">
        ///     The save time in UTC or <c>null</c> if the current time should be used.
        /// </param>
        /// <param name="activationStatus">
        ///     A value indicating whether the models are active or <c>null</c> if activation 
        ///     status should not be assigned.
        /// </param>
        /// <returns>
        ///     The result of <c>DbContext.SaveChanges</c>.
        /// </returns>
        public int SaveModels(DbContext context = null, DateTime? timestamp = null, bool? activationStatus = null)
        {
            var localContext = context ?? this.Context;

            return localContext.SaveModels(this.GetLoggedInUserName(), timestamp, activationStatus);
        }


        /// <summary>
        ///     Sets the audit fields of auditable models.
        /// </summary>
        /// <param name="model">
        ///     The model to be changed.
        /// </param>
        /// <param name="timestamp">
        ///     The timestamp, in UTC, to use or <c>null</c> if the current time should be used.
        /// </param>
        public void SetAuditFields(IAuditedModel model, DateTime? timestamp = null)
        {
            this.SetAuditFields(new[] { model }, timestamp);
        }


        /// <summary>
        ///     Sets the audit fields of auditable models.
        /// </summary>
        /// <param name="models">
        ///     The models to be changed.
        /// </param>
        /// <param name="timestamp">
        ///     The timestamp, in UTC, to use or <c>null</c> if the current time should be used.
        /// </param>
        public void SetAuditFields(IEnumerable<IAuditedModel> models, DateTime? timestamp = null)
        {
            var localTimestamp = timestamp ?? DateTime.UtcNow;
            var userName = this.GetLoggedInUserName();

            foreach (var model in models)
            {
                model.SetAuditFields(userName, localTimestamp);
            }
        }

        public static bool MapIfParsed(string value, Action<decimal?> assignValue)
        {
            var result = value.TryParse<decimal>(decimal.TryParse);

            if (result.Found)
            {
                assignValue(result.Value);
            }

            return result.Found;
        }

        public static bool MapIfParsedInt(string value, Action<int?> assignValue)
        {
            var result = value.TryParse<int>(int.TryParse);

            if (result.Found)
            {
                assignValue(result.Value);
            }

            return result.Found;
        }

        public static bool MapIfParsedLong(string value, Action<long?> assignValue)
        {
            var result = value.TryParse<long>(long.TryParse);

            if (result.Found)
            {
                assignValue(result.Value);
            }

            return result.Found;
        }

        public static bool MapIfParsed(string value, NumberStyles styles, IFormatProvider format, Action<decimal?> assignValue)
        {
            var result = value.TryParse<decimal>(styles, format, decimal.TryParse);

            if (result.Found)
            {
                assignValue(result.Value);
            }

            return result.Found;
        }
    }
}
