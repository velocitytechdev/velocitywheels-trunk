﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Razor.Text;
using CPS_Socks.Tests.Models;
using CsvHelper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using VelocityWheels;
using VelocityWheels.Data.Enum;
using VelocityWheels.Data.Models;
using VelocityWheels.DriveRight;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;
using VehicleImage = VelocityWheels.Data.Models.VehicleImage;

namespace CPS_Socks.Tests
{
    // These aren't unit tests for now, just a quick and dirty way to load external api data into the db. In the future
    // These will need to move to some sort of background job to sync and load data on a more consistant basis (Must be approved first)
    [TestClass]
    public class UnitTest1
    {
        //[TestMethod]
        public void TestGetMakesModelsFuelApi()
        {
            var driveRightData = GetDriveRightData();

            List<FuelAPIMake> makes = GetMakesFuelApi();
            Dictionary<string, List<FuelAPIModel>> fuelDictionary = new Dictionary<string, List<FuelAPIModel>>();
            foreach (FuelAPIMake fuelApiMake in makes)
            {
                try
                {
                    List<FuelAPIModel> models = GetModelsFuelApi(fuelApiMake.id);
                    fuelDictionary.Add(fuelApiMake.name, models);
                    //Console.Out.WriteLine($"{fuelApiMake.id} : {fuelApiMake.name} : {models.Count} models");

                }
                catch (Exception e)
                {
                    Console.Out.WriteLine($"{fuelApiMake.id} : {fuelApiMake.name} : {e.Message}");
                }
            }

            Dictionary<string, DriveRight> notFounDictionary = new Dictionary<string, DriveRight>();
            Dictionary<string, DriveRight> founDictionary = new Dictionary<string, DriveRight>();

            using (VelocityWheelsContext context = VelocityWheelsContext.Create())
            {
                var mmaps = context.MissingMaps.ToList();
                foreach (var missingMap in mmaps)
                {
                    context.MissingMaps.Remove(missingMap);
                }

                var vmaps = context.VehicleMappings.ToList();
                foreach (var vmap in vmaps)
                {
                    context.VehicleMappings.Remove(vmap);
                }

                foreach (DriveRight driveRight in driveRightData)
                {
                    List<FuelAPIModel> models;
                    string key = driveRight.Make.Trim().ToLower() + driveRight.Model.ToLower();
                    if (fuelDictionary.TryGetValue(driveRight.Make.Trim(), out models))
                    {
                        var model =
                            models.FirstOrDefault(
                                m => m.name.Equals(driveRight.Model.Trim(), StringComparison.InvariantCultureIgnoreCase));
                        if (model != null)
                        {
                            if (!founDictionary.ContainsKey(key))
                            {
                                founDictionary.Add(key, driveRight);
                                context.VehicleMappings.Add(new VehicleMapping()
                                {
                                    FromApiId = (int) ApiIdEnum.DriveRight,
                                    ToApiId = (int) ApiIdEnum.FuelApi,
                                    FromMake = driveRight.Make,
                                    ToMake = driveRight.Make,
                                    FromModel = driveRight.Model,
                                    ToModel = model.name
                                });
                            }
                            continue;
                        }
                    }

                    if (!notFounDictionary.ContainsKey(key))
                    {
                        notFounDictionary.Add(key, driveRight);

                        context.VehicleMappings.Add(new VehicleMapping()
                        {
                            FromApiId = (int) ApiIdEnum.DriveRight,
                            ToApiId = (int) ApiIdEnum.FuelApi,
                            FromMake = driveRight.Make,
                            ToMake = models != null ? driveRight.Make : null,
                            FromModel = driveRight.Model,
                            ToModel = null
                        });
                    }
                }

                /*foreach (var driveRight in notFounDictionary)
                {
                    context.MissingMaps.Add(new MissingMap()
                    {
                        ApiId = (int)ApiIdEnum.DriveRight,
                        Make = driveRight.Value.Make,
                        Model = driveRight.Value.Model
                    });
                }*/

                context.SaveChanges();
            }



        }


        public List<string> GetYearsFuelApi()
        {
            var jsonData = "";
            var url = $"https://api.fuelapi.com/v1/json/modelYears/?api_key={VehicleConfiguration.FUEL_API_TOKEN}";
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<string> years = JsonConvert.DeserializeObject<List<string>>(jsonData);
            return years.Select(y => y).OrderByDescending(y => y).ToList();
        }

        private List<FuelAPIMake> GetMakesFuelApi()
        {
            var jsonData = "";
            var url = $"https://api.fuelapi.com/v1/json/makes/?api_key={VehicleConfiguration.FUEL_API_TOKEN}";
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<FuelAPIMake> makes = JsonConvert.DeserializeObject<List<FuelAPIMake>>(jsonData);

            return makes;
        }

        private List<FuelAPIMake> GetMakesFuelApi(string year)
        {
            var jsonData = "";
            var url = string.Format("https://api.fuelapi.com/v1/json/makes/?year={1}&api_key={0}",
                VehicleConfiguration.FUEL_API_TOKEN, year);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<FuelAPIMake> makes = JsonConvert.DeserializeObject<List<FuelAPIMake>>(jsonData);
            return makes.ToList();
        }

        public List<FuelAPIModel> GetModelsFuelApi(int makeId)
        {
            string jsonData;
            //var mID = Global.Session.Instance.FuelAPIMakes.FirstOrDefault(make => make.name == m).id;
            var url = $"https://api.fuelapi.com/v1/json/models/{makeId}/?api_key={VehicleConfiguration.FUEL_API_TOKEN}";
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<FuelAPIModel> models = JsonConvert.DeserializeObject<List<FuelAPIModel>>(jsonData);
            return models;
        }

        public List<FuelAPIModel> GetModelsFuelApi(string year, int makeId)
        {
            try
            {
                string jsonData;
                //var mID = Global.Session.Instance.FuelAPIMakes.FirstOrDefault(make => make.name == m).id;
                var url =
                    $"https://api.fuelapi.com/v1/json/models/{makeId}/?year={year}&api_key={VehicleConfiguration.FUEL_API_TOKEN}";
                using (var wc = new MyWebClient())
                {
                    jsonData = wc.DownloadString(url);
                }
                List<FuelAPIModel> models = JsonConvert.DeserializeObject<List<FuelAPIModel>>(jsonData);
                return models;
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
            }

            return new List<FuelAPIModel>();
        }

        public List<FuelAPISubModel> GetSubModelsFuelApi(string y, string m, string mdl)
        {
            var url = string.Format(
                "https://api.fuelapi.com/v1/json/vehicles/?year={1}&make={2}&model={3}&api_key={0}",
                VehicleConfiguration.FUEL_API_TOKEN, y, m, mdl);
            try
            {
                var jsonData = "";
                using (var wc = new MyWebClient())
                {
                    jsonData = wc.DownloadString(url);
                }
                List<FuelAPISubModel> subModels = JsonConvert.DeserializeObject<List<FuelAPISubModel>>(jsonData);
                foreach (var sm in subModels)
                {
                    if ((sm.trim ?? "") == "")
                        sm.trim = "Base";
                }
                return subModels.ToList();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message + "request = " + url);
                return new List<FuelAPISubModel>();
            }

        }

        public List<DriveRight> GetDriveRightData()
        {
            List<DriveRight> list = new List<DriveRight>();

            using (
                SqlConnection connection =
                    new SqlConnection(
                        "data source=.;initial catalog=VelocityTech;persist security info=True;user id=sa;password=Sw234rfds;MultipleActiveResultSets=True")
            )
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("select YearID, MakeName, ModelName from DriveRight", connection);
                var reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var year = (int) reader["YearID"];
                        var make = (string) reader["MakeName"];
                        var model = (string) reader["ModelName"];
                        list.Add(new DriveRight() {Year = year, Make = make, Model = model});
                    }
                }

            }
            return list;
        }

        //[TestMethod]
        public void TestLoadClassifications()
        {
            var yearsList = GetDriveRightYears();

            using (var context = VelocityWheelsContext.Create())
            {
                foreach (string year in yearsList)
                {
                    var makes = GetMakesDriveRight(year);
                    foreach (var make in makes)
                    {
                        var models = GetModelsDriveRight(year, make);
                        foreach (var model in models)
                        {
                            var bodyTypes = GetBodyTypesDriveRight(year, make, model);
                            foreach (var bodyType in bodyTypes)
                            {
                                var subModels = GetSubModelsDriveRight(year, make, model, bodyType);
                                foreach (var subModel in subModels)
                                {
                                    context.Classifications.Add(new Classification()
                                    {
                                        ApiId = (int) ApiIdEnum.DriveRight,
                                        Year = Int32.Parse(year),
                                        Make = make,
                                        Model = model,
                                        BodyType = bodyType,
                                        SubModel = subModel
                                    });
                                }
                            }
                        }
                        context.SaveChanges();
                    }
                }
                context.SaveChanges();
            }

        }

        public IOrderedEnumerable<string> GetMakesDriveRight(string y)
        {
            var wsc = new VelocityWheels.DriveRight.webserviceSoapClient();
            var makes = wsc.GetAAIAManufacturers(VehicleConfiguration.DRIVE_RIGHT_USERNAME,
                VehicleConfiguration.DRIVE_RIGHT_PASSWORD, Convert.ToInt32(y), 1 /*USA*/);
            IOrderedEnumerable<string> drMakes = makes.Select(m => m.Manufacturer).OrderBy(m => m);
            return drMakes;
        }

        public IOrderedEnumerable<string> GetModelsDriveRight(string y, string m)
        {
            var wsc = new VelocityWheels.DriveRight.webserviceSoapClient();
            var models = wsc.GetAAIAModels(VehicleConfiguration.DRIVE_RIGHT_USERNAME, VehicleConfiguration.DRIVE_RIGHT_PASSWORD,
                Convert.ToInt32(y), 1 /*USA*/, m);
            IOrderedEnumerable<string> drModels = models
                .Select(mdl => mdl.Model)
                .OrderBy(mdl => mdl);

            return drModels;
        }

        public IOrderedEnumerable<string> GetBodyTypesDriveRight(string y, string m, string mdl)
        {
            var wsc = new VelocityWheels.DriveRight.webserviceSoapClient();
            var bodyTypes = wsc.GetAAIABodyTypes(VehicleConfiguration.DRIVE_RIGHT_USERNAME,
                VehicleConfiguration.DRIVE_RIGHT_PASSWORD, Convert.ToInt32(y), 1 /*USA*/, m, mdl);
            return bodyTypes
                .Select(b => b.BodyType)
                .OrderBy(b => b);
        }

        public IOrderedEnumerable<string> GetSubModelsDriveRight(string y, string m, string mdl, string b)
        {
            var wsc = new VelocityWheels.DriveRight.webserviceSoapClient();
            AAIASubModelReturn[] subModels = wsc.GetAAIASubModels(VehicleConfiguration.DRIVE_RIGHT_USERNAME,
                VehicleConfiguration.DRIVE_RIGHT_PASSWORD, Convert.ToInt32(y), 1 /*USA*/, m, mdl, b);
            return subModels
                .Select(sm => sm.SubModel)
                .OrderBy(sm => sm);
        }

        private static IOrderedEnumerable<string> GetDriveRightYears()
        {
            var wsc = new VelocityWheels.DriveRight.webserviceSoapClient();
            var years = wsc.GetAAIAYears(VehicleConfiguration.DRIVE_RIGHT_USERNAME, VehicleConfiguration.DRIVE_RIGHT_PASSWORD);

            IOrderedEnumerable<string> yearsList = years.Where(y => Convert.ToInt32(y.Year) >= 1980)
                .Select(y => y.Year)
                .OrderByDescending(y => y);
            return yearsList;
        }

        //[TestMethod]
        public void TestUpdateWheelImages()
        {
            IWheelService service = new WheelService();
            try
            {
                int count = service.UpdateImagesForAllWheels();
                Console.Out.WriteLine($"Number Updated {count}");
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message + e.StackTrace);
            }
        }

        //[TestMethod]
        public void TestShrinkImagesForPreviews()
        {
            try
            {
                var service = new ImageService();
                service.CreateThumbnailsFromVehicleImages();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message + e.StackTrace);
            }

        }

        [TestMethod]
        public void TestImportWheelDataFromFile()
        {
            string fileName = "C:\\Users\\Home\\Documents\\VelocityT\\WheelPros\\CWSupload_wheelPros-basic.csv";
            IWheelService service = new WheelService();
            using (var stream = File.Open(fileName, FileMode.Open))
            {
                var results = service.ImportWheelDataFromFile(stream);
                Assert.IsNotNull(results);
                Assert.IsTrue(results.Any());
                foreach (var wheelSpec in results)
                {
                    //Console.Out.WriteLine($"{wheelSpec.PartNumber} {wheelSpec.BrandName} {wheelSpec.StyleName} {wheelSpec.FinishName} {wheelSpec.ThumbnailImage}");
                }
            }
            
        }        
    }
}
