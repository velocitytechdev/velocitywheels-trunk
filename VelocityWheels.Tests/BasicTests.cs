﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VelocityWheels.Services;
using VelocityWheels.Util;

namespace CPS_Socks.Tests
{
    [TestClass]
    public class BasicTests
    {
        [TestMethod]
        public void TestPasswordEncrypt()
        {
            string salt;
            var encrypted = PasswordUtil.EncryptPassword("123456", out salt);

            Assert.IsNotNull(salt);
            Assert.IsNotNull(encrypted);

            Console.Out.WriteLine($"salt = {salt}");
            Console.Out.WriteLine($"encrypted = {encrypted}");
        }

        [TestMethod]
        public void TestPasswordVerify()
        {
            string username = "vtwsadmin";
            string salt;
            var encrypted = PasswordUtil.EncryptPassword("!velocity1", out salt);

            Assert.IsNotNull(salt);
            Assert.IsNotNull(encrypted);

            if (!PasswordUtil.ComparePasswords("!velocity1", "EB3B4912F23206945B66EA4A8AE13E05821B39E84775CE70BF171B10BA9C99AC", "EK6xXJVzhbILg4fXHgqNgg=="))
            {
                Assert.Fail("Password doesn't match");
            }

            Console.Out.WriteLine($"salt = {salt}");
            Console.Out.WriteLine($"encrypted = {encrypted}");
        }

        [TestMethod]
        public void TestWheelProsYears()
        {
            WheelProsService service = new WheelProsService();

            var years = service.GetYears();

            foreach (var year in years)
            {
                Console.Out.WriteLine(year);
            }
        }

        [TestMethod]
        public void TestWheelProsMakes()
        {
            WheelProsService service = new WheelProsService();

            var list = service.GetMakes(2017);

            foreach (var item in list)
            {
                Console.Out.WriteLine(item);
            }
        }

        [TestMethod]
        public void TestWheelProsModels()
        {
            WheelProsService service = new WheelProsService();

            var list = service.GetModels(2017, "Ford");

            foreach (var item in list)
            {
                Console.Out.WriteLine(item.Name + item.MoreData);
            }
        }

        [TestMethod]
        public void TestWheelProsSubModels()
        {
            WheelProsService service = new WheelProsService();

            var list = service.GetSubModels(2017, "Ford", "F-150");

            foreach (var item in list)
            {
                Console.Out.WriteLine(item);
            }
        }
    }
}
