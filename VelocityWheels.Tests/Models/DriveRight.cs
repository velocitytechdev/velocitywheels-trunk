﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CPS_Socks.Tests.Models
{
    public class DriveRight
    {
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
    }
}
