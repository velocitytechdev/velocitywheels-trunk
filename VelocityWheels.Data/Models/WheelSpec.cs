namespace VelocityWheels.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WheelSpec : IAuditedModelLegacy
    {
        public int WheelSpecID { get; set; }

        [StringLength(50)]
        public string APIProvider { get; set; }

        [StringLength(50)]
        public string PartNumber { get; set; }
        
        [ForeignKey("Wheel")]
        public int? WheelId { get; set; }

        [ForeignKey("WheelId")]
        public Wheel Wheel { get; set; }

        public decimal? Diameter { get; set; }

        public decimal? Width { get; set; }

        public string BoltPatternList { get; set; }

        [StringLength(255)]
        public string BoltPattern1 { get; set; }

        [StringLength(255)]
        public string BoltPattern2 { get; set; }

        [StringLength(255)]
        public string BoltPattern3 { get; set; }

        [StringLength(255)]
        public string BoltPattern4 { get; set; }

        public decimal? Bore { get; set; }

        [StringLength(255)]
        public string Offset { get; set; }

        /// <summary>
        ///     Backside Measurement. Distance from face of inner rim to inner face of outer rim.
        /// </summary>
        /// <see cref="http://www.americaneaglewheel.com/images/faq-pic.jpg"/>
        public decimal? BSM { get; set; }

        [StringLength(255)]
        public string CapNumber { get; set; }

        public decimal? LoadRating { get; set; }

        public decimal? Weight { get; set; }

        [StringLength(255)]
        public string LugType { get; set; }

        public decimal? BigBreak { get; set; }

        [StringLength(255)]
        public string LipSize { get; set; }

        public decimal? ProductMSRP { get; set; }

        public string ProductDescription { get; set; }

        public int? WheelImageID { get; set; }                           

        [StringLength(50)]
        public string LastUpdated { get; set; }
    }
}
