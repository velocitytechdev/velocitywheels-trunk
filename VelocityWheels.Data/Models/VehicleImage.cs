namespace VelocityWheels.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VehicleImage
    {
        public int VehicleImageID { get; set; }

        public int? ApiId { get; set; }

        public int? VehicleID { get; set; }

        public long? ChassisId { get; set; }

        // This is used for color matching etc.
        [StringLength(25)]
        public string ImageCode { get; set; }

        [StringLength(25)]
        public string Color { get; set; }

        public int? ProductFormatId { get; set; }

        public string URL { get; set; }

        [StringLength(255)]
        public string FileName { get; set; }

        [StringLength(255)]
        public string Thumbnail { get; set; }

        [StringLength(255)]
        public string ImageType { get; set; }

        public virtual List<WheelPosition> WheelPositions { get; set; }

        public virtual List<Classification> Classifications { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}
