﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VelocityWheels.Data.Models
{
    [Table("Wheel")]
    public class Wheel : IAuditedModel, IActivatedModel
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Brand")]
        public long? BrandId { get; set; }

        [ForeignKey("BrandId")]
        public virtual Brand Brand { get; set; }

        [ForeignKey("Finish")]
        public long FinishId { get; set; }

        [ForeignKey("FinishId")]
        public virtual Finish Finish { get; set; }

        [ForeignKey("Style")]
        public long StyleId { get; set; }

        [ForeignKey("StyleId")]
        public virtual Style Style { get; set; }

        public int? WheelImageId { get; set; }

        [StringLength(1024)]
        public string ThumbnailImage { get; set; }

        [StringLength(1024)]
        public string CatalogImage { get; set; }

        [StringLength(1024)]
        public string HighAngleImage { get; set; }

        [StringLength(1024)]
        public string FaceImage { get; set; }

        [StringLength(1024)]
        public string FrontMainImage { get; set; }

        [StringLength(1024)]
        public string FrontWindowImage { get; set; }

        [StringLength(1024)]
        public string FrontFaceImage { get; set; }

        [StringLength(1024)]
        public string RearMainImage { get; set; }

        [StringLength(1024)]
        public string RearWindowImage { get; set; }

        [StringLength(1024)]
        public string RearFaceImage { get; set; }

        [StringLength(1024)]
        public string BaseLayerImage { get; set; }        

        public bool Active { get; set; }

        public bool Customizable { get; set; }

        public DateTime LastUpdated { get; set; }

        [StringLength(50)]
        public string LastUpdatedBy { get; set; }

        public virtual List<WheelSpec> WheelSpecs { get; set; }

        public virtual List<WheelLayer> WheelLayers { get; set; }
    }
}
