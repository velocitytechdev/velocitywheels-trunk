﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace VelocityWheels.Data.Models
{
    [Table("Classification")]
    public class Classification
    {
        [Key]
        public long Id { get; set; }

        public int ApiId { get; set; }

        public int Year { get; set; }

        [StringLength(100)]
        public string Make { get; set; }

        [StringLength(100)]
        public string Model { get; set; }

        [StringLength(100)]
        public string BodyType { get; set; }

        [StringLength(100)]
        public string SubModel { get; set; }

        [StringLength(100)]
        public string DriveTrain { get; set; }

        [StringLength(30)]
        public string Region { get; set; }

        public int RegionId { get; set; }

        /*[StringLength(25)]
        public string ChassisId { get; set; }*/

        [StringLength(25)]
        public string ModelId { get; set; }        

        public long? AcesVehicleId { get; set; }

        [ForeignKey("Mapping")]
        public long? VehicleMappingId { get; set; }
        [ForeignKey("VehicleMappingId")]
        public virtual VehicleMapping Mapping { get; set; }

        public virtual ICollection<VehicleImage> VehicleImages { get; set; }
    }
}
