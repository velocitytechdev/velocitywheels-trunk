﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace VelocityWheels.Data.Models
{
    [Table("WheelLayer")]
    public class WheelLayer
    {
        [Key]
        public long Id { get; set; }

        public short Level { get; set; }

        [StringLength(50)]
        public string LevelName { get; set; }

        [ForeignKey("Wheel")]
        public int? WheelId { get; set; }

        [ForeignKey("WheelId")]
        public Wheel Wheel { get; set; }

        [StringLength(1024)]
        public string BaseLayerImage { get; set; }

        [StringLength(1024)]
        public string FrontLayerImage { get; set; }

        [StringLength(1024)]
        public string RearLayerImage { get; set; }
    }
}
