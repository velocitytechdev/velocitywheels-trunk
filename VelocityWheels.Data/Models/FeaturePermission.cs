﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VelocityWheels.Data.Models
{
    [Table("FeaturePermission")]
    public class FeaturePermission
    {
        [Key]
        public long Id { get; set; }

        [MaxLength(40)]
        public string PermissionName { get; set; }

        public bool Enabled { get; set; }

        public virtual List<Role> Roles { get; set; }
    }
}
