﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace VelocityWheels.Data.Models
{
    [Table("SubAccount")]
    public class SubAccount
    {
        [Key]
        public long Id { get; set; }

        [StringLength(50)]
        public string SubAccountName { get; set; }

        [StringLength(100)]
        public string AccountUrl { get; set; }

        public bool Active { get; set; }        

        [ForeignKey("Account")]
        public long? AccountId { get; set; }

        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        public DateTime LastUpdated { get; set; }

        [StringLength(50)]
        public string LastUpdatedBy { get; set; }
    }
}
