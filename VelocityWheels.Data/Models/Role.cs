﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VelocityWheels.Data.Models
{
    [Table("Role")]
    public class Role
    {
        [Key]
        public long Id { get; set; }

        [MaxLength(40)]
        public string RoleName { get; set; }

        public bool Enabled { get; set; }

        public virtual List<User> Users { get; set; } 

        public virtual List<FeaturePermission> Permissions { get; set; } 
    }
}
