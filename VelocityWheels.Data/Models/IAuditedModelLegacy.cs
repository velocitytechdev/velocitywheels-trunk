﻿using System;

namespace VelocityWheels.Data.Models
{
    public interface IAuditedModelLegacy : IAuditedModelAbstract
    {
        /// <summary>
        ///     Gets or sets the timestamp of the last update--in UTC.
        /// </summary>
        string LastUpdated { get; set; }
    }
}