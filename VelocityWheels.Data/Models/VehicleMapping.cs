﻿using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace VelocityWheels.Data.Models
{
    [Table("VehicleMapping")]
    public class VehicleMapping
    {
        [Key]
        public long Id { get; set; }
        
        public int FromApiId { get; set; }

        public int ToApiId { get; set; }

        public long? FromVehicleId { get; set; }

        public long? ToVehicleId { get; set; }

        public int FromYear { get; set; }

        [StringLength(100)]
        public string FromModel { get; set; }

        [StringLength(100)]
        public string FromMake { get; set; }

        [StringLength(100)]
        public string FromSubModel { get; set; }

        [StringLength(100)]
        public string FromBodyType { get; set; }

        public int ToYear { get; set; }

        [StringLength(100)]
        public string ToModel { get; set; }

        [StringLength(100)]
        public string ToMake { get; set; }

        [StringLength(100)]
        public string ToSubModel { get; set; }

        [StringLength(100)]
        public string ToBodyType { get; set; }

        [StringLength(255)]
        public string Notes { get; set; }

        [StringLength(50)]
        public string OverrideByUserName { get; set; }

        public DateTime? OverrideDate { get; set; }
    }
}
