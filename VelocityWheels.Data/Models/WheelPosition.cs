namespace VelocityWheels.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WheelPosition
    {
        public int WheelPositionID { get; set; }

        [StringLength(50)]
        public string APIProvider { get; set; }

        public int? VehicleID { get; set; }

        public int? ChassisID { get; set; }

        [StringLength(50)]
        public string Year { get; set; }

        [StringLength(100)]
        public string Make { get; set; }

        [StringLength(100)]
        public string Model { get; set; }

        [StringLength(100)]
        public string BodyStyle { get; set; }

        [StringLength(100)]
        public string SubModel { get; set; }

        [StringLength(100)]
        public string Color { get; set; }

        public decimal? FrontLeft { get; set; }

        public decimal? FrontTop { get; set; }

        public decimal? FrontWidth { get; set; }

        public decimal? FrontHeight { get; set; }

        public decimal? FrontAngle { get; set; }

        public decimal? RearLeft { get; set; }

        public decimal? RearTop { get; set; }

        public decimal? RearWidth { get; set; }

        public decimal? RearHeight { get; set; }

        public decimal? RearAngle { get; set; }

        public bool FlipX { get; set; }

        [StringLength(50)]
        public string Orientation { get; set; }

        [ForeignKey("VehicleImage")]
        public int? VehicleImageId { get; set; }
        [ForeignKey("VehicleImageId")]
        public virtual VehicleImage VehicleImage { get; set; }

    }
}
