﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace VelocityWheels.Data.Models
{
    [Table("AccountBrand")]
    public class AccountBrands
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey("Brand")]
        public long? BrandId { get; set; }

        [ForeignKey("BrandId")]
        public virtual Brand Brand { get; set; }

        public DateTime LastUpdated { get; set; }

        [StringLength(50)]
        public string LastUpdatedBy { get; set; }

        [ForeignKey("Account")]
        public long? AccountId { get; set; }

        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
    }
}
