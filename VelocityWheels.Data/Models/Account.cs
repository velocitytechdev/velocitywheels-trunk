﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace VelocityWheels.Data.Models
{
    [Table("Account")]
    public class Account
    {
        [Key]
        public long Id { get; set; }

        [StringLength(50)]
        public string AccountName { get; set; }

        [StringLength(50)]
        public string Token { get; set; }
        
        public bool Active { get; set; }

        public bool VehicleSelection { get; set; }

        public bool VehicleImages { get; set; }

        public bool WheelProducts { get; set; }

        public bool TireProducts { get; set; }

        public bool TireWeb { get; set; }

        [StringLength(100)]
        public string TireWebBaseUrl { get; set; }

        public DateTime LastUpdated { get; set; }

        [StringLength(50)]
        public string LastUpdatedBy { get; set; }

        public virtual List<AccountBrands> AccountBrands { get; set; }

        public virtual List<SubAccount> SubAccounts { get; set; }
    }
}
