﻿using System.ComponentModel.DataAnnotations;

namespace VelocityWheels.Data.Models
{
    /// <summary>
    ///     Indicates that a class implements a recognized form of audit fields.
    /// </summary>
    public interface IAuditedModelAbstract
    {
    }
}