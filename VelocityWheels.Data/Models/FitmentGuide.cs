namespace VelocityWheels.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FitmentGuide")]
    public partial class FitmentGuide
    {
        public int? YearFrom { get; set; }

        public int? YearTo { get; set; }

        [StringLength(255)]
        public string Make { get; set; }

        [StringLength(255)]
        public string Model { get; set; }

        [StringLength(255)]
        public string Submodel { get; set; }

        [StringLength(255)]
        public string BodyStyle { get; set; }

        [StringLength(255)]
        public string Brakes { get; set; }

        [StringLength(255)]
        public string Suspension { get; set; }

        public long? MinDia { get; set; }

        public Decimal? MinWidth { get; set; }

        public long? MaxDia { get; set; }

        public Decimal? MaxWidth { get; set; }

        [StringLength(255)]
        public string BoltMetric { get; set; }

        [StringLength(255)]
        public string BoltStandard { get; set; }

        public long? OffsetMin { get; set; }

        public long? OffsetMax { get; set; }

        [Key]
        public long Id { get; set; }

        public long? ChassisId { get; set; }
    }
}
