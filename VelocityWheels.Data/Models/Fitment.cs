﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace VelocityWheels.Data.Models
{
    [Table("Fitment")]
    public class Fitment
    {
        [Key]
        public long Id { get; set; }

        public int ApiId { get; set; }

        public long? ChassisId { get; set; }

        public long? ModelId { get; set; }

        public bool Restricted { get; set; }

        [StringLength(50)]
        public string PartNumber { get; set; }

        //public long? AcesVehicleId { get; set; }

        [ForeignKey("Brand")]
        public long? BrandId { get; set; }

        [ForeignKey("BrandId")]
        public virtual Brand Brand { get; set; }


        [ForeignKey("Classification")]
        public long? ClassificationId { get; set; }

        [ForeignKey("ClassificationId")]
        public virtual Classification Classification { get; set; }
    }
}
