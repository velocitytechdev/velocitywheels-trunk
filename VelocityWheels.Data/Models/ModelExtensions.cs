﻿using System;
using System.Collections.Generic;

namespace VelocityWheels.Data.Models
{
    public static class ModelExtensions
    {
        /// <summary>
        ///     Sets the model to the active state
        /// </summary>
        /// <param name="model">
        ///     The model
        /// </param>
        /// <returns>
        ///     A value indicating whether the model was originally active.
        /// </returns>
        public static bool Activate(this IActivatedModel model)
        {
            var wasActive = model.Active;

            model.Active = true;

            return wasActive;
        }

        /// <summary>
        ///     Sets the model to the inactive state.
        /// </summary>
        /// <param name="model">
        ///     The model
        /// </param>
        /// <returns>
        ///     A value indicating whether the model was originally active.
        /// </returns>
        public static bool Deactivate(this IActivatedModel model)
        {
            var wasActive = model.Active;

            model.Active = false;

            return wasActive;
        }



        /// <summary>
        ///     Sets the audit fields of auditable models.
        /// </summary>
        /// <param name="model">
        ///     The model to be changed.
        /// </param>
        /// <param name="userName">
        ///     Name of the effective prinicpal making the change.
        /// </param>
        /// <param name="timestamp">
        ///     The timestamp, in UTC, to use or <c>null</c> if the current time should be used.
        /// </param>
        public static void SetAuditFields(this IAuditedModel model, string userName, DateTime? timestamp = null)
        {
            var localTimestamp = timestamp ?? DateTime.UtcNow;

            model.LastUpdated = localTimestamp;
            model.LastUpdatedBy = userName;
        }

        /// <summary>
        ///     Sets the audit fields of auditable models.
        /// </summary>
        /// <param name="models">
        ///     The models to be changed.
        /// </param>
        /// <param name="userName">
        ///     Name of the effective prinicpal making the change.
        /// </param>
        /// <param name="timestamp">
        ///     The timestamp, in UTC, to use or <c>null</c> if the current time should be used.
        /// </param>
        public static void SetAuditFields(this IEnumerable<IAuditedModel> models, string userName, DateTime? timestamp = null)
        {
            var localTimestamp = timestamp ?? DateTime.UtcNow;

            foreach (var model in models)
            {
                model.LastUpdated = localTimestamp;
                model.LastUpdatedBy = userName;
            }
        }
    }
}