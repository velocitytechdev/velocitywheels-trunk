namespace VelocityWheels.Data.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    using Class = VelocityWheelsContext;
    using System.Data.Entity.Infrastructure;

    public class VelocityWheelsContextFactory : IDbContextFactory<VelocityWheelsContext>
    {
        public Class Create()
        {
            return VelocityWheelsContext.Create();
        }
    }

    public partial class VelocityWheelsContext : DbContext
    {
        public const string ConfigurationConnectionSource = "PRODUCTDATA:VTWS:DATA:CONNECTIONSOURCE";
        public const string ConfigurationProductDataVtwsDataConnectionsVelocityTech = "PRODUCTDATA:VTWS:DATA:CONNECTIONS:VELOCITYTECH";
        public const string ConnectionSourceEnvironment = "connectionsourceenvironment";

        public static string GetConnectionString(string connectionStringKey)
        {
            if (connectionStringKey == null) { throw new ArgumentNullException(nameof(connectionStringKey)); }


            var connectionSource = Environment.GetEnvironmentVariable(Class.ConfigurationConnectionSource);

            if (string.IsNullOrEmpty(connectionSource))
            {
                return null;
            }

            if (!connectionSource.Equals(Class.ConnectionSourceEnvironment, StringComparison.OrdinalIgnoreCase))
            {
                var msg = $"ERROR: Invalid configuration environment variable '{Class.ConfigurationConnectionSource}' value '{connectionSource}'.";
                throw new Exception(msg);
            }

            var connectionString = Environment.GetEnvironmentVariable(connectionStringKey);

            if (string.IsNullOrEmpty(connectionString))
            {
                var msg = $"ERROR: Missing configuration environment variable '{connectionStringKey}'.";
                throw new Exception(msg);
            }

            return connectionString;
        }

        public static VelocityWheelsContext Create()
        {
            var connectionString = Class.GetConnectionString(Class.ConfigurationProductDataVtwsDataConnectionsVelocityTech);

            if (connectionString == null)
            {
                return new VelocityWheelsContext();
            }

            return new VelocityWheelsContext(connectionString);
        }

        public static VelocityWheelsContext Create(VelocityWheelsContext source)
        {
            return new VelocityWheelsContext(source.Database.Connection.ConnectionString);
        }

        private static T ValidateNotNullArgument<T>(T value, string name) where T : class
        {
            if (value == null)
            {
                throw new ArgumentNullException(name);
            }

            return value;
        }

        private VelocityWheelsContext(string nameOrConnectionString)
            : base(Class.ValidateNotNullArgument(nameOrConnectionString, nameof(nameOrConnectionString)))
        {
        }

        private VelocityWheelsContext()
            : this("name=VelocityWheelsContext")
        {
        }

        private VelocityWheelsContext(VelocityWheelsContext existingContext)
            : base(existingContext.Database.Connection, contextOwnsConnection: false)
        {
        }

        public virtual DbSet<AccountBrands> AccountBrands { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<Classification> Classifications { get; set; }
        public virtual DbSet<Finish> Finishes { get; set; }
        public virtual DbSet<Fitment> Fitments { get; set; }
        public virtual DbSet<MissingMap> MissingMaps { get; set; }
        public virtual DbSet<Style> Styles { get; set; }
        public virtual DbSet<FeaturePermission> Permissions { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<SubAccount> SubAccounts { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<VehicleImage> VehicleImages { get; set; }
        public virtual DbSet<VehicleMapping> VehicleMappings { get; set; }
        public virtual DbSet<WheelPosition> WheelPositions { get; set; }
        public virtual DbSet<WheelSpec> WheelSpecs { get; set; }
        public virtual DbSet<Wheel> Wheels { get; set; }
        public virtual DbSet<WheelLayer> WheelLayers { get; set; }
        public virtual DbSet<FitmentGuide> FitmentGuides { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WheelPosition>()
                .Property(e => e.FrontLeft)
                .HasPrecision(9, 4);

            modelBuilder.Entity<WheelPosition>()
                .Property(e => e.FrontTop)
                .HasPrecision(9, 4);

            modelBuilder.Entity<WheelPosition>()
                .Property(e => e.FrontWidth)
                .HasPrecision(9, 4);

            modelBuilder.Entity<WheelPosition>()
                .Property(e => e.FrontHeight)
                .HasPrecision(9, 4);

            modelBuilder.Entity<WheelPosition>()
                .Property(e => e.FrontAngle)
                .HasPrecision(9, 4);

            modelBuilder.Entity<WheelPosition>()
                .Property(e => e.RearLeft)
                .HasPrecision(9, 4);

            modelBuilder.Entity<WheelPosition>()
                .Property(e => e.RearTop)
                .HasPrecision(9, 4);

            modelBuilder.Entity<WheelPosition>()
                .Property(e => e.RearWidth)
                .HasPrecision(9, 4);

            modelBuilder.Entity<WheelPosition>()
                .Property(e => e.RearHeight)
                .HasPrecision(9, 4);

            modelBuilder.Entity<WheelPosition>()
                .Property(e => e.RearAngle)
                .HasPrecision(9, 4);

            modelBuilder.Entity<WheelSpec>()
                .Property(e => e.Diameter)
                .HasPrecision(7, 2);

            modelBuilder.Entity<WheelSpec>()
                .Property(e => e.Width)
                .HasPrecision(7, 2);

            modelBuilder.Entity<WheelSpec>()
                .Property(e => e.Bore)
                .HasPrecision(7, 2);

            modelBuilder.Entity<WheelSpec>()
                .Property(e => e.BSM)
                .HasPrecision(7, 2);

            modelBuilder.Entity<WheelSpec>()
                .Property(e => e.LoadRating)
                .HasPrecision(7, 2);

            modelBuilder.Entity<WheelSpec>()
                .Property(e => e.Weight)
                .HasPrecision(7, 2);

            modelBuilder.Entity<WheelSpec>()
                .Property(e => e.BigBreak)
                .HasPrecision(7, 2);

            modelBuilder.Entity<WheelSpec>()
                .Property(e => e.ProductMSRP)
                .HasPrecision(7, 2);

            modelBuilder.Entity<FitmentGuide>()
                .Property(e => e.MinWidth)
                .HasPrecision(7, 2);

            modelBuilder.Entity<FitmentGuide>()
                .Property(e => e.MaxWidth)
                .HasPrecision(7, 2);

            /*modelBuilder.Entity<Classification>()
                .HasOptional(s => s.Mapping)
                .WithRequired(o => o.FromVehicleId);

            modelBuilder.Entity<VehicleMapping>()
                .HasOptional(s => s.FromVehicle)
                .WithRequired(ad => ad.VehicleMappingId);*/



            /*modelBuilder.Entity<VehicleMapping>()
                .HasOptional(s => s.FromVehicle)
                .WithOptionalDependent(o => o.Mapping);*/
        }
    }
}
