﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VelocityWheels.Data.Models
{
    [Table("Finish")]
    public class Finish : IAuditedModel, IActivatedModel
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey("Brand")]
        public long? BrandId { get; set; }

        [ForeignKey("BrandId")]
        public virtual Brand Brand { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(50)]
        public string ShortName { get; set; }

        public bool Active { get; set; }

        public DateTime LastUpdated { get; set; }

        [StringLength(50)]
        public string LastUpdatedBy { get; set; }
    }
}
