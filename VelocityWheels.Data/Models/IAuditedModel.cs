﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VelocityWheels.Data.Models
{
    /// <summary>
    ///     When implemented in a class, indicates that the model includes data auditing properties.
    /// </summary>
    public interface IAuditedModel : IAuditedModelAbstract
    {
        /// <summary>
        ///     Gets or sets the timestamp of the last update--in UTC.
        /// </summary>
        DateTime LastUpdated { get; set; }

        /// <summary>
        ///     Gets or sets the usernamae of the updating principal. Required.
        /// </summary>
        [StringLength(50)]
        string LastUpdatedBy { get; set; }
    }
}