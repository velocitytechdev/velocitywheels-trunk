﻿using System;
using System.Data.Entity;

namespace VelocityWheels.Data.Models
{
    public static class DbContextExtensions
    {
        /// <summary>
        ///     Saves the models to the specified context.
        /// </summary>
        /// <param name="context">
        ///     The context to which to save.
        /// </param>
        /// <param name="userId">
        ///     The system id of the responsible user.
        /// </param>
        /// <param name="timestamp">
        ///     The save time in UTC or <c>null</c> if the current time should be used.
        /// </param>
        /// <param name="activationStatus">
        ///     A value indicating whether the models are active or <c>null</c> if activation 
        ///     status should not be assigned.
        /// </param>
        /// <returns>
        ///     The result of <c>DbContext.SaveChanges</c>.
        /// </returns>
        public static int SaveModels(this DbContext context, string userId, DateTime? timestamp = null, bool? activationStatus = null)
        {
            var localTimestamp = timestamp ?? DateTime.UtcNow;

            foreach (var entiry in context.ChangeTracker.Entries())
            {
                var model = entiry.Entity;
                var activatedModel = model as IActivatedModel;
                var auditedModel = model as IAuditedModelAbstract;

                if (activationStatus != null && activatedModel != null)
                {
                    activatedModel.Active = activationStatus.Value;
                }

                if (auditedModel != null)
                {
                    var legacy = auditedModel as IAuditedModelLegacy;
                    var audited = auditedModel as IAuditedModel;

                    if (legacy != null)
                    {
                        legacy.LastUpdated = localTimestamp.ToString("G");
                    }
                    else if (audited != null)
                    {
                        audited.LastUpdated = localTimestamp;
                        audited.LastUpdatedBy = userId;
                    }
                    else
                    {
                        throw new NotImplementedException("DEVELOPER: Setting audit fields not implemented for IAuditedModelAbstract implementation.");
                    }
                }
            }

            return context.SaveChanges();
        }
    }
}