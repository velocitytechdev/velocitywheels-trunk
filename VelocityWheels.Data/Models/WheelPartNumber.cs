﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace VelocityWheels.Data.Models
{
    [Table("WheelPartNumber")]
    public class WheelPartNumber
    {
        [Key]
        public long Id { get; set; }

        [StringLength(50)]
        public string PartNumber { get; set; }
    }
}
