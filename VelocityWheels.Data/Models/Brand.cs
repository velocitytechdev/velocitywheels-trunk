﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace VelocityWheels.Data.Models
{
    [Table("Brand")]
    public class Brand : IAuditedModel, IActivatedModel
    {
        [Key]
        public long Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        public bool Active { get; set; }

        public DateTime LastUpdated { get; set; }

        [StringLength(50)]
        public string LastUpdatedBy { get; set; }
    }
}
