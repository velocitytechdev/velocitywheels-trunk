﻿namespace VelocityWheels.Data.Models
{
    public interface IActivatedModel
    {
        bool Active { get; set; }
    }
}