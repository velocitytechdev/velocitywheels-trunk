﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace VelocityWheels.Data.Models
{
    public class MissingMap
    {
        [Key]
        public long Id { get; set; }

        //public long VehicleId { get; set; }

        public int ApiId { get; set; }

        //public int Year { get; set; }

        [StringLength(100)]
        public string Make { get; set; }

        [StringLength(100)]
        public string Model { get; set; }

        /*[StringLength(100)]
        public string SubModel { get; set; }*/
    }
}
