namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductFormatToImage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VehicleImages", "ProductFormatId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.VehicleImages", "ProductFormatId");
        }
    }
}
