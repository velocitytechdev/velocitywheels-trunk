namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRestrictedToFitment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fitment", "Restricted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fitment", "Restricted");
        }
    }
}
