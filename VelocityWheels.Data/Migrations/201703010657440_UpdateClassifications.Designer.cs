// <auto-generated />
namespace VelocityWheels.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class UpdateClassifications : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UpdateClassifications));
        
        string IMigrationMetadata.Id
        {
            get { return "201703010657440_UpdateClassifications"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
