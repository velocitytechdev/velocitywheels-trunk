namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateClassificationWithDriveTrain : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classification", "DriveTrain", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Classification", "DriveTrain");
        }
    }
}
