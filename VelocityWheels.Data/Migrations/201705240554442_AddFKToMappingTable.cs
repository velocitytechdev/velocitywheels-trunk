namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFKToMappingTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classification", "VehicleMappingId", c => c.Long());
            CreateIndex("dbo.Classification", "VehicleMappingId");
            AddForeignKey("dbo.Classification", "VehicleMappingId", "dbo.VehicleMapping", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Classification", "VehicleMappingId", "dbo.VehicleMapping");
            DropIndex("dbo.Classification", new[] { "VehicleMappingId" });
            DropColumn("dbo.Classification", "VehicleMappingId");
        }
    }
}
