namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBrandTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Brand",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Active = c.Boolean(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        LastUpdatedBy = c.String(maxLength: 50),
                        AccountId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.AccountId)
                .Index(t => t.AccountId);
            
            AlterColumn("dbo.Account", "LastUpdatedBy", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Brand", "AccountId", "dbo.Account");
            DropIndex("dbo.Brand", new[] { "AccountId" });
            AlterColumn("dbo.Account", "LastUpdatedBy", c => c.String());
            DropTable("dbo.Brand");
        }
    }
}
