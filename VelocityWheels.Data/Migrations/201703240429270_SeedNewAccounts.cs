namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedNewAccounts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account", "AccountUrl", c => c.String(maxLength: 100));

            Guid token = Guid.NewGuid();
            DateTime today = DateTime.Now;
            Sql($"insert into Account (AccountName,AccountUrl,Token,Active,LastUpdated,LastUpdatedBy) " +
                $"values ('localhost1','localhost','5585b38b-2287-4931-af72-16c867d305c9',1,'{today.ToString("G")}','SYSTEM')");

            Sql($"insert into Account (AccountName,AccountUrl,Token,Active,LastUpdated,LastUpdatedBy) " +
                $"values ('TEST','50.112.185.19','5585b38b-2287-4931-af72-16c867d305c9',1,'{today.ToString("G")}','SYSTEM')");

            Sql($"insert into Account (AccountName,AccountUrl,Token,Active,LastUpdated,LastUpdatedBy) " +
                $"values ('PROD','velocity-tech.com','5585b38b-2287-4931-af72-16c867d305c9',1,'{today.ToString("G")}','SYSTEM')");

            Sql($"insert into Account (AccountName,AccountUrl,Token,Active,LastUpdated,LastUpdatedBy) " +
                $"values ('DAI','directautoimport.com','5585b38b-2287-4931-af72-16c867d305c9',1,'{today.ToString("G")}','SYSTEM')");

            Sql($"insert into Account (AccountName,AccountUrl,Token,Active,LastUpdated,LastUpdatedBy) " +
                $"values ('CWS','cws.com','{token}',1,'{today.ToString("G")}','SYSTEM')");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account", "AccountUrl");
        }
    }
}
