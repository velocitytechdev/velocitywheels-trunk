namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLevelToWheelLayer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WheelLayer", "Level", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WheelLayer", "Level");
        }
    }
}
