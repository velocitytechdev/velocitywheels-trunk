namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAccountIndex : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE [dbo].[Brand] DROP CONSTRAINT [FK_dbo.Brand_dbo.Account_AccountId]");
            DropIndex("dbo.Brand", new[] { "Account_Id" });
            DropColumn("dbo.Brand", "Account_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Brand", "Account_Id", c => c.Long());
            CreateIndex("dbo.Brand", "Account_Id");
            AddForeignKey("dbo.Brand", "Account_Id", "dbo.Account", "Id");
        }
    }
}
