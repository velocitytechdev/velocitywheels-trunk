namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFKtoWheelSpec : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.WheelSpecs", "BrandId", c => c.Long());
            CreateIndex("dbo.WheelSpecs", "BrandId");
            AddForeignKey("dbo.WheelSpecs", "BrandId", "dbo.Brand", "Id");

            Sql("update WheelSpecs set BrandId = (select id from Brand b where b.Name = BrandName);");

            Sql("insert into AccountBrand (AccountId,BrandId, LastUpdated,LastUpdatedBy) " +
                "select(select top(1) id from Account where Token = '97B217C1-FBF9-4BA0-B9E6-A7A31C8B701A' and AccountName = 'DAI'), Id, SYSDATETIME(), 'SYSTEM'" +
                "from Brand where Name in ('DAI Alloys', '720Form', 'ART', 'Ruffino'); ");

            Sql("insert into AccountBrand (AccountId,BrandId, LastUpdated,LastUpdatedBy) " +
                "select(select top(1) id from Account where Token = '6b1dc2a2-88eb-49c8-a9c0-42fbf7719800'),Id, SYSDATETIME(), 'SYSTEM' " +
                "from Brand where Name not in ('DAI Alloys', '720Form', 'ART', 'Ruffino'); ");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WheelSpecs", "BrandId", "dbo.Brand");
            DropIndex("dbo.WheelSpecs", new[] { "BrandId" });
            AlterColumn("dbo.WheelSpecs", "BrandId", c => c.Int());
        }
    }
}
