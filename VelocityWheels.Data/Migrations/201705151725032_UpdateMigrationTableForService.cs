namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMigrationTableForService : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VehicleMapping", "FromYear", c => c.Int(nullable: false));
            AddColumn("dbo.VehicleMapping", "ToYear", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VehicleMapping", "ToYear");
            DropColumn("dbo.VehicleMapping", "FromYear");
        }
    }
}
