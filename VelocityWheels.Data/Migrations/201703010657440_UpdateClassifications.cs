namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateClassifications : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classification", "Region", c => c.String(maxLength: 30));
            AddColumn("dbo.Classification", "RegionId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Classification", "RegionId");
            DropColumn("dbo.Classification", "Region");
        }
    }
}
