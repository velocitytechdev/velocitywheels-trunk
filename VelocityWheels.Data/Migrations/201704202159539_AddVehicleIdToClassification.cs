namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddVehicleIdToClassification : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classification", "AcesVehicleId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Classification", "AcesVehicleId");
        }
    }
}
