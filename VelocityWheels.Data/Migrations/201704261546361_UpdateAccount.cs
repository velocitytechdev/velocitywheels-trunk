namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAccount : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Brand", "Account_Id", "dbo.Account");
        }
        
        public override void Down()
        {
            
        }
    }
}
