namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCustomizeToWheelMovedToWheel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Wheel", "Customizable", c => c.Boolean(nullable: false));
            DropColumn("dbo.WheelSpecs", "Customizable");
        }
        
        public override void Down()
        {
            AddColumn("dbo.WheelSpecs", "Customizable", c => c.Boolean(nullable: false));
            DropColumn("dbo.Wheel", "Customizable");
        }
    }
}
