namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatesForFitments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fitment", "ClassificationId", c => c.Long());
            CreateIndex("dbo.Fitment", "ClassificationId");
            AddForeignKey("dbo.Fitment", "ClassificationId", "dbo.Classification", "Id");
            DropColumn("dbo.Classification", "ChassisId");
            DropColumn("dbo.Fitment", "AcesVehicleId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Fitment", "AcesVehicleId", c => c.Long());
            AddColumn("dbo.Classification", "ChassisId", c => c.String(maxLength: 25));
            DropForeignKey("dbo.Fitment", "ClassificationId", "dbo.Classification");
            DropIndex("dbo.Fitment", new[] { "ClassificationId" });
            DropColumn("dbo.Fitment", "ClassificationId");
        }
    }
}
