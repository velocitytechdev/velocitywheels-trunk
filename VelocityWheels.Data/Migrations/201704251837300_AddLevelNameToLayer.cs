namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLevelNameToLayer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WheelLayer", "LevelName", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WheelLayer", "LevelName");
        }
    }
}
