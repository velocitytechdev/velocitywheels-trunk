namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBrandToFitment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fitment", "BrandId", c => c.Long());
            CreateIndex("dbo.Fitment", "BrandId");
            AddForeignKey("dbo.Fitment", "BrandId", "dbo.Brand", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Fitment", "BrandId", "dbo.Brand");
            DropIndex("dbo.Fitment", new[] { "BrandId" });
            DropColumn("dbo.Fitment", "BrandId");
        }
    }
}
