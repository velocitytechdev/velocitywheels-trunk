namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactorWheelSpec : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Finish",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BrandId = c.Long(),
                        Name = c.String(maxLength: 255),
                        ShortName = c.String(maxLength: 50),
                        Active = c.Boolean(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        LastUpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brand", t => t.BrandId)
                .Index(t => t.BrandId);
            
            CreateTable(
                "dbo.Style",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BrandId = c.Long(),
                        Name = c.String(maxLength: 100),
                        ShortName = c.String(maxLength: 50),
                        Active = c.Boolean(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        LastUpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brand", t => t.BrandId)
                .Index(t => t.BrandId);
            
            CreateTable(
                "dbo.Wheel",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BrandId = c.Long(),
                        FinishId = c.Long(nullable: false),
                        StyleId = c.Long(nullable: false),
                        WheelImageId = c.Int(),
                        ThumbnailImage = c.String(maxLength: 1024),
                        CatalogImage = c.String(maxLength: 1024),
                        HighAngleImage = c.String(maxLength: 1024),
                        FaceImage = c.String(maxLength: 1024),
                        FrontMainImage = c.String(maxLength: 1024),
                        FrontWindowImage = c.String(maxLength: 1024),
                        FrontFaceImage = c.String(maxLength: 1024),
                        RearMainImage = c.String(maxLength: 1024),
                        RearWindowImage = c.String(maxLength: 1024),
                        RearFaceImage = c.String(maxLength: 1024),
                        Active = c.Boolean(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        LastUpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brand", t => t.BrandId)
                .ForeignKey("dbo.Finish", t => t.FinishId, cascadeDelete: true)
                .ForeignKey("dbo.Style", t => t.StyleId, cascadeDelete: true)
                .Index(t => t.BrandId)
                .Index(t => t.FinishId)
                .Index(t => t.StyleId);
            
            this.Sql(@"
-- RefactorWheelSpecTable_Migrate
-- Run at the end of the Up method in RefactorWheelSpecTable in this sequence

print N'|'
print N'| Begin Migration'
print N'|'

select ws.WheelID
from dbo.WheelSpecs as ws

print N'>>>>> Delete invalid WheelSpecs.'
delete from dbo.WheelSpecs
output deleted.*
from dbo.WheelSpecs as ws
where ws.PartNumber is null
;
print N'<<<<< Delete invalid WheelSpecs.'

print N'>>>>> Sanity Check.'

if (exists (select 1
    from dbo.WheelSpecs as ws
        left outer join dbo.Brand as b
            on ws.BrandID = b.Id
    where ws.BrandID is null or b.Id is null or ws.StyleName is null or ws.FinishName is null
))
BEGIN
    raiserror (N'WheelSpec missing Brand, Style, or Finish', 16, 1);

    print N'|'
    print N'| Abort Migration'
    print N'|'

    return;
END
;

print N'<<<<< Sanity Check.'

print N'>>>>> Migrate Style.'
; with createDate as
(
    select SYSDATETIME() as Value
)
, style as
(
    select distinct 
          ws.BrandID as BrandId
        , ws.StyleName as Name
        , cast(1 as bit) as Active
        , d.Value as LastUpdated
        , 'SYSTEM' as LastUpdatedBy
    from dbo.WheelSpecs as ws
        inner join dbo.Brand as b
            on ws.BrandID = b.Id
        cross join createDate as d
    where ws.BrandID is not null and ws.StyleName is not null
)
insert into dbo.Style (BrandId, Name, Active, LastUpdated, LastUpdatedBy)
output inserted.*
select s.BrandId, s.Name, s.Active, s.LastUpdated, s.LastUpdatedBy
from style as s
order by s.BrandId, s.Name
;
print N'<<<<< Migrate Style.'

print N'>>>>> Migrate Finish.'
; with createDate as
(
    select SYSDATETIME() as Value
)
, finish as
(
    select distinct 
          ws.BrandID as BrandId
        , ws.FinishName as Name
        , ws.ShortFinishName as ShortName
        , cast(1 as bit) as Active
        , d.Value as LastUpdated
        , 'SYSTEM' as LastUpdatedBy
    from dbo.WheelSpecs as ws
        inner join dbo.Brand as b
            on ws.BrandID = b.Id
        cross join createDate as d
    where ws.BrandID is not null and ws.FinishName is not null
)
insert into dbo.Finish (BrandId, Name, ShortName, Active, LastUpdated, LastUpdatedBy)
output inserted.*
select s.BrandId, s.Name, s.ShortName, s.Active, s.LastUpdated, s.LastUpdatedBy
from finish as s
order by s.BrandId, s.Name
;
print N'<<<<< Migrate Finish.'

print N'>>>>> Migrate Wheel.'
; with createDate as
(
    select SYSDATETIME() as Value
)
, wheel as
(
    select distinct
          ws.BrandID as BrandId
        , s.Id as StyleId
        , f.Id as FinishId
        , ws.WheelImageID as WheelImageID
        , ws.ThumbnailImage as ThumbnailImage
        , ws.CatalogImage as CatalogImage
        , ws.HighAngleImage as HighAngleImage
        , ws.FaceImage as FaceImage
        , ws.FrontMainImage as FrontMainImage
        , ws.FrontWindowImage as FrontWindowImage
        , ws.FrontFaceImage as FrontFaceImage
        , ws.RearMainImage as RearMainImage
        , ws.RearWindowImage as RearWindowImage
        , ws.RearFaceImage as RearFaceImage
        , cast(1 as bit) as Active
        , d.Value as LastUpdated
        , 'SYSTEM' as LastUpdatedBy
    from dbo.WheelSpecs as ws
        cross join createDate as d
        inner join dbo.Brand as b
            on  ws.BrandID = b.Id
        inner join dbo.Style as s
            on ws.BrandID = s.BrandId and ws.StyleName = s.Name
        inner join dbo.Finish as f
            on ws.BrandID = f.BrandId and ws.FinishName = f.Name
)
insert into dbo.Wheel
(
      BrandId
    , StyleId
    , FinishId
    , WheelImageId
    , ThumbnailImage
    , CatalogImage
    , HighAngleImage
    , FaceImage
    , FrontMainImage
    , FrontWindowImage
    , FrontFaceImage
    , RearMainImage
    , RearWindowImage
    , RearFaceImage
    , Active
    , LastUpdated
    , LastUpdatedBy
)
output inserted.*
select
      w.BrandId
    , w.StyleId
    , w.FinishId
    , w.WheelImageId
    , w.ThumbnailImage
    , w.CatalogImage
    , w.HighAngleImage
    , w.FaceImage
    , w.FrontMainImage
    , w.FrontWindowImage
    , w.FrontFaceImage
    , w.RearMainImage
    , w.RearWindowImage
    , w.RearFaceImage
    , w.Active
    , w.LastUpdated
    , w.LastUpdatedBy
from wheel as w
order by w.BrandId, w.StyleId, w.FinishId 
;
print N'<<<<< Migrate Wheel.'


print N'>>>>> Reference Wheel.'
update dbo.WheelSpecs
set WheelID = w.Id
output deleted.*, inserted.*
from dbo.WheelSpecs as ws
    inner join dbo.Style as s
        on ws.BrandID = s.BrandId and ws.StyleName = s.Name
    inner join dbo.Finish as f
        on ws.BrandID = f.BrandId and ws.FinishName = f.Name
    inner join dbo.Wheel as w
        on ws.BrandID = w.BrandId and 
           s.Id = w.StyleId and
           f.Id = w.FinishId
;
print N'<<<<< Reference Wheel.'


print N'|'
print N'| Migration Complete'
print N'|'
");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Wheel", "StyleId", "dbo.Style");
            DropForeignKey("dbo.Wheel", "FinishId", "dbo.Finish");
            DropForeignKey("dbo.Wheel", "BrandId", "dbo.Brand");
            DropForeignKey("dbo.Style", "BrandId", "dbo.Brand");
            DropForeignKey("dbo.Finish", "BrandId", "dbo.Brand");
            DropIndex("dbo.Wheel", new[] { "StyleId" });
            DropIndex("dbo.Wheel", new[] { "FinishId" });
            DropIndex("dbo.Wheel", new[] { "BrandId" });
            DropIndex("dbo.Style", new[] { "BrandId" });
            DropIndex("dbo.Finish", new[] { "BrandId" });
            DropTable("dbo.Wheel");
            DropTable("dbo.Style");
            DropTable("dbo.Finish");
        }
    }
}
