namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImageLinkToClassification : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classification", "VehicleImageId", c => c.Int());
            CreateIndex("dbo.Classification", "VehicleImageId");
            AddForeignKey("dbo.Classification", "VehicleImageId", "dbo.VehicleImages", "VehicleImageID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Classification", "VehicleImageId", "dbo.VehicleImages");
            DropIndex("dbo.Classification", new[] { "VehicleImageId" });
            DropColumn("dbo.Classification", "VehicleImageId");
        }
    }
}
