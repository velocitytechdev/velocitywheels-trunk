namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAccountFeatures : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account", "VehicleSelection", c => c.Boolean(nullable: false));
            AddColumn("dbo.Account", "VehicleImages", c => c.Boolean(nullable: false));
            AddColumn("dbo.Account", "WheelProducts", c => c.Boolean(nullable: false));
            AddColumn("dbo.Account", "TireProducts", c => c.Boolean(nullable: false));
            AddColumn("dbo.Account", "TireWeb", c => c.Boolean(nullable: false));
            AddColumn("dbo.Account", "TireWebBaseUrl", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account", "TireWebBaseUrl");
            DropColumn("dbo.Account", "TireWeb");
            DropColumn("dbo.Account", "TireProducts");
            DropColumn("dbo.Account", "WheelProducts");
            DropColumn("dbo.Account", "VehicleImages");
            DropColumn("dbo.Account", "VehicleSelection");
        }
    }
}
