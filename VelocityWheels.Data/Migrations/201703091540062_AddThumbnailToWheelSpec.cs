namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddThumbnailToWheelSpec : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WheelSpecs", "ThumbnailImage", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WheelSpecs", "ThumbnailImage");
        }
    }
}
