namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFitmentGuide : DbMigration
    {
        public override void Up()
        {
            Sql("delete from dbo.FitmentGuide");

            AddColumn("dbo.FitmentGuide", "Brakes", c => c.String(maxLength: 255));
            AddColumn("dbo.FitmentGuide", "Suspension", c => c.String(maxLength: 255));
            AlterColumn("dbo.FitmentGuide", "MinWidth", c => c.Decimal(precision: 7, scale: 2));
            AlterColumn("dbo.FitmentGuide", "MaxWidth", c => c.Decimal(precision: 7, scale: 2));
            DropColumn("dbo.FitmentGuide", "Brakes/Suspension");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FitmentGuide", "Brakes/Suspension", c => c.String(maxLength: 255));
            AlterColumn("dbo.FitmentGuide", "MaxWidth", c => c.Long());
            AlterColumn("dbo.FitmentGuide", "MinWidth", c => c.Long());
            DropColumn("dbo.FitmentGuide", "Suspension");
            DropColumn("dbo.FitmentGuide", "Brakes");
        }
    }
}
