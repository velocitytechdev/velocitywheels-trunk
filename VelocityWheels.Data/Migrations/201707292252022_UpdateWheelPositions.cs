namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateWheelPositions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WheelPositions", "VehicleImageId", c => c.Int());
            CreateIndex("dbo.WheelPositions", "VehicleImageId");
            AddForeignKey("dbo.WheelPositions", "VehicleImageId", "dbo.VehicleImages", "VehicleImageID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WheelPositions", "VehicleImageId", "dbo.VehicleImages");
            DropIndex("dbo.WheelPositions", new[] { "VehicleImageId" });
            DropColumn("dbo.WheelPositions", "VehicleImageId");
        }
    }
}
