namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChassisToClassification : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Classification", "ChassisId", c => c.String(maxLength: 25));
            AddColumn("dbo.Classification", "ModelId", c => c.String(maxLength: 25));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Classification", "ModelId");
            DropColumn("dbo.Classification", "ChassisId");
        }
    }
}
