namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateVehicleImage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VehicleImages", "Description", c => c.String(maxLength: 255));
            AddColumn("dbo.VehicleImages", "CreatedDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.VehicleImages", "CreatedDate");
            DropColumn("dbo.VehicleImages", "Description");
        }
    }
}
