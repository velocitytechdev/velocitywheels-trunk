namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMappingTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VehicleMapping", "FromVehicleId", c => c.Long());
            AddColumn("dbo.VehicleMapping", "ToVehicleId", c => c.Long());
            AddColumn("dbo.VehicleMapping", "FromSubModel", c => c.String(maxLength: 100));
            AddColumn("dbo.VehicleMapping", "FromBodyType", c => c.String(maxLength: 100));
            AddColumn("dbo.VehicleMapping", "ToSubModel", c => c.String(maxLength: 100));
            AddColumn("dbo.VehicleMapping", "ToBodyType", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VehicleMapping", "ToBodyType");
            DropColumn("dbo.VehicleMapping", "ToSubModel");
            DropColumn("dbo.VehicleMapping", "FromBodyType");
            DropColumn("dbo.VehicleMapping", "FromSubModel");
            DropColumn("dbo.VehicleMapping", "ToVehicleId");
            DropColumn("dbo.VehicleMapping", "FromVehicleId");
        }
    }
}
