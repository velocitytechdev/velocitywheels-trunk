namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveRepeatColumnsFromWheelSpec : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.WheelSpecs", "BrandId", "dbo.Brand");
            DropIndex("dbo.WheelSpecs", new[] { "BrandId" });
            CreateIndex("dbo.WheelSpecs", "WheelId");
            AddForeignKey("dbo.WheelSpecs", "WheelId", "dbo.Wheel", "Id");
            DropColumn("dbo.WheelSpecs", "BrandId");
            DropColumn("dbo.WheelSpecs", "BrandName");
            DropColumn("dbo.WheelSpecs", "StyleID");
            DropColumn("dbo.WheelSpecs", "StyleName");
            DropColumn("dbo.WheelSpecs", "FinishID");
            DropColumn("dbo.WheelSpecs", "FinishName");
            DropColumn("dbo.WheelSpecs", "ShortFinishName");
            DropColumn("dbo.WheelSpecs", "ThumbnailImage");
            DropColumn("dbo.WheelSpecs", "CatalogImage");
            DropColumn("dbo.WheelSpecs", "HighAngleImage");
            DropColumn("dbo.WheelSpecs", "FaceImage");
            DropColumn("dbo.WheelSpecs", "FrontMainImage");
            DropColumn("dbo.WheelSpecs", "FrontWindowImage");
            DropColumn("dbo.WheelSpecs", "FrontFaceImage");
            DropColumn("dbo.WheelSpecs", "RearMainImage");
            DropColumn("dbo.WheelSpecs", "RearWindowImage");
            DropColumn("dbo.WheelSpecs", "RearFaceImage");
        }
        
        public override void Down()
        {
            AddColumn("dbo.WheelSpecs", "RearFaceImage", c => c.String(maxLength: 1024));
            AddColumn("dbo.WheelSpecs", "RearWindowImage", c => c.String(maxLength: 1024));
            AddColumn("dbo.WheelSpecs", "RearMainImage", c => c.String(maxLength: 1024));
            AddColumn("dbo.WheelSpecs", "FrontFaceImage", c => c.String(maxLength: 1024));
            AddColumn("dbo.WheelSpecs", "FrontWindowImage", c => c.String(maxLength: 1024));
            AddColumn("dbo.WheelSpecs", "FrontMainImage", c => c.String(maxLength: 1024));
            AddColumn("dbo.WheelSpecs", "FaceImage", c => c.String(maxLength: 1024));
            AddColumn("dbo.WheelSpecs", "HighAngleImage", c => c.String(maxLength: 1024));
            AddColumn("dbo.WheelSpecs", "CatalogImage", c => c.String(maxLength: 1024));
            AddColumn("dbo.WheelSpecs", "ThumbnailImage", c => c.String(maxLength: 1024));
            AddColumn("dbo.WheelSpecs", "ShortFinishName", c => c.String(maxLength: 50));
            AddColumn("dbo.WheelSpecs", "FinishName", c => c.String(maxLength: 255));
            AddColumn("dbo.WheelSpecs", "FinishID", c => c.Int());
            AddColumn("dbo.WheelSpecs", "StyleName", c => c.String(maxLength: 100));
            AddColumn("dbo.WheelSpecs", "StyleID", c => c.Int());
            AddColumn("dbo.WheelSpecs", "BrandName", c => c.String(maxLength: 100));
            AddColumn("dbo.WheelSpecs", "BrandId", c => c.Long());
            DropForeignKey("dbo.WheelSpecs", "WheelId", "dbo.Wheel");
            DropIndex("dbo.WheelSpecs", new[] { "WheelId" });
            CreateIndex("dbo.WheelSpecs", "BrandId");
            AddForeignKey("dbo.WheelSpecs", "BrandId", "dbo.Brand", "Id");
        }
    }
}
