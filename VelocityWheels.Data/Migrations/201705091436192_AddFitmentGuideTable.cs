namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFitmentGuideTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FitmentGuide",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        YearFrom = c.Int(),
                        YearTo = c.Int(),
                        Make = c.String(maxLength: 255),
                        Model = c.String(maxLength: 255),
                        Submodel = c.String(maxLength: 255),
                        BodyStyle = c.String(maxLength: 255),
                        BrakesSuspension = c.String(name: "Brakes/Suspension", maxLength: 255),
                        MinDia = c.Long(),
                        MinWidth = c.Long(),
                        MaxDia = c.Long(),
                        MaxWidth = c.Long(),
                        BoltMetric = c.String(maxLength: 255),
                        BoltStandard = c.String(maxLength: 255),
                        OffsetMin = c.Long(),
                        OffsetMax = c.Long(),
                        ChassisId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);            
        }
        
        public override void Down()
        {
            DropTable("dbo.FitmentGuide");
        }
    }
}
