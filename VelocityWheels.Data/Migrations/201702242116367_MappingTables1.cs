namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MappingTables1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MissingMaps",
                c => new
                    {
                        ApiId = c.Int(nullable: false),
                        Make = c.String(maxLength: 100),
                        Model = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.ApiId);
            
            CreateTable(
                "dbo.VehicleMapping",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FromApiId = c.Int(nullable: false),
                        ToApiId = c.Int(nullable: false),
                        FromModel = c.String(maxLength: 100),
                        FromMake = c.String(maxLength: 100),
                        ToModel = c.String(maxLength: 100),
                        ToMake = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.VehicleMapping");
            DropTable("dbo.MissingMaps");
        }
    }
}
