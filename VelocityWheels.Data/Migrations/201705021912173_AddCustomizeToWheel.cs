namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCustomizeToWheel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WheelSpecs", "Customizable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WheelSpecs", "Customizable");
        }
    }
}
