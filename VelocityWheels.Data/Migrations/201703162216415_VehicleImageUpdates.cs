namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VehicleImageUpdates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VehicleImages", "ImageCode", c => c.String(maxLength: 5));
            AddColumn("dbo.VehicleImages", "Color", c => c.String(maxLength: 25));
            AddColumn("dbo.VehicleImages", "Thumbnail", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VehicleImages", "Thumbnail");
            DropColumn("dbo.VehicleImages", "Color");
            DropColumn("dbo.VehicleImages", "ImageCode");
        }
    }
}
