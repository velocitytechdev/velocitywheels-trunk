namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLayerImagesToWheel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Wheel", "BaseLayerImage", c => c.String(maxLength: 1024));
            AddColumn("dbo.Wheel", "BaseLayer1Image", c => c.String(maxLength: 1024));
            AddColumn("dbo.Wheel", "FrontLayer1Image", c => c.String(maxLength: 1024));
            AddColumn("dbo.Wheel", "RearLayer1Image", c => c.String(maxLength: 1024));
            AddColumn("dbo.Wheel", "BaseLayer2Image", c => c.String(maxLength: 1024));
            AddColumn("dbo.Wheel", "FrontLayer2Image", c => c.String(maxLength: 1024));
            AddColumn("dbo.Wheel", "RearLayer2Image", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Wheel", "RearLayer2Image");
            DropColumn("dbo.Wheel", "FrontLayer2Image");
            DropColumn("dbo.Wheel", "BaseLayer2Image");
            DropColumn("dbo.Wheel", "RearLayer1Image");
            DropColumn("dbo.Wheel", "FrontLayer1Image");
            DropColumn("dbo.Wheel", "BaseLayer1Image");
            DropColumn("dbo.Wheel", "BaseLayerImage");
        }
    }
}
