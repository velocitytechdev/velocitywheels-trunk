namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddClassifications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Classification",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ApiId = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        Make = c.String(maxLength: 100),
                        Model = c.String(maxLength: 100),
                        BodyType = c.String(maxLength: 100),
                        SubModel = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Classification");
        }
    }
}
