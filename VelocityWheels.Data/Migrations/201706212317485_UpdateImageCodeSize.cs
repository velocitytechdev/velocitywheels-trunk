namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateImageCodeSize : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.VehicleImages", "ImageCode", c => c.String(maxLength: 25));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.VehicleImages", "ImageCode", c => c.String(maxLength: 5));
        }
    }
}
