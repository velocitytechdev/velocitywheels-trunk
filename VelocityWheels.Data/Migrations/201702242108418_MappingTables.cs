namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MappingTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VehicleImages",
                c => new
                    {
                        VehicleImageID = c.Int(nullable: false, identity: true),
                        VehicleID = c.Int(),
                        ChassisID = c.Int(),
                        Year = c.String(maxLength: 50),
                        Make = c.String(maxLength: 100),
                        Model = c.String(maxLength: 100),
                        BodyStyle = c.String(maxLength: 100),
                        SubModel = c.String(maxLength: 100),
                        URL = c.String(),
                        FileName = c.String(maxLength: 255),
                        ImageType = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.VehicleImageID);
            
            CreateTable(
                "dbo.WheelPositions",
                c => new
                    {
                        WheelPositionID = c.Int(nullable: false, identity: true),
                        APIProvider = c.String(maxLength: 50),
                        VehicleID = c.Int(),
                        ChassisID = c.Int(),
                        Year = c.String(maxLength: 50),
                        Make = c.String(maxLength: 100),
                        Model = c.String(maxLength: 100),
                        BodyStyle = c.String(maxLength: 100),
                        SubModel = c.String(maxLength: 100),
                        Color = c.String(maxLength: 100),
                        FrontLeft = c.Decimal(precision: 9, scale: 4),
                        FrontTop = c.Decimal(precision: 9, scale: 4),
                        FrontWidth = c.Decimal(precision: 9, scale: 4),
                        FrontHeight = c.Decimal(precision: 9, scale: 4),
                        FrontAngle = c.Decimal(precision: 9, scale: 4),
                        RearLeft = c.Decimal(precision: 9, scale: 4),
                        RearTop = c.Decimal(precision: 9, scale: 4),
                        RearWidth = c.Decimal(precision: 9, scale: 4),
                        RearHeight = c.Decimal(precision: 9, scale: 4),
                        RearAngle = c.Decimal(precision: 9, scale: 4),
                        Orientation = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.WheelPositionID);
            
            CreateTable(
                "dbo.WheelSpecs",
                c => new
                    {
                        WheelSpecID = c.Int(nullable: false, identity: true),
                        APIProvider = c.String(maxLength: 50),
                        PartNumber = c.String(maxLength: 50),
                        WheelID = c.Int(),
                        BrandID = c.Int(),
                        BrandName = c.String(maxLength: 100),
                        StyleID = c.Int(),
                        StyleName = c.String(maxLength: 100),
                        FinishID = c.Int(),
                        FinishName = c.String(maxLength: 255),
                        ShortFinishName = c.String(maxLength: 50),
                        Diameter = c.Decimal(precision: 7, scale: 2),
                        Width = c.Decimal(precision: 7, scale: 2),
                        BoltPatternList = c.String(),
                        BoltPattern1 = c.String(maxLength: 255),
                        BoltPattern2 = c.String(maxLength: 255),
                        BoltPattern3 = c.String(maxLength: 255),
                        BoltPattern4 = c.String(maxLength: 255),
                        Bore = c.Decimal(precision: 7, scale: 2),
                        Offset = c.String(maxLength: 255),
                        BSM = c.Decimal(precision: 7, scale: 2),
                        CapNumber = c.String(maxLength: 255),
                        LoadRating = c.Decimal(precision: 7, scale: 2),
                        Weight = c.Decimal(precision: 7, scale: 2),
                        LugType = c.String(maxLength: 255),
                        BigBreak = c.Decimal(precision: 7, scale: 2),
                        LipSize = c.String(maxLength: 255),
                        ProductMSRP = c.Decimal(precision: 7, scale: 2),
                        ProductDescription = c.String(),
                        WheelImageID = c.Int(),
                        CatalogImage = c.String(maxLength: 1024),
                        HighAngleImage = c.String(maxLength: 1024),
                        FaceImage = c.String(maxLength: 1024),
                        FrontMainImage = c.String(maxLength: 1024),
                        FrontWindowImage = c.String(maxLength: 1024),
                        FrontFaceImage = c.String(maxLength: 1024),
                        RearMainImage = c.String(maxLength: 1024),
                        RearWindowImage = c.String(maxLength: 1024),
                        RearFaceImage = c.String(maxLength: 1024),
                        LastUpdated = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.WheelSpecID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.WheelSpecs");
            DropTable("dbo.WheelPositions");
            DropTable("dbo.VehicleImages");
        }
    }
}
