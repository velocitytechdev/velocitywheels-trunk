namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReworkVehicleImageAssignment : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Classification", "VehicleImageId", "dbo.VehicleImages");
            DropIndex("dbo.Classification", new[] { "VehicleImageId" });
            CreateTable(
                "dbo.VehicleImageClassifications",
                c => new
                    {
                        VehicleImage_VehicleImageID = c.Int(nullable: false),
                        Classification_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.VehicleImage_VehicleImageID, t.Classification_Id })
                .ForeignKey("dbo.VehicleImages", t => t.VehicleImage_VehicleImageID, cascadeDelete: true)
                .ForeignKey("dbo.Classification", t => t.Classification_Id, cascadeDelete: true)
                .Index(t => t.VehicleImage_VehicleImageID)
                .Index(t => t.Classification_Id);
            
            DropColumn("dbo.Classification", "VehicleImageId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Classification", "VehicleImageId", c => c.Int());
            DropForeignKey("dbo.VehicleImageClassifications", "Classification_Id", "dbo.Classification");
            DropForeignKey("dbo.VehicleImageClassifications", "VehicleImage_VehicleImageID", "dbo.VehicleImages");
            DropIndex("dbo.VehicleImageClassifications", new[] { "Classification_Id" });
            DropIndex("dbo.VehicleImageClassifications", new[] { "VehicleImage_VehicleImageID" });
            DropTable("dbo.VehicleImageClassifications");
            CreateIndex("dbo.Classification", "VehicleImageId");
            AddForeignKey("dbo.Classification", "VehicleImageId", "dbo.VehicleImages", "VehicleImageID");
        }
    }
}
