namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SecurityFeatures : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FeaturePermission",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PermissionName = c.String(maxLength: 40),
                        Enabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RoleName = c.String(maxLength: 40),
                        Enabled = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RoleFeaturePermissions",
                c => new
                    {
                        Role_Id = c.Long(nullable: false),
                        FeaturePermission_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Role_Id, t.FeaturePermission_Id })
                .ForeignKey("dbo.Role", t => t.Role_Id, cascadeDelete: true)
                .ForeignKey("dbo.FeaturePermission", t => t.FeaturePermission_Id, cascadeDelete: true)
                .Index(t => t.Role_Id)
                .Index(t => t.FeaturePermission_Id);
            
            AddColumn("dbo.User", "Salt", c => c.String(maxLength: 50));
            AddColumn("dbo.User", "RoleId", c => c.Long());
            AddColumn("dbo.User", "AccountId", c => c.Long());
            AlterColumn("dbo.User", "Password", c => c.String(maxLength: 100));
            CreateIndex("dbo.User", "RoleId");
            CreateIndex("dbo.User", "AccountId");
            AddForeignKey("dbo.User", "AccountId", "dbo.Account", "Id");
            AddForeignKey("dbo.User", "RoleId", "dbo.Role", "Id");

            Sql("insert into Role (RoleName,Enabled) values ('SuperAdmin', 1)");
            Sql("insert into Role (RoleName,Enabled) values ('AccountAdmin', 1)");
            Sql("insert into Role (RoleName,Enabled) values ('AccountUser', 1)");

            Sql("insert into FeaturePermission (PermissionName,Enabled) values ('IsSuperAdmin', 1)");
            Sql("insert into FeaturePermission (PermissionName,Enabled) values ('IsAccountAdmin', 1)");
            Sql("insert into FeaturePermission (PermissionName,Enabled) values ('IsAccountUser', 1)");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.User", "RoleId", "dbo.Role");
            DropForeignKey("dbo.User", "AccountId", "dbo.Account");
            DropForeignKey("dbo.RoleFeaturePermissions", "FeaturePermission_Id", "dbo.FeaturePermission");
            DropForeignKey("dbo.RoleFeaturePermissions", "Role_Id", "dbo.Role");
            DropIndex("dbo.RoleFeaturePermissions", new[] { "FeaturePermission_Id" });
            DropIndex("dbo.RoleFeaturePermissions", new[] { "Role_Id" });
            DropIndex("dbo.User", new[] { "AccountId" });
            DropIndex("dbo.User", new[] { "RoleId" });
            AlterColumn("dbo.User", "Password", c => c.String(maxLength: 50));
            DropColumn("dbo.User", "AccountId");
            DropColumn("dbo.User", "RoleId");
            DropColumn("dbo.User", "Salt");
            DropTable("dbo.RoleFeaturePermissions");
            DropTable("dbo.Role");
            DropTable("dbo.FeaturePermission");
        }
    }
}
