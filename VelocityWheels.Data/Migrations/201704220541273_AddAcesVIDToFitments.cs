namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAcesVIDToFitments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fitment", "AcesVehicleId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fitment", "AcesVehicleId");
        }
    }
}
