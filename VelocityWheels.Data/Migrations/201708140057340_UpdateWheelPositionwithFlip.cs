namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateWheelPositionwithFlip : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WheelPositions", "FlipX", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WheelPositions", "FlipX");
        }
    }
}
