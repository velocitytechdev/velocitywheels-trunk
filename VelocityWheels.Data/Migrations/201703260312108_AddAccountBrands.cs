namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAccountBrands : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Brand", name: "AccountId", newName: "Account_Id");
            RenameIndex(table: "dbo.Brand", name: "IX_AccountId", newName: "IX_Account_Id");
            CreateTable(
                "dbo.AccountBrand",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BrandId = c.Long(),
                        LastUpdated = c.DateTime(nullable: false),
                        LastUpdatedBy = c.String(maxLength: 50),
                        AccountId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.AccountId)
                .ForeignKey("dbo.Brand", t => t.BrandId)
                .Index(t => t.BrandId)
                .Index(t => t.AccountId);
            
            AddColumn("dbo.Account", "Primary", c => c.Boolean(nullable: false));
            AddColumn("dbo.Brand", "Code", c => c.String(maxLength: 50));

            Sql($"insert into Brand (Name, Code, Active, LastUpdated, LastUpdatedBy) " +
                $"select BrandName, replace(BrandName,' ','_'), 1, sysdatetime(), 'SYSTEM' from WheelSpecs group by BrandName;");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountBrand", "BrandId", "dbo.Brand");
            DropForeignKey("dbo.AccountBrand", "AccountId", "dbo.Account");
            DropIndex("dbo.AccountBrand", new[] { "AccountId" });
            DropIndex("dbo.AccountBrand", new[] { "BrandId" });
            DropColumn("dbo.Brand", "Code");
            DropColumn("dbo.Account", "Primary");
            DropTable("dbo.AccountBrand");
            RenameIndex(table: "dbo.Brand", name: "IX_Account_Id", newName: "IX_AccountId");
            RenameColumn(table: "dbo.Brand", name: "Account_Id", newName: "AccountId");
        }
    }
}
