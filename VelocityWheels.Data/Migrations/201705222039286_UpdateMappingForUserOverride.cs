namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateMappingForUserOverride : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VehicleMapping", "OverrideByUserName", c => c.String(maxLength: 50));
            AddColumn("dbo.VehicleMapping", "OverrideDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.VehicleMapping", "OverrideDate");
            DropColumn("dbo.VehicleMapping", "OverrideByUserName");
        }
    }
}
