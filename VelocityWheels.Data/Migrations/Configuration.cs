using VelocityWheels.Data.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<VelocityWheels.Data.Models.VelocityWheelsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VelocityWheels.Data.Models.VelocityWheelsContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            // Seed up super admin role for initial use
            var superAdmin = context.Users.FirstOrDefault(u => u.UserName == "vtwsadmin");
            if (superAdmin == null)
            {
                var user = new User()
                {
                    Active = true,
                    RoleId = null,
                    Name = "VTWS Super Admin",
                    UserName = "vtwsadmin",
                    AccountId = null,
                    LastUpdated = DateTime.UtcNow,
                    LastUpdatedBy = "SYSTEM"
                };

                string salt;
                var password = PasswordUtil.EncryptPassword("!velocity1", out salt);
                user.Password = password;
                user.Salt = salt;

                var role = context.Roles.FirstOrDefault(r => r.RoleName == "SuperAdmin");
                if (role != null)
                {
                    user.RoleId = role.Id;
                }

                context.Users.Add(user);
            }
        }
    }
}
