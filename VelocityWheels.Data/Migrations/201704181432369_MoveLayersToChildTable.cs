namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoveLayersToChildTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WheelLayer",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        WheelId = c.Int(),
                        BaseLayerImage = c.String(maxLength: 1024),
                        FrontLayerImage = c.String(maxLength: 1024),
                        RearLayerImage = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Wheel", t => t.WheelId)
                .Index(t => t.WheelId);
            
            DropColumn("dbo.Wheel", "BaseLayer1Image");
            DropColumn("dbo.Wheel", "FrontLayer1Image");
            DropColumn("dbo.Wheel", "RearLayer1Image");
            DropColumn("dbo.Wheel", "BaseLayer2Image");
            DropColumn("dbo.Wheel", "FrontLayer2Image");
            DropColumn("dbo.Wheel", "RearLayer2Image");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Wheel", "RearLayer2Image", c => c.String(maxLength: 1024));
            AddColumn("dbo.Wheel", "FrontLayer2Image", c => c.String(maxLength: 1024));
            AddColumn("dbo.Wheel", "BaseLayer2Image", c => c.String(maxLength: 1024));
            AddColumn("dbo.Wheel", "RearLayer1Image", c => c.String(maxLength: 1024));
            AddColumn("dbo.Wheel", "FrontLayer1Image", c => c.String(maxLength: 1024));
            AddColumn("dbo.Wheel", "BaseLayer1Image", c => c.String(maxLength: 1024));
            DropForeignKey("dbo.WheelLayer", "WheelId", "dbo.Wheel");
            DropIndex("dbo.WheelLayer", new[] { "WheelId" });
            DropTable("dbo.WheelLayer");
        }
    }
}
