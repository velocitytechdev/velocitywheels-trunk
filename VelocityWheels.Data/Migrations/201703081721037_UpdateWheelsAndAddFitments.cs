namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateWheelsAndAddFitments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Fitment",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ApiId = c.Int(nullable: false),
                        ChassisId = c.Long(),
                        ModelId = c.Long(),
                        PartNumber = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.VehicleImages", "ApiId", c => c.Int());
            AlterColumn("dbo.VehicleImages", "ChassisId", c => c.Long());
            DropColumn("dbo.VehicleImages", "Year");
            DropColumn("dbo.VehicleImages", "Make");
            DropColumn("dbo.VehicleImages", "Model");
            DropColumn("dbo.VehicleImages", "BodyStyle");
            DropColumn("dbo.VehicleImages", "SubModel");
        }
        
        public override void Down()
        {
            AddColumn("dbo.VehicleImages", "SubModel", c => c.String(maxLength: 100));
            AddColumn("dbo.VehicleImages", "BodyStyle", c => c.String(maxLength: 100));
            AddColumn("dbo.VehicleImages", "Model", c => c.String(maxLength: 100));
            AddColumn("dbo.VehicleImages", "Make", c => c.String(maxLength: 100));
            AddColumn("dbo.VehicleImages", "Year", c => c.String(maxLength: 50));
            AlterColumn("dbo.VehicleImages", "ChassisId", c => c.Int());
            DropColumn("dbo.VehicleImages", "ApiId");
            DropTable("dbo.Fitment");
        }
    }
}
