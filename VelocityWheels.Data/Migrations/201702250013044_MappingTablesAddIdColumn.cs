namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MappingTablesAddIdColumn : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MissingMaps");
            AddColumn("dbo.MissingMaps", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.MissingMaps", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.MissingMaps");
            DropColumn("dbo.MissingMaps", "Id");
            AddPrimaryKey("dbo.MissingMaps", "ApiId");
        }
    }
}
