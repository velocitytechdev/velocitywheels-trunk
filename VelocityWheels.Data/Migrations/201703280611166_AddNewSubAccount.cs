namespace VelocityWheels.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewSubAccount : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubAccount",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SubAccountName = c.String(maxLength: 50),
                        AccountUrl = c.String(maxLength: 100),
                        Active = c.Boolean(nullable: false),
                        AccountId = c.Long(),
                        LastUpdated = c.DateTime(nullable: false),
                        LastUpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.AccountId)
                .Index(t => t.AccountId);

            Sql("insert into SubAccount (SubAccountName,AccountUrl, Active, AccountId, LastUpdated, LastUpdatedBy) " +
                "select AccountName,AccountUrl,1,Id,sysdatetime(),'SYSTEM' from Account");
            
            DropColumn("dbo.Account", "AccountUrl");
            DropColumn("dbo.Account", "Primary");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Account", "Primary", c => c.Boolean(nullable: false));
            AddColumn("dbo.Account", "AccountUrl", c => c.String(maxLength: 100));
            DropForeignKey("dbo.SubAccount", "AccountId", "dbo.Account");
            DropIndex("dbo.SubAccount", new[] { "AccountId" });
            DropTable("dbo.SubAccount");
        }
    }
}
