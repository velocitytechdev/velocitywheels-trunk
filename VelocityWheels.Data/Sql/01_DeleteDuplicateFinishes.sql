:setvar force "WHATIF"
SET NOCOUNT ON

declare @mode nvarchar(25) = upper(N'$(force)');

if @mode not in (N'WHATIF',N'FORCE')
begin
	declare @error nvarchar(max) = N'Invalid force sqlcmd variable value ''' + @mode + ''' Valid value is ''FORCE''';
	raiserror (@error, 16, 1);
	return;
end

select @mode = case when @mode = N'FORCE' then @mode else N'WHATIF' end;

begin transaction

declare @dupFinishes table
(
	  DuplicateFinishId bigint
	, SelectedFinishId bigint
);

; with dupFinishNames as
(
	select f.BrandId, f.Name
	from dbo.Finish as f
	group by f.BrandId, f.Name
	having count(f.Name) > 1
)
, dupFinishes as
(
	select 
		  f.Id as DuplicateFinishId
		, min(f.Id) over (partition by f.BrandId, f.Name) as SelectedFinishId
	from dbo.Finish as f
	inner join dupFinishNames as dfn
		on f.BrandId = dfn.BrandId and f.Name = dfn.Name
)
insert into @dupFinishes
select df.*
from dupFinishes as df
where df.DuplicateFinishId <> df.SelectedFinishId
;

declare @updateWheels table
(
	  Id bigint
	, ReplacedFinishId bigint
	, ReplacingFinishId bigint
);

update w
	set w.FinishId = df.SelectedFinishId
output 
	  deleted.Id
	, deleted.FinishId as ReplacedFinishId
	, inserted.FinishId as ReplacingFinishId
into @updateWheels
from dbo.Wheel as w
	inner join @dupFinishes as df
		on w.FinishId = df.DuplicateFinishId
;

declare @deletedFinishes table
(
	Id bigint,
	BrandId bigint,
	Name nvarchar(255),
	ShortName nvarchar(50),
	Active bit,
	LastUpdated datetime,
	LastUpdatedBy nvarchar(50)
);

delete f
output 
	  deleted.Id
	, deleted.BrandId
	, deleted.Name
	, deleted.ShortName
	, deleted.Active
	, deleted.LastUpdated
	, deleted.LastUpdatedBy
into @deletedFinishes
from dbo.Finish as f
	inner join @dupFinishes as df
		on f.Id = df.DuplicateFinishId
;

declare @newline nvarchar(2) = char(13) + char(10);

declare @updatedWheelsPrinted nvarchar(max);

select @updatedWheelsPrinted =
	case
		when @updatedWheelsPrinted is null then
			N'Id,ReplacedFinishId,ReplacingFinishId' + @newline +
			cast(uw.Id as nvarchar) + N','+ cast(uw.ReplacedFinishId as nvarchar) + N','+ cast(uw.ReplacingFinishId as nvarchar)
		else @updatedWheelsPrinted + @newline + 
			cast(uw.Id as nvarchar) + N','+ cast(uw.ReplacedFinishId as nvarchar) + N','+ cast(uw.ReplacingFinishId as nvarchar)
	end
from  @updateWheels as uw
order by uw.ReplacingFinishId asc, uw.ReplacedFinishId asc, uw.Id asc


declare @deletedFinishesPrinted nvarchar(max);

select @deletedFinishesPrinted =
	case
		when @deletedFinishesPrinted is null then
			N'Id,BrandId,Name,ShortName,Active,LastUpdated,LastUpdatedBy' + @newline +
			cast(df.Id as nvarchar) + N',' + 
			cast(df.BrandId as nvarchar) + N',' + 
			N'"' + df.Name + N'"' + N',' + 
			N'"' + df.ShortName + N'"' + N',' + 
			cast(df.BrandId as nvarchar) + N',' + 
			cast(df.Active as nvarchar) + N',' + 
			convert(nvarchar(max), df.LastUpdated, 127) + N',' + 
			N'"' + df.LastUpdatedBy + N'"'
		else @deletedFinishesPrinted + @newline + 
			cast(df.Id as nvarchar) + N',' + 
			cast(df.BrandId as nvarchar) + N',' + 
			N'"' + df.Name + N'"' + N',' + 
			N'"' + df.ShortName + N'"' + N',' + 
			cast(df.BrandId as nvarchar) + N',' + 
			cast(df.Active as nvarchar) + N',' + 
			convert(nvarchar(max), df.LastUpdated, 127) + N',' + 
			N'"' + df.LastUpdatedBy + N'"'
	end
from  @deletedFinishes as df
order by df.Id

if @mode = N'FORCE'
begin
	print N'FORCE MODE: The following changes were made.'
	print N''
	print N'dbo.Wheel record updates:'
	print @updatedWheelsPrinted
	print N''
	print N'dbo.Finish record deleteions:'
	print @deletedFinishesPrinted
	print N''

	commit transaction
end
else
begin
	print N'WHAT IF MODE: The following changes will be made if the SQLCMD parameter $force is set to FORCE.'
	print N''
	print N'dbo.Wheel record updates:'
	print @updatedWheelsPrinted
	print N''
	print N'dbo.Finish record deleteions:'
	print @deletedFinishesPrinted

	rollback transaction
end

