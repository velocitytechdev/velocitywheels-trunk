﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VelocityWheels.Data.Enum
{
    public enum ApiIdEnum : int
    {
        FuelApi = 1,
        DriveRight = 2,
        Iconfigurators = 3,
        Aces = 4,
        WheelPros = 5,
        Custom = 6
    }
}
