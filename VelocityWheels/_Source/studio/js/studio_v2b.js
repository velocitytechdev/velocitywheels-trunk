$(function() {
	"use strict";
	
	// Objects
	var $mainWheelleStage = $('#vtws-mainWheel');
	var $mainVehicleStage = $('#vtws-mainVehicle');
	var $uploadVehicle = document.getElementById('imgLoader');
	var $vehicleCanvas = $('canvas#v');
	var $studioWheelGrid = $('#studioWheelGrid');
		
	//Variables
	var vwiDir = 'studio/wheels/';
	var defaultWheel = vwiDir + 'VPS-306';
	var baseVehicleWidth = '1440';
	var baseVehicleHeight = '900';
	console.log( $mainVehicleStage.width() );
	var vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth ;
 	var setVehicleStageScale = function(startWidth, startHeight) {
		c_mainVehicle.setWidth(  startWidth * vehicleStageScale);
		c_mainVehicle.setHeight( startHeight * vehicleStageScale);
	};
	var paintLayerAlpha = 1;
	
	// Settings
	var handleSize = '15';
	var handleColor = '#0f0';
	
	// Main Vehicle Image
	function setVehicleBackground(canvas,image) {
		canvas.setBackgroundImage(image, canvas.renderAll.bind(canvas), {
			width: canvas.width,
			height: canvas.height,
			// Needed to position backgroundImage at 0/0
			originX: 'left',
			originY: 'top'
		});
	}


	// INITIATE VEHICLE CANVAS ============
	
	var c_mainVehicle = new fabric.Canvas('v');
	c_mainVehicle.setWidth( baseVehicleWidth * vehicleStageScale);
	c_mainVehicle.setHeight( baseVehicleHeight * vehicleStageScale);
	
	// Set Background Image
	setVehicleBackground(c_mainVehicle,'/studio/vehicles/Ford-Mustang_GT-2015-1024-01.jpg');
	

	// Front Wheel Group
	var wvF_main;
	var wvF_window;
	var wvF_face;
	var group_wvF;
	
	var wvf_defautlleft = c_mainVehicle.width*0.534;
	var wvf_defautltop = c_mainVehicle.height*0.595;
	var wvf_defautlwidth =  150 * vehicleStageScale;
	var wvf_defautlheight = 240 * vehicleStageScale;
	var wvf_defautlangle = 0.18; 
	
	fabric.Image.fromURL(defaultWheel+'/layers/wvF_main.png', function(img) {
		wvF_main = img.set({ 
	  		id:'main', visible:true, width: wvf_defautlwidth, height: wvf_defautlheight
		});
	
	  fabric.Image.fromURL(defaultWheel+'/layers/wvF_window.png', function(img) {
			wvF_window = img.set({ 
				id:'window', visible:false, opacity:paintLayerAlpha, width: wvf_defautlwidth, height: wvf_defautlheight 
			});
	
		 fabric.Image.fromURL(defaultWheel+'/layers/wvF_face.png', function(img) {
			wvF_face = img.set({ 
				id:'face', visible:false, opacity:paintLayerAlpha, width: wvf_defautlwidth, height: wvf_defautlheight 
			});
	
			group_wvF = new fabric.Group([ wvF_main, wvF_window, wvF_face], { 
				left: wvf_defautlleft,
				top: wvf_defautltop,
				angle: wvf_defautlangle,
				cornerSize: handleSize,
				cornerColor: handleColor,
				borderColor: handleColor
			});
			c_mainVehicle.add(group_wvF);
			$studioWheelGrid.height( $vehicleCanvas.height() )
		 });
	  });
	});
	
	// Rear Wheel Group
	var wvR_main;
	var wvR_window;
	var wvR_face;
	var group_wvR;
	
	var wvR_defautlleft = c_mainVehicle.width*0.888;
	var wvR_defautltop = c_mainVehicle.height*0.527;
	var wvR_defautlwidth =  69 * vehicleStageScale;
	var wvR_defautlheight = 175 * vehicleStageScale;
	var wvR_defautlangle = 3.6; 
	
	fabric.Image.fromURL(defaultWheel+'/layers/wvR_main.png', function(img) {
		wvR_main = img.set({ 
	  		id:'main', visible:true, width: wvR_defautlwidth, height: wvR_defautlheight
		});
	
	  fabric.Image.fromURL(defaultWheel+'/layers/wvR_window.png', function(img) {
			wvR_window = img.set({ 
				id:'window', visible:false, width: wvR_defautlwidth, height: wvR_defautlheight 
			});
	
		 fabric.Image.fromURL(defaultWheel+'/layers/wvR_face.png', function(img) {
			wvR_face = img.set({ 
				id:'face', visible:false, width: wvR_defautlwidth, height: wvR_defautlheight 
			});
	
			group_wvR = new fabric.Group([ wvR_main, wvR_window, wvR_face], { 
				left: wvR_defautlleft,
				top: wvR_defautltop,
				angle: wvR_defautlangle,
				cornerSize: handleSize,
				cornerColor: handleColor,
				borderColor: handleColor
			});
			c_mainVehicle.add(group_wvR);
		 });
	  });
	});

	
	// INITIATE WHEEL CANVAS ============
	// If Wheel Canvase is present
	if ( $mainWheelleStage.length ) {
		var c_mainWheel = new fabric.Canvas('w');
		c_mainWheel.setHeight($mainWheelleStage.width()).setWidth($mainWheelleStage.width());
		
		// Load Wheels
		function loadWheelLayer(canvas, target, imageSrc, opacity, name) {
			fabric.Image.fromURL(defaultWheel+'/layers/'+imageSrc, function(img) {
			target = img.set({ 
					id: name,
					opacity: paintLayerAlpha,
					visible: opacity,
					width: c_mainWheel.width,
					height: c_mainWheel.height,
					selectable: false,
					crossOrigin: "Anonymous"
			});
			canvas.add(target);
			//canvas.setActiveObject(target);
			if(name==='ib' || name==='main') {
				canvas.sendToBack(target);
			} else {
				canvas.bringToFront(target);
			}
			canvas.renderAll();
			//setLayerOrder();
			});
		}
		// Wheel Layers
		//var w_ib;
		//loadWheelLayer(c_mainWheel, w_ib, 'Inner-Barrel_base.png', true, 'ib');
		var w_main;
		loadWheelLayer(c_mainWheel, w_main, 'main.png', true, 'main');
		var w_window;
		loadWheelLayer(c_mainWheel, w_window, 'window.png', false, 'window');
		var w_face;
		loadWheelLayer(c_mainWheel, w_face, 'face.png', false, 'face');
	}
	
	// COLOR LAYER
	function colorLayer (canvas,target,color) {
		//var layer = canvas.item(id);
		//var layer = getObjectById(canvas,target);
		switch (color) {
			case ('null') :
				target.set({ visible:false });
				canvas.renderAll();
				break;
			default :
				target.set({ visible:true }).bringToFront();
				
				var colorize = new fabric.Image.filters.Blend({ color: color, mode: 'overlay' });
				target.filters[0] = colorize;
				
				setLayerOrder(); // to make sure layers are ordered correctly
				target.applyFilters(canvas.renderAll.bind(canvas));
				break;
		}
	}
	
	/*
	// HIDE LAYER
	function hideLayer(canvas,id) {
		var layer = getObjectById(canvas,id);
		layer.set({ visible:false });
   	canvas.renderAll();
	}
	function hideObject(layer) {
		layer.set({ visible:false });	
	}
	*/
	
	// CHANGE COLOR
	function changeLayerColor(layer, newColor) {
		var color = String(newColor);
		var wbTarget = getObjectById(c_mainWheel,layer);
		var wvfTarget = eval("wvF_"+layer);
		var wvrTarget = eval("wvR_"+layer);
	
		colorLayer(c_mainWheel, wbTarget, color);
		colorLayer(c_mainVehicle, wvfTarget, color);
		colorLayer(c_mainVehicle, wvrTarget, color);
	}
	
	// SET LAYER ORDER
	function setLayerOrder() {
		//var innerBarrel = getObjectById(c_mainWheel,'ib');
		var main = getObjectById(c_mainWheel,'main');
		
		c_mainWheel.sendToBack(main);
		//c_mainWheel.sendToBack(innerBarrel);
	}
	
	
	function getObjectById(canvas,id){
		var objsInCanvas = canvas.getObjects();
		for(var obj in objsInCanvas){
			if(objsInCanvas[obj].get('id')===id) {
				return objsInCanvas[obj]; 
			}
		}
	}	
	// COLOR PICKER
	$(".colorpicker").spectrum({
		 flat: false,
		 allowEmpty: true,
		 showInput: false,
		 chooseText: "Paint It",
		 cancelText: "Cancel",
		 showInitial: false,
		 showPalette: true,
		 showSelectionPalette: true,
		 replacerClassName: 'pickerBox',
		 maxPaletteSize: 10,
		 preferredFormat: "hex",
		 localStorageKey: "cp.wheel",
		 show: function () {
		 },
		 change: function() {
		 },
		 hide: function (newColor) {
		 		// Action
				var layer = String( $(this).parent().data('layer') );
				changeLayerColor(layer, newColor);
		 },
		 palette: [
			  ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
			  "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)", 
			  "rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
			  "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"]
		 ]
	});
	
	// Flip X
	$('#vtws-flipx').click(function(){
		// Angles
		var wvF_angle = group_wvF.getAngle();
		var wvR_angle = group_wvR.getAngle();
		
		// Left
		var width = c_mainVehicle.getWidth();
		var wvF_left = group_wvF.getLeft();
		var wvF_newLeft = width - wvF_left;
		
		var wvR_left = group_wvR.getLeft();
		var wvR_newLeft = width - wvR_left;
		console.log(wvR_left + " " + width + " " +wvR_newLeft);
		
		group_wvF.toggle('flipX').set({ angle:-wvF_angle, left:wvF_newLeft }).setCoords();
		group_wvR.toggle('flipX').set({ angle:-wvR_angle, left:wvR_newLeft }).setCoords();
		c_mainVehicle.renderAll();
	});

	// USER UPLOAD OWN VEHICLE IMAGE
	$uploadVehicle.onchange = function handleImage(e) {
		var reader = new FileReader();
		reader.onload = function(event) { 
				var image = new Image();
				image.src = event.target.result;
				image.onload = function() {
					vehicleStageScale = $mainVehicleStage.width() / this.width ;
					setVehicleStageScale(this.width, this.height);
					setVehicleBackground(c_mainVehicle,image.src) ;      
				 };
			 };
		reader.readAsDataURL(e.target.files[0]);
		
		// Toggle Steps
		//$('#step1').foundation('toggle');
		//$('#step2').foundation('toggle');
	};
	
	// CHANGE IMAGE
	var $changeVehicle = $('.changeImage');
	var $selectImageDiv = $('#vtws-imageSelect');

	$changeVehicle.click( function(){
		$('input#imgLoader').val('');
		$selectImageDiv.foundation('toggle');
		//location.reload();
	});
		
});