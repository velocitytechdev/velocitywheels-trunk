<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/header.php'); ?>

<div class="headerSpacer"></div>

<div id="content" class="page">

	<div class="row show-for-small-only">
		<div class="columns">
			<a class="toggleLink clearButton w100" data-toggletarget='#sidebar'>Vehicle Info </a>
		</div>
	</div>
	
	<div class="row">
		<div id="sidebar" class="columns" data-sticky-container>
			<div class="sidebar sticky" data-sticky data-margin-top="7" data-anchor="sidebar">
				<div class="sidebar-section">
					<?php include('inc/vehicleSelection.php'); ?>
				</div>
				<div class="sidebar-section">
					<h5>Wheels</h5>
					<?php include('inc/userWheels.php'); ?>
					
					<h5 style="margin-top:1.5em">Tires</h5>
					<?php include('inc/userTires.php'); ?>
				</div>
				<div class="sidebar-section actions">
					<a href="/shoppingList.php" class="button">Review Build List</a>
				</div>
			</div>
		</div>
		
		<div class="columns pageBody">
			
			<h1 class="pageTitle">Wheels</h1>
			
			<div class="noVehicle">
				<div class="vehicleSelection-h">
					Please select your vehicle on the left.
				</div>
			</div>
			
			<div class="yesVehicle hide">
				<div class="noSubmodel">
					<p><strong>Which type of Mustang do you have?</strong></p>
					
					<select name="vs-submodel" id="vs-submodel">
						<option value="">PLEASE SELECT ------</option>
						<option value="EcoBoost">EcoBoost</option>
						<option value="EcoBoost Premium">EcoBoost Premium</option>
						<option value="GT">GT</option>
						<option value="GT Performance">GT Performance</option>
						<option value="GT Premium">GT Premium</option>
						<option value="Shelby GT350">Shelby GT350</option>
						<option value="Shelby GT350R">Shelby GT350R</option>
						<option value="V6">V6</option>
						<option value="V6 Performance Pkg">V6 Performance Pkg</option>
					</select>
				</div>
				
				<div class="yesSubmodel hide">
					<a href="wheel-results.php" class="button">View Wheels</a>
				</div>
			</div>
						
		</div>
	</div>
</div>


<!-- Change Vehicle Modal -->
<div class="reveal" id="addOptions" data-reveal  data-animation-in="hinge-in-from-top" data-animation-out="hinge-out-from-top">
	<button class="close-button" data-close aria-label="Close modal" type="button"><span aria-hidden="true">&times;</span></button>
	<h2 class="headerPadded">Please Select Your Product Options</h2>
	<?php require($_SERVER['DOCUMENT_ROOT'].'/snippets/wheelSizeOptions.php'); ?>
</div>



<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>


<!-- Tips Slider -->

<script>
	$(document).ready(function(){
		
	// Get VID from Cookie
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length,c.length);
			}
		}
		return "";
	}
	if ( getCookie('smdl') ) {
			window.location ='wheel-results.php';
		}
	});
	
</script>


</body>
</html>