<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/header.php'); ?>

<div class="headerSpacer"></div>

<div id="content" class="page">	

	<div class="row show-for-small-only">
		<div class="columns">
			<a class="toggleLink clearButton w100" data-toggletarget='#sidebar'>Vehicle Info </a>
		</div>
	</div>
	
	<div class="row">
		<div id="sidebar" class="columns" data-sticky-container>
			<div class="sidebar sticky" data-sticky data-margin-top="7" data-anchor="sidebar">
				<div class="sidebar-section">
					<?php include('inc/vehicleSelection.php'); ?>
				</div>
				<div class="sidebar-section">
					<h5>Wheels</h5>
					<?php include('inc/userWheels.php'); ?>
					
					<h5 style="margin-top:1.5em">Tires</h5>
					<?php include('inc/userTires.php'); ?>
				</div>
				<div class="sidebar-section actions">
					<a href="/shoppingList.php" class="button">Review Build List</a>
				</div>
			</div>
		</div>
		
		<div class="columns pageBody">
			
			<h1 class="pageTitle">Tires</h1>
			
			<div class="noWheels">
				<p>To get started, please tell us a little more about the tires you are looking for.</p>
				
				<ul class="accordion" data-accordion>
					<li class="accordion-item" data-accordion-item>
					
						<a href="#" class="accordion-title"><h5>Search by Vehicle</h5></a>
						
						<div class="accordion-content" data-tab-content>
						<div class="noVehicle">
							Please select a vehicle on the left to search by vehicle.
						</div>
						
						<div class="yesVehicle hide">
							<div class="noSubmodel">
								<p><strong>Which type of Mustang do you have?</strong></p>
								
								<select name="vs-submodel" id="vs-submodel">
									<option value="">PLEASE SELECT ------</option>
									<option value="EcoBoost">EcoBoost</option>
									<option value="EcoBoost Premium">EcoBoost Premium</option>
									<option value="GT">GT</option>
									<option value="GT Performance">GT Performance</option>
									<option value="GT Premium">GT Premium</option>
									<option value="Shelby GT350">Shelby GT350</option>
									<option value="Shelby GT350R">Shelby GT350R</option>
									<option value="V6">V6</option>
									<option value="V6 Performance Pkg">V6 Performance Pkg</option>
								</select>
							</div>
							
							<div class="yesSubmodel hide">
								<p><strong>Please select your wheel size.</strong></p>
								<div class="row">
									<div class="columns text-center"><a class="button addWheels">18x8.5"</a></div>
									<div class="columns text-center"><a class="button addWheels">19x8.5"</a></div>
									<div class="columns text-center"><a class="button addWheels">19x9" + 19x9.5"</a></div>
									<div class="columns text-center"><a class="button addWheels">20x9"</a></div>
								</div>
							</div>
						</div>
						</div>
					</li>
					
					<li class="accordion-item is-active" data-accordion-item>
						<a href="#" class="accordion-title"><h5>Search by Size</h5></a>
						<div class="accordion-content" data-tab-content>
							<div class="row">
								<div class="columns">
									<div class="row noGutter">
										<div class="columns">
											<select name="" id="">
												<option value="">225</option>
											</select>
										</div>
										
										<div class="columns">
											<select name="" id="">
												<option value="">45</option>
											</select>
										</div>
										
										<div class="columns">
											<select name="" id="">
												<option value="">17</option>
											</select>
										</div>
									</div>
								
								</div>
								
								<div class="columns">
									<div class="noStaggered text-center">
										<a href="#" class="button button-gray"><i class="fa fa-plus"></i> Add a staggered rear size</a>
									</div>
									<div class="yesStaggered hide">
									</div>
								</div>
							</div>
							<div style="margin-top:0.5em;">
								
							</div>
						</div>
					</li>
				</ul>
								
			</div>
			
			<div class="yesWheels hide">
				<div class="box-padded text-center">
					<p><strong>Please select your tire size(s):</strong></p>
					
					<div class="row">
						<div class="columns">
							<fieldset>
								<legend>Front Size</legend>
								<input type="radio" class="radioAsBtn text-center" name="tireFr" value="255/40-19" id="frt1" checked>
								<label for="frt1"> 255/40-19</label>
								
								<input type="radio" class="radioAsBtn text-center" name="tireFr" value="265/35-19" id="frt2">
								<label for="frt2"> 265/35-19</label>
							</fieldset>
						</div>
						
						<div class="columns">
							<div class="columns">
							<fieldset>
								<legend>Rear Size</legend>
								<input type="radio" class="radioAsBtn text-center" name="tireRr" value="275/35-19" id="rrt1" checked>
								<label for="rrt1"> 275/35-19</label>
								
								<input type="radio" class="radioAsBtn text-center" name="tireRr" value="295/30-19" id="rrt2" >
								<label for="rrt2"> 295/30-19</label>
							</fieldset>
						</div>
						</div>
						
						<div class="columns shrink">
							<legend></legend><br/>
							<a href="tire-results.php" class="button">Go</a>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
</div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>


</body>
</html>