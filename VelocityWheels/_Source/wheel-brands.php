<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/header.php'); ?>

<div class="headerSpacer"></div>

<div id="content" class="page">
	<div class="row show-for-small-only">
		<div class="columns">
			<a class="toggleLink clearButton w100" data-toggletarget='#sidebar'>Vehicle Info </a>
		</div>
	</div>
	
	<div class="row">
		<div id="sidebar" class="columns" data-sticky-container>
			<div class="sidebar sticky" data-sticky data-margin-top="7" data-anchor="sidebar">
				<div class="sidebar-section">
					<?php include('inc/vehicleSelection.php'); ?>
				</div>
				<div class="sidebar-section">
					<h5>Wheels</h5>
					<?php include('inc/userWheels.php'); ?>
					
					<h5 style="margin-top:1.5em">Tires</h5>
					<?php include('inc/userTires.php'); ?>
				</div>
				<div class="sidebar-section actions">
					<a href="/shoppingList.php" class="button">Review Build List</a>
				</div>
			</div>
		</div>
		
		<div class="columns pageBody">
			
			<h1>Wheel Brands We Carry</h1>
			
			
			<div class="row small-up-2 medium-up-5 logoGrid">
				<?php  for ($x = 0; $x <= 50; $x++) { ?>
				<div class="column brandTile">
					<a href="#">
					<div class="brand">
						<div class="image">
							<img src="http://placehold.it/130x20">
						</div>
						<div class="brandName">
							Brand Name
						</div>
					</div>
					</a>
				</div>
				<div class="column brandTile">
					<a href="#">
					<div class="brand">
						<div class="image">
							<img src="http://placehold.it/120x30">
						</div>
						<div class="brandName">
							Brand Name
						</div>
					</div>
					</a>
				</div>
				<?php } ?>
			</div>
			
		</div>
	</div>
</div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>


</body>
</html>