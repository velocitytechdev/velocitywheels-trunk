<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/header.php'); ?>

<div class="headerSpacer"></div>

<div id="content" class="page">

	<div class="row show-for-small-only">
		<div class="columns">
			<a class="toggleLink clearButton w100" data-toggletarget='#sidebar'>Vehicle Info </a>
		</div>
	</div>
	
	<div class="row">
		<div id="sidebar" class="columns" data-sticky-container>
			<div class="sidebar sticky" data-sticky data-margin-top="7" data-anchor="sidebar">
				<div class="sidebar-section">
					<?php include('inc/vehicleSelection.php'); ?>
				</div>
				<div class="sidebar-section">
					<h5>Wheels</h5>
					<?php include('inc/userWheels.php'); ?>
					
					<h5 style="margin-top:1.5em">Tires</h5>
					<?php include('inc/userTires.php'); ?>
				</div>
				<div class="sidebar-section actions">
					<a href="/shoppingList.php" class="button">Review Build List</a>
				</div>
			</div>
		</div>
		
		<div class="columns pageBody">
			
			<h1 class="pageTitle">Wheels</h1>
			
				<div class="noSubmodel">
					<p><strong>Which type of Mustang do you have?</strong></p>
					
					<select name="vs-submodel" id="vs-submodel">
						<option value="">PLEASE SELECT ------</option>
						<option value="EcoBoost">EcoBoost</option>
						<option value="EcoBoost Premium">EcoBoost Premium</option>
						<option value="GT">GT</option>
						<option value="GT Performance">GT Performance</option>
						<option value="GT Premium">GT Premium</option>
						<option value="Shelby GT350">Shelby GT350</option>
						<option value="Shelby GT350R">Shelby GT350R</option>
						<option value="V6">V6</option>
						<option value="V6 Performance Pkg">V6 Performance Pkg</option>
					</select>
				</div>
				
				<div class="yesSubmodel">
					
					<div id="vtws">
					
						<!-- Main Vehicle Image -->
						<div id="vtws-mainImage" data-toggler=".hide">
						
							<!-- Action Buttons -->
							<div id="vtws-mainActions">
								<div class="row">
									<div class="columns">
										<a id="vtws-changeVehicle" data-open="vtws-imageSelect" class="button button-small">Change Image</a>
									</div>
									<div class="columns text-right">
										<a id="vtws-flipx" class="button button-small"><i class="fa fa-exchange"></i> <span>Flip Wheels</span></a>
										<a class="button button-small lauchTour"><i class="fa fa-question-circle"></i></a>
									</div>
								</div>
						    </div>
							
							<!-- Main Vehicle -->
							<div id="vtws-mainVehicle">
								
								<div class="vtws-vehicleWrapper">
									<canvas id="v"></canvas>
							    </div>

						    </div><!--mainVehicle-->

						</div>


						<!--- Select Image --->
						<div id="vtws-imageSelect" class="large reveal" data-reveal>
							<button class="close-button" data-close aria-label="Close modal" type="button">
								<span aria-hidden="true">&times;</span>
							</button>
							<?php include($_SERVER['DOCUMENT_ROOT'].'/snippets/vtws-imageSelect.php'); ?>
						</div>
					

					
					</div>
					
					<br />
					<div class="callout primary">Please note, this site, and the wheels listed below are <strong>for demonstration purposes only</strong>. They are just a few examples to demonstrate functionality and do not take into account fitment. </div>
					
					<!-- Product Listing -->
					<?php include('snippets/filters-wheels.php'); ?>
				
					<div id="productGrid" class="productGrid" data-equalizer="wheels">
						<?php  for ($x = 0; $x <= 3; $x++) { ?>
						<div class="wheelTile">
							<div class="tileContent">
								<div class="productImage">
									<a class="previewWheelBtn" data-wheelid="o_wheel2"><img src="images/wheels/km694/km694_black.png" width="500" height="500" alt="KMC KM694"/></a>
								</div>
								<div class="productBrand">KMC</div>
								<div class="productName">KM694 Wishbone</div>
								<div class="productFinish">Satin Black</div>
								<div class="productOptions">
									<ul class="wheelSizeOptions">
										<li>19"</li>
										<li>20"</li>
									</ul>
								</div>
								<div class="productInfo">
									<small>Starting At:</small>
									<span class="price">$176</span> <small>ea.</small>
								</div>
								<div class="actions">
									<a href="wheel-details.php" class="button button-small button-gray">Details</a>
									<a class="previewWheelBtn button button-small button-gray" data-wheelid="o_wheel2">Preview</a>
									<a class="button button-small" data-open="addOptions">Add to List</a>
								</div>
					
							</div>
						</div>
						
						<div class="wheelTile">
							<div class="tileContent">
								<div class="productImage colorable">
									<a class="previewWheelBtn" data-wheelid="o_wheel1"><img src="images/wheels/VPS-306/VPS-306.png" width="476" height="480" alt="KMC KM694"/></a>
								</div>
								<div class="productBrand">Vossen</div>
								<div class="productName">VPS-306</div>
								<div class="productFinish">Customizable Finish</div>
								<div class="productOptions">
									<ul class="wheelSizeOptions">
										<li>19"</li>
										<li>20"</li>
										<li>21"</li>
										<li>22"</li>
									</ul>
								</div>
								<div class="productInfo">
									<small>Starting At:</small>
									<span class="price">$1,350</span> <small>ea.</small> 
								</div>
								<div class="actions">
									<a class="button button-small button-gray" href="wheel-details-custom.php">Details</a>
									<a class="previewWheelBtn button button-small button-gray" data-wheelid="o_wheel1">Preview</a>
									<a class="previewWheelBtn button button-small" data-open="paintWheel" data-wheelid="o_wheel1">Customize</a>
									<a class="button button-small" data-open="addOptions">Add to List</a>
								</div>
					
							</div>
						</div>
						
						<div class="wheelTile">
							<div class="tileContent">
								<div class="productImage">
									<a class="previewWheelBtn" data-wheelid="o_wheel3"><img src="images/wheels/Anza/Anza_bronze.png" width="500" height="500" alt="KMC KM694"/></a>
								</div>
								<div class="productBrand">Fuel Off Road</div>
								<div class="productName">ANZA</div>
								<div class="productFinish">Matte Bronze w/ Black Ring</div>
								<div class="productOptions">
									<ul class="wheelSizeOptions">
										<li>17"</li>
										<li>18"</li>
									</ul>
								</div>
								<div class="productInfo">
									<small>Starting At:</small>
									<span class="price">$232</span> <small>ea.</small>
								</div>
								<div class="actions">
									<a href="wheel-details.php" class="button button-small button-gray">Details</a>
									<a class="previewWheelBtn button button-small button-gray" data-wheelid="o_wheel3">Preview</a>
									<a class="button button-small" data-open="addOptions">Add to List</a>
								</div>
					
							</div>
						</div>
						
						<div class="wheelTile">
							<div class="tileContent">
								<div class="productImage">
									<a class="previewWheelBtn" data-wheelid="o_wheel4"><img src="images/wheels/MR_305/machined.png" width="500" height="500" alt="Method MR 305"/></a>
								</div>
								<div class="productBrand">Method Race Wheels</div>
								<div class="productName">MR305</div>
								<div class="productFinish">Matte Black Machined Face</div>
								<div class="productOptions">
									<ul class="wheelSizeOptions">
										<li>16"</li>
										<li>17"</li>
										<li>18"</li>
										<li>20"</li>
									</ul>
								</div>
								<div class="productInfo">
									<small>Starting At:</small>
									<span class="price">$183.50</span> <small>ea.</small>
								</div>
								<div class="actions">
									<a href="wheel-details.php" class="button button-small button-gray">Details</a>
									<a class="previewWheelBtn button button-small button-gray" data-wheelid="o_wheel4">Preview</a>
									<a class="button button-small" data-open="addOptions">Add to List</a>
								</div>
					
							</div>
						</div>
						<?php } ?>
					</div>

				</div>
			</div>
						
		</div>
	</div>
</div>

<!-- Joyride Outline -->
<ol id="vtws-tour" class="hide">
	<li data-id="vtws-changeVehicle" data-button="Next" data-options="tipLocation:bottom;">
		<h2>Change Vehicle Image</h2>
		<p>Choose from our vehicle image library of images, or upload your own.</p>
	</li>
	<li class="vtwsTour-mainWindow" data-id="v" data-button="Next" data-options="tipLocation:bottom;">
		<h2>Touch And Reposition</h2>
		<p>You can press on the wheels and resize, reposition and rotate each wheel to put them exactly where you want them.</p>
	</li>
	<li class="vtwsTour-left" data-id="vtws-flipx" data-button="Close" data-options="tipLocation:left;">
		<h2>Flip Wheels</h2>
		<p>If the wheels are facing the wrong direction, flip the horizontally with this button.</p>
	</li>
</ol>

<!-- Change Vehicle Modal -->
<div class="reveal" id="addOptions" data-reveal  data-animation-in="hinge-in-from-top" data-animation-out="hinge-out-from-top">
	<button class="close-button" data-close aria-label="Close modal" type="button"><span aria-hidden="true">&times;</span></button>
	<h2 class="headerPadded">Please Select Your Product Options</h2>
	<?php require($_SERVER['DOCUMENT_ROOT'].'/snippets/wheelSizeOptions.php'); ?>
</div>


<!-- Customize Wheel Modal -->
<div class="reveal" id="paintWheel" data-reveal  data-animation-in="hinge-in-from-top" data-animation-out="hinge-out-from-top">
	<button class="close-button" data-close aria-label="Close modal" type="button"><span aria-hidden="true">&times;</span></button>
	<?php require($_SERVER['DOCUMENT_ROOT'].'/snippets/paintWheel.php'); ?>
</div>


<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>

<!-- Photo Studio -->
<script src="/studio/js/colorpicker.js"></script>
<script src="/studio/js/colorizer1.5.js"></script>
<script src="/studio/js/studio_v2d.js"></script>

<!-- Joyride -->
<script type="text/javascript" src="/js/joyride.js"></script>


<!-- Tips Slider -->

<script>
// Tour

$(document).ready(function(){
	
	// Tour
	var startTour = function(){
		$('#vtws-tour').joyride({
			autoStart : true,
			modal: false,
			expose: true,
			'tipAnimation': 'pop',
			//'timer': 3000,
			'startTimerOnClick': false
		});
	}
	$('.lauchTour').click(startTour);
	//startTour();
	
	// Upload Tips
	$('#uploadTips').slick({
	  dots: true,
	  arrows: false,
	  infinite: true,
	  speed: 500,
	  slidesToShow: 1,
	  autoplay: true,
	  fade: false,
	  autoplaySpeed: 4000,
	  adaptiveHeight: false,
	  dotsClass: 'slick-dots slick-dots-block',
	  lazyLoad: 'ondemand'
	});
	
	// Wheel Listings
	function slickWheels() {
		$('#productGrid').slick({
			dots: true,
			arrows: true,
			infinite: true,
			speed: 500,
			slidesToShow: 4,
			slidesToScroll: 4,
			autoplay: false,
			fade: false,
			autoplaySpeed: 6000,
			adaptiveHeight: false,
			responsive: [
				{
				  breakpoint: 560,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				}
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
				// instead of a settings object
			],
			dotsClass: 'slick-dots-numbers',
			lazyLoad: 'ondemand'
		});
	}
	slickWheels();
});
</script>


</body>
</html>