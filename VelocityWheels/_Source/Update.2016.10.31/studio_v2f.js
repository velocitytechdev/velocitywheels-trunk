$(function() {
	"use strict";
	
	// Objects
	var $mainWheelleStage = $('#vtws-mainWheel');
	var $mainVehicleStage = $('#vtws-mainVehicle');
	var $uploadVehicle = document.getElementById('imgLoader');
	var $vehicleCanvas = $('canvas#v');
	//var $wheelGrid = $('#studioWheelGrid');
		
	//Variables
	var vwiDir = 'studio/wheels/';
	var defaultWheel = vwiDir + 'VPS-306';
	var baseVehicleWidth = '1440';
	var baseVehicleHeight = '900';
	//console.log( $mainVehicleStage.width() );
	var vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth ;
 	var setVehicleStageScale = function(startWidth, startHeight) {
		c_mainVehicle.setWidth(  startWidth * vehicleStageScale);
		c_mainVehicle.setHeight( startHeight * vehicleStageScale);
	};
	var paintLayerAlpha = 1;
	
	var wheelBrightness = 0;
	var wheelContrast = 0;

	
	// Settings
	var handleSize = '15';
	var handleColor = '#0f0';
	
	// Main Vehicle Image
	function setVehicleBackground(canvas,image) {
		canvas.setBackgroundImage(image, canvas.renderAll.bind(canvas), {
			width: canvas.width,
			height: canvas.height,
			// Needed to position backgroundImage at 0/0
			originX: 'left',
			originY: 'top'
		});
	}


	// INITIATE VEHICLE CANVAS ============
	var c_mainVehicle = new fabric.Canvas('v');
	c_mainVehicle.setWidth( baseVehicleWidth * vehicleStageScale);
	c_mainVehicle.setHeight( baseVehicleHeight * vehicleStageScale);
	
	// Set Default Background Image
	setVehicleBackground(c_mainVehicle,'/studio/vehicles/Ford-Mustang_GT-2015-1024-01.jpg');
	
	// Front Wheel Group
	var wvF_main;
	var wvF_window;
	var wvF_face;
	var group_wvF;
	
	var wvF_left = c_mainVehicle.width*0.534;
	var wvF_top = c_mainVehicle.height*0.595;
	var wvF_width =  150 * vehicleStageScale;
	var wvF_height = 240 * vehicleStageScale;
	var wvF_angle = 0.18; 

	
	// Rear Wheel Group
	var wvR_main;
	var wvR_window;
	var wvR_face;
	var group_wvR;
	
	var wvR_left = c_mainVehicle.width*0.888;
	var wvR_top = c_mainVehicle.height*0.527;
	var wvR_width =  69 * vehicleStageScale;
	var wvR_height = 175 * vehicleStageScale;
	var wvR_angle = 3.6; 
	
	// Load Wheel Images
	var o_wheel1 = {
		frontWheelBase: 	defaultWheel+'/layers/wvF_main.png',
		frontWheelFace: 	defaultWheel+'/layers/wvF_face.png',
		frontWheelWindow:	defaultWheel+'/layers/wvF_window.png',
		rearWheelBase: 		defaultWheel+'/layers/wvR_main.png',
		rearWheelFace: 		defaultWheel+'/layers/wvR_face.png',
		rearWheelWindow: 	defaultWheel+'/layers/wvR_window.png'
	};
	
	var o_wheel2 = {
		frontWheelBase: 	vwiDir + 'KM694/km694_ov_front.png',
		rearWheelBase:  	vwiDir + 'KM694/km694_ov_rear.png',
		frontWheelFace: 	vwiDir + 'blank.png',
		frontWheelWindow:	vwiDir + 'blank.png',
		rearWheelFace: 		vwiDir + 'blank.png',
		rearWheelWindow: 	vwiDir + 'blank.png'
	};
	
	var o_wheel3 = {
		frontWheelBase: 	vwiDir + 'Anza-bronze/d583_ov_front.png',
		rearWheelBase:  	vwiDir + 'Anza-bronze/d583_ov_rear.png',
		frontWheelFace: 	vwiDir + 'blank.png',
		frontWheelWindow:	vwiDir + 'blank.png',
		rearWheelFace: 		vwiDir + 'blank.png',
		rearWheelWindow: 	vwiDir + 'blank.png'
	};
	
	var o_wheel4 = {
		frontWheelBase: 	vwiDir + 'MR-305/mr-305_ov_front.png',
		rearWheelBase:  	vwiDir + 'MR-305/mr-305_ov_rear.png',
		frontWheelFace: 	vwiDir + 'blank.png',
		frontWheelWindow:	vwiDir + 'blank.png',
		rearWheelFace: 		vwiDir + 'blank.png',
		rearWheelWindow: 	vwiDir + 'blank.png'
	};
	
	function loadWheels(wheel) {
		
		//Set Layer Array (used to load group for customizable wheels)
		var wheelLayersFront = [];
		var wheelLayersRear = [];
		
		// Load Front Wheel
		fabric.Image.fromURL( wheel.frontWheelBase, function(img) {
		wvF_main = img.set({ 
	  		id:'main', visible:true, width: wvF_width, height: wvF_height
			});
			wheelLayersFront.push(wvF_main);
	
		  	fabric.Image.fromURL( wheel.frontWheelWindow, function(img) {
				wvF_window = img.set({ 
					id:'window', visible:false, opacity:paintLayerAlpha, width: wvF_width, height: wvF_height 
				});
			   wheelLayersFront.push(wvF_window);

			fabric.Image.fromURL( wheel.frontWheelFace, function(img) {
				wvF_face = img.set({ 
					id:'face', visible:false, opacity:paintLayerAlpha, width: wvF_width, height: wvF_height 
				});
				wheelLayersFront.push(wvF_face);

				group_wvF = new fabric.Group(wheelLayersFront, { 
					left: wvF_left,
					top: wvF_top,
					angle: wvF_angle,
					//cornerSize: handleSize,
					cornerColor: handleColor,
					borderColor: handleColor,
					
				});
				c_mainVehicle.add(group_wvF);
				
				// set Brightness
				setBrightness(wvR_main, wheelBrightness);
				
			 });
		  });
		});
		
		
		// Load Rear Wheel
		fabric.Image.fromURL( wheel.rearWheelBase, function(img) {
		wvR_main = img.set({ 
	  		id:'main', visible:true, width: wvR_width, height: wvR_height
		});
		wheelLayersRear.push(wvR_main);
	
		  fabric.Image.fromURL( wheel.rearWheelWindow, function(img) {
				wvR_window = img.set({ 
					id:'window', visible:false, width: wvR_width, height: wvR_height 
				});
			   wheelLayersRear.push(wvR_window);

			 fabric.Image.fromURL( wheel.rearWheelFace, function(img) {
				wvR_face = img.set({ 
					id:'face', visible:false, width: wvR_width, height: wvR_height 
				});
				wheelLayersRear.push(wvR_face);

				group_wvR = new fabric.Group( wheelLayersRear, { 
					left: wvR_left,
					top: wvR_top,
					angle: wvR_angle,
					//cornerSize: handleSize,
					cornerColor: handleColor,
					borderColor: handleColor
				});
				c_mainVehicle.add(group_wvR);
				 
				// set Brightness
				setBrightness(wvR_main, wheelBrightness);

			 });
		  });
		});
		
		
	}
	//Load Default Wheels
	//loadWheels(o_wheel1);
	
	// WPDATE WHEEL FROM PAGE
	var $previewWheelBtn = $('.previewWheelBtn');
	$previewWheelBtn.click(function(){
		var thisID = $(this).data('wheelid');		
		
		//Test if Wheels Already Loaded
		if( group_wvR ){
			// WHEELS ARE LOADED
			// Remove Previous Objects since layers may be different
			c_mainVehicle.remove(group_wvF);
			c_mainVehicle.remove(group_wvR);
			
			// Test Position
			updatePositions();
			
		}
		// USED FOR TESTING ONLY -- EVAL() SHOULD BE REPLACED!--------
		loadWheels( eval(thisID) );
	
	});
	
	// TEST POSITION OF WHEELS	
	function updatePositions() {
		
		// Front Wheel
		wvF_left = group_wvF.left ;
		wvF_top = group_wvF.top;
		wvF_width =  group_wvF.getWidth();
		wvF_height = group_wvF.getHeight();
		wvF_angle = group_wvF.angle; 

		// Rear Wheel
		wvR_left = group_wvR.left ;
		wvR_top = group_wvR.top;
		wvR_width =  group_wvR.width;
		wvR_height = group_wvR.height;
		wvR_angle = group_wvR.angle; 

	}
	

	// INITIATE WHEEL CANVAS FOR CUSTOM WHEELS ============
	// If Wheel Canvas is present
	if ( $mainWheelleStage.length ) {
		var c_mainWheel = new fabric.Canvas('w');
		c_mainWheel.setHeight($mainWheelleStage.width()).setWidth($mainWheelleStage.width());
		
		// Load Wheels
		function loadWheelLayer(canvas, target, imageSrc, opacity, name) {
			fabric.Image.fromURL(defaultWheel+'/layers/'+imageSrc, function(img) {
			target = img.set({ 
					id: name,
					opacity: paintLayerAlpha,
					visible: opacity,
					width: c_mainWheel.width,
					height: c_mainWheel.height,
					selectable: false,
					crossOrigin: "Anonymous"
			});
			canvas.add(target);
			//canvas.setActiveObject(target);
			if(name==='ib' || name==='main') {
				canvas.sendToBack(target);
			} else {
				canvas.bringToFront(target);
			}
			canvas.renderAll();
			//setLayerOrder();
			});
		}
		// Wheel Layers
		//var w_ib;
		//loadWheelLayer(c_mainWheel, w_ib, 'Inner-Barrel_base.png', true, 'ib');
		var w_main;
		loadWheelLayer(c_mainWheel, w_main, 'main.png', true, 'main');
		var w_window;
		loadWheelLayer(c_mainWheel, w_window, 'window.png', false, 'window');
		var w_face;
		loadWheelLayer(c_mainWheel, w_face, 'face.png', false, 'face');
	}

	
	// COLOR LAYER
	function colorLayer (canvas,target,color) {
		//var layer = canvas.item(id);
		//var layer = getObjectById(canvas,target);
		switch (color) {
			case ('null') :
				target.set({ visible:false });
				canvas.renderAll();
				break;
			default :
				target.set({ visible:true }).bringToFront();
				
				var colorize = new fabric.Image.filters.Blend({ color: color, mode: 'overlay' });
				target.filters[0] = colorize;
				
				setLayerOrder(); // to make sure layers are ordered correctly
				target.applyFilters(canvas.renderAll.bind(canvas));
				break;
		}
	}
	
	// HIDE LAYER
	function hideLayer(canvas,id) {
		var layer = getObjectById(canvas,id);
		layer.set({ visible:false });
   	canvas.renderAll();
	}
	function hideObject(layer) {
		layer.set({ visible:false });	
	}
	
	// CHANGE COLOR
	function changeLayerColor(layer, newColor) {
		var color = String(newColor);
		var wbTarget = getObjectById(c_mainWheel,layer);
		var wvfTarget = eval("wvF_"+layer);
		console.log( "layer: wvF_" + layer);
		var wvrTarget = eval("wvR_"+layer);
	
		colorLayer(c_mainWheel, wbTarget, color);
		colorLayer(c_mainVehicle, wvfTarget, color);
		colorLayer(c_mainVehicle, wvrTarget, color);
	}
	
	// SET LAYER ORDER
	function setLayerOrder() {
		//var innerBarrel = getObjectById(c_mainWheel,'ib');
		var main = getObjectById(c_mainWheel,'main');
		
		c_mainWheel.sendToBack(main);
		//c_mainWheel.sendToBack(innerBarrel);
	}
	
	
	function getObjectById(canvas,id){
		var objsInCanvas = canvas.getObjects();
		for(var obj in objsInCanvas){
			if(objsInCanvas[obj].get('id')===id) {
				return objsInCanvas[obj]; 
			}
		}
	}	
	
	// COLOR PICKER
	$(".colorpicker").spectrum({
		 flat: false,
		 allowEmpty: true,
		 showInput: false,
		 chooseText: "Paint It",
		 cancelText: "Cancel",
		 showInitial: false,
		 showPalette: true,
		 showSelectionPalette: true,
		 replacerClassName: 'pickerBox',
		 maxPaletteSize: 10,
		 preferredFormat: "hex",
		 localStorageKey: "cp.wheel",
		 show: function () {
		 },
		 change: function() {
		 },
		 hide: function (newColor) {
		 		// Action
				var layer = String( $(this).parent().data('layer') );
				changeLayerColor(layer, newColor);
		 },
		 palette: [
			  ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
			  "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)", 
			  "rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
			  "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"]
		 ]
	});
	
	
	// CONTROLS ========================
	// Flip X
	$('#vtws-flipx').click(function(){
		// Angles
		var wvF_angle = group_wvF.getAngle();
		var wvR_angle = group_wvR.getAngle();
		
		// Left
		var width = c_mainVehicle.getWidth();
		var wvF_left = group_wvF.getLeft();
		var wvF_newLeft = width - wvF_left;
		
		var wvR_left = group_wvR.getLeft();
		var wvR_newLeft = width - wvR_left;
		//console.log(wvR_left + " " + width + " " +wvR_newLeft);
		
		group_wvF.toggle('flipX').set({ angle:-wvF_angle, left:wvF_newLeft }).setCoords();
		group_wvR.toggle('flipX').set({ angle:-wvR_angle, left:wvR_newLeft }).setCoords();
		c_mainVehicle.renderAll();
	});


		
	// IMAGE FILTERS -------------------- 
	
	function setBrightness(target, thisValue) {
		
		// Check target existis before doing anything
		if (target) {
			wheelBrightness = thisValue;
		
			var brightFilter = new fabric.Image.filters.Brightness({ brightness:thisValue/4 });

			// Apply to Front Wheels
			target.filters[0] = brightFilter;
			target.applyFilters(c_mainVehicle.renderAll.bind(c_mainVehicle));
		}
	}
	
	function setContrast(target, thisValue) {
		
		// Check target existis before doing anything
		if (target) {
			wheelContrast = thisValue;

			var contrastFilter = new fabric.Image.filters.Contrast({ contrast:thisValue/4 });

			// Apply to Front Wheels
			target.filters[1] = contrastFilter;
			target.applyFilters(c_mainVehicle.renderAll.bind(c_mainVehicle));
		}
	}
	
	// BRIGHTNESS 
	var $editBrightness = $('#vtws-brightness');
	var $brightnessSlider = $('#vtws-brightnessSlider');
	var $brightnessSliderInput = $('#vtws-brightnessSliderInput');
	
	$editBrightness.click(function(){ $brightnessSlider.slideToggle('fast'); });
		
	$brightnessSliderInput.change( function() {
		setBrightness(wvF_main, this.value);
		setBrightness(wvR_main, this.value);
	});
	
	// CONTRAST 
	var $contrastSliderInput = $('#vtws-contrastSliderInput');
		
	$contrastSliderInput.change( function() {
		console.log("Contrast: " + this.value );
		setContrast(wvF_main, this.value);
		setContrast(wvR_main, this.value);
	});
		
	
	
	
	// USER UPLOAD OWN VEHICLE IMAGE
	$uploadVehicle.onchange = function handleImage(e) {
		var reader = new FileReader();
		reader.onload = function(event) { 
				var image = new Image();
				image.src = event.target.result;
				image.onload = function() {
					vehicleStageScale = $mainVehicleStage.width() / this.width ;
					setVehicleStageScale(this.width, this.height);
					setVehicleBackground(c_mainVehicle,image.src) ;      
				 };
			 };
		reader.readAsDataURL(e.target.files[0]);
		
		// Toggle Steps
		//$('#step1').foundation('toggle');
		//$('#step2').foundation('toggle');
	};
	
	// CHANGE IMAGE
	var $changeVehicle = $('.changeImage');
	var $selectImageDiv = $('#vtws-imageSelect');

	$changeVehicle.click( function(){
		$('input#imgLoader').val('');
		$selectImageDiv.foundation('toggle');
		//location.reload();
	});
});


