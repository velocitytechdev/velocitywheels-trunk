<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/header.php'); ?>

<div class="headerSpacer"></div>

<div id="content" class="page">

	<div class="row show-for-small-only">
		<div class="columns">
			<a class="toggleLink clearButton w100" data-toggletarget='#sidebar'>Vehicle Info </a>
		</div>
	</div>
	
	<div class="row">
		<div class="expand columns">
			<h1 class="pageTitle" style="margin-top:30px;">Wheel Showroom</h1>
		</div>
		<div class="shrink columns">
			<div class="vehicleSelection-h">
				<?php include('inc/vehicleSelection.php'); ?>
			</div>
		</div>
	</div>
	
			
	<div style="height:1em;"></div>
			
	<div class="noVehicle">
		<div class="row">
			<div class="column">
				<div class="vehicleSelection-h">
					Please select your vehicle above.
				</div>
			</div>
		</div>
	</div>
			
	<div class="yesVehicle hide">
		<!--- Photo Studio Steps --->
			
		<div class="vtws-steps">
 					
			<!--- Step 1 --->
			<div id="step1" data-toggler=".hide">
				<div class="row">
					<div class="small-12 medium-4 columns">

						<h2>Use Your Own Image</h2>

						<div class="box-light box-padded" style="min-height:80%;">
							<div class="userUpload">
								<div class="mainText">
									<i class="fa fa-upload fa-2x"></i><br/>
									Upload Your Image
								</div>
								<input type="file" id="imgLoader">
							</div>
							
							<div style="height:3em;"></div>
							
							<div class="tips-header">Tips For Selecting a Good Image</div>
							
							<div id="uploadTips" class="uploadTips">
								<div class="tipSlide  text-center">
									<p>Angeled images that show the entire wheel work best</p>
									<div class="row">
										<div class="columns text-center">
											<img src="/studio/tips/goodImage.jpg" alt="Good Image">
											<i class="fa fa-check fa-2x tip-good"></i>
										</div>
										<div class="columns text-center">
											<img src="/studio/tips/sideProfile.jpg" alt="Bad Image -- To much to the side">
											<i class="fa fa-times fa-2x tip-bad"></i>
										</div>
										
									</div>
								</div>
								<div class="tipSlide  text-center">
									<p>Both wheels should be visible</p>
									<div class="row">
										<div class="columns text-center">
											<img src="/studio/tips/goodImage2.jpg" alt="Good Image">
											<i class="fa fa-check fa-2x tip-good"></i>
										</div>
										<div class="columns text-center">
											<img src="/studio/tips/only1wheel.jpg" alt="Bad Image -- Need to See Both Wheels">
											<i class="fa fa-times fa-2x tip-bad"></i>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="small-12 medium-8 columns">
					
						<h2>Pick An Image to Start With</h2>
						
						<div style="height:0.5em;"></div>
						<?php include('snippets/filters-vehicleThums.php'); ?>
						<div style="height:0.5em;"></div>
						
						<div class="row small-up-2 medium-up-4 studioVehicleList">
							<?php  for ($v = 0; $v <=6; $v++) { ?>
								<div class="column vehicleThumb">
									<a  data-toggle="step1 step2"><img class="chooseVehicle" src="/studio/vehicles/Ford-Mustang_GT-2015-1024-01.jpg" alt=""></a>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				
				<div style="height:1em;"></div>
			</div>
					
			<!--- Step 2 --->
			<div id="step2" class="hide" data-toggler=".hide">
			
				<!-- Photo Studio -->
				<div id="vtws-wrapper" class="row">
					<!-- Main Vehicle -->
					<div id="vtws-mainVehicle">
								
						<canvas id="v"></canvas>
								
						<div class="vehicle-actions row align-middle">
							<div class="shrink columns">
								<button id="flipx" class="ui-button"><i class="fa fa-exchange"></i> Flip Wheels</button>
							</div>
							<div class="expand columns text-right">
								<small>Press on the wheels above to move and resize them.</small>
							</div>
						</div>
								
					</div><!--mainVehicle-->
						
					<!-- Wheel Column -->
					<div id="vtws-wheels">
					
						<?php //include('snippets/filters-wheels.php'); ?>
					
						<div id="studioWheelGrid">
							<div class="row small-up-1 medium-up-2 studioWheelGrid">
								<?php  for ($x = 0; $x <=15; $x++) { ?>
								<div class="column wheelTile">
									<div class="tileContent">
										<div class="productImage">
											<a href="wheel-details-custom.php"><img src="images/wheels/VPS-306/base.jpg" width="476" height="480" alt="KMC KM694"/></a>
										</div>
										<!--
										<div class="productBrand">Vossen</div>
										<div class="productName">VPS-306</div>
										<div class="productFinish">Customizable Finish</div>
										<div class="productOptions">
											<ul class="wheelSizeOptions">
												<li>19"</li>
												<li>20"</li>
												<li>21"</li>
												<li>22"</li>
											</ul>
										</div>
										<div class="productInfo">
											<small>Starting At:</small>
											<span class="price">$1,350</span> <small>ea.</small> 
										</div>
										-->
										<div class="actions">
											<a href="wheel-details.php" class="button button-tiny button-gray">Details</a>
											<a class="button button-tiny" data-open="addOptions">Select</a>
										</div>
							
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
	
						
						<!--- Customize Wheel
						<div class="wheel-info">
							<div class="wheel-brand">
								Vossen
							</div>
							<h2 class="wheel-name">VPS-306</h2>
						</div>
								
						<div class="row align-middle clearfix">
									
							<div class="small-8 medium-12 columns left">
								<div id="vtws-mainWheel">
									<canvas id="w"></canvas>
								</div>
							</div>
									
							<div class="small-3 medium-12 columns right">
								<div class="row builder_buttons">
									<div class="small-expand columns wbLayer" data-layer="face">
										<label class="small">Face:</label> 
										<input type='text' class="colorpicker"/>
									</div>
									<div class="small-expand columns wbLayer" data-layer="window">
										<label class="small">Window:</label> 
										<input type='text' class="colorpicker"/>
									</div>
								</div>
							</div>
									
						</div>
						--->
					</div>
							
				</div>
		
				<!-- Change Vehicle -->
				<div class="row">
					<div class="columns text-center">
						
						<div style="height:1em;"></div>
						
						<a class="button changeImage">Change Image</a>
					</div>
				</div>
			</div>

		</div>
	</div>
	
	<div style="height:1em"></div>
</div>

<div class="reveal" id="addOptions" data-reveal  data-animation-in="hinge-in-from-top" data-animation-out="hinge-out-from-top">
	<button class="close-button" data-close aria-label="Close modal" type="button"><span aria-hidden="true">&times;</span></button>
	<h2 class="headerPadded">Please Select Your Product Options</h2>
	<?php require($_SERVER['DOCUMENT_ROOT'].'/snippets/wheelSizeOptions.php'); ?>
</div>



<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>

<!-- Photo Studio -->
<script src="/studio/js/colorpicker.js"></script>
<script src="/studio/js/colorizer1.5.js"></script>
<script src="/studio/js/studio_v2b.js"></script>


<!-- Tips Slider -->
<script>
$(document).ready(function(){
	$('#uploadTips').slick({
	  dots: true,
	  arrows: false,
	  infinite: true,
	  speed: 500,
	  slidesToShow: 1,
	  autoplay: true,
	  fade: false,
	  autoplaySpeed: 4000,
	  adaptiveHeight: false,
	  dotsClass: 'slick-dots slick-dots-block',
	  lazyLoad: 'ondemand'
	});
});
</script>

</body>
</html>