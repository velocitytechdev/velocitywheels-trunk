$(document).ready(function(){
	"use strict";
	
	/* ===== VEHICLE SELECTION ==== */
	// Vars
	var $make = $('#vs-make');
	var $model = $('#vs-model');
	var $year = $('#vs-year');
	var $noVehicleWrapper = $('.noVehicle');
	var $yesVehicleWrapper = $('.yesVehicle');
	var $clearVehicle = $('.clearVehicle');
	
	
	// Create Cookie
	function setCookie(name, value) {
		document.cookie = name +'='+ value ;
	}
		
	// Get VID from Cookie
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length,c.length);
			}
		}
		return "";
	}
	
	// Kill Cookie
	function eraseCookie(name) {
		document.cookie = name+""+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
	}
	
	// Start Over Vehicle Selection
	$clearVehicle.click(function() {
		eraseCookie('vid');
		eraseCookie('smdl');
		toggleVSmenu('');
		// Also clear Wheels
		eraseCookie('w');
		toggleUserWheels('');
		// Also clear Tires
		eraseCookie('t');
		toggleUserTires('');
	});
	
	// Toggle Vehicle
	var toggleVSmenu = function(mode) {
		if(mode=='selected') {
			$yesVehicleWrapper.removeClass('hide');	
			$noVehicleWrapper.addClass('hide');	
		} else {
			$yesVehicleWrapper.addClass('hide');	
			$noVehicleWrapper.removeClass('hide');	
		}
	};
	
	
	/* ===== VEHICLE ==== */
	// If already Selected Vehicle, load that
	if ( getCookie('vid') ) {
		toggleVSmenu('selected');	
	}
	
	// Make
	$make.change( function(){$model.removeAttr('disabled');} );
	// Model
	$model.change( function(){$year.removeAttr('disabled');} );
	//Year
	$year.change( function(){
		setCookie('vid', '53009') ;
		$noVehicleWrapper.addClass('hide');	
		toggleVSmenu('selected');
	});
	
	/* SUBMODEL */
	var $submodelText = $('.submodel-text');
	var usersubmodel = '';
	var $submodelSelector = $('#vs-submodel');
	var $noSubmodel = $('.noSubmodel');
	var $yesSubmodel = $('.yesSubmodel');
	
	// Toggle Submodel
	var toggleSubmodel = function(mode) {
		if(mode=='selected') {
			$yesSubmodel.removeClass('hide');	
			$noSubmodel.addClass('hide');
			updateSubmodel();
		} else {
			$yesSubmodel.addClass('hide');	
			$noSubmodel.removeClass('hide');
			updateSubmodel();	
		}
	};
	
	//Change Submodel
	$submodelSelector.change( function(){
		setCookie('smdl', $(this).val() ) ;
		toggleSubmodel('selected');
		updateSubmodel();	
	});
	
	// If already Selected Vehicle, load that
	if ( getCookie('smdl') ) {
		toggleSubmodel('selected');
		updateSubmodel();
	}
	
	function updateSubmodel() {
		// If Submodel in cookies
		if(getCookie('smdl')) {
			$submodelText.text(getCookie('smdl'));
			
			// If on wheels page, redirect to wheel results
			if ( $(location).attr('pathname') == '/wheels.php' ) { 
				window.location ='wheel-results.php';
			}
			
		} else {
			$submodelText.text('');
		}
	}

	
	/* ===== WHEELS ==== */
	// Vars
	var $yesWheels = $('.yesWheels');
	var $noWheels = $('.noWheels');
	var $addWheels = $('.addWheels');
	var $clearWheels = $('.clearWheels');
	
	// Check if user wheel existis
	var toggleUserWheels = function(mode) {
		if(mode=='selected') {
			$yesWheels.removeClass('hide');	
			$noWheels.addClass('hide');	
		} else {
			$yesWheels.addClass('hide');	
			$noWheels.removeClass('hide');	
		}
	};
		
	// Create Wheel Cookie
	$addWheels.click( function(){
		setCookie('w', 'KM875');
		toggleUserWheels('selected');
	});
	
	// Clear Wheels Selection
	$clearWheels.click( function(){
		eraseCookie('w');
		toggleUserWheels('');
		// Also clear Tires
		eraseCookie('t');
		toggleUserTires('');
	});
	
	// Staggered fitments
	var $showStaggered = $('#showStaggered');
	var $staggeredRearWheel = $('.staggeredRearWheel');
	
	function toggleStaggeredFitments() {
		if($showStaggered.is(':checked')) {
			$staggeredRearWheel.hide(); 
			console.log( "Square Fitment");
		} else {
			$staggeredRearWheel.show(); 
			console.log( "Staggered Fitment");
		}
	}
	$showStaggered.change(function() {
		toggleStaggeredFitments();
	});
	
	// If already selected wheel, load that
	if ( getCookie('w') ) {
		toggleUserWheels('selected');
	}
	
	/* ===== TIRES ==== */
	// Vars
	var $yesTires = $('.yesTires');
	var $noTires = $('.noTires');
	var $addTires = $('.addTires');
	var $clearTires = $('.clearTires');
	
	// Check if user wheel existis
	var toggleUserTires = function(mode) {
		if(mode=='selected') {
			$yesTires.removeClass('hide');	
			$noTires.addClass('hide');	
		} else {
			$yesTires.addClass('hide');	
			$noTires.removeClass('hide');	
		}
	};
		
	// Create Wheel Cookie
	$addTires.click( function(){
		setCookie('t', 'NM387472, NM92938');
		toggleUserTires('selected');
	});
	
	// Clear Tires Selection
	$clearTires.click( function(){
		eraseCookie('t');
		toggleUserTires('');
	});
	
	// If already selected wheel, load that
	if ( getCookie('t') ) {
		toggleUserTires('selected');
	}
	
	
});