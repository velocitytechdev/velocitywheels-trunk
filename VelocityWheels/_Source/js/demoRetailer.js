$(document).foundation();
"use strict";

// Header on Scroll
var $headerPlaceholder = $('.headerPlaceholder');
var $header = $('header');

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();    
    if (scroll >= $header.height() ) {
        $header.addClass('fixed');
		//$headerPlaceholder.height($header.height());
    } else {
        $header.removeClass('fixed');
		$headerPlaceholder.height(0);
	}
});

// Header Spacer
var $headerSpacer = $('.headerSpacer');
$headerSpacer.height( $header.height() );

// Home Slider
var $slideHead = $('.slideHead');
var $slideHeadContent = $('.slideHead>div');
$slideHead.height($slideHeadContent.height());
$slideHead.parent().css("min-height", $slideHeadContent.height() + 150);

// TOGGLER
var $toggleLink = $('.toggleLink');
$toggleLink.click(function() {
	var $target = $(this).data('toggletarget');
	$( $target ).slideToggle();
});

// HODE SIDEBAR ON MOBILE
var $sidebar = $('#sidebar');
var isMobile = window.matchMedia("only screen and (max-width: 760px)");
if (isMobile.matches) {
	$sidebar.css("display","none");
}