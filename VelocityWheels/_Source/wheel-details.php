<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/header.php'); ?>

<div class="headerSpacer"></div>

<div id="content" class="product">
	<div class="row show-for-small-only">
		<div class="columns">
			<a class="toggleLink clearButton w100" data-toggletarget='#sidebar'>Vehicle Info </a>
		</div>
	</div>
	
	<div class="row">
		<div id="sidebar" class="columns" data-sticky-container>
			<div class="sidebar sticky" data-sticky data-margin-top="7" data-anchor="sidebar">
				<div class="sidebar-section">
					<?php include('inc/vehicleSelection.php'); ?>
				</div>
				<div class="sidebar-section">
					<h5>Wheels</h5>
					<?php include('inc/userWheels.php'); ?>
					
					<h5 style="margin-top:1.5em">Tires</h5>
					<?php include('inc/userTires.php'); ?>
				</div>
				<div class="sidebar-section actions">
					<a href="/shoppingList.php" class="button">Review Build List</a>
				</div>
			</div>
		</div>
		
		<div class="columns pageBody">
			
			<div class="breadcrumb">
				<nav aria-label="You are here:" role="navigation">
				<ul class="breadcrumbs">
					<li><a href="/wheels.php"><i class="fa fa-angle-left"></i> Back to Wheel Listing</li></a>
				</ul>
				</nav>
			</div>
			
			<div class="brandName">KMC</div>
			<h1>KM695 Wishbone</h1>
			<h4 class="font-normal allCaps">Finish: Satin Black</h4>
			
			<div class="row">
				<div class="small-12 medium-5 columns productImages">
					<div class="mainImage">
						<img src="images/wheels/km694/km694_black.png" width="500" height="500" alt=""/>
					</div>
					<div class="row">
						<div class="columns imageThumb">
							<img src="images/wheels/km694/km694_black.png" width="500" height="500" alt=""/>
						</div>
						<!-- Place additional thumbs here inside a div.columns -->
					</div>
				</div>
				
				<div class="small-12 medium-6 columns productDetails">
					<div class="productDescription">				
					</div>
					
					<h2>Sizes For Your Mustang</h2>
					
					<?php require($_SERVER['DOCUMENT_ROOT'].'/snippets/wheelSizeOptions.php'); ?>
					
					
				</div>
			</div>
			
			
			<div class="row">
				<div class="columns">
					
					<div style="height:1em;"></div>
									
					<h2 class="headerPadded">Tech Specs</h2>
					
					<table class="specsTable">
						<tr>
							<td nowrap>Construction:</td>
							<td width="90%">Cast</td>
						</tr>
					</table>
					<a class="button" data-toggle="specSheet">View Tech Spec Sheet</a>
					
					<div class="callout" id="specSheet" data-toggler data-animate="slide-in-right slide-out-right"  style="display:none;">
						<table class="specsTable">
						<thead>
						<tr>
						<th>Part Number</th>
						<th>Size</th>
						<th>Bolt Pattern</th>
						<th>Offset</th>
						<th>B/S</th>
						<th>Finish</th>
						<th>Bore</th>
						<th>Load Rating</th>
						<th>MSRP</th>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td>
						KM69488012735
						</td>
						<td nowrap="">18" x 8"</td>
						<td>5x114.30</td>
						<td>
						35
						</td>
						<td>5.88</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">72.6</td>
						<td nowrap="nowrap">1900 lbs</td>
						<td>$176</td>
						</tr>
						<tr>
						<td>
						KM69488052735
						</td>
						<td nowrap="">18" x 8"</td>
						<td>5x120.00</td>
						<td>
						35
						</td>
						<td>5.88</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">74.1</td>
						<td nowrap="nowrap">1900 lbs</td>
						<td>$176</td>
						</tr>
						<tr>
						<td>
						KM69428512720
						</td>
						<td nowrap="">20" x 8.5"</td>
						<td>5x114.30</td>
						<td>
						20
						</td>
						<td>5.54</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">72.6</td>
						<td nowrap="nowrap">1400 lbs</td>
						<td>$210</td>
						</tr>
						<tr>
						<td>
						KM69428512738
						</td>
						<td nowrap="">20" x 8.5"</td>
						<td>5x114.30</td>
						<td>
						38
						</td>
						<td>6.25</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">72.6</td>
						<td nowrap="nowrap">1900 lbs</td>
						<td>$210</td>
						</tr>
						<tr>
						<td>
						KM69428515720
						</td>
						<td nowrap="">20" x 8.5"</td>
						<td>5x115.00</td>
						<td>
						20
						</td>
						<td>5.54</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">72.6</td>
						<td nowrap="nowrap">1400 lbs</td>
						<td>$210</td>
						</tr>
						<tr>
						<td>
						KM69428552720
						</td>
						<td nowrap="">20" x 8.5"</td>
						<td>5x120.00</td>
						<td>
						20
						</td>
						<td>5.54</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">74.1</td>
						<td nowrap="nowrap">1400 lbs</td>
						<td>$210</td>
						</tr>
						<tr>
						<td>
						KM69428552738
						</td>
						<td nowrap="">20" x 8.5"</td>
						<td>5x120.00</td>
						<td>
						38
						</td>
						<td>6.25</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">74.1</td>
						<td nowrap="nowrap">1900 lbs</td>
						<td>$210</td>
						</tr>
						<tr>
						<td>
						KM69429512745
						</td>
						<td nowrap="">20" x 9.5"</td>
						<td>5x114.30</td>
						<td>
						45
						</td>
						<td>7.02</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">72.6</td>
						<td nowrap="nowrap">1900 lbs</td>
						<td>$219</td>
						</tr>
						<tr>
						<td>
						KM69429552738
						</td>
						<td nowrap="">20" x 9.5"</td>
						<td>5x120.00</td>
						<td>
						38
						</td>
						<td>6.75</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">74.1</td>
						<td nowrap="nowrap">1900 lbs</td>
						<td>$219</td>
						</tr>
						<tr>
						<td>
						KM69422912720
						</td>
						<td nowrap="">22" x 9"</td>
						<td>5x114.30</td>
						<td>
						20
						</td>
						<td>5.79</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">72.6</td>
						<td nowrap="nowrap">1400 lbs</td>
						<td>$310</td>
						</tr>
						<tr>
						<td>
						KM69422915720
						</td>
						<td nowrap="">22" x 9"</td>
						<td>5x115.00</td>
						<td>
						20
						</td>
						<td>5.79</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">72.6</td>
						<td nowrap="nowrap">1400 lbs</td>
						<td>$310</td>
						</tr>
						<tr>
						<td>
						KM69422952720
						</td>
						<td nowrap="">22" x 9"</td>
						<td>5x120.00</td>
						<td>
						20
						</td>
						<td>5.79</td>
						<td>SATIN BLACK</td>
						<td nowrap="nowrap">74.1</td>
						<td nowrap="nowrap">1400 lbs</td>
						<td>$310</td>
						</tr>
						</tbody>
						</table>
					</div>
					
					<div style="height:1em;"></div>
					
					<h2 class="headerPadded">Additional Finishes</h2>
						
					<div class="row small-up-2 medium-up-4 large-up-6 productGrid">
						<div class="columns text-center active">
							<img src="images/wheels/km694/km694_black.png" width="500" height="500" alt=""/>
							<div class="textLabel">Satin Black</div>
						</div>
						<div class="columns text-center">
							<img src="images/wheels/km694/km694_bronze.png" width="500" height="500" alt=""/>
							<div class="textLabel">Bronze</div>
						</div>
					</div>
					
					

				</div>
			</div>
		</div>
		
	</div>
	
</div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>


</body>
</html>