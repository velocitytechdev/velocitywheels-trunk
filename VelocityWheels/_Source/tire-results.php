<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/header.php'); ?>

<div class="headerSpacer"></div>

<div id="content" class="page">
	<div class="row show-for-small-only">
		<div class="columns">
			<a class="toggleLink clearButton w100" data-toggletarget='#sidebar'>Vehicle Info </a>
		</div>
	</div>
	
	<div class="row">
		<div id="sidebar" class="columns" data-sticky-container>
			<div class="sidebar sticky" data-sticky data-margin-top="7" data-anchor="sidebar">
				<div class="sidebar-section">
					<?php include('inc/vehicleSelection.php'); ?>
				</div>
				<div class="sidebar-section">
					<h5>Wheels</h5>
					<?php include('inc/userWheels.php'); ?>
					
					<h5 style="margin-top:1.5em">Tires</h5>
					<?php include('inc/userTires.php'); ?>
				</div>
				<div class="sidebar-section actions">
					<a href="/shoppingList.php" class="button">Review Build List</a>
				</div>
			</div>
		</div>
		
		<div class="columns pageBody">
			
			<h1 class="pageTitle">Tires</h1>
			
			<div class="row filters">
				<div class="columns">
					<div class="box-normal">
						<ul class="dropdown menu filterList" data-dropdown-menu>
							<li>
								<a href="#">Brand <i class="fa fa-angle-down"></i></a>
								<ul class="menu">
									<li><a href="#">Nitto</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Category <i class="fa fa-angle-down"></i></a>
								<ul class="menu">
									<li><a href="#">Summer Passenger Tire</a></li>
									<li><a href="#">All-Season Passenger Tire</a></li>
									<li><a href="#">Winter/Snow Passenger Tire</a></li>
									<li><a href="#">Light Trck Tire</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Speed Rating <i class="fa fa-angle-down"></i></a>
								<ul class="menu">
									<li><a href="#">W (168mph)</a></li>
									<li><a href="#">Y (186mph)</a></li>
									<li><a href="#">Y+ (186mph+)</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="row small-up-2 medium-up-3 productGrid">
				<?php  for ($x = 0; $x <= 10; $x++) { ?>
				<div class="column tireTile">
					<div class="tileContent">
						<div class="productImage">
							<a href="tire-details.php"><img src="images/tireFPO.jpg" width="350" height="300" alt="Nitto Motivo"/></a>
						</div>
						<div class="productBrand">Nitto</div>
						<div class="productName">Motivo</div>
						<div class="productDescription">
							All-Season Ultra High Performance Radial Passenger Car tire.
						</div>
						<div class="productInfo">
							<div class="size">
								<label class="block">Size</label>
								<div class="row">
									<div class="columns">
										255/40-19 <br/>
										<span class="price">$296.25</span> <small>ea.</small>
									</div>
									<div class="columns">
										275/35-19 <br/>
										<span class="price">$326.25</span> <small>ea.</small>
									</div>
								</div>
								
							</div>
						</div>
						<div class="actions">
							<a href="tire-details.php" class="button button-gray">Details</a>
							<a class="button button-action addTires">Add to Cart</a>
						</div>
	
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>


</body>
</html>