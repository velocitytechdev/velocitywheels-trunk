<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/header.php'); ?>

<div id="homeMain">
	<div>
		<div class="homeSlider">
		
			<div class="homeSlide">
				<div class="slideImage">
					<img src="/images/slider/wheel-close2.jpg" alt=""/>
				</div>
				<div class="slideHead v-bottom w-left mediumUp-text-white" style="padding-bottom:2em;">
					<div class="slideHeadContent">
						<h1>This is a Demo Site</h1>
						<p>This site was built for click-through testing and to demonstrate functionality. It is not a fully functioning site and should not be used as such.</p>
					</div>
				</div>
			</div>
			
			<div class="homeSlide">
				<div class="slideImage">
					<img src="/images/slider/tire-close.jpg" alt=""/>
				</div>
				<div class="slideHead v-middle w-right">
					<div class="slideHeadContent">
						<h1>We Can Do Tires and Wheels</h1>
						<p>Finally, we are bringing tires and wheels together like never before.</p>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div id="content" class="home">
	<div class="segment">
		<div class="row">
			<div class="columns">
				<div class="box-dark text-center">
					<div class="vehicleSelection-h">
						<?php include('inc/vehicleSelection.php'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div style="height:2em;"></div>
	<div class="row">
		<div class="columns">
			<h1>Coming Soon</h1>
			<p>We promise.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque commodo, dolor vitae egestas rutrum, ex eros convallis nisl, quis suscipit justo tellus at tortor. Aliquam molestie iaculis ligula, sed tincidunt mauris venenatis in. Donec justo arcu, condimentum ac consequat molestie, mattis non mi. Aenean lorem purus, laoreet sed odio id, dictum porttitor metus. Ut a feugiat ex. Praesent sem nulla, pretium ac aliquet sit amet, pellentesque in urna. Fusce risus arcu, tristique sed nibh ut, efficitur pellentesque erat. Morbi mauris metus, rhoncus sed neque et, feugiat ultrices urna. Fusce at maximus dui, sed ultrices eros. Phasellus porttitor ex vitae arcu mattis porta. Aenean ac est in orci condimentum volutpat in ultrices enim.</p>			
		</div>
	</div>
</div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>

<script>
$(document).ready(function(){
	$('.homeSlider').slick({
	  dots: true,
	  arrows: false,
	  infinite: true,
	  speed: 500,
	  slidesToShow: 1,
	  autoplay: true,
	  fade: true,
	  autoplaySpeed: 4000,
	  adaptiveHeight: true,
	  lazyLoad: 'ondemand'
	});
});
</script>

</body>
</html>