<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/header.php'); ?>

<div class="headerSpacer"></div>

<div id="content" class="checkoutPage">	


	<div class="row">
	
		<div class="columns checkoutBody">
			
			<h1 class="pageTitle">Your Shopping List</h1>
			<div style="height:2em;"></div>
			
			<div class="row" data-sticky-container>
				<div class="small-12 columns shoppingList-left">
					
					<h4>Review Your Product List</h4>
					<div style="height:2em;"></div>
					
					<!--- Shoppint List --->
					<div class="">
						<div class="row">
							<div class="medium-2 columns hide-for-small-only">
								<img src="images/wheels/km694/km694_black.png" width="500" height="500" alt=""/>
							</div>

							<div class="small-12 medium-10 columns">
								<table class="shoppingList">
									<tbody>
										<tr>
											<td colspan=3>
												<div class="productBrand">KMC</div>
												<div class="productName">KM694 Wishbone</div>
												<div class="productFinish">Satin Black</div>
											</td>
										</tr>
										<tr>
											<td>
												<strong>Front:</strong><br/>
												<div class="productMeta">20" x 8.5" et 35</div>
												<div class="productSKU">KM6952853514</div>
											</td>
											<td>
												Qty:
												<input type="number" class="qty" value="2" />
											</td>
											<td class="text-right" nowrap>
												<span class="price">$698.45</span>
												<i class="fa fa-trash"></i>
											</td>
										</tr>
										<tr>
											<td>
												<strong>Rear:</strong><br/>
												<div class="productName">20" x 10.5" et 35</div>
												<div class="productSKU">KM6952103514</div>
											</td>
											<td>
												Qty:
												<input type="number" class="qty" value="2" />
											</td>
											<td class="text-right" nowrap>
												<span class="price">$738.45</span>
												<i class="fa fa-trash"></i>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
					<div class="">
						<div class="row">
							<div class="medium-2 columns hide-for-small-only">
								<img src="images/tireFPO.jpg" width="350" height="300" alt="Nitto Motivo"/>
							</div>

							<div class="small-12 medium-10 columns">
								<table class="shoppingList">
									<tbody>
										<tr>
											<td colspan="3">
												<div class="productBrand">Nitto</div>
												<div class="productName">Motivo</div>
											</td>
										</tr>
										<tr>
											<td>
												<strong>Front:</strong><br/>
												<div class="productMeta">255/35-20</div>
												<div class="productSKU">NT95232553520</div>
											</td>
											<td>
												Qty:
												<input type="number" class="qty" value="2" />
											</td>
											<td class="text-right" nowrap>
												<span class="price">$468.69</span>
												<i class="fa fa-trash"></i>
											</td>
										</tr>
										<tr>
											<td>
												<strong>Rear:</strong><br/>
												<div class="productName">274/30-20</div>
												<div class="productSKU">NT95232753020</div>
											</td>
											<td>
												Qty:
												<input type="number" class="qty" value="2" />
											</td>
											<td class="text-right" nowrap>
												<span class="price">$928.45</span>
												<i class="fa fa-trash"></i>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					
					<table class="shoppingList">
						<tbody>
							<tr class="total">
								<td colspan="2" class="text-right">
									Est. Tax:
								</td>
								<td class="text-right">
									$93.23
								</td>
							</tr>
							<tr class="total">
								<td colspan="2" class="text-right">
									Est. Shipping:
								</td>
								<td class="text-right">
									$168.43
								</td>
							</tr>
							<tr class="grandTotal">
								<td colspan="2" class="text-right">
									Est. Grand Total:
								</td>
								<td class="text-right">
									$1,190.11
								</td>
							</tr>
						</tbody>
					</table>

					
				
				</div>
				<div id="shoppingList-right" class="small-12 columns shoppingList-right" data-sticky-container>
					<div class="sticky" data-sticky data-margin-top="7" data-anchor="shoppingList-right">
						<h4>What Would You Like To Do With This List?</h4>
						<div style="height:2em;"></div>

						<!--- Actions --->

						<ul class="accordion" data-accordion>
							<li class="accordion-item is-active" data-accordion-item>
								<a href="#" class="accordion-title">Get a Price Quote</a>
								<div class="accordion-content" data-tab-content>
									<a href="" class="button">Get a Quote</a>
								</div>
							</li>
							<li class="accordion-item" data-accordion-item>
								<a href="#" class="accordion-title">Send in An Email</a>
								<div class="accordion-content" data-tab-content>
									<div class="input-group">
										<span class="input-group-label">Eamil to:</span>
										<input class="input-group-field" type="text">
										<div class="input-group-button">
											<input type="submit" class="button" value="Send">
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>				
				</div>
			</div>
			
			
		</div>
	</div>
</div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>


</body>
</html>