<!doctype html>
<html>
<head>
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Resource-type" content="Document" />

	<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">

	<link rel="stylesheet" type="text/css" href="/css/foundation.min.css" />
	<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/css/slick.css" />
	
	<!-- Joyride -->
	<link rel="stylesheet" href="/css/joyride.css" />
	
	<!-- Photo Studio CSS -->
	<link rel="stylesheet" href="/studio/css/colorpicker.css" />
	<link rel="stylesheet" href="/studio/css/studio.css" />
	
	<!-- Site CSS -->
	<link rel="stylesheet" type="text/css" href="/css/vt-retailer.css" />
	<link rel="stylesheet" type="text/css" href="/css/demoRetailer.css" />
	<!--[if IE]>
		<script type="text/javascript">
			 var console = { log: function() {} };
		</script>
	<![endif]-->



	<title>DEMO SITE</title>

</head>

<body>

<header>
	<div class="row">
		<div class="columns">
			<div class="top-bar">
				<div class="top-bar-title">
					
					<a href="/"><img id="mainLogo" src="/images/logo.png" alt=""/></a>
					
					<span id="menuToggle" data-responsive-toggle="responsive-menu" data-hide-for="medium">
						<button class="menuToggle-button" type="button" data-toggle>Menu</button>
					</span>
				</div>
				<div id="responsive-menu">
					<div class="top-bar-left">
					</div>
					<div class="top-bar-right">
						<ul id="minorNav" class="menu align-right">
							<li><span class="fauxButton strong">888-555-1234</span></li>
							<li class="divider"></li>
							<li><a href="/">Home</a></li>
							<li class="divider"></li>
							<li><a href="#">Contact</a></li>
							<li class="divider"></li>
							<li><a href="http://facebook.com" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a href="http://twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
						</ul>
						<ul id="mainNav" class="dropdown menu align-right" data-dropdown-menu>
							<li>
								<a href="/wheels.php">Wheels <i class="fa fa-angle-down"></i></a>
								<ul class="menu vertical">
									<li><a href="/wheels.php">Wheels</a></li>
									<li><a href="/wheel-brands.php">Wheel Brands We Carry</a></li>
									<li><a href="#">Know Your Wheels</a></li>
								</ul>
							</li>
							<li>
								<a href="/tires.php">Tires <i class="fa fa-angle-down"></i></a>
								<ul class="menu vertical">
									<li><a href="/tires.php">Browse Tires</a></li>
									<li><a href="/tire-brands.php">Tire Brands We Carry</a></li>
									<li><a href="#">Know Your Tires</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Comapny <i class="fa fa-angle-down"></i></a>
								<ul class="menu vertical">
									<li><a href="#">What Makes Us Different</a></li>
									<li><a href="#">Contact Us</a></li>
								</ul>
							</li>
							<li>
								<a href="shoppingList.php"><i class="fa fa-shopping-cart"></i></a>
							</li>
						</ul>
					</div>
				</div>
				</div>
		</div>
	</div>
</header>
<div class="headerPlaceholder"></div>

<div id="page">
