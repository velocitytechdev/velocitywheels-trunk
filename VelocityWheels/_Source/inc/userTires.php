<div id="userTires">
	<div class="userTires-content">
		<div class="noTires">
			<div class="note" style="margin-bottom:5px;">No Tires Selected </div>
			<a href="/tires.php" class="button button-small button-white">Choose Tires</a>
			<!---
			<br/><a href="#" class="small addTires">DEBUG: Add Tires</a>
			--->
		</div>
		<div class="yesTires hide">
			<div class="productMeta">
				<span class="brand">Nitto</span> 
				<span class="sytle">Motivo</span>
				<a class="small pull-right" href="tire-details.php">Details</a>
				
			</div> 
			<div class="row">
				<div class="small-6 columns">
					<div class="smallSubhead">Front:</div>
					<span class="size">255/40-19</span>
				</div>
				<div class="small-6 columns">
					<div class="smallSubhead">Rear:</div>
					<div class="size staggeredRearWheel">
						<span class="size">275/35-19</span>
					</div>
				</div>
			</div>
			<a href="#" class="small clearTires"><i class="fa fa-times" aria-hidden="true"></i> Remove Tires</a>
		</div>
	</div>
</div>