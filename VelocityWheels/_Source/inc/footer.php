</div><!-- /#page -->

<footer>
	<div class="row">
		<div class="columns text-center">
			<span class="siteName">Demo Retail Site</span>
			<hr/>
		</div>
	</div>
	<div class="row">
		<div class="columns">
			<div style="font-weight:bold;">Products</div>
			<ul class="no-bullet">
				<li><a href="/wheels.php">Browse Wheels</a></li>
				<li><a href="/tires.php">Browse Tires</a></li>
			</ul>
		</div>
		
		<div class="columns">
			<div style="font-weight:bold;">Info</div>
			<ul class="no-bullet">
				<li><a href="#">Wheels 101</a></li>
				<li><a href="#">Tires 101</a></li>
			</ul>
		</div>
		
		<div class="columns">
			<div style="font-weight:bold;">Contact Us</div>
			<ul class="no-bullet">
				<li><a href="#">Map & Contact</a></li>
				<li><a href="#">Set Appointment</a></li>
			</ul>
		</div>
	</div>
</footer>


<script src="/js/vendor/jquery.min.js"></script>
<script src="/js/vendor/what-input.min.js"></script>
<script src="/js/foundation.min.js"></script>
<script src="/js/slick.min.js"></script>
<script src="/js/vehicleSelect.js"></script>
<script src="/js/demoRetailer.js"></script>


