<div id="userWheels">
	<div class="userWheels-content">
		<div class="noWheels">
			<div class="note" style="margin-bottom:5px;">No Wheels Selected </div>
			<a href="/wheels.php" class="button button-small button-white">Choose Wheels</a>
			<!---
			<br/><a href="#" class="small addWheels">DEBUG: Add Wheel</a>
			--->
		</div>
		<div class="yesWheels hide">
			<div class="productMeta">
				<span class="brand">KMC</span> 
				<span class="sytle">Wishbone</span>
				<a class="small pull-right" href="/wheel-details.php">Details</a>
				
			</div> 
			<div class="row">
				<div class="small-6 columns">
					<div class="smallSubhead">Front:</div>
					<span class="size">20x8.5</span>
					<span class="offset">et35</span>
				</div>
				<div class="small-6 columns">
					<div class="smallSubhead">Rear:</div>
					<div class="size staggeredRearWheel">
						<span class="size">20x10</span>
						<span class="offset">et42</span>
					</div>
				</div>
			</div>
			<a href="#" class="small clearWheels"><i class="fa fa-times" aria-hidden="true"></i> Remove Wheels</a>
		</div>
	</div>
</div>
