
<!-- No Vehicle Selected -->
<div class="noVehicle">
	<div class="vsForm-title text-left">
		<h5>Select Your Vehicle to Get Started:</h5>
	</div>
	<div class="vsForm-content">
		<form action="">
			<div class="row innerRow">
				<div class="colums vsForm-selectWrapper">
					<select name="vs-make" id="vs-make">
						<option value="">MAKE</option>
						<option value="">Ford</option>
					</select>
				</div>
				<div class="colums vsForm-selectWrapper">
					<select name="vs-model" id="vs-model" disabled>
						<option value="">MODEL</option>
						<option value="">Mustang</option>
					</select>
				</div>
				<div class="colums vsForm-selectWrapper">
					<select name="vs-year" id="vs-year" disabled>
						<option value="">YEAR</option>
						<option value="">2016</option>
					</select>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Vehicle Selected -->
<div class="yesVehicle hide">
	<div class="vsForm-image">
		<img src="/images/vehicles/2016mustang-thumb.png" width="150" height="68" alt=""/>
	</div>
	<div class="vsForm-title text-left">
		<span class="title-text">2016 Ford Mustang</span>
		<div class="submodel-text"></div>
		<a href="#" class="small clearVehicle"><i class="fa fa-reply" aria-hidden="true"></i> Change Vehicle</a>
	</div>
	<div class="vsForm-content">
		<div class="home-only">
			<a href="/wheels.php" class="button sectionButton">Wheels</a>
			<a href="/tires.php" class="button sectionButton">Tires</a>
		</div>
	</div>
</div>
