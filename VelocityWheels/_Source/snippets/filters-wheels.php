	
	<div class="row filters">
		<div class="columns">
			<div class="box-normal">
				<ul class="dropdown menu filterList" data-dropdown-menu>
					<li>
						<a href="#">Contruction <i class="fa fa-angle-down"></i></a>
						<ul class="menu">
							<li><a href="#">Cast</a></li>
							<li><a href="#">Flow Formed</a></li>
							<li><a href="#">1pc Forged</a></li>
							<li><a href="#">Multi-piece Forged</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Finish <i class="fa fa-angle-down"></i></a>
						<ul class="menu">
							<li><a href="#">Black</a></li>
							<li><a href="#">Silver</a></li>
							<li><a href="#">Gray</a></li>
							<li><a href="#">Polished</a></li>
							<li><a href="#">Machined</a></li>
							<li><a href="#">Custom Color</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Brand <i class="fa fa-angle-down"></i></a>
						<ul class="menu">
							<li><a href="#">KMC</a></li>
							<li><a href="#">Vossen</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Customizeable <i class="fa fa-angle-down"></i></a>
						<ul class="menu">
							<li><a href="#">All</a></li>
							<li><a href="#">Yes</a></li>
							<li><a href="#">No</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>

