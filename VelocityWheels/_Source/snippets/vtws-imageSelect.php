		
		<div class="row">
			<div class="columns">
				<h2>Change The Vehicle Image</h2>
				<p>You can change the image of the vehicle to preview the wheels by one of the options below.</p>
			</div>
		</div>
		
		<div class="row">
			<div class="small-12 medium-4 columns">

				<h3><strong>Use Your Own Image</strong></h3>

				<div class="box-light box-padded" style="min-height:80%;">
					<div class="userUpload">
						<div class="mainText">
							<i class="fa fa-upload fa-2x"></i><br/>
							Upload Your Image
						</div>
						<input type="file" id="imgLoader">
					</div>

					<div style="height:3em;"></div>

					<div class="tips-header">Tips For Selecting a Good Image</div>

					<div id="uploadTips" class="uploadTips">
						<div class="tipSlide  text-center">
							<p>Angeled images that show the entire wheel work best</p>
							<div class="row">
								<div class="columns text-center">
									<img src="/studio/tips/goodImage.jpg" alt="Good Image">
									<i class="fa fa-check fa-2x tip-good"></i>
								</div>
								<div class="columns text-center">
									<img src="/studio/tips/sideProfile.jpg" alt="Bad Image -- To much to the side">
									<i class="fa fa-times fa-2x tip-bad"></i>
								</div>

							</div>
						</div>
						<div class="tipSlide  text-center">
							<p>Both wheels should be visible</p>
							<div class="row">
								<div class="columns text-center">
									<img src="/studio/tips/goodImage2.jpg" alt="Good Image">
									<i class="fa fa-check fa-2x tip-good"></i>
								</div>
								<div class="columns text-center">
									<img src="/studio/tips/only1wheel.jpg" alt="Bad Image -- Need to See Both Wheels">
									<i class="fa fa-times fa-2x tip-bad"></i>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="small-12 medium-8 columns">

				<h3><strong>Pick An Image to Start With</strong></h3>

				<div style="height:0.5em;"></div>
				<?php include('snippets/filters-vehicleThums.php'); ?>
				<div style="height:0.5em;"></div>

				<div class="row small-up-2 medium-up-4 studioVehicleList">
					<?php  for ($v = 0; $v <=6; $v++) { ?>
						<div class="column vehicleThumb">
							<a  data-toggle="step1 step2"><img class="chooseVehicle" src="/studio/vehicles/Ford-Mustang_GT-2015-1024-01.jpg" alt=""></a>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>

		<div style="height:1em;"></div>