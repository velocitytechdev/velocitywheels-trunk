
			<div class="wheel-info">

				<h2 class="wheel-name">Add Custom Paint</h2>
				
				<div class="wheel-brand">Vossen</div>
				<h2 class="wheel-name">VPS-306</h2>
			</div>
				

				
			<div id="wheel">

				<div class="row align-middle clearfix">
					
					<div class="small-8 columns left">
						<div id="vtws-mainWheel">
							<canvas id="w"></canvas>
						</div>
					</div>
					
					<div class="small-3 columns right">
						<div class="builder_buttons">
							<div class=" wbLayer" data-layer="face">
								<label class="small">Face:</label> 
								<input type='text' class="colorpicker"/>
							</div>
							<div class="wbLayer" data-layer="window">
								<label class="small">Window:</label> 
								<input type='text' class="colorpicker"/>
							</div>
						</div>
					</div>
					
				</div>
				
			</div><!--mainWheel-->
			