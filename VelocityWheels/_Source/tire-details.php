<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/header.php'); ?>

<div class="headerSpacer"></div>

<div id="content" class="product">
	<div class="row show-for-small-only">
		<div class="columns">
			<a class="toggleLink clearButton w100" data-toggletarget='#sidebar'>Vehicle Info </a>
		</div>
	</div>
	
	<div class="row">
		<div id="sidebar" class="columns" data-sticky-container>
			<div class="sidebar sticky" data-sticky data-margin-top="7" data-anchor="sidebar">
				<div class="sidebar-section">
					<?php include('inc/vehicleSelection.php'); ?>
				</div>
				<div class="sidebar-section">
					<h5>Wheels</h5>
					<?php include('inc/userWheels.php'); ?>
					
					<h5 style="margin-top:1.5em">Tires</h5>
					<?php include('inc/userTires.php'); ?>
				</div>
				<div class="sidebar-section actions">
					<a href="/shoppingList.php" class="button">Review Build List</a>
				</div>
			</div>
		</div>
		
		<div class="columns pageBody">
			
			<div class="breadcrumb">
				<nav aria-label="You are here:" role="navigation">
				<ul class="breadcrumbs">
					<li><a href="/tire-results.php"><i class="fa fa-angle-left"></i> Back to Tire Listing</li></a>
				</ul>
				</nav>
			</div>
			
			<div class="brandName">Nitto</div>
			<h1>Motivo</h1>
			
			<div class="row">
				<div class="small-12 medium-5 columns productImages">
					<div class="mainImage">
						<img src="images/tireFPO.jpg" width="350" height="300" alt=""/>
					</div>
					<div class="row">
						<div class="columns imageThumb">
							<img src="images/tireFPO.jpg" width="350" height="300" alt=""/>
						</div>
						<!-- Place additional thumbs here inside a div.columns -->
					</div>
				</div>
				
				<div class="small-12 medium-6 columns productDetails">
					<div class="productDescription">
						<p class="description">All-Season Ultra High Performance Radial Passenger Car tire.</p>
						<h2>Features</h2>
						<ul>
							<li> Made in the USA using proprietary advanced tire manufacturing technology</li>
							<li> All-new silica rubber compound</li>
							<li> Unique design </li>
							<li> Tread block arrangement</li>
							<li> 60,000 Mile Limited Treadwear Warranty</li>
						</ul>
						<h2>Benefits</h2>
						<ul>
							<li> Delivers consistent tire uniformity for a smooth and enjoyable driving experience</li>
							<li> Prolongs treadlife of the tire while maintaining traction and handling in winter, wet and dry conditions</li>
							<li> Distinct pattern which attracts the eye</li>
							<li> Designed to minimize road noise</li>
						</ul>
					</div>
					
					<h2 class="headerPadded">Specs</h2>
					
					<div class="productItem">
						<div class="row">
							<div class="expand columns">
								<h4>255/40-19</h4>
								<table class="specsTable">
									<tr>
										<td>Speed Rating</td>
										<td>W</td>
									</tr>
									<tr>
										<td>UTQG</td>
										<td>560 A A</td>
									</tr>
									<tr>
										<td>Load Rating / Capacity</td>
										<td>93 / 1433</td>
									</tr>
								</table>
							</div>
							
							<div class="shrink columns text-right productPricing">
								<span class="price">$296.25</span> <small>ea.</small>
								<div class="meta">
									<small>Price as of 8/31/2016</small>
								</div>
								<div class="availability">
									965 available
								</div>
								<div class="qtyBox">
									Qty. 
									<input type="number" value="2" style="width:60px;">
								</div>
							</div>
						</div>
					</div>
					
					<div class="productItem">
						<div class="row">
							<div class="expand columns">
								<h4>275/35-19</h4>
								<table class="specsTable">
									<tr>
										<td>Speed Rating</td>
										<td>W</td>
									</tr>
									<tr>
										<td>UTQG</td>
										<td>560 A A</td>
									</tr>
									<tr>
										<td>Load Rating / Capacity</td>
										<td>108 / 1821</td>
									</tr>
								</table>
							</div>
							
							<div class="shrink columns text-right productPricing">
								<span class="price">$326.25</span> <small>ea.</small>
								<div class="meta">
									<small>Price as of 8/31/2016</small>
								</div>
								<div class="availability">
									1033 available
								</div>
								<div class="qtyBox">
									Qty. 
									<input type="number" value="2" style="width:60px;">
								</div>
							</div>
						</div>
					</div>

					<div class="actions">
						<div class="productTotal">
							<small>TOTAL</small>
							<span class="totalPrice">$1,245</span>
						</div>
						<a href="" class="button addTires">Add To Cart</a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<?php require($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); ?>


</body>
</html>