﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using VelocityWheels.Services;
using VelocityWheels.Util;

namespace VelocityWheels
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(MvcApplication));

        protected void Application_Start()
        {
            _logger.Info("Application_Start");
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            log4net.Config.XmlConfigurator.Configure();
            
            try
            {
                Thread t = new Thread(ProcessThumnails);
                t.Start();
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
            }
        }

        protected void ProcessThumnails()
        {
            _logger.Info("ProcessThumnails - starting");
            try
            {
                ImageService service = new ImageService();
                service.CreateThumbnailsFromVehicleImages();
            }
            catch (Exception e)
            {
                _logger.LogException(e.Message, e);
            }

            _logger.Info("ProcessThumnails - complete");
        }

        protected void Application_Error()
        {
            Exception ex = Server.GetLastError();
            if (ex != null)
            {
                _logger.LogException("Unhandled Exception", ex);
            }

            Application[HttpContext.Current.Request.UserHostAddress] = ex;
        }
    }
}