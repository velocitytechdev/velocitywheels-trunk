﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VelocityWheels.Models;
using VelocityWheels.Services;

namespace VelocityWheels.Controllers
{
    public class WidgetController : Controller
    {
        readonly log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(WidgetController));

        public ActionResult Home()
        {
            return new FilePathResult("~/Views/Widget/Home.html", "text/html");
        }

        public ActionResult Demo()
        {
            return View("DemoIndex");
            //return new FilePathResult("~/Views/Widget/Demo.html", "text/html");
        }

        public ActionResult BuilderDemo()
        {
            return View("BuilderDemo");
            //return new FilePathResult("~/Views/Widget/Demo.html", "text/html");
        }

        //Validate and setup token
        private bool ValidateToken(string id, string origin)
        {
            id = id ?? "";
            //var url = Request.UrlReferrer.AbsoluteUri;

            var service = new AccountService();
            if (service.IsValidToken(id, origin))
            {
                Global.Session.Instance.Client = new ClientViewModel
                {
                    Token = id,
                    VehicleSelectorDataSource = VehicleConfiguration.DataSource.DriveRight,
                    ImageDataSource = VehicleConfiguration.DataSource.FuelAPI
                };
                return true;
            }

            Global.Session.Instance.Client = null;
            return false;
        }

        //Widget Components
        public ActionResult VehicleSelector(string id, string origin)
        {
            if (ValidateToken(id, origin))
                return PartialView("_VehicleSelector");
            else
                return Content("");
        }

        public ActionResult WheelStudio(string id, string origin)
        {
            if (ValidateToken(id, origin))
                return PartialView("_WheelStudio");
            else
                return Content("");
        }
    }
}