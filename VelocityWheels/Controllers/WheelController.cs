﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net.Repository.Hierarchy;
using Microsoft.Ajax.Utilities;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;

namespace VelocityWheels.Controllers
{
    public class WheelController : BaseController
    {
        public WheelController()
        {
            Logger = AppLogger.GetLogger(GetType());
        }

        public ActionResult Studio()
        {
            return View();
        }

        public ActionResult Studio2()
        {
            return View();
        }

        public ActionResult WheelExists(string partNumber)
        {
            return null;
        }

        [HttpGet]
        public ActionResult GetWheel(int? wheelId, long? cid, string token)
        {
            try
            {
                WheelService wheelService = new WheelService();

                // get account to filter results
                var accountService = new AccountService(wheelService.Context);
                var account = accountService.GetAccountFromToken(token);
                if (account != null)
                {
                    var wheel = wheelService.GetWheelById(wheelId.GetValueOrDefault(), true);

                    if (wheel != null)
                    {
                        var fitments = wheelService.GetChassisFitments(cid.GetValueOrDefault());
                        var matchingParts = wheel.Products.Where(p => fitments.Contains(p.PartNumber));
                        matchingParts.ForEach(p =>
                        {
                            p.FitsChassis = true;
                        });                        

                        return Json(wheel, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
            }
            
            BaseModel model = new BaseModel() {Error = "No results found"};
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetLayers(int? wheelId, string partNumber, string token)
        {
            try
            {
                WheelService wheelService = new WheelService();

                // get account to filter results
                var accountService = new AccountService(wheelService.Context);
                var account = accountService.GetAccountFromToken(token);
                if (account != null)
                {
                    WheelModel wheel = null;
                    if (wheelId != null)
                    {
                        wheel = wheelService.GetWheelById(wheelId.GetValueOrDefault());
                    }
                    else if (!string.IsNullOrWhiteSpace(partNumber))
                    {
                        wheel = wheelService.GetWheelByPartNumber(partNumber);
                    }

                    if (wheel != null)
                    {
                        if (wheel.WheelLayers != null && wheel.WheelLayers.Any())
                        {
                            wheel.WheelLayers = wheel.WheelLayers.OrderBy(w => w.Level).ToList();
                        }
                        return Json(wheel, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e.Message, e);
            }

            BaseModel model = new BaseModel() { Error = "No results found" };
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}