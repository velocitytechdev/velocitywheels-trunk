﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VelocityWheels.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/

        public ActionResult Grid(string id)
        {
            return PartialView("_Grid", id);
        }

    }
}
