﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace VelocityWheels.Controllers
{
    public class BaseController : Controller
    {
        public log4net.ILog Logger { get; set; }

        public BaseController()
        {

        }
    }
}