﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel.Syndication;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using VelocityWheels.Data.Enum;
using VelocityWheels.Data.Models;
using VelocityWheels.ExternalServices;
using VelocityWheels.Global;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Services.Models;
using VelocityWheels.Util;
using VehicleImage = VelocityWheels.Models.VehicleImage;

namespace VelocityWheels.Controllers
{
    public class VehicleController : BaseController
    {
        public VehicleController()
        {
            Logger = AppLogger.GetLogger(this);
        }

        #region Stub Actions (Vehicle Selector) -- they will call other Actions based on DataSource for Client

        public ActionResult GetYears()
        {
            return GetApiYears();
        }

        public ActionResult GetMakes(string y)
        {
            return GetApiMakes(y);            
        }

        public ActionResult GetModels(string y, string m)
        {
            return GetApiModels(y, m);            
        }

        public ActionResult GetBodyTypes(string y, string m, string mdl)
        {
            return GetApiBodyTypes(y, m, mdl);            
        }

        public ActionResult GetSubModels(string y, string m, string mdl, string b)
        {
            return GetApiSubModels(y, m, mdl, b);            
        }

        #endregion

        #region Stub Actions (Vehicle Images) -- they will call other Actions based on DataSource for Client

        public ActionResult GetVehicleImages(string y, string m, string mdl, string b, string sm, string o, int? imageId)
        {
            if (Global.Session.Instance.Client.ImageDataSource == VehicleConfiguration.DataSource.Iconfigurators)
            {
                if (Global.Session.Instance.Client.VehicleSelectorDataSource ==
                    VehicleConfiguration.DataSource.DriveRight)
                    return GetVehicleImagesIconfigurators(y, m, mdl, "", "");
                else if (Global.Session.Instance.Client.VehicleSelectorDataSource ==
                         VehicleConfiguration.DataSource.Iconfigurators)
                    return GetVehicleImagesIconfigurators(y, m, mdl, "", sm);
                else if (Global.Session.Instance.Client.VehicleSelectorDataSource == VehicleConfiguration.DataSource.FuelAPI)
                    return GetVehicleImagesIconfigurators(y, m, mdl, "", "");
            }
            else if (Global.Session.Instance.Client.ImageDataSource == VehicleConfiguration.DataSource.FuelAPI)
            {
                /*if (Global.Session.Instance.Client.VehicleSelectorDataSource ==
                    VehicleConfiguration.DataSource.DriveRight)
                    return GetVehicleImagesFuelAPI(y, m, mdl, b, sm, o);
                else if (Global.Session.Instance.Client.VehicleSelectorDataSource ==
                         VehicleConfiguration.DataSource.Iconfigurators)
                    return GetVehicleImagesFuelAPI(y, m, mdl, b, sm, o);
                else if (Global.Session.Instance.Client.VehicleSelectorDataSource == VehicleConfiguration.DataSource.FuelAPI)*/
                    return GetVehicleImagesFuelAPI(y, m, mdl, b, sm, o, imageId);
            }
            return null;
        }

        public ActionResult GetVehicleImageSwatches(int vid)
        {
            if (Global.Session.Instance.Client.ImageDataSource == VehicleConfiguration.DataSource.Iconfigurators)
                return GetVehicleImageSwatchesIconfigurators(vid);
            else
                return null;
        }

        #endregion

        #region Drive Right Actions

        private void OutputExecutionTime(string method, DateTime start)
        {
            var end = DateTime.Now;
            var diff = end - start;
            Logger.Info($"{method} excecuted in {diff.TotalMilliseconds}ms");
        }

        public ActionResult GetApiYears()
        {
            var start = DateTime.Now;
            IClassificationService service = new ClassificationService();
            var years = service.GetYears((ApiIdEnum)VelocityWheelsSettings.ExternalApiId);
            OutputExecutionTime("GetYearsDriveRight", start);
            return Json(years, JsonRequestBehavior.AllowGet);            
        }

        public ActionResult GetApiMakes(string y)
        {
            var start = DateTime.Now;
            IClassificationService service = new ClassificationService();
            var makes = service.GetMakes((ApiIdEnum)VelocityWheelsSettings.ExternalApiId, int.Parse(y));
            OutputExecutionTime("GetMakesDriveRight", start);
            return Json(makes, JsonRequestBehavior.AllowGet);            
        }

        public ActionResult GetApiModels(string y, string m)
        {
            var start = DateTime.Now;
            IClassificationService service = new ClassificationService();
            var models = service.GetModels((ApiIdEnum)VelocityWheelsSettings.ExternalApiId, int.Parse(y), m);
            OutputExecutionTime("GetModelsDriveRight", start);
            return Json(models, JsonRequestBehavior.AllowGet);            
        }

        public ActionResult GetApiBodyTypes(string y, string m, string mdl)
        {
            var start = DateTime.Now;
            IClassificationService service = new ClassificationService();
            var bodyTypes = service.GetBodyTypes((ApiIdEnum)VelocityWheelsSettings.ExternalApiId, int.Parse(y), m, mdl);
            OutputExecutionTime("GetBodyTypesDriveRight", start);
            return Json(bodyTypes, JsonRequestBehavior.AllowGet);            
        }

        public ActionResult GetApiSubModels(string y, string m, string mdl, string b)
        {
            var start = DateTime.Now;
            IClassificationService service = new ClassificationService();
            var subModels = service.GetSubModels((ApiIdEnum)VelocityWheelsSettings.ExternalApiId, int.Parse(y), m, mdl, b);
            OutputExecutionTime("GetSubModelsDriveRight", start);
            return Json(subModels, JsonRequestBehavior.AllowGet);            
        }

        #endregion

        #region Iconfigurators Actions

        public ActionResult GetYearsIconfigurators()
        {
            var jsonData = "";
            var url =
                $"http://iconfigurators.com/api/?function=getYears&key={VehicleConfiguration.ICONFIGURATORS_TOKEN}";
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            IconfiguratorYearsResult yr = JsonConvert.DeserializeObject<IconfiguratorYearsResult>(jsonData);
            return Json(yr.years
                .Where(y => y.year >= 1980)
                .Select(y => y.year)
                .OrderByDescending(y => y), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMakesIconfigurators(string y)
        {
            var jsonData = "";
            var url = string.Format("http://iconfigurators.com/api/?function=getMakes&year={1}&key={0}",
                VehicleConfiguration.ICONFIGURATORS_TOKEN, y);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            IconfiguratorMakesResult mr = JsonConvert.DeserializeObject<IconfiguratorMakesResult>(jsonData);
            return Json(mr.makes
                .Select(m => m.make)
                .OrderBy(m => m), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetModelsIconfigurators(string y, string m)
        {
            var jsonData = "";
            var url = string.Format("http://iconfigurators.com/api/?function=getModels&year={1}&make={2}&key={0}",
                VehicleConfiguration.ICONFIGURATORS_TOKEN, y, m);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            IconfiguratorModelsResult mr = JsonConvert.DeserializeObject<IconfiguratorModelsResult>(jsonData);
            return Json(mr.models
                .Select(mdl => mdl.model)
                .OrderBy(mdl => mdl), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBodyTypesIconfigurators(string y, string m, string mdl)
        {
            var jsonData = "";
            var url = string.Format("http://iconfigurators.com/api/?function=getVehicleTypes&key={0}",
                VehicleConfiguration.ICONFIGURATORS_TOKEN);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            IconfiguratorVehicleTypesResult vtr =
                JsonConvert.DeserializeObject<IconfiguratorVehicleTypesResult>(jsonData);
            return Json(vtr.Vehicle_Types
                .Select(vt => vt.vehicleTypeName)
                .OrderBy(vt => vt), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSubModelsIconfigurators(string y, string m, string mdl, string b)
        {
            var jsonData = "";
            var url =
                string.Format(
                    "http://iconfigurators.com/api/?function=getSubmodels&year={1}&make={2}&model={3}&key={0}",
                    VehicleConfiguration.ICONFIGURATORS_TOKEN, y, m, mdl);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            IconfiguratorSubModelsResult smr = JsonConvert.DeserializeObject<IconfiguratorSubModelsResult>(jsonData);
            return Json(smr.submodels
                .Select(sm => sm.submodel)
                .Distinct()
                .OrderBy(sm => sm), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVehicleImagesIconfigurators(string y, string m, string mdl, string b, string sm)
        {
            var jsonData = "";
            var url =
                string.Format(
                    "http://iconfigurators.com/api/?function=getVehicleImages&year={1}&make={2}&model={3}&submodel={4}&key={0}",
                    VehicleConfiguration.ICONFIGURATORS_TOKEN, y, m, mdl, sm);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVehicleImageSwatchesIconfigurators(int vid)
        {
            var jsonData = "";
            var url =
                string.Format("http://iconfigurators.com/api/?function=getVehicleColors&vehicleImageID={1}&key={0}",
                    VehicleConfiguration.ICONFIGURATORS_TOKEN, vid);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region FuelAPI Actions

        public ActionResult GetYearsFuelAPI()
        {
            var start = DateTime.Now;

            var jsonData = "";
            var url = string.Format("https://api.fuelapi.com/v1/json/modelYears/?api_key={0}",
                VehicleConfiguration.FUEL_API_TOKEN);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<string> years = JsonConvert.DeserializeObject<List<string>>(jsonData);

            var end1 = DateTime.Now;
            var diff1 = end1 - start;
            Logger.Info($"GetYearsFuelAPI excecuted in {diff1.TotalMilliseconds}ms");
            return Json(years
                .Select(y => y)
                .OrderByDescending(y => y), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMakesFuelAPI(string y)
        {
            var jsonData = "";
            var url = string.Format("https://api.fuelapi.com/v1/json/makes/?year={1}&api_key={0}",
                VehicleConfiguration.FUEL_API_TOKEN, y);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<FuelAPIMake> makes = JsonConvert.DeserializeObject<List<FuelAPIMake>>(jsonData);
            Global.Session.Instance.FuelAPIMakes = makes;
            return Json(makes
                .Select(m => m.name)
                .OrderBy(m => m), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetModelsFuelAPI(string y, string m)
        {
            var jsonData = "";
            var mID = Global.Session.Instance.FuelAPIMakes.FirstOrDefault(make => make.name == m).id;
            var url = string.Format("https://api.fuelapi.com/v1/json/models/{2}/?year={1}&api_key={0}",
                VehicleConfiguration.FUEL_API_TOKEN, y, mID);
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<FuelAPIModel> models = JsonConvert.DeserializeObject<List<FuelAPIModel>>(jsonData);
            return Json(models
                .Select(mdl => mdl.name)
                .OrderBy(mdl => mdl), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBodyTypesFuelAPI(string y, string m, string mdl)
        {
            return null;
        }

        public ActionResult GetSubModelsFuelAPI(string y, string m, string mdl, string b)
        {
            var jsonData = "";
            var url = string.Format(
                "https://api.fuelapi.com/v1/json/vehicles/?year={1}&make={2}&model={3}&api_key={0}",
                VehicleConfiguration.FUEL_API_TOKEN, y, m, mdl);

            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            List<FuelAPISubModel> subModels = JsonConvert.DeserializeObject<List<FuelAPISubModel>>(jsonData);
            foreach (var sm in subModels)
            {
                if ((sm.trim ?? "") == "")
                    sm.trim = "Base";
            }
            Global.Session.Instance.FuelAPISubModels = subModels;
            return Json(subModels
                .Select(sm => sm.trim)
                .Distinct()
                .OrderBy(sm => sm), JsonRequestBehavior.AllowGet);
        }

        public string GetChassisIdFromApi(int year, string make, string model, string bodyType, string subModel)
        {
            try
            {
                ClassificationService classificationService = new ClassificationService();
                var chassis = classificationService.GetChassis(year, make, model, bodyType, subModel);
                if (chassis.Any())
                {
                    return chassis.FirstOrDefault().ToString();
                }
            }
            catch (Exception e)
            {
                Logger.Error("GetChassisIdFromApi exception " + e.Message + e.StackTrace);
            }

            return null;
        }

        public ActionResult VehicleExists(string y, string m, string mdl, string b, string sm, string partNumber)
        {
            VehicleExistsModel model = new VehicleExistsModel();

            try
            {
                if (!string.IsNullOrWhiteSpace(y) && !string.IsNullOrWhiteSpace(m) && !string.IsNullOrWhiteSpace(mdl))
                {
                    var jsonData = GetVehicleFromFuelApi(y, m, mdl, b, sm, string.Empty, null);
                    VehicleService service = new VehicleService();
                    var vehicle = service.GetVehicleFromJson(jsonData);
                    if (vehicle?.Products != null && vehicle.Products.Any())
                    {
                        var formats = vehicle.Products[0].ProductFormats;
                        if (formats != null && formats.Any() && formats[0].Assets.Any())
                        {
                            model.Exists = true;
                        }
                    }
                }

                if (!string.IsNullOrWhiteSpace(partNumber))
                {
                    ProductService service = new ProductService();
                    if (!service.ProductExists(partNumber))
                    {
                        model.Exists = false;
                    }
                }
            }
            catch (Exception e)
            {
                model.Error = e.Message;
            }
            

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVehicleImagesFuelAPI(string y, string m, string mdl, string b, string sm, string o, int? imageId = null)
        {
            var start = DateTime.Now;
            
            var jsonData = GetVehicleFromFuelApi(y, m, mdl, b, sm, o, imageId);

            var end = DateTime.Now;
            var diff = end - start;
            Logger.Info($"GetVehicleImagesFuelAPI excecuted in {diff.TotalMilliseconds}ms");
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private string GetVehicleFromFuelApi(string y, string m, string mdl, string b, string sm, string o, int? imageId = null)
        {
            var session = Global.Session.Instance;
            string jsonData = null; //session.GetFuelApiMapResponse(y, m, mdl);

            var vehicleService = new VehicleService();
            List<FuelAPISubModel> subModels = vehicleService.GetVehicles(y, m, mdl, b, sm, out jsonData);

            int year = int.Parse(y);
            string chassid = GetChassisIdFromApi(year, m, mdl, b, sm);
            long chassisId = 0;
            long.TryParse(chassid, out chassisId);

            if (string.IsNullOrWhiteSpace(jsonData) || jsonData == "[]")
            {
                jsonData = new JavaScriptSerializer().Serialize(new FuelApiVehicle());
            }

            // Insert default Chassis Id                     
            jsonData = InsertChassisIdIntoJson(jsonData, chassisId);           

            var subModel = subModels.Count > 1 ? GetFuelApiSubModel(b, sm, subModels) : subModels.FirstOrDefault();

            if (subModel != null)
            {
                int smId = subModel.id;

                string productFormatId = "43";
                if ((o ?? "") == "Rear")
                {
                    productFormatId = "41";
                }

                Logger.Info(
                    $"{y} {m} {mdl} {b} {sm} -> {subModel.Year} {subModel.make_name} {subModel.model_name} {subModel.bodytype_desc} {subModel.trim}");

                string jsonDataSm = null; //session.GetFuelApiMapResponse(smId.ToString(), productFormatId, "submodel");
                if (jsonDataSm.IsNullOrWhiteSpace())
                {
                    string url = $"https://api.fuelapi.com/v1/json/vehicle/{smId}?productID=2&productFormatIDs={productFormatId}&api_key={VehicleConfiguration.FUEL_API_TOKEN}";
                    Logger.Info($"Getting sub model for {url}");
                    using (var wc = new MyWebClient())
                    {
                        jsonData = wc.DownloadString(url);
                        // Insert Chassis Id
                        string bodyType = b;
                        if (string.IsNullOrWhiteSpace(b) || b.Contains("undefined"))
                        {
                            bodyType = "";
                        }
                        DateTime startTime = DateTime.Now;

                        OutputExecutionTime("GetChassisIdFromApi", startTime);
                        jsonData = InsertChassisIdIntoJson(jsonData, chassisId);

                        session.SetFuelApiMap(smId.ToString(), productFormatId, "submodel", jsonData);
                    }
                }
                else
                {
                    jsonData = jsonDataSm;
                }
            }
            else
            {
                //session.SetFuelApiMap(y, m, mdl, jsonData);
                ClassificationService service = new ClassificationService(vehicleService.Context);
                var vehicles = service.GetFullVehicles(ApiIdEnum.Aces, year, m, mdl, b, sm);
                var vehicle = vehicles.FirstOrDefault(v => v.VehicleImages.Any());
                if (vehicle != null && vehicle.VehicleImages.Any())
                {
                    var vehicleImage = vehicle.VehicleImages.FirstOrDefault();
                    var fuelApiVehicle = new FuelApiVehicle
                    {
                        ApiId = (int)ApiIdEnum.Aces,
                        VehicleImageId = vehicleImage.VehicleImageID,
                        Bodytype = b,
                        Id = vehicle.Id.ToString(),
                        Trim = vehicle.SubModel,
                        Model = new FuelAPIModel()
                        {
                            id = (int) vehicle.Id,
                            code = vehicle.Model,
                            name = vehicle.Model
                        },
                        Products = new List<FuelApiProduct>()
                        {
                            new FuelApiProduct()
                            {
                                Id = "2",
                                Name = "Color",
                                Type = "images",
                                ProductFormats = new List<FuelApiProductFormats>()
                                {
                                    new FuelApiProductFormats()
                                    {
                                        Id = "43",
                                        Type = "images",
                                        Assets = new List<FuelApiAssets>()
                                        {
                                            new FuelApiAssets()
                                            {
                                                //Url = $"{service.VehicleImagesBaseUrl}/{vehicle.VehicleImage.FileName}",
                                                Url = $"{vehicleImage.FileName}",
                                                ShotCode = new FuelApiShotCode()
                                                {
                                                    Type = "color",
                                                    Code = "NONE",
                                                    Color = new FuelApiColor()
                                                    {
                                                        Oem_name = "Default",
                                                        Simple_name = "White",
                                                        Rgb1 = "FFFFFF"
                                                    }
                                                },
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    };



                    //jsonData = new JavaScriptSerializer().Serialize(fuelApiVehicle);
                    jsonData = GetJsonResult(fuelApiVehicle);
                    jsonData = InsertChassisIdIntoJson(jsonData, chassisId);
                    
                }
            }
            return jsonData;
        }

        private static ContentResult GetJsonContentResult(object data)
        {
            var formatter = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            var json = new ContentResult()
            {
                Content = JsonConvert.SerializeObject(data, formatter),
                ContentType = "application/json"
            };
            return json;
        }

        private static string GetJsonResult(object data)
        {
            var formatter = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            return JsonConvert.SerializeObject(data, formatter);
        }

        private static FuelAPISubModel GetFuelApiSubModel(string b, string sm, List<FuelAPISubModel> subModels)
        {
            List<FuelAPISubModel> bodyTypes = subModels.Where(s => s.bodytype_desc?.Equals(b, StringComparison.InvariantCultureIgnoreCase) ?? 
                                                  s.bodytype.Equals(b, StringComparison.InvariantCultureIgnoreCase)).OrderBy(s => s.id).ToList();
            if (!bodyTypes.Any())
            {
                bodyTypes = subModels;
            }

            var subModel = bodyTypes.FirstOrDefault(subm => subm.trim == sm);
            if (subModel == null && bodyTypes.Any())
            {
                subModel = bodyTypes.First();
            }
            return subModel;
        }

        private static string InsertChassisIdIntoJson(string jsonData, long? chassisId)
        {
            if (jsonData == null)
            {
                return null;
            }            
            
            string id = "\"id\"";
            var idPos = jsonData.IndexOf(id, StringComparison.InvariantCultureIgnoreCase);
            if (idPos >= 0)
            {
                var endPos = jsonData.IndexOf(",", idPos + id.Length);
                if (endPos > idPos)
                {
                    jsonData = jsonData.Insert(endPos + 1, $"\"chassisId\": {chassisId.GetValueOrDefault()},");
                }
            }
            return jsonData;
        }

        #endregion

        [System.Web.Mvc.HttpGet]
        public ActionResult GetWheelResults(long? cid, string brand, decimal? diameter, string sortField, int sortDirection, string token)
        {
            WheelService wheelService = new WheelService();

            // get account to filter results
            var accountService = new AccountService(wheelService.Context);
            var account = accountService.GetAccountFromToken(token);
            if (account != null)
            {
                SortOption sort = null;
                if (sortField != null)
                {
                    sort = new SortOption() {Field = sortField , Direction = sortDirection};
                }
                var results = wheelService.GetWheels(cid, new WheelFilter() {Brand = brand, Diameter = diameter}, account.Id, null, sort);
                if (results != null)
                {
                    results.TireWeb = account.TireWeb;
                    results.TireWebBaseUrl = account.TireWebBaseUrl;
                    return Json(results, JsonRequestBehavior.AllowGet);
                }
            }
            
            return Json(new WheelResults() { Error = "No Results Found"}, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult GetVehicleThumbs(string y, string m, string mdl)
        {
            try
            {
                string json;
                VehicleService service = new VehicleService();
                var subModels = service.GetVehicles(y, m, mdl, null, null, out json);
                var customImages = service.GetCustomImageVehicles(y, m, mdl);
                if (subModels.Any())
                {
                    var thumbs = service.GetVehicleThumbs(int.Parse(y), subModels);
                    if (customImages.Any())
                    {
                        thumbs.AddRange(customImages);
                    }
                    return Json(thumbs, JsonRequestBehavior.AllowGet);
                }

                return Json(customImages, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.LogException("GetVehicleThumbs exception = " + e.Message, e);
            }
            return Json(new List<VehicleImageResult>(), JsonRequestBehavior.AllowGet);
        }

        #region Wheel functions

        public ActionResult GetWheelPosition(int vid, string o, string pn, bool? clientUpload, int? imageId = null)
        {
            ProductService service = new ProductService();
            VehicleService vservice = new VehicleService(service.Context);
            var wheelPosition = vservice.GetWheelPosition(vid, o, imageId);

            if (wheelPosition != null)
            {
                var w = new WheelViewModel
                {
                    VehicleID = wheelPosition.VehicleID,
                    VehicleImageId = wheelPosition.VehicleImageId,
                    FrontLeft = wheelPosition.FrontLeft,
                    FrontTop = wheelPosition.FrontTop,
                    FrontWidth = wheelPosition.FrontWidth,
                    FrontHeight = wheelPosition.FrontHeight,
                    FrontAngle = wheelPosition.FrontAngle,
                    RearLeft = wheelPosition.RearLeft,
                    RearTop = wheelPosition.RearTop,
                    RearWidth = wheelPosition.RearWidth,
                    RearHeight = wheelPosition.RearHeight,
                    RearAngle = wheelPosition.RearAngle,
                    FlipX = wheelPosition.FlipX
                };

                if ((pn ?? "") != "")
                {
                    GetPartNumberImages(pn, service, w);

                }
                
                return Json(w, JsonRequestBehavior.AllowGet);
            }

            if (clientUpload.HasValue && clientUpload.Value)
            {
                WheelViewModel w;
                // return default wheel positions
                if (o == "Front")
                {
                    w = new WheelViewModel
                    {
                        VehicleID = vid,
                        VehicleImageId = imageId,
                        FrontLeft = 1001,
                        FrontTop = 694,
                        FrontWidth = 184,
                        FrontHeight = 268,
                        FrontAngle = 0,
                        RearLeft = 1831,
                        RearTop = 652,
                        RearWidth = 120,
                        RearHeight = 217,
                        RearAngle = 0                        
                    };
                }
                else
                {
                    w = new WheelViewModel
                    {
                        VehicleID = vid,
                        VehicleImageId = imageId,
                        FrontLeft = 1847,
                        FrontTop = 664,
                        FrontWidth = 125,
                        FrontHeight = 221,
                        FrontAngle = new decimal(2.431),
                        RearLeft = 1008,
                        RearTop = 690,
                        RearWidth = 188,
                        RearHeight = 279,
                        RearAngle = new decimal(1.838)
                    };
                }

                GetPartNumberImages(pn, service, w);

                return Json(w, JsonRequestBehavior.AllowGet);
            }

            // No positioning and not a client upload ... don't return any positions
            return Json(new WheelViewModel{VehicleID = null}, JsonRequestBehavior.AllowGet);
        }

        private void GetPartNumberImages(string pn, ProductService service, WheelViewModel w)
        {
            Data.Models.WheelSpec wi = service.GetByPartNumber(pn);
            if (wi != null)
            {
                // Wheel images need to exist or we treat this as a not found part
                if (!string.IsNullOrWhiteSpace(wi.Wheel.FrontMainImage) &&
                    !string.IsNullOrWhiteSpace(wi.Wheel.FrontMainImage))
                {
                    w.FrontMainImage = $"/Content/wheel-images/{wi.Wheel.Brand.Name}/{wi.Wheel.FrontMainImage}";
                    w.RearMainImage = $"/Content/wheel-images/{wi.Wheel.Brand.Name}/{wi.Wheel.RearMainImage}";
                }
                else
                {
                    Logger.Error(
                        $"GetWheelPosition, part number {pn} found but wheel images are null, please upload images.");
                }
            }            
        }

        #endregion

        #region General functions

        public ActionResult GetImage(string url)
        {
            byte[] data;
            using (var wc = new MyWebClient())
            {
                data = wc.DownloadData(url);
            }
            return File(data, "image/" + url.Substring(url.LastIndexOf('.') + 1));
        }

        public ActionResult GetCroppedImage(string url, string vid, string code, int fid, int? apiId)
        {
            try
            {
                var localPath = VelocityWheelsSettings.GetVehicleContentPath;

                if (apiId != null)
                {
                    return File(string.Format(localPath, url), "image/png");
                }
                
                VehicleService service = new VehicleService();

                int id;
                int.TryParse(vid, out id);
                var image = service.GetVehicleImage(url, id, code, fid);

                if (image != null)
                {
                    if (!image.VehicleID.HasValue)
                    {
                        image.VehicleID = id;
                        image.ImageCode = code;
                        image.ProductFormatId = fid;
                        service.SaveOrUpdate(image);
                    }
                }
                else
                {
                    image = service.DownloadVehicleImageFromFuelApi(url, id, code, fid);                    
                }

                return File(string.Format(localPath, image.FileName), image.ImageType);
            }
            catch (Exception e)
            {
                Logger.LogException("GetCroppedImage execption = " + e.Message, e);
            }

            return null;
        }

        

        

        //public ActionResult Get

        #endregion

        #region Download Wheel Data

        public ActionResult GetWheelBrandsIconfigurators()
        {
            var jsonData = "";
            var url = $"http://iconfigurators.com/api/?function=getBrands&key={VehicleConfiguration.ICONFIGURATORS_TOKEN}";
            using (var wc = new MyWebClient())
            {
                jsonData = wc.DownloadString(url);
            }
            IconfiguratorWheelBrandsResult wbr = JsonConvert.DeserializeObject<IconfiguratorWheelBrandsResult>(jsonData);
            var db = new VelocityTechEntities();
            foreach (var wb in wbr.wheelBrands)
            {
                db.IconWheelBrands.Add(new IconWheelBrand
                {
                    IconWheelBrandID = wb.brandID,
                    Name = wb.brandName,
                    LogoSmall = wb.logoSmall,
                    LogoMedium = wb.logoMedium
                });
            }
            db.SaveChanges();
            return Content("Done");
        }

        public ActionResult GetWheelSpecsIconfigurators()
        {
            var sb = new StringBuilder();

            var db = new VelocityTechEntities();
            var db2 = new VelocityTechEntities();
            try
            {
                foreach (var wb in db.IconWheelBrands.Where(wb => wb.IconWheelBrandID != 374))
                {
                    //Exclude 374 because of O-O-Memory issues
                    try
                    {
                        sb.AppendFormat("{0} {1} - ", wb.IconWheelBrandID, wb.Name);

                        //This is to download data
                        //var jsonData = "";
                        //var url = string.Format("http://iconfigurators.com/api/?function=getWheelSpecs&brandID={1}&key={0}", VehicleConfiguration.ICONFIGURATORS_TOKEN, wb.IconWheelBrandID);
                        //using(var wc = new MyWebClient()) {
                        //  jsonData = wc.DownloadString(url);
                        //}
                        //var wbws = db2.IconWheelBrands.Find(wb.IconWheelBrandID);
                        //wbws.WheelSpecs = jsonData;
                        //db2.SaveChanges();

                        //This is to process downloaded data
                        var jsonData = wb.WheelSpecs;
                        jsonData = jsonData.Replace("\"weight\" : ,", "\"weight\" : 0,");
                        jsonData = jsonData.Replace("\"loadRating\" : ,", "\"loadRating\" : 0,");
                        //IconfiguratorWheelSpecResult wsr = JsonConvert.DeserializeObject<IconfiguratorWheelSpecResult>(jsonData);
                        IconfiguratorWheelSpecResultDB wsr =
                            JsonConvert.DeserializeObject<IconfiguratorWheelSpecResultDB>(jsonData);
                        if (wsr.Result > 0)
                        {
                            db2.IconWheelSpecs.AddRange(wsr.WheelSpecs);
                            //foreach(var ws in wsr.WheelSpecs) {
                            //  db2.IconWheelSpecs.Add(new IconWheelSpec {
                            //    wheelID = ws.wheelID,
                            //    bigBreak = ws.bigBreak,
                            //    boltPat1 = ws.boltPat1,
                            //    boltPat2 = ws.boltPat2,
                            //    boltPat3 = ws.boltPat3,
                            //    boltPat4 = ws.boltPat4,
                            //    boltPatternList = ws.boltPatternList,
                            //    bore = ws.bore,
                            //    brandID = ws.brandID,
                            //    brandName = ws.brandName,
                            //    BSM = ws.BSM,
                            //    capNumber = ws.capNumber,
                            //    dateUpdated = ws.dateUpdated,
                            //    decDiameter = ws.decDiameter,
                            //    decWidth = ws.decWidth,
                            //    FinishID = ws.FinishID,
                            //    lipSize = ws.lipSize,
                            //    loadRating = ws.loadRating,
                            //    longFinishName = ws.longFinishName,
                            //    lugType = ws.lugType,
                            //    offSet = ws.offSet,
                            //    partNumber = ws.partNumber,
                            //    productImages_faceImage = ws.productImages.faceImage,
                            //    productImages_highAngleImage = ws.productImages.highAngleImage,
                            //    productImages_maxImage = ws.productImages.maxImage,
                            //    productMSRP = ws.productMSRP,
                            //    producttDescription = ws.producttDescription,
                            //    shortFinishName = ws.shortFinishName,
                            //    styleName = ws.styleName,
                            //    vehicleImages_frontWheelImage = ws.vehicleImages.frontWheelImage,
                            //    vehicleImages_rearWheelImage = ws.vehicleImages.rearWheelImage,
                            //    weight = ws.weight,
                            //    wheelImageID = ws.wheelImageID
                            //  });
                            //}
                            db2.SaveChanges();
                        }

                        sb.Append("Done<br>");
                    }
                    catch (Exception ex)
                    {
                        sb.AppendFormat("{0}<br>", ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                sb.AppendFormat("{0}<br>", ex.Message);
            }
            return Content(sb.ToString());
        }

        public ActionResult ExportWheelSpecsIconfigurators()
        {
            var sb = new StringBuilder();

            var db = new VelocityTechEntities();
            var db2 = new VelocityTechEntities();
            try
            {
                foreach (
                    var wb in db.IconWheelBrands.Where(wb => wb.IconWheelBrandID != 374 && wb.IconWheelBrandID > 635))
                {
                    //Exclude 374 because of O-O-Memory issues
                    try
                    {
                        sb.AppendFormat("{0} {1} - ", wb.IconWheelBrandID, wb.Name);
                        var csv =
                            new CsvHelper.CsvWriter(
                                new StreamWriter(string.Format("{0}\\{1}.csv", Server.MapPath("/WheelDataByBrand"),
                                    wb.Name.Replace("/", "-"))));
                        csv.WriteRecords(
                            db2.IconWheelSpecs.Where(ws => ws.brandID == wb.IconWheelBrandID)
                                .Select(ws => new IconfiguratorWheelNoImage
                                {
                                    bigBreak = ws.bigBreak ?? 0,
                                    boltPat1 = ws.boltPat1,
                                    boltPat2 = ws.boltPat2,
                                    boltPat3 = ws.boltPat3,
                                    boltPat4 = ws.boltPat4,
                                    boltPatternList = ws.boltPatternList,
                                    bore = ws.bore ?? 0,
                                    brandName = ws.brandName,
                                    BSM = ws.BSM ?? 0,
                                    capNumber = ws.capNumber,
                                    dateUpdated = ws.dateUpdated,
                                    decDiameter = ws.decDiameter ?? 0,
                                    decWidth = ws.decWidth ?? 0,
                                    lipSize = ws.lipSize,
                                    loadRating = ws.loadRating ?? 0,
                                    longFinishName = ws.longFinishName,
                                    lugType = ws.lugType,
                                    offSet = ws.offSet,
                                    partNumber = ws.partNumber,
                                    productMSRP = ws.productMSRP ?? 0,
                                    producttDescription = ws.producttDescription,
                                    shortFinishName = ws.shortFinishName,
                                    styleName = ws.styleName,
                                    weight = ws.weight ?? 0,
                                    wheelID = ws.IconWheelSpecID
                                }));
                        sb.Append("Done<br>");
                    }
                    catch (Exception ex)
                    {
                        sb.AppendFormat("{0}<br>", ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                sb.AppendFormat("{0}<br>", ex.Message);
            }
            return Content(sb.ToString());
        }

        #endregion
    }
}