﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VelocityWheels.Data.Models;
using VelocityWheels.Models;

namespace VelocityWheels.Controllers
{
    public class HomeController : Controller
    {
        log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(HomeController));
        //
        // GET: /Home/

        public ActionResult Index()
        {            
            return View();
        }

    }
}
