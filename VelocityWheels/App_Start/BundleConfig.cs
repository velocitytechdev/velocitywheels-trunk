﻿using System.Web.Optimization;

namespace VelocityWheels
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Content/studio/bundles/WheelStudioScripts").Include(
                "~/Content/studio/js/fabric1.6.6.js",
                "~/Content/studio/js/colorpicker.js",
                "~/Content/studio/js/leanModal.js",
                "~/Content/studio/js/studio_v2d.js",
                "~/Content/js/vt-vehicle-selector.js"
            ));

            bundles.Add(new StyleBundle("~/Content/studio/bundles/WheelStudioStyles").Include(
                "~/Content/studio/css/colorpicker.css",
                /*"~/Content/css/font-awesome.min.css",*/
                "~/Content/studio/css/vtws-icons.css",
                "~/Content/studio/css/studio.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}