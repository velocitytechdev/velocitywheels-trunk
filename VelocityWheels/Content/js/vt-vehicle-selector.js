﻿//BASE URL
//var baseURL = "";
//baseURL = "http://apps.velocity-tech.com";
//baseURL = "http://localhost:8091";

var autoSelect = false;
var autoSelectYear = 2017;
var autoSelectMake = 'Ford';
var autoSelectModel = 'Mustang';
var autoSelectBodyType = 'Coupe';
var autoSelectSubModel = 'V6';
var vehicleList = null;
var vehicleYears = [];

function resetYears() {
    var s = $("#vtws-vehicle-year");
    if (vehicleYears.length === 0) {
        s.html('<option>YEAR</option>');
        s.val('YEAR');
        s.prop('disabled', 'disabled');
    }
  

  if (typeof setVehicleBackground !== "undefined")
    setVehicleBackground(c_mainVehicle, '');
}
function resetMakes() {
  var s = $("#vtws-vehicle-make");
  s.html('<option>MAKE</option>');
  s.val('MAKE');
  s.prop('disabled', 'disabled');
}
function resetModels() {
  var s = $("#vtws-vehicle-model");
  s.html('<option>MODEL</option>');
  s.val('MODEL');
  s.prop('disabled', 'disabled');
}
function resetBodyTypes() {
  var s = $("#vtws-vehicle-body-type");
  s.html('<option>BODY TYPE</option>');
  s.val('BODY TYPE');
  s.prop('disabled', 'disabled');
}
function resetSubModels() {
  var s = $("#vtws-vehicle-sub-model");
  s.html('<option>SUB-MODEL</option>');
  s.val('SUB-MODEL');
  s.prop('disabled', 'disabled');
}
function getYears(reset) {
    //if (reset || vehicleYears.length === 0) {

        resetYears();
        resetMakes();
        resetModels();
        resetBodyTypes();
        resetSubModels();

        $.get(baseURL + "/Vehicle/GetYears", function (data) {
            vehicleYears = data;
            var s = $("#vtws-vehicle-year");
            $.each(data, function (index, value) {
                s.append($('<option></option>').html(value));
            });
            s.prop('disabled', false);
            if (autoSelect)
                s.val(autoSelectYear);
            getMakes();
        });
    //}
}
function getMakes() {
  var y = $("#vtws-vehicle-year").val();
  if (y == "YEAR")
    return;

  resetMakes();
  resetModels();
  resetBodyTypes();
  resetSubModels();
  $.get(baseURL + "/Vehicle/GetMakes/?y=" + y, function (data) {
    var s = $("#vtws-vehicle-make");
    $.each(data, function (index, value) {
      s.append($('<option></option>').html(value));
    });
    s.prop('disabled', false);
    if (autoSelect)
      s.val(autoSelectMake);
    getModels();
  });
}
function getModels() {
  var y = $("#vtws-vehicle-year").val();
  var m = $("#vtws-vehicle-make").val();
  if (y == "YEAR" || m == "MAKE")
    return;

  resetModels();
  resetBodyTypes();
  resetSubModels();
  $.get(baseURL + "/Vehicle/GetModels/?y=" + y + "&m=" + m, function (data) {
    var s = $("#vtws-vehicle-model");
    $.each(data, function (index, value) {
      s.append($('<option></option>').html(value));
    });
    s.prop('disabled', false);
    if (autoSelect)
      s.val(autoSelectModel);
    else if (s.children().length == 2)
      s.prop('selectedIndex', 1);
    getBodyTypes();
  });
}
function getBodyTypes() {
  var y = $("#vtws-vehicle-year").val();
  var m = $("#vtws-vehicle-make").val();
  var mdl = $("#vtws-vehicle-model").val();
  if (y == "YEAR" || m == "MAKE" || mdl == "MODEL")
    return;

  //Enable this if we need Vehicle Image to show upon Model being selected
  //getVehicleImage();

  resetBodyTypes();
  resetSubModels();
  $.get(baseURL + "/Vehicle/GetBodyTypes/?y=" + y + "&m=" + m + "&mdl=" + mdl, function (data) {
    var s = $("#vtws-vehicle-body-type");
    $.each(data, function (index, value) {
      s.append($('<option></option>').html(value));
    });
    s.prop('disabled', false);
    if (autoSelect)
      s.val(autoSelectBodyType);
    else if (s.children().length == 2)
      s.prop('selectedIndex', 1);
    getSubModels();
  });
}
function getSubModels() {
  var y = $("#vtws-vehicle-year").val();
  var m = $("#vtws-vehicle-make").val();
  var mdl = $("#vtws-vehicle-model").val();
  var b = $("#vtws-vehicle-body-type").val();    
  if (y == "YEAR" || m == "MAKE" || mdl == "MODEL" || b == "BODY TYPE")
    return;

  resetSubModels();
  $.get(baseURL + "/Vehicle/GetSubModels/?y=" + y + "&m=" + m + "&mdl=" + mdl + "&b=" + b, function (data) {
    var s = $("#vtws-vehicle-sub-model");
    $.each(data, function (index, value) {
      s.append($('<option></option>').html(value));
    });
    s.prop('disabled', false);

    autoTrigger = false;
    if (autoSelect) {
      s.val(autoSelectSubModel);
      autoSelect = false; //Only do once on page load
      //autoTrigger = true; //Disabled because of the separation between the vehicle selector and wheel studio
    }
    else if (s.children().length == 2) {
      s.prop('selectedIndex', 1);
      autoTrigger = true;
    }
    if (autoTrigger) { //Trigger right function based on who we are
      subModelSelected();
    }
  });
}
function subModelSelected() {
  var y = $("#vtws-vehicle-year").val();
  var m = $("#vtws-vehicle-make").val();
  var mdl = $("#vtws-vehicle-model").val();
  var b = $("#vtws-vehicle-body-type").val();
  var sm = $("#vtws-vehicle-sub-model").val();
  if (y == "YEAR" || m == "MAKE" || mdl == "MODEL" || b == "BODY TYPE" || sm == "SUB-MODEL")
    return;

  var v = y + "|" + m + "|" + mdl + "|" + b + "|" + sm;

  var cb = $("#vtws-vehicle-selector").data("callback");
  if (cb && cb != "") {
    eval(cb + "('" + v + "', 0)");
  }
  else {
    var url = location.href.split('?');
    history.pushState({}, '', url[0] + "?v=" + v);
    showVehicleHeaderText(y,m,mdl,b,sm);
    getVehicleImage();
  }
}

function showVehicleHeaderText(y,m,mdl,b,sm) {
    $("#vtws-selected-vehicle > span").html(
    '<span class="vtws-year">' + y + ' </span>'
   + '<span class="vtws-make">' + m + ' </span>'
   + '<span class="vtws-model">' + mdl + ' </span>'
   + '<span class="vtws-bodyStyle">' + b + ' </span>'
   + '<span class="vtws-subModel">' + sm + ' </span>');
    $("#vtws-selected-vehicle").show();
    $("#vtws-select-menus").hide();
};

function prepareVehicleArea() {
    $("#vtws-vehicleSelect").hide();
    $("#vtws-mainImage").show();
};

function vtwsSelectVehicle(y, m, mdl, b, sm, pn, checkUrl, brand, getWheelResults, imageId) {
    // Check to see if there is a vehicle on the url
    if (getWheelResults == null) {
        getWheelResults = true;
    }
    if (checkUrl === null || checkUrl === undefined || checkUrl === true) {
        var v = qs("v");
        if (v && v != "") {
            // We are already processing a url request ... return.
            return;
        }
    }

    // If vehicle select shown, we need to hide it
    prepareVehicleArea();

    //console.log("vtwsSelectVehicle = " + y + " " + m + " " + mdl + " " + b + " " + sm);

    showVehicleHeaderText(y, m, mdl, b, sm);

    // If vehicle select shown, we need to hide it
    prepareVehicleArea();

  vtwsSetAndSelectVehicleOption("year", y);
  vtwsSetAndSelectVehicleOption("make", m);
  vtwsSetAndSelectVehicleOption("model", mdl);
  vtwsSetAndSelectVehicleOption("body-type", b);
  vtwsSetAndSelectVehicleOption("sub-model", sm);

    // set brand search
  if (brand !== null && brand !== undefined) {
      $("#vtws-productBrand").val(brand);
  }  

  getVehicleImage(pn, getWheelResults, imageId);

  /*
  autoSelectYear = y;
  autoSelectMake = m;
  autoSelectModel = mdl;
  autoSelectBodyType = b;
  autoSelectSubModel = sm;
  autoSelect = true;

  getYears();
  */
}

function vtwsLoadBuilder(partNumber) {
    var builder = $("#vtws-wheelBuilder");
    if (builder.length === 0) {
        return null;
    }
    builder.html("");    
    displayCustomizeWheel(null, partNumber, false);
    var customizeSection = $("#vtws-customize-wheel-section");
    builder.append(customizeSection);
}

function vtwsSetAndSelectVehicleOption(option, value) {
  var s = $("#vtws-vehicle-" + option);
  s.append($('<option></option>').html(value));
  s.val(value);
}

function vtwsChangeVehicle() {
    if (vtwsSettings && vtwsSettings.vehicleChangeClicked) {
        vtwsSettings.vehicleChangeClicked();
    }
  showWheelResults(false);
  $("#vtws-selected-vehicle").hide();  
  getYears(false);
  $("#vtws-select-menus").show();

  // If vehicle select shown, we need to hide it
  prepareVehicleArea();
}

$(function () {
    console.log("vt-vehicle-selector.js Startup");

    $("#vtws-selected-vehicle").hide();
    $("#vtws-change-vehicle").click(vtwsChangeVehicle);
    $("#vtws-vehicle-year").change(getMakes);
    $("#vtws-vehicle-make").change(getModels);
    $("#vtws-vehicle-model").change(getBodyTypes);
    $("#vtws-vehicle-body-type").change(getSubModels);
    $("#vtws-vehicle-sub-model").change(subModelSelected);
    $("#vtws-change-vehicle").click(vtwsChangeVehicle);

    var brand = qs("b");
    
    var v = qs("v");
    if (v && v != "") {
        var tokens = v.split("|");
        if (tokens && tokens.length == 5) {
            var y = tokens[0];
            var m = tokens[1];
            var mdl = tokens[2];
            var b = tokens[3];
            var sm = tokens[4];
            //console.log("loading from url ..." + y + " " + m + " " + mdl + " " + b + " " + sm);
            vtwsSelectVehicle(y, m, mdl, b, sm, null, false, brand);
        }
    }
    else {
        if (brand && brand != "") {
            searchWheels({ brand: brand });
        }
        if ($('#vtws-select-menus').is(':visible')) {
            getYears(true);
        }
    }
});