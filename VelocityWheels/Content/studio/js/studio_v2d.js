//$(document).foundation();

//BASE URL
//var baseURL = "";
//baseURL = "http://apps.velocity-tech.com";
//baseURL = "http://localhost:8091";
//baseURL = "http://http://50.112.185.19/";

// OBJECTS
var $mainVehicleStage = $('#vtws-mainVehicle');
var $mainWheelleStage = $('#vtws-mainWheel');
var $uploadVehicle = document.getElementById('imgLoader');
//var $wheelGrid = $('#studioWheelGrid');

var clientImageUpload = false;

var $editBrightness = $('#vtws-brightness');
var $brightnessSlider = $('#vtws-brightnessSlider');
var $brightnessSliderInput = $('#vtws-brightnessSliderInput');
var $contrastSliderInput = $('#vtws-contrastSliderInput');

// Front Wheel Group
var wvF_main;
var wvF_window;
var wvF_face;
var wvfWheelGroup = {};

// Rear Wheel Group
var wvR_main;
var wvR_window;
var wvR_face;
var wvrWheelGroup = {};

// SETTINGS
var baseVehicleWidth = 725 * 3;
var baseVehicleHeight = 380 * 3;
var vwiDir = "http://apps.velocity-tech.com" + '/Content/wheel-images/';
//var baseVehicleWidth = '960' //'1440';
//var baseVehicleHeight = '420' //'900';
//var vwiDir = '/Content/studio/wheels/';
var vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;
var vehicleWheelScale = vehicleStageScale;

var handleSize = 15;
var handleColor = '#0f0';
var paintLayerAlpha = .85;
var wheelColorSet = false;
var wheelColorLayer;
var wheelColorMap = {};
var wheelColor;
var flipWheelToggle = false;

// INITIATE VEHICLE CANVAS ============
var c_mainVehicle = null;
if ($("#v").length > 0)
  c_mainVehicle = new fabric.Canvas('v');
var c_mainWheel;
var img_vc;
var group_wvF;
var group_wvR;

// WHEEL RESULTS
var resultHitCount = 0;
var sortingFiltering = false;
var currentWheelResults = null;
var currentFilter = {};

// Vehicle History
var lastVehicleId = 0;

// Custom Vehicle Image
var vehicleImageId = null;

// Wheel Sizing
var sizeUpRatio = 1;

function getVehicleCanvas() {
    return c_mainVehicle;
};

function sizeWheel(dir) {
    var sizeRatio = 1;
    if (sizeUpRatio === 1 && dir === "down") {
        return;
    }

    if (dir === "down") {
        sizeRatio = 0.95;        
    } else {
        sizeRatio = 1.05;
    }
    sizeUpRatio *= sizeRatio;
    if (sizeUpRatio <= 1) {
        sizeUpRatio = 1;
    }
    sizeWheelToRatio(sizeRatio, sizeUpRatio);
};

function sizeWheelToRatio(sizeRatio, newRatio) {
    var frontW = group_wvF.getWidth();
    var frontH = group_wvF.getHeight();
    var frontDiffW = (frontW - frontW * sizeRatio) / 2;
    var frontX = group_wvF.getLeft();
    var frontDiffH = (frontH - frontH * sizeRatio) / 2;
    var frontY = group_wvF.getTop();
    var rearDiffW = (group_wvR.getWidth() - group_wvR.getWidth() * sizeRatio) / 2;
    var rearX = group_wvR.getLeft();
    var rearDiffH = (group_wvR.getHeight() - group_wvR.getHeight() * sizeRatio) / 2;
    var rearY = group_wvR.getTop();
    group_wvF.set({ scaleX: newRatio, scaleY: newRatio, left: frontX + frontDiffW, top: frontY + frontDiffH }).setCoords();
    group_wvR.set({ scaleX: newRatio, scaleY: newRatio, left: rearX + rearDiffW, top: rearY + rearDiffH }).setCoords();
    c_mainVehicle.renderAll();
};

function resetSizeRatio() {
    sizeUpRatio = 1;
};


// Wheel Size Up
$('#sizeUp').click(function () {
    sizeWheel("up");
});
// Wheel Size Down
$('#sizeDown').click(function () {
    sizeWheel("down");
});

function setupWheelFilters(element) {
    var $filterOption = element.find('.vtws-filter');
    var openDuration = 8 * 1000;
    var closeOnPause;

    $filterOption.click(function () {
        
        // Clear All Timers
        function closeDDs() { $filterOption.removeClass('open'); };

        // Only for Horizontal Filters
        if (element.hasClass('vtws-filters-horizontal')) {
            // Open this dropdown, if not already open
            if ($(this).hasClass('open')) {
                $(this).removeClass('open');
            } else {
                // Close all other dropdowns and open this one
                closeDDs();
                $(this).addClass('open');

                // Set timer to close dropdown
                clearInterval(closeOnPause);
                closeOnPause = setInterval(closeDDs, openDuration);
            }
        }
    });
};

function applyColorPicker() {
    // COLOR PICKER
    $(".colorpicker").spectrum({
        flat: false,
        allowEmpty: true,
        showInput: false,
        chooseText: "Paint It",
        cancelText: "Cancel",
        showInitial: false,
        showPalette: true,
        showSelectionPalette: true,
        replacerClassName: 'pickerBox',
        maxPaletteSize: 10,
        preferredFormat: "hex",
        localStorageKey: "cp.wheel",
        show: function () {
        },
        change: function () {
        },
        hide: function (newColor) {
            // Action
            wheelColorSet = true;
            wheelColorLayer = String($(this).parent().data('layer'));
            wheelColor = newColor;
            wheelColorMap[wheelColorLayer] = wheelColor;
            changeLayerColor(wheelColorLayer, wheelColor);
        },
        palette: [
           ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
           "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)",
           "rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
           "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"]
        ]
    });
};

function clearWheelResults() {
    var wheelResults = $("#vtws-wheel-results");
    if (wheelResults === null || wheelResults === undefined) {
        return null;
    }
    wheelResults.addClass("vtws-hide");
    wheelResults.html("");
    return wheelResults;
};

function showWheelResults(show) {
    var wheelResults = $("#vtws-wheel-results");
    if (wheelResults.length === 0) {
        return null;
    }

    if (show !== true && !wheelResults.hasClass("vtws-hide")) {
        wheelResults.addClass("vtws-hide");
    } else if (show === true) {
        wheelResults.removeClass("vtws-hide");
        if (vtwsSettings && vtwsSettings.onProductsLoaded) {
            vtwsSettings.onProductsLoaded(resultHitCount);
        }
    }
    
    return wheelResults;
};

function resetVehicleCanvas() {
    vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;
    vehicleWheelScale = vehicleStageScale;
  if (c_mainVehicle) {
    c_mainVehicle.setWidth(baseVehicleWidth * vehicleStageScale);
    c_mainVehicle.setHeight(baseVehicleHeight * vehicleStageScale);
  }
}
// Main Vehicle Image
function setVehicleBackground(canvas, image) {
  if (!c_mainVehicle)
    return;

  /*if (image.indexOf("data:image/jpeg") < 0)
    resetVehicleCanvas();*/

  //Test if Wheels Already Loaded
  if (group_wvF) {
    // Remove Previous Objects since layers may be different
    c_mainVehicle.remove(group_wvF);
    c_mainVehicle.remove(group_wvR);

    group_wvF = null;
    group_wvR = null;
  }
  if (img_vc) {
    c_mainVehicle.remove(img_vc);
    img_vc = null;
  }

  canvas.overlayImage = null;
  fabric.Image.fromURL(image, function (img) {
      vehicleStageScale = $mainVehicleStage.width() / img.width;
      vehicleWheelScale = $mainVehicleStage.width() / baseVehicleWidth;
      setVehicleStageScale(img.width, img.height);

      //img.set({ width: canvas.width, height: canvas.height, originX: 'left', originY: 'top' });

    canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
        width: canvas.width,
        height: canvas.height,
        // Needed to position backgroundImage at 0/0
        originX: 'left',
        originY: 'top',
        crossOrigin: 'anonymous'
    });
  });



  

  if (image == '') {
    canvas.backgroundImage = false;
    $("#vtws-mainImage").css('visibility', 'hidden');
    $("#vtws-mainActions").hide();
    $("#vtws-product-grid").html("");
    $("#vtws-product-grid").hide();
  }
  else {
    $("#vtws-mainImage").css('visibility', 'visible');
    $("#vtws-mainActions").show();
    //slickWheels();
  }

  $("#vtws-customize-wheel").show();

  flipWheelToggle = false;
}
// Vehicle Color Image 
function setVehicleColor(canvas, image) {
  canvas.setOverlayImage(image, canvas.renderAll.bind(canvas), {
    width: canvas.width,
    height: canvas.height
  });
  return;

  fabric.Image.fromURL(image, function (img) {
    img_vc = img.set({
      //opacity: paintLayerAlpha,
      //visible: true,
      //width: canvas.width,
      //height: canvas.height,
      selectable: false
    });
    canvas.add(img);
  });
}

// TEST POSITION OF WHEELS	
function updatePositions(wheel) {
  // Front Wheel
  wheel.wvF_left = group_wvF.left;
  wheel.wvF_top = group_wvF.top;
  wheel.wvF_width = group_wvF.getWidth();
  wheel.wvF_height = group_wvF.getHeight();
  wheel.wvF_angle = group_wvF.getAngle();

  // Rear Wheel
  wheel.wvR_left = group_wvR.left;
  wheel.wvR_top = group_wvR.top;
  wheel.wvR_width = group_wvR.getWidth();
  wheel.wvR_height = group_wvR.getHeight();
  wheel.wvR_angle = group_wvR.getAngle();
}

// LOAD WHEELS	
function scaleWheel(wheel, additionalScaling) {
  if (additionalScaling === undefined) {
      additionalScaling = 1;
  }

  var newScale = vehicleWheelScale;

  //Scaling
  wheel.wvF_left = newScale * additionalScaling * wheel.wvF_left;
  wheel.wvF_top = newScale * additionalScaling * wheel.wvF_top;
  wheel.wvF_width = newScale * additionalScaling * wheel.wvF_width;
  wheel.wvF_height = newScale * additionalScaling * wheel.wvF_height;

  wheel.wvR_left = newScale * additionalScaling * wheel.wvR_left;
  wheel.wvR_top = newScale * additionalScaling * wheel.wvR_top;
  wheel.wvR_width = newScale * additionalScaling * wheel.wvR_width;
  wheel.wvR_height = newScale * additionalScaling * wheel.wvR_height;
}

function loadWheels(wheel) {
    //wheelColorSet = false;
    var sizeRatio = sizeUpRatio;
    if (sizeRatio > 1) {
        resetSizeRatio();
    } 

    //Scaling
    scaleWheel(wheel);    

    //Test if Wheels Already Loaded
  if (group_wvF) {
    // Remove Previous Objects since layers may be different
    c_mainVehicle.remove(group_wvF);
    c_mainVehicle.remove(group_wvR);

      // Test Position
    updatePositions(wheel);
  }

  //Set Layer Array (used to load group for customizable wheels)
  var wheelLayersFront = [];
  var wheelLayersRear = [];

    // Load Front Wheel
  fabric.Image.fromURL(wheel.frontWheelBase, function (img) {
    wvF_main = img.set({
        id: 'main', visible: true, width: wheel.wvF_width, height: wheel.wvF_height
    });
    wheelLayersFront.push(wvF_main);

      // Load Front Wheel Window
    if (wheel.frontWheelWindow) {
        fabric.Image.fromURL(wheel.frontWheelWindow, function (img) {
            wvF_window = img.set({
              id: 'window', visible: wheelColorSet, opacity: paintLayerAlpha, left: -wheel.wvF_width / 2, top: -wheel.wvF_height / 2, width: wheel.wvF_width, height: wheel.wvF_height
            });
            wheelLayersFront.push(wvF_window);
            wvfWheelGroup["window"] = wvF_window;
            if (wheel.frontWheelWindow && wheelColorSet) {
                var colorFace = wheelColorMap["window"];
                changeLayerColor("window", colorFace);
            }
      });
    }

      // Load Front Wheel Face
    if (wheel.frontWheelFace) {
        fabric.Image.fromURL(wheel.frontWheelFace, function (img) {
            wvF_face = img.set({
                id: 'face',
                visible: wheelColorSet,
                opacity: paintLayerAlpha,
                left: -wheel.wvF_width / 2,
                top: -wheel.wvF_height / 2,
                width: wheel.wvF_width,
                height: wheel.wvF_height,                
            });
            wheelLayersFront.push(wvF_face);
            wvfWheelGroup["face"] = wvF_face;
            if (wheel.frontWheelFace && wheelColorSet) {
                var colorFace = wheelColorMap["face"];
                changeLayerColor("face", colorFace);
            }
      });
    }

    group_wvF = new fabric.Group(wheelLayersFront, {
      left: wheel.wvF_left,
      top: wheel.wvF_top,
      angle: wheel.wvF_angle,
      cornerSize: handleSize,
      cornerColor: handleColor,
      borderColor: handleColor
    });

    if (wheel.wvF_flipX === true) {
        flipWheelToggle = true;
    }

    if (flipWheelToggle) {
        group_wvF.toggle('flipX');
    }

    if (sizeRatio > 1) {        
        var frontW = group_wvF.getWidth() * sizeRatio;
        var frontH = group_wvF.getHeight() * sizeRatio;
        var frontDiffW = (frontW - (frontW * sizeRatio)) / 2;
        var frontX = group_wvF.getLeft();
        var frontDiffH = (frontH - (frontH * sizeRatio)) / 2;
        var frontY = group_wvF.getTop();
        group_wvF.set({ scaleX: sizeRatio, scaleY: sizeRatio, left: frontX + frontDiffW, top: frontY + frontDiffH }).setCoords();
        sizeUpRatio = sizeRatio;
      }

    c_mainVehicle.add(group_wvF);

    setBrightness(wvF_main, $brightnessSliderInput.val());
    setContrast(wvF_main, $contrastSliderInput.val());    
  });

  // Load Rear Wheel
  fabric.Image.fromURL(wheel.rearWheelBase, function (img) {
    wvR_main = img.set({
      id: 'main', visible: true, width: wheel.wvR_width, height: wheel.wvR_height
    });
    wheelLayersRear.push(wvR_main);

      // Load Window    
    if (wheel.rearWheelWindow) {
        fabric.Image.fromURL(wheel.rearWheelWindow, function (img) {
            wvR_window = img.set({
              id: 'window', visible: wheelColorSet, opacity: paintLayerAlpha, left: -wheel.wvR_width / 2, top: -wheel.wvR_height / 2, width: wheel.wvR_width, height: wheel.wvR_height
            });
            wheelLayersRear.push(wvR_window);
            wvrWheelGroup["window"] = wvR_window;
            if (wheel.rearWheelWindow && wheelColorSet) {
                var colorFace = wheelColorMap["window"];
                changeLayerColor("window", colorFace);
            }
      });
    }

      // Load Face
    if (wheel.rearWheelFace) {
        fabric.Image.fromURL(wheel.rearWheelFace, function (img) {
            wvR_face = img.set({
              id: 'face', visible: wheelColorSet, opacity: paintLayerAlpha, left: -wheel.wvR_width / 2, top: -wheel.wvR_height / 2, width: wheel.wvR_width, height: wheel.wvR_height
            });
            wheelLayersRear.push(wvR_face);
            wvrWheelGroup["face"] = wvR_face;
            if (wheel.rearWheelFace && wheelColorSet) {
                var colorFace = wheelColorMap["face"];
                changeLayerColor("face", colorFace);
            }
      });
    }

    group_wvR = new fabric.Group(wheelLayersRear, {
      left: wheel.wvR_left,
      top: wheel.wvR_top,
      angle: wheel.wvR_angle,
      cornerSize: handleSize,
      cornerColor: handleColor,
      borderColor: handleColor
    });

    if (wheel.wvR_flipX === true) {
        flipWheelToggle = true;
    }

    if (flipWheelToggle) {
        group_wvR.toggle('flipX');
    }

    if (sizeRatio > 1) {
        var rearDiffW = (group_wvR.getWidth() - (group_wvR.getWidth() * sizeRatio)) / 2;
        var rearX = group_wvR.getLeft();
        var rearDiffH = (group_wvR.getHeight() - (group_wvR.getHeight() * sizeRatio)) / 2;
        var rearY = group_wvR.getTop();
        group_wvR.set({ scaleX: sizeRatio, scaleY: sizeRatio, left: rearX + rearDiffW, top: rearY + rearDiffH }).setCoords();
        sizeUpRatio = sizeRatio;
    }
    

    c_mainVehicle.add(group_wvR);

    setBrightness(wvR_main, $brightnessSliderInput.val());
    setContrast(wvR_main, $contrastSliderInput.val());
    
  });

  
}

// COLOR LAYER
function colorLayer(canvas, target, color) {
  //var layer = canvas.item(id);
  //var layer = getObjectById(canvas,target);
  switch (color) {
    case ('null'):
      target.set({ visible: false });
      canvas.renderAll();
      break;
    default:
      target.set({ visible: true }); //.bringToFront();

      var colorize = new fabric.Image.filters.Blend({ color: color, mode: 'multiply' });
      target.filters[0] = colorize;

      setLayerOrder(); // to make sure layers are ordered correctly
      target.applyFilters(canvas.renderAll.bind(canvas));
      break;
  }
}

// HIDE LAYER
function hideLayer(canvas, id) {
  var layer = getObjectById(canvas, id);
  layer.set({ visible: false });
  canvas.renderAll();
}
function hideObject(layer) {
  layer.set({ visible: false });
}

// CHANGE COLOR
function changeLayerColor(layer, newColor) {
  var color = String(newColor);
  var wbTarget = getObjectById(c_mainWheel, layer);
 
  var layerName = layer.toLowerCase();
  console.log(layerName);

    
    var ftarget = wvfWheelGroup[layerName];
    var rtarget = wvrWheelGroup[layerName];

  /*var wvfTarget = eval("wvF_" + layer);
  var wvrTarget = eval("wvR_" + layer);*/

  console.log(ftarget);
  console.log(rtarget);
  console.log(layerName);
  /*console.log(wvfTarget);
  console.log(wvrTarget);*/

  colorLayer(c_mainWheel, wbTarget, color);
  /*colorLayer(c_mainVehicle, wvfTarget, color);
  colorLayer(c_mainVehicle, wvrTarget, color);*/
  
  if (ftarget !== undefined) {
      colorLayer(c_mainVehicle, ftarget, color);
  }
  if (rtarget !== undefined) {
      colorLayer(c_mainVehicle, rtarget, color);
  }
  
}

// SET LAYER ORDER
function setLayerOrder() {
  //var innerBarrel = getObjectById(c_mainWheel,'ib');
  var main = getObjectById(c_mainWheel, 'main');

  c_mainWheel.sendToBack(main);
  //c_mainWheel.sendToBack(innerBarrel);
}

function getObjectById(canvas, id) {
  var objsInCanvas = canvas.getObjects();
  for (var obj in objsInCanvas) {
    if (objsInCanvas[obj].get('id') === id) {
      return objsInCanvas[obj];
    }
  }
}

// IMAGE FILTERS -------------------- 
function setBrightness(target, thisValue) {
  // Check target existis before doing anything
  if (target) {
    wheelBrightness = thisValue;

    var brightFilter = new fabric.Image.filters.Brightness({ brightness: thisValue / 4 });

    // Apply to Front Wheels
    if (target.filters) {
      target.filters[1] = brightFilter;
      target.applyFilters(c_mainVehicle.renderAll.bind(c_mainVehicle));
    }
  }
}

function setContrast(target, thisValue) {
    // Check target existis before doing anything
  if (target) {
      wheelContrast = thisValue;

    var contrastFilter = new fabric.Image.filters.Contrast({ contrast: thisValue / 4 });

    // Apply to Front Wheels
    if (target.filters) {
      target.filters[2] = contrastFilter;
      target.applyFilters(c_mainVehicle.renderAll.bind(c_mainVehicle));
    }
  }
}

// BRIGHTNESS 
$editBrightness.click(function () { $brightnessSlider.slideToggle('fast'); });

$brightnessSliderInput.change(function () {
  setBrightness(wvF_main, this.value);
  setBrightness(wvR_main, this.value);
});

// CONTRAST 
$contrastSliderInput.change(function () {
  setContrast(wvF_main, this.value);
  setContrast(wvR_main, this.value);
});
// END IMAGE FILTERS -------------------- 

// Wheel Listings
function reslickWheels() {
  return;

  $('#vtws-product-grid').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: false,
    fade: false,
    autoplaySpeed: 6000,
    adaptiveHeight: false,
    responsive: [
      {
        breakpoint: 560,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ],
    dotsClass: 'slick-dots-numbers',
    lazyLoad: 'ondemand'
  });
}
function slickWheels() {
  $.get("/Product/Grid/" + autoSelectPartNumbers, function (data) {
    $("#vtws-product-grid").show();
    $("#vtws-product-grid").html(data);
    //reslickWheels();
  });
}
//END Wheel Listings

//Vehicle Images and Swatches

function showUploadVehicleForm(year, make, model) {    
    /*$("#vtws-vehicle-select-first-image").show();*/
    $("#vtws-vehicle-select-pick-an-image").addClass('vtws-hide');
    $('#vtws-vehicle-tips-section').removeClass('vtws-hide');
    $('#vtws-use-own-image-tips').addClass('vtws-hide');

    // commented out code to show modal dialog
    //$("#vtws-changeVehicle").click();    $("#vtws-vehicleSelect").show();
    $("#vtws-mainImage").addClass("vtws-hide");
};

function getYear() {
    return $("#vtws-vehicle-year").val();
};

function getMake() {
    return $("#vtws-vehicle-make").val();
};

function getModel() {
    return $("#vtws-vehicle-model").val();
};

function getBodyStyle() {
    return $("#vtws-vehicle-body-type").val();
};

function getSubModel() {
    return $("#vtws-vehicle-sub-model").val();
};

function getVehicleImage(pn, getWheelResults, apiId, imageFileName, imageId) {
    if (getWheelResults == null) {
        getWheelResults = true;
    }
    $.loadingSpinner();

    // make sure the scale is reset to the correct base size.
    setVehicleStageScaleToDefault();

    if (vtwsSettings && vtwsSettings.onVehicleChanging) {
        vtwsSettings.onVehicleChanging();
    }

    if (getWheelResults === true) {
        showWheelResults(false);
    }
    $("#vtws-vehicleSelect").hide();
    //$("#vtws-mainImage").addClass("vtws-hide");

    var y = getYear();
    var m = getMake();
    var mdl = getModel();
    var b = getBodyStyle();
    if (b == "BODY TYPE")
        b = "";
    var sm = getSubModel();
    if (sm == "SUB-MODEL")
        sm = "";
    var o = $("#vtws-changeOrientation");

    if (pn === null || pn === undefined)
        pn = "";

    var brand = $("#vtws-productBrand").val();

    $("#vtws-mainVehicle").data("partNumber", pn);

    vehicleList = null;    setVehicleBackground(c_mainVehicle, '');
    $("#vtws-swatches").hide();
    $("#vtws-secondaryVehicle").html("");
    $(".studioVehicleList").html("");
    $(".customStudioVehicleList").html("");
    $("#vtws-vehicle-select-pick-an-image").removeClass("vtws-hide");
    $("#vtws-vehicle-select-first-image").hide();
    //console.log("getVehicleImage = " + y + " " + m + " " + mdl + " " + b + " " + sm + " pn=" + pn);

    
    if (apiId === undefined) {
        apiId = null;
    }

    $.get(baseURL + "/Vehicle/GetVehicleImages/?y=" + y + "&m=" + m + "&mdl=" + mdl + "&b=" + b + "&sm=" + sm + "&o=" + o.data("orientationPrimary"),
        function (data) {
            //console.log(data);
            var vr = null;
            try {
                //FuelAPI Images
                vr = JSON.parse(data);

                console.log("chassisid = " + vr.chassisId);
                //console.log(vr);

                $("#vtws-mainVehicle").data("vehicleID", vr.id);

                var ch = $("#vtws-mainVehicle").data("chassisId");
                if (getWheelResults === true) {
                    if (ch !== vr.chassisId) {
                        clearWheelResults();
                        $("#vtws-mainVehicle").data("chassisId", vr.chassisId);
                        sortingFiltering = false;
                        renderWheelResults({brand: brand});
                    } else {
                        showWheelResults(true);
                    }
                }                
                
                if (vr.products != null && vr.products[0].productFormats[0].assets.length > 0) {
                    var index = renderSwatches(vr.products[0].productFormats[0].assets);

                    var shotCode = null;
                    var vUrl = null;
                    var pAssets = vr.products[0].productFormats[0].assets;

                    shotCode = pAssets[index].shotCode.code;
                    //console.log("shotCode = " + shotCode);

                    vUrl = pAssets[index].url;
                    //console.log("vUrl = " + vUrl);

                    lastVehicleId = vr.id;

                    vehicleImageId = vr.vehicleImageId;
                    if (apiId !== null && imageId !== null && imageId !== undefined) {
                        vr.apiId = apiId;
                        vUrl = imageFileName;
                        vehicleImageId = imageId;
                        vr.id = imageId;
                        clearSecondaryWheels();
                    }
                    
                    showVehicleLoadWheelPosition(vUrl, vr.id, shotCode, vr.products[0].productFormats[0].id, vr.apiId);

                    $.loadingSpinner();

                    if (vr.apiId === null || vr.apiId === undefined || vr.apiId !== 4) {
                        $.get(baseURL +
                                "/Vehicle/GetVehicleImages/?y=" +
                                y +
                                "&m=" +
                                m +
                                "&mdl=" +
                                mdl +
                                "&b=" +
                                b +
                                "&sm=" +
                                sm +
                                "&o=" +
                                o.data("orientationSecondary"),
                                function(data) {
                                    try {
                                        var vr2 = JSON.parse(data);

                                        if (vr2.products[0].productFormats[0].assets.length > 0) {
                                            var assets = vr2.products[0].productFormats[0].assets;
                                            $("#vtws-secondaryVehicle").show();
                                            // default
                                            var v2 = vr2.products[0].productFormats[0].assets[index];
                                            // search for shot code
                                            for (var idx = 0; idx < assets.length; ++idx) {
                                                if (shotCode === assets[idx].shotCode.code) {
                                                    v2 = assets[idx];
                                                    break;
                                                }
                                            }

                                            var image2 =
                                                formatImageUrl(v2.url,
                                                    vr.id,
                                                    v2.shotCode.code,
                                                    vr2.products[0].productFormats[0].id,
                                                    vr.apiId);

                                            $("#vtws-secondaryVehicle")
                                                .html("<img crossOrigin=\"anonymous\" src='" + image2 + "'>");
                                            loadSecondaryWheelPosition();
                                            if (vtwsSettings && vtwsSettings.onVehicleLoaded) {
                                                vtwsSettings.onVehicleLoaded();
                                            }
                                        }
                                    } catch (ex) {
                                        console.log(ex);
                                    }
                                })
                            .fail(function() {
                                console.log("failed get - /Vehicle/GetVehicleImages/ - orientationSecondary");
                            })
                            .always(function() {
                                $.removeLoadingSpinner();
                            });
                    } else {
                        if (vtwsSettings && vtwsSettings.onVehicleLoaded) {
                            vtwsSettings.onVehicleLoaded();
                        }
                        $.removeLoadingSpinner();
                    }
                } else {
                    console.log("No vehicle found...");
                }

                //Iconfigurators Images
                //console.log("Iconfigurators " + data);
                //var vr = JSON.parse(data);
                //console.log("Iconfigurators : vr " + vr.Results);
                /*if (vr.SQLResult != undefined && vr.SQLResultSet != null && vr.Result > 0) {
                    var v = vr.vehicles[0];
                    console.log("Iconfigurators : vr.Result " + v.baseImage);
                    chooseVehicle(v, pn);

                    var vehicleList = vr.vehicles;
                    $.each(vr.vehicles, function (index, v) {
                        $(".studioVehicleList").append('<div class="vehicleThumb"><a data-vehicle-id="' + index + '"><img class="chooseVehicle" src="' + v.baseImage + '" alt=""><span>' + v.vehicleImageName + '</span></a></div>');
                    });
                }*/
            }
            catch (ex) {
                clearWheelResults();
                console.log(ex);
            }
            if (!vr || vr.products == null || vr.products[0].productFormats[0].assets.length == 0) {
                //clearWheelResults();
                showUploadVehicleForm(y, m, mdl);
            }
        }).fail(function () {            
            clearWheelResults();
            showUploadVehicleForm(y, m, mdl);            
        })
        .always(function () {
            $.removeLoadingSpinner();
        });
};


//FuelAPI Images
function formatImageUrl(url, vid, code, formatId, apiId) {
    if (apiId === null || apiId === undefined) {
        return baseURL + "/Vehicle/GetCroppedImage?url=" + url + "&vid=" + vid + "&code=" + code + "&fid=" + formatId;
    } else {
        return baseURL + "/Vehicle/GetCroppedImage?url=" + url + "&vid=" + vid + "&code=" + code + "&fid=" + formatId + "&apiId=" + apiId;
        //return url;
    }    
};

function showVehicle(url, vid, code, formatId, apiId) {
    //console.log(vid + " ,code=" + code + " ,format=" + formatId + " ,apiId=" + apiId);
    var imageUrl = formatImageUrl(url, vid, code, formatId, apiId);  
    $(".vsForm-image img").attr("src", imageUrl);
    setVehicleBackground(c_mainVehicle, imageUrl);
};

function displayCustomizeWheel(wheelId, partNumber, showModal) {
    
    $.get(baseURL + "/Wheel/GetLayers?wheelId=" + wheelId + "&partNumber=" + partNumber + "&token=" + vtwsToken,
       function (data) {
           //console.log(data);

           if (data.Error == null) {
               var wheelSection = $("#vtws-customize-wheel-section");
               wheelSection.find('.wheel-brand').html(data.Brand);
               wheelSection.find('.wheel-name').html(data.Style);               
               // Add wheel and layers to paintable modal

               var wMain;
               loadWheelLayer(c_mainWheel, wMain, data.BaseLayerImage, true, 'main');
               /*if (data.WheelLayers.length >= 2) {
                   o_wheel1.frontWheelWindow = data.WheelLayers[0].FrontLayerImage;
                   o_wheel1.rearWheelWindow = data.WheelLayers[0].RearLayerImage;
                   var wWindow;
                   loadWheelLayer(c_mainWheel, wWindow, data.WheelLayers[0].BaseLayerImage, false, 'window');

                   o_wheel1.frontWheelFace = data.WheelLayers[1].FrontLayerImage;
                   o_wheel1.rearWheelFace = data.WheelLayers[1].RearLayerImage;
                   var wFace;
                   loadWheelLayer(c_mainWheel, wFace, data.WheelLayers[1].BaseLayerImage, false, 'face');
               }*/

               // Update color pickers for layers
               var builderButtons = wheelSection.find(".builder_buttons");
               if (builderButtons.length > 0) {
                   console.log(data.WheelLayers);
                   builderButtons.html("");
                   for (var i = 0; i < data.WheelLayers.length; ++i) {
                       var layer = data.WheelLayers[i];
                       var targetLayer;
                       loadWheelLayer(c_mainWheel, targetLayer, data.WheelLayers[i].BaseLayerImage, false, layer.LevelName);
                       builderButtons.append('<div class="wbLayer" data-layer="' + layer.LevelName + '"><label class="small">' + layer.LevelName + '</label><input type="text" class="colorpicker" /></div>');
                   }

                   applyColorPicker();
               }

               //applyColorPicker();

               vtwsSelectWheel(partNumber);

               if (showModal) {
                   $(this).openLeanModal('#vtws-paintWheel', { top: 100, overlay: 0.5, closeButton: ".vtws-close-button" });
               } else {
                   wheelSection.find('.vtws-modal-title').html("");
               }
           } else {
               if (data.Error != null) {
                   console.log("Sorry, we couldn't find any layers that match your wheel " + wheelId);
               }
               console.log(data.Error);
           }

       }).fail(function () {
           console.log("failed to get wheel layers");
       });
};

function addCustomizeWheelEvent(element, wheelId) {    
    element.find("a.customize-btn").click(function (e) {
        e.preventDefault();
        var vs = $(this);
        var partnumber = vs.data("vtwsWheelPartNumber");
        displayCustomizeWheel(wheelId, partnumber, true);
    });
}

function addSpecsClickEvent(element, wheelId, chassis) {
    element.find("a.view-specs-btn").click(function (e) {
        e.preventDefault();
        //$(".vtws-productBrand").html("SOME BRAND");
        //$(".vtws-productSpecs").find(".vtws-productBrand").html("Look Up Brand here");

        $.get(baseURL + "/Wheel/GetWheel?cid=" + chassis + "&wheelId=" + wheelId + "&token=" + vtwsToken,
        function (data) {

            if (data.Error == null) {
                var detailsTemplate = $("#wheelDetailsModal");

                if (detailsTemplate.length > 0) {
                    detailsTemplate.find(".brandName").html(data.Brand);
                    detailsTemplate.find(".vtws-details-style").html(data.Style);
                    detailsTemplate.find(".vtws-details-finish").html(data.Finish);
                    detailsTemplate.find("#vtws-wheelThumb-img").attr('src', data.Image);
                    var body = detailsTemplate.find('#vtws-wheelspecs-body');
                    body.html("");
                    var table = "";
                    for (var i = 0; i < data.Products.length; ++i) {
                        var product = data.Products[i];
                        if (product.FitsChassis) {
                            table += '<tr class="vtws-specFits">';
                        } else {
                            table += "<tr>";
                        }

                        table += '<td><span class="vtws-sku">' + product.PartNumber + "</span></td>";
                        table += '<td><span>' + product.Size + "</span></td>";
                        table += '<td><span>' + product.BoltPattern + "</span></td>";
                        table += '<td><span>' + product.Offset + "</span></td>";

                        var price = product.Msrp;
                        if (price != null) {
                            table += '<td><span class="vtws-price">$' + price + "</span>";
                            table += '<span class="vtws-ea">/ea.</span></td>';
                        } else {
                            table += '<td>&nbsp;</td>';
                        }

                        table += "</tr>";
                    }
                    body.html(table);
                    //detailsTemplate.find("#vtws-wheelThumb").html("<img width=\"300\" height=\"300\" src=" + data.Image + " alt=\"" + data.Brand + " " + data.PartNumber + " \" />");

                    $(this).openLeanModal('#wheelDetailsModal', { top: 100, overlay: 0.5, closeButton: ".vtws-close-button" });
                }
                
            } else {
                if (data.Error != null) {
                    console.log("Sorry, we couldn't find any products that match your search using chassis " + chassis + " and wheel " + wheelId);
                    //outerTemplate.html("<div class=\"vtws-error-msg\">Sorry, we couldn't find any products that match your search<div>");
                }
                console.log(data.Error);
            }

        }).fail(function () {
            console.log("failed to get wheel specs");
        });
    });
};

function addWheelClickEvent(element, wheelId, chassis) {
    element.find("a.vtws-wheel-selector").click(function (e) {
        e.preventDefault();
        var vs = $(this);
        vtwsSelectWheel(vs.data("vtwsWheelPartNumber"));
    });

    element.find("a.vtws-previewWheelBtn").click(function (e) {
        e.preventDefault();
        var vs = $(this);
        vtwsSelectWheel(vs.data("vtwsWheelPartNumber"));
    });    
};

function specModalCallback(e, modalId) {
    if (modalId == '#wheelDetailsModal') {
        //console.info(modalId);
        return false;
    }
    return true;
};

function redirectPost(location, args)
{
    var form = $('<form></form>');
    form.attr("method", "post");
    form.attr("action", location);

    $.each( args, function( key, value ) {
        var field = $('<input></input>');

        field.attr("type", "hidden");
        field.attr("name", key);
        field.attr("value", value);

        form.append(field);
    });
    $(form).appendTo('body').submit();
};


function addQuoteClickEvent(element, url, params) {
    element.find(".vtws-size-btn").click(function (e) {
        e.preventDefault();
        redirectPost(url, params);
    });
};

function buildWheelFromTemplate(result, chassis, enableQuote, tireWebBaseUrl, y, m, mdl, bs, sm) {
    //
    var outerTemplate = $("#vtws-productGrid");
    var template = outerTemplate.find(".vtws-productTile").clone();
    if (result.Customizable) {
        template.addClass("customize");
    }
    template.find(".vtws-productName").html(result.ProductName);
    template.find(".vtws-productBrand").html(result.Brand);
    template.find(".vtws-productFinish").html(result.Finish);
    if (result.Msrp != null && result.Msrp !== 0) {
        template.find(".vtws-price").html('$' + result.Msrp);
    } else {
        template.find(".vtws-productInfo").addClass("vtws-hide");
    }

    var wheelSelector = template.find(".vtws-wheel-selector");

    if (result.FrontImage == null || result.RearImage == null) {
        template.find(".vtws-previewWheelBtn").addClass("vtws-hide");
        wheelSelector.removeAttr('data-vtws-wheel-part-number');
        wheelSelector.removeAttr('href');
    } else {
        template.find(".vtws-previewWheelBtn").attr('data-vtws-wheel-part-number', result.PartNumber);
        wheelSelector.attr('data-vtws-wheel-part-number', result.PartNumber);
        addWheelClickEvent(template, result.WheelId, chassis);
    }

    addSpecsClickEvent(template, result.WheelId, chassis);

    if (result.Customizable) {
        var customizeBtnSection = template.find(".vtws-view-customize");
        customizeBtnSection.removeClass("vtws-hide");
        customizeBtnSection.find(".customize-btn").attr('data-vtws-wheel-part-number', result.PartNumber);        
        addCustomizeWheelEvent(customizeBtnSection, result.WheelId);
    }
           
    wheelSelector.html("<img width=\"150\" height=\"150\" src=" + result.Image + " alt=\"" + result.Brand + " " + result.PartNumber + " \" />");
    // Add sizes to template
    var sizeOptions = template.find('.vtws-sizeOptions');
    var listItem = sizeOptions.find('.vtws-size-list-item').clone();

    sizeOptions.html("");
    for (var i = 0; i < result.Sizes.length; i++) {
        var size = result.Sizes[i];
        var sizeItem = listItem.clone();
        sizeItem.find('.vtws-size-text').html(size);
        // Quote enabled?
        if (enableQuote) {
            sizeItem.find('.vtws-size-btn').removeClass('vtws-hide');
            addQuoteClickEvent(sizeItem, tireWebBaseUrl, {
                v_year: y, v_make: m, v_model: mdl, v_bodystyle: bs, v_submodel: sm, w_partnumber: result.PartNumber, w_brand: result.Brand,
                w_style: result.ProductName, w_finish: result.Finish, w_size: size, w_price: result.Msrp, w_image: result.Image
            });
        }
        sizeOptions.append(sizeItem);
    }
    
    return template;
};

function addSortEvent(element) {
    element.find("a.vtws-sort-link").click(function (e) {
        e.preventDefault();
        sortingFiltering = true;
        var vs = $(this);
        var direction = vs.data("vtwsSortDirection");
        var sortOptions = { field: vs.data("vtwsSort"), direction: direction };
        if (vtwsSettings && vtwsSettings.onSortClicked) {
            vtwsSettings.onSortClicked(sortOptions);
        }
        renderWheelResults(currentFilter, sortOptions);
    });    
};

function addFilterEvent(element) {
    var brands = element.find('.filter-brands');
    var diameters = element.find('.filter-wheelSize');

    brands.find("a.vtws-filter-link").click(function (e) {
        e.preventDefault();
        sortingFiltering = true;
        var vs = $(this);
        var filter = vs.data("vtwsFilter");
        currentFilter.brand = filter;
        if (vtwsSettings && vtwsSettings.onFilterClicked) {
            vtwsSettings.onFilterClicked(currentFilter);
        }
        renderWheelResults(currentFilter);
    });

    diameters.find("a.vtws-filter-link").click(function (e) {
        e.preventDefault();
        sortingFiltering = true;
        var vs = $(this);
        var filter = vs.data("vtwsFilter");
        currentFilter.diameter = filter;
        if (vtwsSettings && vtwsSettings.onFilterClicked) {
            vtwsSettings.onFilterClicked(currentFilter);
        }
        renderWheelResults(currentFilter);
    });
};

function buildSortsFilters(results) {
    var sortsFilters = $('#vtws-product-filters');
    var template = sortsFilters.clone();

    var brandFilterLinkTemplate = sortsFilters.find('.filter-brands').find('.vtws-filter-link');
    var filterOptionsTemplate = sortsFilters.find('.filter-brands').find('.vtws-filter-option');

    var diameterFilterLinkTemplate = sortsFilters.find('.filter-wheelSize').find('.vtws-filter-link');
    var diameterOptionsTemplate = sortsFilters.find('.filter-wheelSize').find('.vtws-filter-option');

    var filterBody1 = template.find('.filter-brands').find('.vtws-filter-list');
    var filterBody2 = template.find('.filter-wheelSize').find('.vtws-filter-list');

    // set titles
    if (currentFilter !== null && currentFilter.brand !== null) {
        template.find(".filter-brands").find(".vtws-filter-option-title").html(currentFilter.brand === '' ? "All Brands" : currentFilter.brand);
    }
    if (currentFilter !== null && currentFilter.diameter !== null) {
        template.find(".filter-wheelSize").find(".vtws-filter-option-title").html(currentFilter.diameter === '' ? "All Sizes" : currentFilter.diameter);
    }
    
    var brandMap = [];
    var brands = [];
    for (var b = 0; b < results.WheelResultsModels.length; b++) {
        var brand = results.WheelResultsModels[b].Brand;
        if (brandMap[brand] == null) {
            brandMap[brand] = { brand: brand, hits: 1 };
            brands.push(brand);
        } else {
            brandMap[brand].hits++;
        }                
    }

    // list all diameters in results as a filter
    for (var x = 0; x < results.Diameters.length; x++) {
        var dia = results.Diameters[x];
        var dt = diameterFilterLinkTemplate.clone();
        dt.html(dia + '"');
        dt.attr('data-vtws-filter', dia);
        var diaOptions = diameterOptionsTemplate.clone();
        diaOptions.html(dt);
        filterBody2.append(diaOptions);
    }

    // sort brands
    brands.sort();

    // list all brands in results as a filter
    for (var i = 0; i < brands.length; i++) {
        var br = brands[i];        
        var bt = brandFilterLinkTemplate.clone();
        bt.html(br + " (" + brandMap[br].hits + ")");
        bt.attr('data-vtws-filter', br);
        var filterOptions = filterOptionsTemplate.clone();
        filterOptions.html(bt);
        filterBody1.append(filterOptions);
    }

    addSortEvent(template);
    addFilterEvent(template);
    setupWheelFilters(template);
    return template;
};

function searchWheels(filters, chassis, sortOption) {
    var wheelResults = clearWheelResults();
    if (wheelResults == null) {
        return;
    }

    wheelResults.removeClass("vtws-hide");

    if (chassis == null) {
        $("#vtws-mainImage").addClass("vtws-hide");
    }

    var brand = filters.brand !== undefined && filters.brand !== null ? filters.brand : '';
    var diameterFilter = filters.diameter !== undefined && filters.diameter !== null ? '&diameter=' + filters.diameter : '&diameter=';
    var sortField = sortOption !== undefined && sortOption !== null ? sortOption.field : "ProductName";
    var sortDirection = sortOption !== undefined && sortOption !== null ? sortOption.direction : 0;

    $.get(baseURL + "/Vehicle/GetWheelResults?cid=" + chassis + "&brand=" + brand + diameterFilter +
        "&sortField=" + sortField + "&sortDirection=" + sortDirection + "&token=" + vtwsToken,
        function (data) {
            //console.log(data);
            if (data.Error == null) {
                var y = getYear();
                var m = getMake();
                var mdl = getModel();
                var b = getBodyStyle();
                if (b == "BODY TYPE")
                    b = "";
                var sm = getSubModel();
                if (sm == "SUB-MODEL")
                    sm = "";

                var outerTemplate = $("#vtws-productGrid");
                outerTemplate = outerTemplate.clone();
                outerTemplate.html("");

                // build sorts/filters
                if (sortingFiltering === false) {
                    wheelResults.append(buildSortsFilters(data));
                    currentWheelResults = data;
                    currentFilter = {};
                } else {
                    wheelResults.append(buildSortsFilters(currentWheelResults));
                }

                for (var i = 0; i < data.WheelResultsModels.length; i++) {
                    var result = data.WheelResultsModels[i];
                    var template = buildWheelFromTemplate(result, chassis, data.TireWeb, data.TireWebBaseUrl, y, m, mdl, b, sm);
                    outerTemplate.append(template);
                }
                if (data.WheelResultsModels.length === 0) {
                    console.log("Sorry, we couldn't find any products that match your search using chassis " + chassis + " and brand " + brand);
                    outerTemplate.html("<div class=\"vtws-error-msg\">Sorry, we couldn't find any products that match your search<div>");
                }
                wheelResults.append(outerTemplate);

                $(".vtws-customize-close-button").click(function (e) {
                    e.preventDefault();                    
                    $("#vtws-paintWheel").addClass("vtws-hide");
                });

                $("a[rel*=leanModal]").leanModal({ top: 100, overlay: 0.5, closeButton: ".modal_close", callback: specModalCallback });

                if (vtwsSettings && vtwsSettings.onProductsLoaded) {
                    resultHitCount = data.WheelResultsModels.length;
                    vtwsSettings.onProductsLoaded(resultHitCount);
                }                
                
            } else {
                console.log(data.Error);
            }

        }).fail(function () {
            console.log("failed to get wheel results from chassis");
        });
};

function renderWheelResults(filters, sortOption) {
    var ch = $("#vtws-mainVehicle").data("chassisId");
    if (ch == null || ch === 0) {
        clearWheelResults();
        return;
    }

    searchWheels(filters, ch, sortOption);
};


function showVehicleLoadWheelPosition(url, vid, code, formatId, apiId) {
  clientImageUpload = false;
  showVehicle(url, vid, code, formatId, apiId);
  loadWheelPosition();
};

function loadDefaultPosition() {
    var wheel = default_wheel_info;
    console.info(default_wheel_info);
    //if (w.FrontMainImage != null && w.RearMainImage != null) {
        //wheel.vehicleID = w.VehicleID;

        //console.log("loadWheelPosition - found part " + pn);

        /*wheel.frontWheelBase = baseURL + w.FrontMainImage;
        wheel.rearWheelBase = baseURL + w.RearMainImage;

        wheel.wvF_left = w.FrontLeft;
        wheel.wvF_top = w.FrontTop;
        wheel.wvF_width = w.FrontWidth;
        wheel.wvF_height = w.FrontHeight;
        wheel.wvF_angle = w.FrontAngle;

        wheel.wvR_left = w.RearLeft;
        wheel.wvR_top = w.RearTop;
        wheel.wvR_width = w.RearWidth;
        wheel.wvR_height = w.RearHeight;
        wheel.wvR_angle = w.RearAngle;*/

        loadWheels(wheel);
        $("#vtws-mainImage").removeClass("vtws-hide");
    //}
};

function loadWheelPosition() {
    if (clientImageUpload === undefined || clientImageUpload === null) {
        clientImageUpload = false;
    }
  var vid = $("#vtws-mainVehicle").data("vehicleID");
  var pn = $("#vtws-mainVehicle").data("partNumber");
  var o = $("#vtws-changeOrientation").data("orientationPrimary");

    var imageId = vehicleImageId;
    //console.log("imageid =" + imageId);

    if (imageId === undefined) {
        // Since this isn't a fuel image, use the custom image
        imageId = null;
    }

  var encodedPn = encodeURIComponent(pn);
  console.log(encodedPn);
  $.get(baseURL + "/Vehicle/GetWheelPosition?vid=" + vid + "&o=" + o + "&pn=" + encodedPn + "&clientUpload=" + clientImageUpload + "&imageId=" + imageId,
      function (data) {
          var w = data;   
    if (w.VehicleID != null || w.VehicleImageId != null) {
        var wheel = o_wheel1;
      if (w.FrontMainImage != null && w.RearMainImage != null) {
          
          wheel.vehicleID = w.VehicleID;
          //console.log("loadWheelPosition - found part " + pn);

          wheel.frontWheelBase = baseURL + w.FrontMainImage;
          wheel.rearWheelBase = baseURL + w.RearMainImage;

          wheel.wvF_left = w.FrontLeft;
          wheel.wvF_top = w.FrontTop;
          wheel.wvF_width = w.FrontWidth;
          wheel.wvF_height = w.FrontHeight;
          wheel.wvF_angle = w.FrontAngle;
          wheel.wvF_flipX = w.FlipX;

          wheel.wvR_left = w.RearLeft;
          wheel.wvR_top = w.RearTop;
          wheel.wvR_width = w.RearWidth;
          wheel.wvR_height = w.RearHeight;
          wheel.wvR_angle = w.RearAngle;
          wheel.wvR_flipX = w.FlipX;

          loadWheels(wheel);
          $("#vtws-mainImage").removeClass("vtws-hide");
      } else {
          if (pn !== null && pn !== undefined && pn !== "") {
              //$("#vtws-mainImage").hide();
              //$("#vtws-mainImage").show();
              console.log("Part number not available for vehicle preview ");
          } else {
              console.log("Wheel not available for vehicle preview ");
              $("#vtws-mainImage").removeClass("vtws-hide");
          }
      }      
    }
    else {
        //$("#vtws-mainImage").hide();
        //loadDefaultPosition();
        console.log("Wheel not available for vehicle preview ");
    }     
  });
};

function clearSecondaryWheels() {
    var fsWheel = $('#secondary-wheel-front');
    if (fsWheel != null) {
        fsWheel.remove();
    }
    var rsWheel = $('#secondary-wheel-rear');
    if (rsWheel != null) {
        rsWheel.remove();
    }
} 

function loadSecondaryWheelPosition() {
  var vid = $("#vtws-mainVehicle").data("vehicleID");
  var pn = $("#vtws-mainVehicle").data("partNumber");
  var o = $("#vtws-changeOrientation").data("orientationSecondary");

    //console.log("loadSecondaryWheelPosition, vid=" + vid + " pn=" + pn + " o=" + o);
    // remove secondary wheels to ready for load of new wheels.
  clearSecondaryWheels();

  var imageId = vehicleImageId;
    //console.log("imageid =" + imageId);

  if (imageId === undefined) {
      // Since this isn't a fuel image, use the custom image
      imageId = null;
  }

  $.get(baseURL + "/Vehicle/GetWheelPosition?vid=" + vid + "&o=" + o + "&pn=" + pn + "&imageId=" + imageId, function (data) {
    var w = data;
    if (w.VehicleID != null) {
        if (w.FrontMainImage != null && w.RearMainImage != null) {
            wheel.wvF_left = w.FrontLeft;
            wheel.wvF_top = w.FrontTop;
            wheel.wvF_width = w.FrontWidth;
            wheel.wvF_height = w.FrontHeight;
            wheel.wvF_angle = w.FrontAngle;

            wheel.wvR_left = w.RearLeft;
            wheel.wvR_top = w.RearTop;
            wheel.wvR_width = w.RearWidth;
            wheel.wvR_height = w.RearHeight;
            wheel.wvR_angle = w.RearAngle;

            scaleWheel(wheel, 0.3);            

            $("#vtws-secondaryVehicle").prepend('<div id="secondary-wheel-front" style="position: absolute; left: ' + wheel.wvF_left + 'px; top : ' + wheel.wvF_top + 'px; width: ' + wheel.wvF_width + 'px; height: ' + wheel.wvF_height + 'px;"><img src="' + baseURL + w.FrontMainImage + '" alt="" style="width: 100%; height: 100%; transform: rotate(' + wheel.FrontAngle + 'deg); -ms-transform: rotate(' + wheel.FrontAngle + 'deg); -webkit-transform: rotate(' + wheel.FrontAngle + 'deg);"></div>');
            $("#vtws-secondaryVehicle").prepend('<div id="secondary-wheel-rear" style="position: absolute; left: ' + wheel.wvR_left + 'px; top : ' + wheel.wvR_top + 'px; width: ' + wheel.wvR_width + 'px; height: ' + wheel.wvR_height + 'px;"><img src="' + baseURL + w.RearMainImage + '" alt="" style="width: 100%; height: 100%; transform: rotate(' + wheel.FrontAngle + 'deg); -ms-transform: rotate(' + wheel.FrontAngle + 'deg); -webkit-transform: rotate(' + wheel.FrontAngle + 'deg);"></div>');
        } else {
            console.log("Part number not available for secondary vehicle preview ");
        }
    }
    else {
        console.log("Wheel not available for secondary vehicle preview");
    }
  });
};

function vtwsSelectWheel(partNumber) {
    //console.log("vtwsSelectWheel pn=" + partNumber + " vid=" + o_wheel1.vehicleID);
    if (partNumber === null || partNumber === undefined)
        partNumber = "";
    $("#vtws-mainVehicle").data("partNumber", partNumber);

    if (group_wvF && sizeUpRatio > 1) {
        // reloading wheel positions causes the sized wheels to double        
        c_mainVehicle.remove(group_wvF);
        c_mainVehicle.remove(group_wvR);

        group_wvF = null;
        group_wvR = null;
    }

    loadWheelPosition(/*o_wheel1.vehicleID, partNumber*/);
    loadSecondaryWheelPosition();
};

function renderSwatches(assets) {
    var currentColor = $("#vtws-swatches").data("color");

    //console.log("currentColor=" + currentColor);
  var selectedIndex = 0;

  $("#vtws-swatches").html('');
  var colors = "";
  $.each(assets, function (index, asset) {
    var color = asset.shotCode.color.rgb1;
    if (color && colors.indexOf(color + ",") < 0) {
      colors += color + ",";
      $("#vtws-swatches").append("<a href='#' title='"
        + asset.shotCode.color.oem_name
        + "' data-color='"
        + asset.shotCode.color.rgb1
        + "' data-vehicle-image-url='"
        + asset.url
        + "'><div class='vtws-colorSwatch' style='background-color: #"
        + asset.shotCode.color.rgb1
        + "'></div></a>");

      if (currentColor == color)
        selectedIndex = index;
    }
  });

  if (currentColor === null || currentColor === '' || currentColor === undefined) {
      var color = assets[selectedIndex].shotCode.color.rgb1;
      $("#vtws-swatches").data("color", color);
  }
  
  $("#vtws-swatches").show();

  return selectedIndex;
};

function loadSwatch(e) {
  e.preventDefault();

  var s = $(this);
  $("#vtws-swatches").data("color", s.data("color"));   
  getVehicleImage($("#vtws-mainVehicle").data("partNumber"));
  //showVehicleLoadWheelPosition(s.data("vehicleImageUrl"));
}
function changeOrientation() {
  var o = $("#vtws-changeOrientation");
  var p = o.data("orientationPrimary");
  var s = o.data("orientationSecondary");
  o.data("orientationPrimary", s);
  o.data("orientationSecondary", p);

  getVehicleImage($("#vtws-mainVehicle").data("partNumber"));
}

//Iconfigurators Images
//function chooseVehicle(v, pn) {
//  $(".vsForm-image img").attr("src", v.baseImage);
//  setVehicleBackground(c_mainVehicle, v.baseImage);
//  var w = {
//    frontWheelBase: baseURL + "/Vehicle/GetImage/?url=" + v.frontWheel.stockWheelImage,
//    rearWheelBase: baseURL + "/Vehicle/GetImage/?url=" + v.rearWheel.stockWheelImage,

//    wvF_left: v.frontWheel.wheelX,
//    wvF_top: v.frontWheel.wheelY,
//    wvF_width: 184.0 * v.frontWheel.scaleX / 100,
//    wvF_height: 205.0 * v.frontWheel.scaleY / 100,
//    wvF_angle: v.frontWheel.rotation,

//    wvR_left: v.rearWheel.wheelX,
//    wvR_top: v.rearWheel.wheelY,
//    wvR_width: 115.0 * v.rearWheel.scaleX / 100,
//    wvR_height: 196.0 * v.rearWheel.scaleY / 100,
//    wvR_angle: v.rearWheel.rotation,
//  };
//  loadWheels(w);
//  getSwatches(v.vehicleImageID);

//  if (pn != undefined)
//    window.setTimeout(function () { vtwsSelectWheel(pn); }, 500);
//}
//function getSwatches(vid) {
//  $("#vtws-swatches").html('');
//  $.get(baseURL + "/Vehicle/GetVehicleImageSwatches/?vid=" + vid, function (data) {
//    var vr = JSON.parse(data);
//    if (vr.Result > 0) {
//      //Auto-paint first swatch
//      setVehicleColor(c_mainVehicle, vr.colors[0].vehicleColorImage);

//      $.each(vr.colors, function (index, color) {
//        $("#vtws-swatches").append("<a href='#' data-vehicle-color-image='" + color.vehicleColorImage + "'><img class='vtws-colorSwatch' src='" + color.swatchImage + "'></a>")
//      });
//      $("#vtws-swatches").show();
//    }
//  });
//}
//function loadSwatch(e) {
//  e.preventDefault();
//  setVehicleColor(c_mainVehicle, $(this).data("vehicleColorImage"));
//}
//function vtwsSelectWheel(partNumber) {
//  if (partNumber == "VPS306") {
//    loadWheels(o_wheel1);
//    $("#vtws-customize-wheel").show();
//  }
//  else if (partNumber == "KM694" || partNumber == "km694") {
//    loadWheels(o_wheel2);
//    $("#vtws-customize-wheel").show();
//  }
//  else if (partNumber == "ANZA") {
//    loadWheels(o_wheel3);
//    $("#vtws-customize-wheel").show();
//  }
//  else if (partNumber == "MR305") {
//    loadWheels(o_wheel4);
//    $("#vtws-customize-wheel").show();
//  }
//}
//END Vehicle Images and Swatches

// Load Wheel Images
var defaultWheel = vwiDir + 'VPS-306';

var o_wheel1 = {
  vehicleID: 0,

  frontWheelBase: defaultWheel + '/layers/wvF_main.png',
  frontWheelFace: defaultWheel + '/layers/wvF_face.png',
  frontWheelWindow: defaultWheel + '/layers/wvF_window.png',

  rearWheelBase: defaultWheel + '/layers/wvR_main.png',
  rearWheelFace: defaultWheel + '/layers/wvR_face.png',
  rearWheelWindow: defaultWheel + '/layers/wvR_window.png',

  wvF_left: c_mainVehicle.width * 0.534,
  wvF_top: c_mainVehicle.height * 0.595,
  wvF_width: 150 * vehicleStageScale,
  wvF_height: 240 * vehicleStageScale,
  wvF_angle: 0.18,

  wvR_left: c_mainVehicle.width * 0.888,
  wvR_top: c_mainVehicle.height * 0.527,
  wvR_width: 69 * vehicleStageScale,
  wvR_height: 175 * vehicleStageScale,
  wvR_angle: 3.6
};

var default_wheel_info = {
    vehicleID: 0,

    frontWheelBase: defaultWheel + '/layers/wvF_main.png',
    frontWheelFace: defaultWheel + '/layers/wvF_face.png',
    frontWheelWindow: defaultWheel + '/layers/wvF_window.png',

    rearWheelBase: defaultWheel + '/layers/wvR_main.png',
    rearWheelFace: defaultWheel + '/layers/wvR_face.png',
    rearWheelWindow: defaultWheel + '/layers/wvR_window.png',

    wvF_left: c_mainVehicle.width * 0.534,
    wvF_top: c_mainVehicle.height * 0.595,
    wvF_width: 150 * vehicleStageScale,
    wvF_height: 240 * vehicleStageScale,
    wvF_angle: 0.18,

    wvR_left: c_mainVehicle.width * 0.888,
    wvR_top: c_mainVehicle.height * 0.527,
    wvR_width: 69 * vehicleStageScale,
    wvR_height: 175 * vehicleStageScale,
    wvR_angle: 3.6
};

/*
//Iconfigurators Images
var o_wheel2 = {
  frontWheelBase: vwiDir + 'KM694/km694_ov_front.png',
  rearWheelBase: vwiDir + 'KM694/km694_ov_rear.png',

  wvF_left: c_mainVehicle.width * 0.534,
  wvF_top: c_mainVehicle.height * 0.595,
  wvF_width: 150 * vehicleStageScale,
  wvF_height: 240 * vehicleStageScale,
  wvF_angle: 0.18,

  wvR_left: c_mainVehicle.width * 0.888,
  wvR_top: c_mainVehicle.height * 0.527,
  wvR_width: 69 * vehicleStageScale,
  wvR_height: 175 * vehicleStageScale,
  wvR_angle: 3.6
};

var o_wheel3 = {
  frontWheelBase: vwiDir + 'Anza-bronze/d583_ov_front.png',
  rearWheelBase: vwiDir + 'Anza-bronze/d583_ov_rear.png',

  wvF_left: c_mainVehicle.width * 0.534,
  wvF_top: c_mainVehicle.height * 0.595,
  wvF_width: 150 * vehicleStageScale,
  wvF_height: 240 * vehicleStageScale,
  wvF_angle: 0.18,

  wvR_left: c_mainVehicle.width * 0.888,
  wvR_top: c_mainVehicle.height * 0.527,
  wvR_width: 69 * vehicleStageScale,
  wvR_height: 175 * vehicleStageScale,
  wvR_angle: 3.6
};

var o_wheel4 = {
  frontWheelBase: vwiDir + 'MR-305/mr-305_ov_front.png',
  rearWheelBase: vwiDir + 'MR-305/mr-305_ov_rear.png',

  wvF_left: c_mainVehicle.width * 0.534,
  wvF_top: c_mainVehicle.height * 0.595,
  wvF_width: 150 * vehicleStageScale,
  wvF_height: 240 * vehicleStageScale,
  wvF_angle: 0.18,

  wvR_left: c_mainVehicle.width * 0.888,
  wvR_top: c_mainVehicle.height * 0.527,
  wvR_width: 69 * vehicleStageScale,
  wvR_height: 175 * vehicleStageScale,
  wvR_angle: 3.6
};
*/

var setVehicleStageScale = function (startWidth, startHeight) {
    c_mainVehicle.setWidth(startWidth * vehicleStageScale);
    c_mainVehicle.setHeight(startHeight * vehicleStageScale);
};

var setVehicleStageScaleToDefault = function () {
    vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;
    vehicleWheelScale = vehicleStageScale;
    setVehicleStageScale(baseVehicleWidth, baseVehicleHeight);
};

function loadWheelLayer(canvas, target, imageSrc, opacity, name) {
    fabric.Image.fromURL(imageSrc, function (img) {
        target = img.set({
            id: name,
            opacity: paintLayerAlpha,
            visible: opacity,
            width: c_mainWheel.width,
            height: c_mainWheel.height,
            selectable: false,
            crossOrigin: "Anonymous"
        });
        canvas.add(target);
        //canvas.setActiveObject(target);
        if (name === 'ib' || name === 'main') {
            canvas.sendToBack(target);
        } else {
            canvas.bringToFront(target);
        }
        canvas.renderAll();
        //setLayerOrder();
    });
};

$(function () {
    "use strict";

    console.log("studio_v2d.js Startup");

  if (!c_mainVehicle)
    return;

  // INITIATE VEHICLE CANVAS ============
    setVehicleStageScaleToDefault();

  // Set Default Background Image
  setVehicleBackground(c_mainVehicle, '');

  //Load Default Wheels
  //loadWheels(o_wheel1);

  // ScrollTo 
  var scrollTarget = 'stage'; // stage, top
  function scrollTo(target) {
    $("html,body").animate({ scrollTop: $(target).offset().top });
    return false;
  }
  // UPDATE WHEEL FROM PAGE
  //var $previewWheelBtn = $('.previewWheelBtn');
  $(document).on("click", ".previewWheelBtn", function () {
    var thisID = $(this).data('wheelid');

    // Move page to scrollTarget
    if (scrollTarget === 'top') {
      //scrollTo('body');
    } else {
      //scrollTo('#vtws');
    }
    // USED FOR TESTING ONLY -- EVAL() SHOULD BE REPLACED!--------
    //loadWheels(eval(thisID));
  });

  // INITIATE WHEEL CANVAS FOR CUSTOM WHEELS ============
  // If Wheel Canvas is present
  if ($mainWheelleStage.length) {
    c_mainWheel = new fabric.Canvas('w');
    c_mainWheel.setHeight(277).setWidth(300);
    console.log($mainWheelleStage.height() + "X" + $mainWheelleStage.width());
    //$(c_mainWheel).css("width", "100%");
    //c_mainWheel.setHeight($mainWheelleStage.height()).setWidth($mainWheelleStage.width());    
  }

  //Iconfigurators   
  

  // CONTROLS ========================
  // Flip X
  $('#vtws-flipx').click(function () {
    // Angles
    var wvF_angle = group_wvF.getAngle();
    var wvR_angle = group_wvR.getAngle();

    // Left
    var width = c_mainVehicle.getWidth();
    var wvF_left = group_wvF.getLeft();
    var wvF_newLeft = width - wvF_left;

    var wvR_left = group_wvR.getLeft();
    var wvR_newLeft = width - wvR_left;
    //console.log(wvR_left + " " + width + " " +wvR_newLeft);

    group_wvF.toggle('flipX').set({ angle: -wvF_angle, left: wvF_newLeft }).setCoords();
    group_wvR.toggle('flipX').set({ angle: -wvR_angle, left: wvR_newLeft }).setCoords();
    c_mainVehicle.renderAll();

    flipWheelToggle = !flipWheelToggle;
  });

    // USER UPLOAD OWN VEHICLE IMAGE
  $('#vtws-vehicleSelect .vtws-close-button').click(function () {
      $("#vtws-vehicleSelect").hide();
      $("#vtws-mainImage").removeClass("vtws-hide");
  });


  function addThumnailClickEvent(element) {
      element.find("a.vtws-vehicle-list-selector").click(function (e) {
          e.preventDefault();
          var vs = $(this);
          var apiId = vs.data("vtwsVehicleImageApiid");
          if (apiId === null || apiId === undefined || apiId !== 4) {
              vtwsSelectVehicle(vs.data("vtwsVehicleYear"), vs.data("vtwsVehicleMake"), vs.data("vtwsVehicleModel"), vs.data("vtwsVehicleBodyStyle"), vs.data("vtwsVehicleSubModel"), null, false, null, false);
          } else {
              prepareVehicleArea();
              getVehicleImage(null, false, vs.data("vtwsVehicleImageApiid"), vs.data("vtwsVehicleImageFileName"), vs.data("vtwsVehicleImageId"));
          }
          
      });
      
  };

  function buildVehicleThumbnailFromTemplate(result) {
      //
      var outerTemplate = $("#vtws-vehicleThumbTiles");

      var template = outerTemplate.find(".vtws-vehicle-list-image-section").clone();

      var vehicleSelector = template.find(".vtws-vehicle-list-selector");
      vehicleSelector.attr('data-vtws-vehicle-year', result.Year);
      vehicleSelector.attr('data-vtws-vehicle-make', result.Make);
      vehicleSelector.attr('data-vtws-vehicle-model', result.Model);
      vehicleSelector.attr('data-vtws-vehicle-body-style', result.BodyStyle);
      vehicleSelector.attr('data-vtws-vehicle-sub-model', result.SubModel);
      vehicleSelector.attr('data-vtws-vehicle-image-id', result.VehicleImageID);
      vehicleSelector.attr('data-vtws-vehicle-image-file-name', result.FileName);
      vehicleSelector.attr('data-vtws-vehicle-image-apiid', result.ApiId);

      var vehicleImage = template.find(".vtws-vehicle-list-image");
      vehicleImage.attr('src', result.Thumbnail);
      vehicleImage.attr('alt', result.Year + " " + result.Make + " " + result.Model + " " + result.BodyStyle + " " + result.SubModel );

      //VehicleImageID
      addThumnailClickEvent(template);
     
      return template;
  };

  $('#vtws-changeVehicle').click(function () {           
      var y = $("#vtws-vehicle-year").val();
      var m = $("#vtws-vehicle-make").val();
      var mdl = $("#vtws-vehicle-model").val();

      $('#vtws-vehicle-tips-section').addClass('vtws-hide');
      $('#vtws-use-own-image-tips').removeClass('vtws-hide');

      var studioPhotosSection = $("#vtws-studio-photos");
      var locationPhotosSection = $("#vtws-location-shoots");
      if (!studioPhotosSection.hasClass("vtws-hide")) {
          studioPhotosSection.addClass("vtws-hide");
      }
      if (!locationPhotosSection.hasClass("vtws-hide")) {
          locationPhotosSection.addClass("vtws-hide");
      }            

      var vehicleResults = $(".studioVehicleList");
      vehicleResults.html("");
      var customVehicleResults = $(".customStudioVehicleList");
      customVehicleResults.html("");

      $.get(baseURL + "/Vehicle/GetVehicleThumbs?y=" + y + "&m=" + m + "&mdl=" + mdl,
        function (data) {           
            if (data.Error == null) {
                var outerTemplate = $("#vtws-vehicleThumbTiles");
                outerTemplate = outerTemplate.clone();
                outerTemplate.html("");

                var outerTemplate2 = $("#vtws-vehicleThumbTiles");
                outerTemplate2 = outerTemplate2.clone();
                outerTemplate2.html("");

                var studioPhotos = false;
                var locationPhotos = false;

                for (var i = 0; i < data.length; i++) {
                    var result = data[i];
                    var template = buildVehicleThumbnailFromTemplate(result);
                    if (result.ApiId === null || result.ApiId === undefined || result.ApiId === 1) {                        
                        outerTemplate.append(template);
                        studioPhotos = true;
                    } else {
                        outerTemplate2.append(template);
                        locationPhotos = true;
                    }
                }

                if (studioPhotos) {
                    vehicleResults.html(outerTemplate);
                    studioPhotosSection.removeClass("vtws-hide");
                }
                if (locationPhotos) {
                    customVehicleResults.html(outerTemplate2);
                    locationPhotosSection.removeClass("vtws-hide");
                }
            } else {
                console.log(data.Error);
            }
            $("#vtws-vehicleSelect").show();
            $("#vtws-mainImage").addClass("vtws-hide");
        }).fail(function () {
            console.log("failed to thumbnails for year, make model");
        });      
  });

 
 // Upload your own image.
  if ($uploadVehicle) {
    $uploadVehicle.onchange = function handleImage(e) {
      $("#vtws-swatches").hide();

      var reader = new FileReader();
      reader.onload = function (event) {
        resetVehicleCanvas();
        var image = new Image();
        image.src = event.target.result;
        image.onload = function () {
            $("#vtws-secondaryVehicle").hide();            
            vehicleStageScale = $mainVehicleStage.width() / this.width;
            vehicleWheelScale = $mainVehicleStage.width() / baseVehicleWidth;
            setVehicleStageScale(this.width, this.height);
            setVehicleBackground(c_mainVehicle, image.src);
            clientImageUpload = true;
            loadWheelPosition();
        };
      };
      reader.readAsDataURL(e.target.files[0]);

      $uploadVehicle.value = '';
      $('#vtws-vehicleSelect .vtws-close-button').click();

        // Toggle Steps
        //$('#step1').foundation('toggle');
        //$('#step2').foundation('toggle');
    };
  }

  // Tour
  /*
  var startTour = function () {
    $('#vtws-tour').joyride({
      autoStart: true,
      modal: false,
      expose: true,
      'tipAnimation': 'pop',
      //'timer': 3000,
      'startTimerOnClick': false
    });
  }
  */
  //$('.lauchTour').click(startTour);
  //startTour();

  // Upload Tips
  /*
  $('#uploadTips').slick({
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    autoplay: true,
    fade: false,
    autoplaySpeed: 4000,
    adaptiveHeight: false,
    dotsClass: 'slick-dots slick-dots-block',
    lazyLoad: 'ondemand'
  });*/

  //Swatches
  $(document).on("click", "#vtws-swatches a", loadSwatch);

  //Secondary Image
  $("#vtws-secondaryVehicle").click(changeOrientation);

  /*
  //Iconfigurators Images
  //Vehicle Image and Swatches
  //$(document).on("click", "#vtws-swatches a", loadSwatch);

  $(document).on("click", ".studioVehicleList a", function (e) {
    e.preventDefault();
    var vid = $(this).data("vehicleId");
    if (vehicleList && vid >= 0) {
      chooseVehicle(vehicleList[vid]);
    }
    $('#vtws-vehicleSelect .vtws-close-button').click();
  });
  */

  $("button[rel*=leanModal]").leanModal({ closeButton: ".vtws-close-button" });
});

(function () {
    // Retain count concept: http://stackoverflow.com/a/2420247/260665
    // Callers should make sure that for every invocation of loadingSpinner method there has to be an equivalent invocation of removeLoadingSpinner
    var retainCount = 0;

    // http://stackoverflow.com/a/13992290/260665 difference between $.fn.extend and $.extend
    $.extend({
        loadingSpinner: function () {
            // add the overlay with loading image to the page           
            if (0 === retainCount) {
                var over = '<div id="custom-loading-overlay" class="loading spin-modal"></div>';
                $(over).appendTo('body');
            }
            retainCount++;
        },
        removeLoadingSpinner: function () {
            retainCount--;
            if (retainCount <= 0) {
                $('#custom-loading-overlay').remove();               
                retainCount = 0;
            }
        }
    });

    
}());