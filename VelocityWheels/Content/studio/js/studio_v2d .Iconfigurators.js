//$(document).foundation();

// OBJECTS
var $mainVehicleStage = $('#vtws-mainVehicle');
var $mainWheelleStage = $('#vtws-mainWheel');
var $uploadVehicle = document.getElementById('imgLoader');
//var $wheelGrid = $('#studioWheelGrid');

var $editBrightness = $('#vtws-brightness');
var $brightnessSlider = $('#vtws-brightnessSlider');
var $brightnessSliderInput = $('#vtws-brightnessSliderInput');
var $contrastSliderInput = $('#vtws-contrastSliderInput');

// Front Wheel Group
var wvF_main;
var wvF_window;
var wvF_face;

// Rear Wheel Group
var wvR_main;
var wvR_window;
var wvR_face;

// SETTINGS
var baseVehicleWidth = '960' //'1440';
var baseVehicleHeight = '420' //'900';
var vwiDir = '/Content/studio/wheels/';
var vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;

var handleSize = 15;
var handleColor = '#0f0';
var paintLayerAlpha = 1;
var wheelColorSet = false;
var wheelColorLayer;
var wheelColor;
var flipWheelToggle = false;

// INITIATE VEHICLE CANVAS ============
var c_mainVehicle = null;
if ($("#v").length > 0)
  c_mainVehicle = new fabric.Canvas('v');
var c_mainWheel;
var img_vc;
var group_wvF;
var group_wvR;

function resetVehicleCanvas() {
  vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;
  if (c_mainVehicle) {
    c_mainVehicle.setWidth(baseVehicleWidth * vehicleStageScale);
    c_mainVehicle.setHeight(baseVehicleHeight * vehicleStageScale);
  }
}
// Main Vehicle Image
function setVehicleBackground(canvas, image) {
  if (!c_mainVehicle)
    return;

  //resetVehicleCanvas();
  if (image.indexOf("data:image/jpeg") < 0)
    resetVehicleCanvas();

  //Test if Wheels Already Loaded
  if (group_wvF) {
    // Remove Previous Objects since layers may be different
    c_mainVehicle.remove(group_wvF);
    c_mainVehicle.remove(group_wvR);

    group_wvF = null;
    group_wvR = null;
  }
  if (img_vc) {
    c_mainVehicle.remove(img_vc);
    img_vc = null;
  }

  canvas.overlayImage = null;
  canvas.setBackgroundImage(image, canvas.renderAll.bind(canvas), {
    width: canvas.width,
    height: canvas.height,
    // Needed to position backgroundImage at 0/0
    originX: 'left',
    originY: 'top'
  });
  if (image == '') {
    $("#vtws-mainImage").css('visibility', 'hidden');
    $("#vtws-mainActions").hide();
    $("#vtws-product-grid").html("");
    $("#vtws-product-grid").hide();
  }
  else {
    $("#vtws-mainImage").css('visibility', 'visible');
    $("#vtws-mainActions").show();
    //slickWheels();
  }
  $("#vtws-customize-wheel").hide();

  flipWheelToggle = false;
}
// Vehicle Color Image 
function setVehicleColor(canvas, image) {
  canvas.setOverlayImage(image, canvas.renderAll.bind(canvas), {
    width: canvas.width,
    height: canvas.height
  });
  return;

  fabric.Image.fromURL(image, function (img) {
    img_vc = img.set({
      //opacity: paintLayerAlpha,
      //visible: true,
      //width: canvas.width,
      //height: canvas.height,
      selectable: false
    });
    canvas.add(img);
  });
}

// TEST POSITION OF WHEELS	
function updatePositions(wheel) {
  // Front Wheel
  wheel.wvF_left = group_wvF.left;
  wheel.wvF_top = group_wvF.top;
  wheel.wvF_width = group_wvF.getWidth();
  wheel.wvF_height = group_wvF.getHeight();
  wheel.wvF_angle = group_wvF.getAngle();

  // Rear Wheel
  wheel.wvR_left = group_wvR.left;
  wheel.wvR_top = group_wvR.top;
  wheel.wvR_width = group_wvR.getWidth();
  wheel.wvR_height = group_wvR.getHeight();
  wheel.wvR_angle = group_wvR.getAngle();
}

// LOAD WHEELS	
function loadWheels(wheel) {
  wheelColorSet = false;

  //Scaling
  wheel.wvF_left = vehicleStageScale * wheel.wvF_left;
  wheel.wvF_top = vehicleStageScale * wheel.wvF_top;
  wheel.wvF_width = vehicleStageScale * wheel.wvF_width;
  wheel.wvF_height = vehicleStageScale * wheel.wvF_height;

  wheel.wvR_left = vehicleStageScale * wheel.wvR_left;
  wheel.wvR_top = vehicleStageScale * wheel.wvR_top;
  wheel.wvR_width = vehicleStageScale * wheel.wvR_width;
  wheel.wvR_height = vehicleStageScale * wheel.wvR_height;

  //Test if Wheels Already Loaded
  if (group_wvF) {
    // Remove Previous Objects since layers may be different
    c_mainVehicle.remove(group_wvF);
    c_mainVehicle.remove(group_wvR);

    // Test Position
    updatePositions(wheel);
  }

  //Set Layer Array (used to load group for customizable wheels)
  var wheelLayersFront = [];
  var wheelLayersRear = [];

  // Load Front Wheel
  fabric.Image.fromURL(wheel.frontWheelBase, function (img) {
    wvF_main = img.set({
      id: 'main', visible: true, width: wheel.wvF_width, height: wheel.wvF_height
    });
    wheelLayersFront.push(wvF_main);

    // Load Window
    if (wheel.frontWheelWindow) {
      fabric.Image.fromURL(wheel.frontWheelWindow, function (img) {
        wvF_window = img.set({
          id: 'window', visible: wheelColorSet, opacity: paintLayerAlpha, left: -wheel.wvF_width / 2, top: -wheel.wvF_height / 2, width: wheel.wvF_width, height: wheel.wvF_height
        });
        wheelLayersFront.push(wvF_window);
      });
    }

    // Load Face
    if (wheel.frontWheelFace) {
      fabric.Image.fromURL(wheel.frontWheelFace, function (img) {
        wvF_face = img.set({
          id: 'face', visible: wheelColorSet, opacity: paintLayerAlpha, left: -wheel.wvF_width / 2, top: -wheel.wvF_height / 2, width: wheel.wvF_width, height: wheel.wvF_height
        });
        wheelLayersFront.push(wvF_face);
      });
    }

    group_wvF = new fabric.Group(wheelLayersFront, {
      left: wheel.wvF_left,
      top: wheel.wvF_top,
      angle: wheel.wvF_angle,
      cornerSize: handleSize,
      cornerColor: handleColor,
      borderColor: handleColor
    });

    if (flipWheelToggle)
      group_wvF.toggle('flipX');

    c_mainVehicle.add(group_wvF);

    setBrightness(wvF_main, $brightnessSliderInput.val());
    setContrast(wvF_main, $contrastSliderInput.val());
  });

  // Load Rear Wheel
  fabric.Image.fromURL(wheel.rearWheelBase, function (img) {
    wvR_main = img.set({
      id: 'main', visible: true, width: wheel.wvR_width, height: wheel.wvR_height
    });
    wheelLayersRear.push(wvR_main);

    // Load Window
    if (wheel.rearWheelWindow) {
      fabric.Image.fromURL(wheel.rearWheelWindow, function (img) {
        wvR_window = img.set({
          id: 'window', visible: wheelColorSet, opacity: paintLayerAlpha, left: -wheel.wvR_width / 2, top: -wheel.wvR_height / 2, width: wheel.wvR_width, height: wheel.wvR_height
        });
        wheelLayersRear.push(wvR_window);
      });
    }

    // Load Face
    if (wheel.rearWheelFace) {
      fabric.Image.fromURL(wheel.rearWheelFace, function (img) {
        wvR_face = img.set({
          id: 'face', visible: wheelColorSet, opacity: paintLayerAlpha, left: -wheel.wvR_width / 2, top: -wheel.wvR_height / 2, width: wheel.wvR_width, height: wheel.wvR_height
        });
        wheelLayersRear.push(wvR_face);
      });
    }

    group_wvR = new fabric.Group(wheelLayersRear, {
      left: wheel.wvR_left,
      top: wheel.wvR_top,
      angle: wheel.wvR_angle,
      cornerSize: handleSize,
      cornerColor: handleColor,
      borderColor: handleColor
    });

    if (flipWheelToggle)
      group_wvR.toggle('flipX');

    c_mainVehicle.add(group_wvR);

    setBrightness(wvR_main, $brightnessSliderInput.val());
    setContrast(wvR_main, $contrastSliderInput.val());

    //if (wheel.rearWheelFace && wheelColorSet)
    //changeLayerColor(wheelColorLayer, wheelColor);
  });
}

// COLOR LAYER
function colorLayer(canvas, target, color) {
  //var layer = canvas.item(id);
  //var layer = getObjectById(canvas,target);
  switch (color) {
    case ('null'):
      target.set({ visible: false });
      canvas.renderAll();
      break;
    default:
      target.set({ visible: true }); //.bringToFront();

      var colorize = new fabric.Image.filters.Blend({ color: color, mode: 'overlay' });
      target.filters[0] = colorize;

      setLayerOrder(); // to make sure layers are ordered correctly
      target.applyFilters(canvas.renderAll.bind(canvas));
      break;
  }
}

// HIDE LAYER
function hideLayer(canvas, id) {
  var layer = getObjectById(canvas, id);
  layer.set({ visible: false });
  canvas.renderAll();
}
function hideObject(layer) {
  layer.set({ visible: false });
}

// CHANGE COLOR
function changeLayerColor(layer, newColor) {
  var color = String(newColor);
  var wbTarget = getObjectById(c_mainWheel, layer);
  var wvfTarget = eval("wvF_" + layer);
  var wvrTarget = eval("wvR_" + layer);

  colorLayer(c_mainWheel, wbTarget, color);
  colorLayer(c_mainVehicle, wvfTarget, color);
  colorLayer(c_mainVehicle, wvrTarget, color);
}

// SET LAYER ORDER
function setLayerOrder() {
  //var innerBarrel = getObjectById(c_mainWheel,'ib');
  var main = getObjectById(c_mainWheel, 'main');

  c_mainWheel.sendToBack(main);
  //c_mainWheel.sendToBack(innerBarrel);
}

function getObjectById(canvas, id) {
  var objsInCanvas = canvas.getObjects();
  for (var obj in objsInCanvas) {
    if (objsInCanvas[obj].get('id') === id) {
      return objsInCanvas[obj];
    }
  }
}

// IMAGE FILTERS -------------------- 
function setBrightness(target, thisValue) {
  // Check target existis before doing anything
  if (target) {
    wheelBrightness = thisValue;

    var brightFilter = new fabric.Image.filters.Brightness({ brightness: thisValue / 4 });

    // Apply to Front Wheels
    if (target.filters) {
      target.filters[1] = brightFilter;
      target.applyFilters(c_mainVehicle.renderAll.bind(c_mainVehicle));
    }
  }
}

function setContrast(target, thisValue) {
  // Check target existis before doing anything
  if (target) {
    wheelContrast = thisValue;

    var contrastFilter = new fabric.Image.filters.Contrast({ contrast: thisValue / 4 });

    // Apply to Front Wheels
    if (target.filters) {
      target.filters[2] = contrastFilter;
      target.applyFilters(c_mainVehicle.renderAll.bind(c_mainVehicle));
    }
  }
}

// BRIGHTNESS 
$editBrightness.click(function () { $brightnessSlider.slideToggle('fast'); });

$brightnessSliderInput.change(function () {
  setBrightness(wvF_main, this.value);
  setBrightness(wvR_main, this.value);
});

// CONTRAST 
$contrastSliderInput.change(function () {
  console.log("Contrast: " + this.value);
  setContrast(wvF_main, this.value);
  setContrast(wvR_main, this.value);
});
// END IMAGE FILTERS -------------------- 

// Wheel Listings
function reslickWheels() {
  return;

  $('#vtws-product-grid').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: false,
    fade: false,
    autoplaySpeed: 6000,
    adaptiveHeight: false,
    responsive: [
      {
        breakpoint: 560,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ],
    dotsClass: 'slick-dots-numbers',
    lazyLoad: 'ondemand'
  });
}
function slickWheels() {
  $.get("/Product/Grid/" + autoSelectPartNumbers, function (data) {
    $("#vtws-product-grid").show();
    $("#vtws-product-grid").html(data);
    //reslickWheels();
  });
}
//END Wheel Listings

//Vehicle Images and Swatches
function getVehicleImage(pn) {
  var y = $("#vtws-vehicle-year").val();
  var m = $("#vtws-vehicle-make").val();
  var mdl = $("#vtws-vehicle-model").val();
  var b = $("#vtws-vehicle-body-type").val();
  if (b == "BODY TYPE")
    b = "";
  var sm = $("#vtws-vehicle-sub-model").val();
  if (sm == "SUB-MODEL")
    sm = "";

  vehicleList = null;
  setVehicleBackground(c_mainVehicle, '')
  $("#vtws-swatches").hide();
  $(".studioVehicleList").html("");
  $("#vtws-vehicle-select-pick-an-image").show();
  $("#vtws-vehicle-select-first-image").hide();

  $.get("http://apps.velocity-tech.com/Vehicle/GetVehicleImages/?y=" + y + "&m=" + m + "&mdl=" + mdl + "&b=" + b + "&sm=" + sm, function (data) {
    var vr = null;
    try {
      var vr = JSON.parse(data);
      if (vr.Result > 0) {
        var v = vr.vehicles[0];
        chooseVehicle(v, pn);

        vehicleList = vr.vehicles;
        $.each(vr.vehicles, function (index, v) {
          $(".studioVehicleList").append('<div class="vehicleThumb"><a data-vehicle-id="' + index + '"><img class="chooseVehicle" src="' + v.baseImage + '" alt=""><span>' + v.vehicleImageName + '</span></a></div>');
        })
      }
    }
    catch (ex) { }
    if (!vr || vr.Result == 0) {
      $("#vtws-vehicle-select-first-image").show();
      $("#vtws-vehicle-select-pick-an-image").hide();
      $("#vehicle-select-first-image h3 strong").html("Be the first one to upload an image for your " + y + " " + m + " " + mdl);
      $("#vtws-changeVehicle").click();
    }
  });
}
function chooseVehicle(v, pn) {
  $(".vsForm-image img").attr("src", v.baseImage);
  setVehicleBackground(c_mainVehicle, v.baseImage);
  var w = {
    frontWheelBase: "http://apps.velocity-tech.com/Vehicle/GetImage/?url=" + v.frontWheel.stockWheelImage,
    rearWheelBase: "http://apps.velocity-tech.com/Vehicle/GetImage/?url=" + v.rearWheel.stockWheelImage,

    wvF_left: v.frontWheel.wheelX,
    wvF_top: v.frontWheel.wheelY,
    wvF_width: 184.0 * v.frontWheel.scaleX / 100,
    wvF_height: 205.0 * v.frontWheel.scaleY / 100,
    wvF_angle: v.frontWheel.rotation,

    wvR_left: v.rearWheel.wheelX,
    wvR_top: v.rearWheel.wheelY,
    wvR_width: 115.0 * v.rearWheel.scaleX / 100,
    wvR_height: 196.0 * v.rearWheel.scaleY / 100,
    wvR_angle: v.rearWheel.rotation,
  };
  loadWheels(w);
  getSwatches(v.vehicleImageID);

  if (pn != undefined)
    window.setTimeout(function () { vtwsSelectWheel(pn); }, 500);
}
function getSwatches(vid) {
  $("#vtws-swatches").html('');
  $.get("http://apps.velocity-tech.com/Vehicle/GetVehicleImageSwatches/?vid=" + vid, function (data) {
    var vr = JSON.parse(data);
    if (vr.Result > 0) {
      //Auto-paint first swatch
      setVehicleColor(c_mainVehicle, vr.colors[0].vehicleColorImage);

      $.each(vr.colors, function (index, color) {
        $("#vtws-swatches").append("<a href='#' data-vehicle-color-image='" + color.vehicleColorImage + "'><img class='vtws-colorSwatch' src='" + color.swatchImage + "'></a>")
      });
      $("#vtws-swatches").show();
    }
  });
}
function showSwatch(e) {
  e.preventDefault();
  setVehicleColor(c_mainVehicle, $(this).data("vehicleColorImage"));
}
function vtwsSelectWheel(partNumber) {
  if (partNumber == "VPS306") {
    loadWheels(o_wheel1);
    $("#vtws-customize-wheel").show();
  }
  else if (partNumber == "KM694" || partNumber == "km694") {
    loadWheels(o_wheel2);
    $("#vtws-customize-wheel").show();
  }
  else if (partNumber == "ANZA") {
    loadWheels(o_wheel3);
    $("#vtws-customize-wheel").show();
  }
  else if (partNumber == "MR305") {
    loadWheels(o_wheel4);
    $("#vtws-customize-wheel").show();
  }
}
//END Vehicle Images and Swatches

// Load Wheel Images
var defaultWheel = vwiDir + 'VPS-306';

var o_wheel1 = {
  frontWheelBase: defaultWheel + '/layers/wvF_main.png',
  frontWheelFace: defaultWheel + '/layers/wvF_face.png',
  frontWheelWindow: defaultWheel + '/layers/wvF_window.png',

  rearWheelBase: defaultWheel + '/layers/wvR_main.png',
  rearWheelFace: defaultWheel + '/layers/wvR_face.png',
  rearWheelWindow: defaultWheel + '/layers/wvR_window.png',

  wvF_left: c_mainVehicle.width * 0.534,
  wvF_top: c_mainVehicle.height * 0.595,
  wvF_width: 150 * vehicleStageScale,
  wvF_height: 240 * vehicleStageScale,
  wvF_angle: 0.18,

  wvR_left: c_mainVehicle.width * 0.888,
  wvR_top: c_mainVehicle.height * 0.527,
  wvR_width: 69 * vehicleStageScale,
  wvR_height: 175 * vehicleStageScale,
  wvR_angle: 3.6
};

var o_wheel2 = {
  frontWheelBase: vwiDir + 'KM694/km694_ov_front.png',
  rearWheelBase: vwiDir + 'KM694/km694_ov_rear.png',

  wvF_left: c_mainVehicle.width * 0.534,
  wvF_top: c_mainVehicle.height * 0.595,
  wvF_width: 150 * vehicleStageScale,
  wvF_height: 240 * vehicleStageScale,
  wvF_angle: 0.18,

  wvR_left: c_mainVehicle.width * 0.888,
  wvR_top: c_mainVehicle.height * 0.527,
  wvR_width: 69 * vehicleStageScale,
  wvR_height: 175 * vehicleStageScale,
  wvR_angle: 3.6
};

var o_wheel3 = {
  frontWheelBase: vwiDir + 'Anza-bronze/d583_ov_front.png',
  rearWheelBase: vwiDir + 'Anza-bronze/d583_ov_rear.png',

  wvF_left: c_mainVehicle.width * 0.534,
  wvF_top: c_mainVehicle.height * 0.595,
  wvF_width: 150 * vehicleStageScale,
  wvF_height: 240 * vehicleStageScale,
  wvF_angle: 0.18,

  wvR_left: c_mainVehicle.width * 0.888,
  wvR_top: c_mainVehicle.height * 0.527,
  wvR_width: 69 * vehicleStageScale,
  wvR_height: 175 * vehicleStageScale,
  wvR_angle: 3.6
};

var o_wheel4 = {
  frontWheelBase: vwiDir + 'MR-305/mr-305_ov_front.png',
  rearWheelBase: vwiDir + 'MR-305/mr-305_ov_rear.png',

  wvF_left: c_mainVehicle.width * 0.534,
  wvF_top: c_mainVehicle.height * 0.595,
  wvF_width: 150 * vehicleStageScale,
  wvF_height: 240 * vehicleStageScale,
  wvF_angle: 0.18,

  wvR_left: c_mainVehicle.width * 0.888,
  wvR_top: c_mainVehicle.height * 0.527,
  wvR_width: 69 * vehicleStageScale,
  wvR_height: 175 * vehicleStageScale,
  wvR_angle: 3.6
};

$(function () {
  "use strict";

  if (!c_mainVehicle)
    return;

  //console.log( $mainVehicleStage.width() );
  var vehicleStageScale = $mainVehicleStage.width() / baseVehicleWidth;
  var setVehicleStageScale = function (startWidth, startHeight) {
    c_mainVehicle.setWidth(startWidth * vehicleStageScale);
    c_mainVehicle.setHeight(startHeight * vehicleStageScale);
  };

  // INITIATE VEHICLE CANVAS ============
  c_mainVehicle.setWidth(baseVehicleWidth * vehicleStageScale);
  c_mainVehicle.setHeight(baseVehicleHeight * vehicleStageScale);

  // Set Default Background Image
  setVehicleBackground(c_mainVehicle, '');

  //Load Default Wheels
  //loadWheels(o_wheel1);

  // ScrollTo 
  var scrollTarget = 'stage'; // stage, top
  function scrollTo(target) {
    $("html,body").animate({ scrollTop: $(target).offset().top });
    return false;
  }
  // UPDATE WHEEL FROM PAGE
  //var $previewWheelBtn = $('.previewWheelBtn');
  $(document).on("click", ".previewWheelBtn", function () {
    var thisID = $(this).data('wheelid');

    // Move page to scrollTarget
    if (scrollTarget === 'top') {
      //scrollTo('body');
    } else {
      //scrollTo('#vtws');
    }
    // USED FOR TESTING ONLY -- EVAL() SHOULD BE REPLACED!--------
    loadWheels(eval(thisID));
  });

  // INITIATE WHEEL CANVAS FOR CUSTOM WHEELS ============
  // If Wheel Canvas is present
  if ($mainWheelleStage.length) {
    c_mainWheel = new fabric.Canvas('w');
    c_mainWheel.setHeight(250).setWidth(250);
    //c_mainWheel.setHeight($mainWheelleStage.width()).setWidth($mainWheelleStage.width());

    // Load Wheels
    function loadWheelLayer(canvas, target, imageSrc, opacity, name) {
      fabric.Image.fromURL(defaultWheel + '/layers/' + imageSrc, function (img) {
        target = img.set({
          id: name,
          opacity: paintLayerAlpha,
          visible: opacity,
          width: c_mainWheel.width,
          height: c_mainWheel.height,
          selectable: false,
          crossOrigin: "Anonymous"
        });
        canvas.add(target);
        //canvas.setActiveObject(target);
        if (name === 'ib' || name === 'main') {
          canvas.sendToBack(target);
        } else {
          canvas.bringToFront(target);
        }
        canvas.renderAll();
        //setLayerOrder();
      });
    }
    // Wheel Layers
    //var w_ib;
    //loadWheelLayer(c_mainWheel, w_ib, 'Inner-Barrel_base.png', true, 'ib');
    var w_main;
    loadWheelLayer(c_mainWheel, w_main, 'main.png', true, 'main');
    var w_window;
    loadWheelLayer(c_mainWheel, w_window, 'window.png', false, 'window');
    var w_face;
    loadWheelLayer(c_mainWheel, w_face, 'face.png', false, 'face');
  }

  // COLOR PICKER
  $(".colorpicker").spectrum({
    flat: false,
    allowEmpty: true,
    showInput: false,
    chooseText: "Paint It",
    cancelText: "Cancel",
    showInitial: false,
    showPalette: true,
    showSelectionPalette: true,
    replacerClassName: 'pickerBox',
    maxPaletteSize: 10,
    preferredFormat: "hex",
    localStorageKey: "cp.wheel",
    show: function () {
    },
    change: function () {
    },
    hide: function (newColor) {
      // Action
      wheelColorSet = true;
      wheelColorLayer = String($(this).parent().data('layer'));
      wheelColor = newColor;
      changeLayerColor(wheelColorLayer, wheelColor);
    },
    palette: [
       ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
       "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)",
       "rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
       "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"]
    ]
  });


  // CONTROLS ========================
  // Flip X
  $('#vtws-flipx').click(function () {
    // Angles
    var wvF_angle = group_wvF.getAngle();
    var wvR_angle = group_wvR.getAngle();

    // Left
    var width = c_mainVehicle.getWidth();
    var wvF_left = group_wvF.getLeft();
    var wvF_newLeft = width - wvF_left;

    var wvR_left = group_wvR.getLeft();
    var wvR_newLeft = width - wvR_left;
    //console.log(wvR_left + " " + width + " " +wvR_newLeft);

    group_wvF.toggle('flipX').set({ angle: -wvF_angle, left: wvF_newLeft }).setCoords();
    group_wvR.toggle('flipX').set({ angle: -wvR_angle, left: wvR_newLeft }).setCoords();
    c_mainVehicle.renderAll();

    flipWheelToggle = !flipWheelToggle;
  });

  // USER UPLOAD OWN VEHICLE IMAGE
  if ($uploadVehicle) {
    $uploadVehicle.onchange = function handleImage(e) {
      $("#vtws-swatches").hide();

      resetVehicleCanvas();

      var reader = new FileReader();
      reader.onload = function (event) {
        var image = new Image();
        image.src = event.target.result;
        image.onload = function () {
          vehicleStageScale = $mainVehicleStage.width() / this.width;
          setVehicleStageScale(this.width, this.height);
          setVehicleBackground(c_mainVehicle, image.src);
        };
      };
      reader.readAsDataURL(e.target.files[0]);

      $uploadVehicle.value = '';
      $('#vtws-vehicleSelect .vtws-close-button').click();

      // Toggle Steps
      //$('#step1').foundation('toggle');
      //$('#step2').foundation('toggle');
    };
  }

  // Tour
  /*
  var startTour = function () {
    $('#vtws-tour').joyride({
      autoStart: true,
      modal: false,
      expose: true,
      'tipAnimation': 'pop',
      //'timer': 3000,
      'startTimerOnClick': false
    });
  }
  */
  //$('.lauchTour').click(startTour);
  //startTour();

  // Upload Tips
  /*
  $('#uploadTips').slick({
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    autoplay: true,
    fade: false,
    autoplaySpeed: 4000,
    adaptiveHeight: false,
    dotsClass: 'slick-dots slick-dots-block',
    lazyLoad: 'ondemand'
  });*/

  //Vehicle Image and Swatches
  $(document).on("click", "#vtws-swatches a", showSwatch);

  $(document).on("click", ".studioVehicleList a", function (e) {
    e.preventDefault();
    var vid = $(this).data("vehicleId");
    if (vehicleList && vid >= 0) {
      chooseVehicle(vehicleList[vid]);
    }
    $('#vtws-vehicleSelect .vtws-close-button').click();
  });

  $("button[rel*=leanModal]").leanModal({ closeButton: ".vtws-close-button" });
});

