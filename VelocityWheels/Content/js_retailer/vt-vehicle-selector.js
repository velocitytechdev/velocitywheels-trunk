﻿var autoSelect = false;
var autoSelectYear = 2017;
var autoSelectMake = 'Ford';
var autoSelectModel = 'Mustang';
var autoSelectBodyType = 'Coupe';
var autoSelectSubModel = 'V6';
var vehicleList = null;

function resetYears() {
  var s = $("#vtws-vehicle-year");
  s.html('<option>YEAR</option>');
  s.val('YEAR');
  s.prop('disabled', 'disabled');

  setVehicleBackground(c_mainVehicle, '');
}
function resetMakes() {
  var s = $("#vtws-vehicle-make");
  s.html('<option>MAKE</option>');
  s.val('MAKE');
  s.prop('disabled', 'disabled');
}
function resetModels() {
  var s = $("#vtws-vehicle-model");
  s.html('<option>MODEL</option>');
  s.val('MODEL');
  s.prop('disabled', 'disabled');
}
function resetBodyTypes() {
  var s = $("#vtws-vehicle-body-style");
  s.html('<option>BODY TYPE</option>');
  s.val('BODY TYPE');
  s.prop('disabled', 'disabled');
}
function resetSubModels() {
  var s = $("#vtws-vehicle-sub-model");
  s.html('<option>SUB-MODEL</option>');
  s.val('SUB-MODEL');
  s.prop('disabled', 'disabled');
}
function getYears() {
  resetYears();
  resetMakes();
  resetModels();
  resetBodyTypes();
  resetSubModels();
  $.get("/Vehicle/GetYears", function (data) {
    var s = $("#vtws-vehicle-year");
    $.each(data, function (index, value) {
      s.append($('<option></option>').html(value));
    });
    s.prop('disabled', false);
    if (autoSelect)
      s.val(autoSelectYear);
    getMakes();
  });
}
function getMakes() {
  var y = $("#vtws-vehicle-year").val();
  if (y == "YEAR")
    return;

  resetMakes();
  resetModels();
  resetBodyTypes();
  resetSubModels();
  $.get("/Vehicle/GetMakes/?y=" + y, function (data) {
    var s = $("#vtws-vehicle-make");
    $.each(data, function (index, value) {
      s.append($('<option></option>').html(value));
    });
    s.prop('disabled', false);
    if (autoSelect)
      s.val(autoSelectMake);
    getModels();
  });
}
function getModels() {
  var y = $("#vtws-vehicle-year").val();
  var m = $("#vtws-vehicle-make").val();
  if (y == "YEAR" || m == "MAKE")
    return;

  resetModels();
  resetBodyTypes();
  resetSubModels();
  $.get("/Vehicle/GetModels/?y=" + y + "&m=" + m, function (data) {
    var s = $("#vtws-vehicle-model");
    $.each(data, function (index, value) {
      s.append($('<option></option>').html(value));
    });
    s.prop('disabled', false);
    if (autoSelect)
      s.val(autoSelectModel);
    else if (s.children().length == 2)
      s.prop('selectedIndex', 1);
    getBodyTypes();
  });
}
function getBodyTypes() {
  var y = $("#vtws-vehicle-year").val();
  var m = $("#vtws-vehicle-make").val();
  var mdl = $("#vtws-vehicle-model").val();
  if (y == "YEAR" || m == "MAKE" || mdl == "MODEL")
    return;

  //Enable this if we need Vehicle Image to show upon Model being selected
  //getVehicleImage();

  resetBodyTypes();
  resetSubModels();
  $.get("/Vehicle/GetBodyTypes/?y=" + y + "&m=" + m + "&mdl=" + mdl, function (data) {
    var s = $("#vtws-vehicle-body-style");
    $.each(data, function (index, value) {
      s.append($('<option></option>').html(value));
    });
    s.prop('disabled', false);
    if (autoSelect)
      s.val(autoSelectBodyType);
    else if (s.children().length == 2)
      s.prop('selectedIndex', 1);
    getSubModels();
  });
}
function getSubModels() {
  var y = $("#vtws-vehicle-year").val();
  var m = $("#vtws-vehicle-make").val();
  var mdl = $("#vtws-vehicle-model").val();
  var b = $("#vtws-vehicle-body-style").val();
  if (y == "YEAR" || m == "MAKE" || mdl == "MODEL" || b == "BODY TYPE")
    return;

  resetSubModels();
  $.get("/Vehicle/GetSubModels/?y=" + y + "&m=" + m + "&mdl=" + mdl + "&b=" + b, function (data) {
    var s = $("#vtws-vehicle-sub-model");
    $.each(data, function (index, value) {
      s.append($('<option></option>').html(value));
    });
    s.prop('disabled', false);

    autoTrigger = false;
    if (autoSelect) {
      s.val(autoSelectSubModel);
      autoSelect = false; //Only do once on page load
      //autoTrigger = true; //Disabled because of the separation between the vehicle selector and wheel studio
    }
    else if (s.children().length == 2) {
      s.prop('selectedIndex', 1);
      autoTrigger = true;
    }
    if (autoTrigger) { //Trigger right function based on who we are
      getVehicleImage();
    }
  });
}
function vtwsSelectVehicle(y, m, mdl, b, sm, pn) {
  vtwsSetAndSelectVehicleOption("year", y);
  vtwsSetAndSelectVehicleOption("make", m);
  vtwsSetAndSelectVehicleOption("model", mdl);
  vtwsSetAndSelectVehicleOption("body-style", b);
  vtwsSetAndSelectVehicleOption("sub-model", sm);
  getVehicleImage(pn);

  autoSelectYear = y;
  autoSelectMake = m;
  autoSelectModel = mdl;
  autoSelectBodyType = b;
  autoSelectSubModel = sm;
  autoSelect = true;

  getYears();
}
function vtwsSetAndSelectVehicleOption(option, value) {
  var s = $("#vtws-vehicle-" + option);
  s.append($('<option></option>').html(value));
  s.val(value);
}

$(function () {
  $("#vtws-vehicle-year").change(getMakes);
  $("#vtws-vehicle-make").change(getModels);
  $("#vtws-vehicle-model").change(getBodyTypes);
  $("#vtws-vehicle-body-style").change(getSubModels);
  $("#vtws-vehicle-sub-model").change(getVehicleImage);

  getYears();
});