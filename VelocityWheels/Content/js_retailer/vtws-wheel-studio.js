﻿$(function () {
  //Load Widget Components
  $.get("/Widget/WheelStudio", function (data) {
    $("#vtws-wheel-studio").html(data);
    $.get("/Widget/VehicleSelector", function (data) {
      $("#vtws-vehicle-selector").html(data);
    });
  });

  //Vehicle and Wheel Selectors
  $("a.vtws-vehicle-selector").click(function(e) {
    e.preventDefault();
    var vs = $(this);    
    vtwsSelectVehicle(vs.data("vtwsVehicleYear"), vs.data("vtwsVehicleMake"), vs.data("vtwsVehicleModel"), vs.data("vtwsVehicleBodyStyle"), vs.data("vtwsVehicleSubModel"));
  });
  $("a.vtws-wheel-selector").click(function(e) {
    e.preventDefault();
    var vs = $(this);
    vtwsSelectWheel(vs.data("vtwsWheelPartNumber"));
  });
  $("a.vtws-vehicle-wheel-selector").click(function(e) {
    e.preventDefault();
    var vs = $(this);    
    vtwsSelectVehicle(vs.data("vtwsVehicleYear"), vs.data("vtwsVehicleMake"), vs.data("vtwsVehicleModel"), vs.data("vtwsVehicleBodyStyle"), vs.data("vtwsVehicleSubModel"), vs.data("vtwsWheelPartNumber"));
  });
});
