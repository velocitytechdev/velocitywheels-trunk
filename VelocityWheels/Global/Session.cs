﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VelocityWheels.Models;
using VelocityWheels.Services;
using VelocityWheels.Services.Models;

namespace VelocityWheels.Global
{
    [Serializable]
    public class Session
    {
        private static string KEY = "__USER_SESSION_ABCD0123__";
        private static Session _instance;

        public static Session Instance
        {
            get
            {
                try
                {
                    _instance = (Session) HttpContext.Current.Session[KEY];
                }
                catch
                {
                }
                if (_instance == null)
                {
                    _instance = new Session();
                    try
                    {
                        HttpContext.Current.Session.Add(KEY, _instance);
                    }
                    catch
                    {
                    }
                }
                return _instance;
            }
        }

        private ClientViewModel _Client;

        public ClientViewModel Client
        {
            get
            {
                if (_Client == null)
                {
                    _Client = new ClientViewModel
                    {
                        Token = "5585b38b-2287-4931-af72-16c867d305c9",
                        VehicleSelectorDataSource = VehicleConfiguration.DataSource.DriveRight,
                        ImageDataSource = VehicleConfiguration.DataSource.FuelAPI
                    };
                }
                return _Client;
            }
            set { _Client = value; }
        }

        public List<FuelAPIMake> FuelAPIMakes { get; set; }
        public List<FuelAPISubModel> FuelAPISubModels { get; set; }
        public Dictionary<string, string> FuelApiRequestMap { get; set; }

        public void SetFuelApiMap(string year, string make, string model, string response)
        {
            if (FuelApiRequestMap == null)
            {
                FuelApiRequestMap = new Dictionary<string, string>();
            }

            string key = year + make + model;
            if (!FuelApiRequestMap.ContainsKey(key))
            {
                FuelApiRequestMap.Add(key, response);
            }
            else
            {
                FuelApiRequestMap[key] = response;
            }            
        }

        public string GetFuelApiMapResponse(string year, string make, string model)
        {
            string key = year + make + model;
            string response = null;
            FuelApiRequestMap?.TryGetValue(key, out response);
            return response;
        }
    }
}