﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VelocityWheels {
  public static class Helpers {
    public static int LowestOf(params int[] nums) {
      var n = nums.Where(i => i > 0);
      if(n.Count() == 0)
        return 0;
      else
        return n.Min();
    }
    public static int HighestOf(params int[] nums) {
      return nums.Max();
    }
  }
}