﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using CPS_Socks.Models;

namespace CPS_Socks.Global {
  [Serializable]
  public class Cache {
    private static string KEY = "__USER_CACHE_ABCD0123__";
    private static Cache _instance;
    public static Cache Instance {
      get {
        try {
          _instance = (Cache) MemoryCache.Default[KEY];
        }
        catch { }
        if(_instance == null) {
          _instance = new Cache();
          try {
            MemoryCache.Default.Add(KEY, _instance, MemoryCache.InfiniteAbsoluteExpiration);
          }
          catch { }
        }
        return _instance;
      }
    }
  }
}