﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace VelocityWheels {
  public static class Extensions {
    public static bool AnyIncludingChildren<T>(
        this IEnumerable<T> source, Func<T, IEnumerable<T>> childrenSelector,
        Predicate<T> condition) {
      // return default if no items
      if(source == null || !source.Any()) return false;

      // return result if found and stop traversing hierarchy
      bool found = source.Any(t => condition(t));
      if(found) return true;

      // recursively call this function on lower levels of the
      // hierarchy until a match is found or the hierarchy is exhausted
      return source.SelectMany(childrenSelector).AnyIncludingChildren(childrenSelector, condition);
    }
    public static T FirstOrDefaultIncludingChildren<T>(
        this IEnumerable<T> source, Func<T, IEnumerable<T>> childrenSelector,
        Predicate<T> condition) {
      // return default if no items
      if(source == null || !source.Any()) return default(T);

      // return result if found and stop traversing hierarchy
      var attempt = source.FirstOrDefault(t => condition(t));
      if(!Equals(attempt, default(T))) return attempt;

      // recursively call this function on lower levels of the
      // hierarchy until a match is found or the hierarchy is exhausted
      return source.SelectMany(childrenSelector).FirstOrDefaultIncludingChildren(childrenSelector, condition);
    }
    public static string Serialize<T>(this T value) {
      if(value == null) {
        return string.Empty;
      }
      try {
        var xmlserializer = new XmlSerializer(typeof(T));
        var stringWriter = new StringWriter();
        using(var writer = XmlWriter.Create(stringWriter)) {
          xmlserializer.Serialize(writer, value);
          return stringWriter.ToString();
        }
      }
      catch(Exception ex) {
        throw new Exception("An error occurred", ex);
      }
    }
  }
}