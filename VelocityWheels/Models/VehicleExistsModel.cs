﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VelocityWheels.Models
{
    public class VehicleExistsModel
    {
        public bool Exists { get; set; }

        public string Error { get; set; }
    }
}