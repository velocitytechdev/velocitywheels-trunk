﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VelocityWheels.Services;

namespace VelocityWheels.Models {
  [Serializable]
  public class ClientViewModel {
    public string Token { get; set; }
    public VehicleConfiguration.DataSource VehicleSelectorDataSource { get; set; }
    public VehicleConfiguration.DataSource ImageDataSource { get; set; }
  }
}